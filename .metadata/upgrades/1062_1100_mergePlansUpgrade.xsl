<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>	

<xsl:param name="fileName"/>    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>
    
    <xsl:variable name="entity" select="replace($fileName, '(.*)_merge.comp', '$1')"/>

<!-- Record Counters naming -->
    <xsl:template match="step[@className='com.ataccama.dqc.tasks.flow.RecordCounter' and matches(@id, 'counter_.*_.*_in')]">
		<step id="counter_{$entity}_merge_in" className="{@className}" mode="NORMAL" xmlns:comm="{@xmlns:comm}">
			<xsl:copy-of select="properties"/>
			<xsl:copy-of select="visual-constraints"/>
		</step>
    </xsl:template>
    
	<xsl:template match="step[@className='com.ataccama.dqc.tasks.flow.RecordCounter' and matches(@id, 'counter_.*_.*_out')]">
		<step id="counter_{$entity}_merge_out" className="{@className}" mode="NORMAL" xmlns:comm="{@xmlns:comm}">
			<xsl:copy-of select="properties"/>
			<xsl:copy-of select="visual-constraints"/>
		</step>
    </xsl:template>
       
<!-- connections -->
	<xsl:template match="connection[matches(source/@step, 'counter_.*_.*_.*') or matches(target/@step, 'counter_.*_.*_.*')]">
		
		<xsl:variable name="sourceStep">
  			<xsl:choose>
    			<xsl:when test="matches(source/@step, 'counter_.*_.*_.*')">
    				<xsl:if test="contains(source/@step, '_out')">
						<xsl:value-of select="concat('counter_',$entity,'_merge_out')"/>
					</xsl:if>
					<xsl:if test="contains(source/@step, '_in')">
						<xsl:value-of select="concat('counter_',$entity,'_merge_in')"/>
					</xsl:if>
				</xsl:when>
    			<xsl:otherwise>
    				<xsl:value-of select="source/@step"/>
    			</xsl:otherwise>
  			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="targetStep">
  			<xsl:choose>
    			<xsl:when test="matches(target/@step, 'counter_.*_.*_.*')">
    				<xsl:if test="contains(target/@step, '_out')">
						<xsl:value-of select="concat('counter_',$entity,'_merge_out')"/>
					</xsl:if>
					<xsl:if test="contains(target/@step, '_in')">
						<xsl:value-of select="concat('counter_',$entity,'_merge_in')"/>
					</xsl:if>
				</xsl:when>
    			<xsl:otherwise>
    				<xsl:value-of select="target/@step"/>
    			</xsl:otherwise>
  			</xsl:choose>
		</xsl:variable>
		
		<connection className="{@className}" disabled="{@disabled}">
			<source step="{$sourceStep}" endpoint="{source/@endpoint}"/>
			<target step="{$targetStep}" endpoint="{target/@endpoint}"/>
			<xsl:copy-of select="visual-constraints"/>
		</connection>
	
	</xsl:template>
		       	
</xsl:stylesheet>