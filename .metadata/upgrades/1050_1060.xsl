<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sf="http://www.ataccama.com/xslt/functions" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl sf"> -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/metadata">
		<metadata>
			<xsl:copy-of select="settings"/>
			<xsl:copy-of select="systems"/>
			<logicalModel>
				<xsl:copy-of select="logicalModel/instanceModel"/>
				<xsl:copy-of select="logicalModel/masterModels"/>				
				<xsl:copy-of select="logicalModel/dictionary"/>
			</logicalModel>
			<outputOperations>			
				<xsl:copy-of select="outputOperations/exportModel"/>					
				<eventHandler>
					<handlers>
						<xsl:for-each select="outputOperations/eventHandler/handlers/handler">
							<handler desc="{@desc}" persistenceDir="{@persistenceDir}" name="{@name}" enable="{@enable}" class="{@class}">
								<publishers>
									<xsl:for-each select="publishers/stdOutPublisher">
										<stdOutPublisher enable="{@enable}">
											<description><xsl:value-of select="description|@description"/></description>
											<xsl:copy-of select="transformers"/>
											<advanced>
												<filteringPublisher filter="{advanced/filteringPublisher/@filter}">
													<xsl:value-of select="advanced/filteringPublisher/expression"/>
													<filteredEntities>
														<xsl:for-each select="advanced/filteringPublisher/filteredEntities/entity">
															<entity name="{lower-case(@name)}">
																<xsl:copy-of select="expression"/>
															</entity>
														</xsl:for-each>
													</filteredEntities>
												</filteringPublisher>
												<xsl:copy-of select="advanced/retryingPublisher"/>							
											</advanced>
										</stdOutPublisher>
									</xsl:for-each>
									<xsl:for-each select="publishers/eventPlanPublisher">
										<eventPlanPublisher enable="{@enable}" layerName="{lower-case(@layerName)}" suffix="{@suffix}">
											<xsl:copy-of select="description|@description"/>
											<xsl:copy-of select="planAdvancedSettings"/>
											<xsl:copy-of select="entityName"/>
											<xsl:copy-of select="columnsMeta"/>
											<xsl:copy-of select="columns"/>
										</eventPlanPublisher>
									</xsl:for-each>	
									<xsl:for-each select="publishers/jmsPublisher">			
										<jmsPublisher enable="{@enable}" contentType="{@contentType}" connectionName="{@connectionName}" destination="{@destination}">
											<xsl:copy-of select="description|@description"/>
											<transformers>
												<xsl:for-each select="transformers/expressionTemplateTransformer">
													<expressionTemplateTransformer name="{@name}" entityName="{@entityName}">
														<template><xsl:value-of select="template|@template"/></template>
													</expressionTemplateTransformer>
												</xsl:for-each>
											</transformers>
											<xsl:copy-of select="headers"/>
											<advanced>
												<filteringPublisher filter="{advanced/filteringPublisher/@filter}">
													<xsl:value-of select="advanced/filteringPublisher/expression"/>
													<filteredEntities>
														<xsl:for-each select="advanced/filteringPublisher/filteredEntities/entity">
															<entity name="{lower-case(@name)}">
																<xsl:copy-of select="expression"/>
															</entity>
														</xsl:for-each>
													</filteredEntities>
												</filteringPublisher>
												<xsl:copy-of select="advanced/retryingPublisher"/>							
											</advanced>
										</jmsPublisher>
									</xsl:for-each>
									<xsl:for-each select="publishers/httpSoapPublisher">	
										<httpSoapPublisher urlResourceName="{@urlResourceName}" soapAction="{@soapAction}" encoding="{@encoding}" enable="{@enable}" soapVersion="{@soapVersion}" delay="{@delay}" timeout="{@timeout}">
											<xsl:copy-of select="description|@description"/>
											<advanced>
												<filteringPublisher filter="{advanced/filteringPublisher/@filter}">
													<xsl:value-of select="advanced/filteringPublisher/expression"/>
													<filteredEntities>
														<xsl:for-each select="advanced/filteringPublisher/filteredEntities/entity">
															<entity name="{lower-case(@name)}">
																<xsl:copy-of select="expression"/>
															</entity>
														</xsl:for-each>
													</filteredEntities>
												</filteringPublisher>
												<xsl:copy-of select="advanced/retryingPublisher"/>							
											</advanced>
											<transformers>
												<xsl:for-each select="transformers/expressionTemplateTransformer">
													<expressionTemplateTransformer name="{@name}" entityName="{@entityName}">
														<template><xsl:value-of select="template|@template"/></template>
													</expressionTemplateTransformer>
												</xsl:for-each>
											</transformers>
										</httpSoapPublisher>
									</xsl:for-each>
									<xsl:for-each select="publishers/eventSqlPublisher">	
										<eventSqlPublisher dataSource="{@dataSource}" enable="{@enable}">
											<xsl:copy-of select="description|@description"/>
											<sqlTemplates>
												<xsl:for-each select="sqlTemplates/template">
													<template name="{lower-case(@name)}">
														<xsl:value-of select="template"/>
													</template>
												</xsl:for-each>												
											</sqlTemplates>
											<advanced>
												<filteringPublisher filter="{advanced/filteringPublisher/@filter}">
													<xsl:value-of select="advanced/filteringPublisher/expression"/>
													<filteredEntities>
														<xsl:for-each select="advanced/filteringPublisher/filteredEntities/entity">
															<entity name="{lower-case(@name)}">
																<xsl:copy-of select="expression"/>
															</entity>
														</xsl:for-each>
													</filteredEntities>
												</filteringPublisher>
												<xsl:copy-of select="advanced/retryingPublisher"/>							
											</advanced>
										</eventSqlPublisher>
									</xsl:for-each>
									<xsl:for-each select="publishers/ismPublisher">	
										<ismPublisher port="{@port}" host="{@host}" targetSystem="{@targetSystem}" enable="{@enable}">
											<xsl:copy-of select="description|@description"/>
											<advanced>
												<filteringPublisher filter="{advanced/filteringPublisher/@filter}">
													<xsl:value-of select="advanced/filteringPublisher/expression"/>
													<filteredEntities>
														<xsl:for-each select="advanced/filteringPublisher/filteredEntities/entity">
															<entity name="{lower-case(@name)}">
																<xsl:copy-of select="expression"/>
															</entity>
														</xsl:for-each>
													</filteredEntities>
												</filteringPublisher>
												<xsl:copy-of select="advanced/retryingPublisher"/>							
											</advanced>
										</ismPublisher>
									</xsl:for-each>
								</publishers>
								<filter>
									<xsl:copy-of select="filter/expression"/>
									<entities>
										<xsl:for-each select="filter/entities/entity">
											<entity name="{lower-case(@name)}">
												<xsl:copy-of select="expression"/>
											</entity>
										</xsl:for-each>
									</entities>
								</filter>
							</handler>
						</xsl:for-each>
					</handlers>
				</eventHandler>				
			</outputOperations>
			<xsl:copy-of select="nativeServices"/>
			<xsl:copy-of select="advancedSettings"/>		
		</metadata>
	</xsl:template>
</xsl:stylesheet>
