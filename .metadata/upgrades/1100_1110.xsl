<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sf="http://www.ataccama.com/xslt/functions" xmlns:exsl="http://exslt.org/common" extension-element-prefixes="exsl sf"> -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:functx="http://www.functx.com"
	exclude-result-prefixes="sf fn functx">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/metadata">
		<metadata>
			<xsl:copy-of select="settings"/>
			<xsl:copy-of select="systems"/>
			<xsl:copy-of select="logicalModel"/>
			<xsl:copy-of select="outputOperations"/>			
			<xsl:copy-of select="nativeServices"/>
			<streaming enable="{streaming/@enable}">
				<consumers>
					<xsl:for-each select="streaming/consumers/consumer">
						<consumer enable="{@enable}" name="{@name}">
							<xsl:copy-of select="description"/>
							<xsl:copy-of select="source"/>
							<xsl:copy-of select="batching"/>
							<processedEntities allEntities="false">
								<entities>
									<xsl:for-each select="entities/entity">
										<entity name="{@name}"/>
									</xsl:for-each>
								</entities>
							</processedEntities>							
							<messageTransformer>					
								<transformers>
									<planTransformer plan="com.ataccama.nme.dqc.stream.PlanTransformer"/>
								</transformers>
								<headers>
									<xsl:for-each select="transformer/headers/header">
										<header column="{@column}" header="{@header}"/>
									</xsl:for-each>
								</headers>
								<properties>
									<xsl:for-each select="transformer/properties/property">
										<property column="{@column}" property="{@property}" type="{@type}"/>
									</xsl:for-each>
								</properties>								
							</messageTransformer>
						</consumer>
					</xsl:for-each>
				</consumers>
			</streaming>
			<xsl:copy-of select="advancedSettings"/>		
		</metadata>
	</xsl:template>
</xsl:stylesheet>
