<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

	<xsl:template match="config/persistenceLayer">
		<persistenceLayers>
			<xsl:copy-of select="."/>
		</persistenceLayers>
	</xsl:template>

	<xsl:template match="modelDefinitionFile">
		<xsl:variable name="event_handler">
			<xsl:text disable-output-escaping="yes">&lt;eventHandlersConfigFile&gt;nme-event_handler.gen.xml&lt;/eventHandlersConfigFile&gt;</xsl:text>
		</xsl:variable>
		<model>
			<models>
				<consolidation>
					<configFile><xsl:value-of select="."/></configFile>
					<executionPlanConfigFile>nme-consolidation-plan.xml</executionPlanConfigFile>
					<xsl:value-of select="$event_handler"/>
				</consolidation>
			</models>
		</model>
	</xsl:template>

</xsl:stylesheet>