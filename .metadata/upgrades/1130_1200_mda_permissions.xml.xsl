<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/> 
         
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>       
    
	<xsl:template match="permissions">
		<permissions>
			<issueRoles>
				<role name="MDA_viewer"/>
				<role name="MDA_user"/>
				<role name="MDA_superuser"/>
			</issueRoles>
		
			<authFactory class="com.ataccama.auth.keycloak.KeycloakAuthManagerFactory">
				<serverUrl>http://localhost:8083/auth</serverUrl>
				<realm>ataccamaone</realm>
				<clientId>one-keycloak-client</clientId>
				<clientSecret>one-keycloak-client-s3cret</clientSecret>
				<userName>kc_user</userName>
				<password>s3cret</password>	
			</authFactory>
		
			<securityProviders>		
				
				<xsl:text disable-output-escaping="yes">&lt;!--</xsl:text> <securityProvider class="com.ataccama.mda.core.config.security.MdaAllPermissionsProvider" /> <xsl:text disable-output-escaping="yes">--&gt;</xsl:text> <xsl:comment> Full permissions; use just for test/dev purposes! </xsl:comment>
				
				<securityProvider class="com.ataccama.mda.core.config.security.MdaDefaultPermissionsProvider"> <xsl:text disable-output-escaping="yes">&lt;!-- Configuration below works only with predefined, default workflows "consolidation" and "sor" - do not use workflow and step section in the permission definition. --&gt;</xsl:text>
					<roles>
						<viewRoles>
							<role name="MDA_viewer"/>
						</viewRoles>
						<userRoles>
							<role name="MDA_user"/>
						</userRoles>
						<adminRoles>
							<role name="MDA_superuser"/>   
						</adminRoles>
					</roles>
				</securityProvider>		
				
				<xsl:comment> Configuration below works only with custom-defined workflows. 
				When you use predefined workflows "consolidation" and "sor", do not use the workflow section in the definition of permissions </xsl:comment>
				
					  <xsl:comment>
				  
					Workflow permissions:
					
					    If a role contains a workflow status, the role can view drafts in this status
					    If role contains a workflow transition, the role can execute this transition (move drafts to another status)
					    If a status contains the attributes discard="true" or publish="true", the role can discard/publish drafts in this status
					
					Entity permissions:
					
					    If a role contains an entity, the role can view the entity
					    If an entity contains the attribute createIssue="true", the role can create issues on this entity
					    If an entity contains a status (identified by the status name and the workflow name), the role can execute actions. The following actions can be specified ([action]="true"):
					        override. Set and remove overrides on attributes
					        ovrMatch. Merge master records, merge instances, and split instances
					        ovrActive. Override the activity status of the record: set as active/inactive and remove activity overrides
					        sorEdit, sorCreate, sorDelete. Actions in the SoR hub
					    If an entity contains columns, the the role can view these columns
					    If a column contains the attribute write="true", the role can override/edit this value if the workflow status allows this action		  
				   </xsl:comment>
				
				
				<xsl:text disable-output-escaping="yes">&lt;!--</xsl:text> <securityProvider class="com.ataccama.mda.core.config.security.MdaPermissionsProvider">
					<rolesBean class="com.ataccama.mda.core.config.security.MdaXmlRolesProviderConfig">
						<roles>
							<role name="MDA_user">
								<description>The role has some permission I guess... All for the
									DEFAULT instance, none for the INVIS</description>
								<workflows>
									<workflow name="def">
										<steps>
											<step name="draft" discard="true" />
											<step name="waiting_for_publish" />
										</steps>
										<transitions>
											<transition name="move" />
										</transitions>
									</workflow>
									<workflow name="long">
										<steps>
											<step name="draft" discard="true" />
											<step name="waiting_for_supervisor" />
										</steps>
										<transitions>
											<transition name="move_supervisor" />
										</transitions>
									</workflow>
								</workflows>
								<instanceLayer>
									<entities>
										<entity name="party">
											<steps>
												<step name="draft" workflow="def" override="true" />
												<step name="draft" workflow="long" override="true" />
											</steps>
											<columns>
												<column name="id" write="true" />
												<column name="src_first_name" write="true" />
											</columns>
										</entity>
									</entities>
								</instanceLayer>
								<masterLayers>
									<masterLayer name="masters">
										<entities>
											<entity name="party">
												<steps>
													<step name="draft" workflow="long" ovrMatch="true" />
													<step name="draft" workflow="def" ovrMatch="true" />
												</steps>
												<columns>
													<column name="id" write="true" />
													<column name="cmo_first_name" write="true" />
												</columns>
											</entity>
										</entities>
									</masterLayer>
								</masterLayers>
							</role>
							<role name="MDA_superuser">
								<description>The role has some permission I guess... All for the
									DEFAULT instance, none for the INVIS</description>
								<workflows>
									<workflow name="def">
										<steps>
											<step name="draft" discard="true" />
											<step name="waiting_for_publish" discard="true" publish="true" />
										</steps>
										<transitions>
											<transition name="move" />
											<transition name="return" />
										</transitions>
									</workflow>
									<workflow name="long">
										<steps>
											<step name="draft" discard="true" />
											<step name="waiting_for_supervisor" discard="true" />
											<step name="waiting_for_publish" discard="true" publish="true" />
										</steps>
										<transitions>
											<transition name="move_supervisor" />
											<transition name="move_publish" />
											<transition name="move_back" />
											<transition name="return" />
										</transitions>
									</workflow>
								</workflows>
								<instanceLayer>
									<entities>
										<entity name="party" createIssue="true">
											<steps>
												<step name="draft" workflow="def" override="true" />
												<step name="draft" workflow="long" override="true" />
												<step name="waiting_for_publish" workflow="def" override="true" />
											</steps>
											<columns>
												<column name="id" write="true" />
												<column name="src_first_name" write="true" />
											</columns>
										</entity>
									</entities>
								</instanceLayer>
								<masterLayers>
									<masterLayer name="masters">
										<entities>
											<entity name="party" createIssue="true">
												<steps>
													<step name="draft" workflow="long" ovrMatch="true" />
												</steps>
												<columns>
													<column name="id" write="true" />
													<column name="cmo_first_name" write="true" />
												</columns>
											</entity>
											<entity name="party_instance" createIssue="true">
												<steps>
													<step name="draft" workflow="long" ovrMatch="true" />
												</steps>
												<columns>
													<column name="id" write="true" />
													<column name="src_first_name" write="true" />
												</columns>
											</entity>
										</entities>
									</masterLayer>
								</masterLayers>
								<sorLayer>
									<entities>
										<entity name="party" createIssue="true">
											<columns>
												<column name="id" write="true" />
												<column name="src_first_name" write="true" />
												<column name="src_last_name" write="true" />
											</columns>
										</entity>
									</entities>
								</sorLayer>
								<datasetLayer>
									<entities>
										<entity name="party_contact_inst" createIssue="true">
											<columns>
												<column name="id" write="true" />
												<column name="first_name" write="true" />
												<column name="last_name" write="true" />
												<column name="cio_type" write="true" />
												<column name="cio_value" write="true" />
											</columns>
										</entity>
									</entities>
								</datasetLayer>
							</role>
						</roles>
					</rolesBean>
				</securityProvider> <xsl:text disable-output-escaping="yes">--&gt;</xsl:text>
			</securityProviders>
		</permissions>

	</xsl:template>        

</xsl:stylesheet>