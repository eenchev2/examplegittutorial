<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="dataSource[@driverclass='org.apache.derby.jdbc.ClientDriver' and @name='mda_im_db']">
    	<dataSource driverclass="org.apache.derby.jdbc.ClientDriver" name="it_db" user="it_db" password="it_db" url="jdbc:derby://localhost:1528/it_db;create=true" />
	</xsl:template>

</xsl:stylesheet>