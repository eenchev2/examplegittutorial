<?xml version='1.0' encoding='UTF-8'?>
<metadata xmlns:ame="http://www.ataccama.com/ame/md">
	<settings ame:include="md/settings.md"/>
	<systems>
		<system ame:include="md/connected_systems/system_crm.md"/>
		<system ame:include="md/connected_systems/system_life.md"/>
		<system ame:include="md/connected_systems/system_invest.md"/>
		<system ame:include="md/connected_systems/system_sor.md"/>
	</systems>
	<logicalModel>
		<instanceModel ame:include="md/logical_model/instance_model.md"/>
		<masterModels>
			<masterModel ame:include="md/logical_model/master_Model_masters.md"/>
			<masterModel ame:include="md/logical_model/master_Model_norm.md"/>
		</masterModels>
		<dictionary ame:include="md/logical_model/reference_data_model.md"/>
		<sorModel ame:include="md/logical_model/sor_model.md"/>
		<datasets ame:include="md/logical_model/datasets.md"/>
	</logicalModel>
	<outputOperations>
		<exportModel ame:include="md/output_interfaces/export_operations.md"/>
		<eventHandler ame:include="md/output_interfaces/event_handler.md"/>
	</outputOperations>
	<nativeServices ame:include="md/services.md"/>
	<streaming enable="true">
		<consolidationStreaming>
			<consumers>
				<jmsStreamSource ame:include="md/streaming/consumer_CRM_Party_Stream.md"/>
			</consumers>
		</consolidationStreaming>
		<sorStreaming>
			<consumers/>
		</sorStreaming>
	</streaming>
	<guiConfig>
		<searchTab ame:include="md/gui_config/searchTab.md"/>
		<actions ame:include="md/gui_config/actions.md"/>
		<indicators ame:include="md/gui_config/indicators.md"/>
		<wfConfig ame:include="md/gui_config/wfConfig.md"/>
		<globalValidations ame:include="md/gui_config/globalValidations.md"/>
		<guiPreferences ame:include="md/gui_config/guiPreferences.md"/>
		<dataNavigation ame:include="md/gui_config/dataNavigation.md"/>
		<hierarchies ame:include="md/gui_config/hierarchies.md"/>
		<auditing ame:include="md/gui_config/auditing.md"/>
	</guiConfig>
	<preview/>
	<documentationRoot calculateDoc="true"/>
	<advancedSettings>
		<taskInfoExport ame:include="md/advanced_settings/taskInfoExport.md"/>
		<manualMatch ame:include="md/advanced_settings/manualMatch.md"/>
		<reprocessSettings ame:include="md/advanced_settings/reprocessSettings.md"/>
		<historyPlugin ame:include="md/advanced_settings/historyPlugin.md"/>
		<migration ame:include="md/advanced_settings/migration.md"/>
		<sorInitialBatchLoad>
			<sorLoadOperations>
				<sorLoadOperation elemId="27823937" allEntities="false" allowLoad="false" name="initial" preserveIds="false">
					<selectedEntities>
						<selectedEntity elemId="27853201" name="party"/>
						<selectedEntity elemId="27853202" name="address"/>
						<selectedEntity elemId="27853203" name="consent"/>
						<selectedEntity elemId="27853204" name="rel_party2party"/>
						<selectedEntity elemId="27853205" name="rd_gender"/>
						<selectedEntity elemId="27853206" name="rd_party_type"/>
						<selectedEntity elemId="27853207" name="rd_rel_pty2pty_type"/>
						<selectedEntity elemId="27853208" name="rd_rel_pty2pty_ctg"/>
						<selectedEntity elemId="27853209" name="rd_address_type"/>
						<selectedEntity elemId="27853210" name="rd_legal_form"/>
						<selectedEntity elemId="27853211" name="rd_consent_category"/>
						<selectedEntity elemId="27853212" name="rd_consent_purpose"/>
						<selectedEntity elemId="27853213" name="rd_consent_system"/>
						<selectedEntity elemId="27853214" name="rd_consent_consumer"/>
						<selectedEntity elemId="27853215" name="rd_consent_channel"/>
						<selectedEntity elemId="27853216" name="rd_consent_status"/>
					</selectedEntities>
				</sorLoadOperation>
			</sorLoadOperations>
		</sorInitialBatchLoad>
	</advancedSettings>
</metadata>