<?xml version='1.0' encoding='UTF-8'?>
<system elemId="27829500" name="sor">
	<description>System of Record</description>
	<model>
		<relationships/>
		<tables>
			<table elemId="27830713" name="party">
				<description></description>
				<columns/>
			</table>
			<table elemId="27831299" name="address">
				<description></description>
				<columns/>
			</table>
			<table elemId="27831308" name="consent">
				<description></description>
				<columns/>
			</table>
			<table elemId="27838620" name="rel_party2party">
				<description></description>
				<columns/>
			</table>
		</tables>
	</model>
	<sourceMappings>
		<mapping elemId="27834035" customOrigin="" entity="party" table="party">
			<description></description>
		</mapping>
		<mapping elemId="27834036" customOrigin="" entity="address" table="address">
			<description></description>
		</mapping>
		<mapping elemId="27834037" customOrigin="" entity="consent" table="consent">
			<description></description>
		</mapping>
		<mapping elemId="27839223" customOrigin="" entity="rel_party2party" table="rel_party2party">
			<description></description>
		</mapping>
	</sourceMappings>
	<loadOperations/>
</system>