<?xml version='1.0' encoding='UTF-8'?>
<system elemId="34731" name="life">
	<description>Life insurance system</description>
	<model>
		<relationships>
			<relationship childTable="contract" elemId="37503" name="product_is_bound_on_contract" parentTable="product">
				<foreignKey>
					<column elemId="639801" childColumn="PRODUCT" parentColumn="ID"/>
				</foreignKey>
			</relationship>
			<relationship childTable="party_contract" elemId="345006" name="party_has_role_on_contract" parentTable="party">
				<foreignKey>
					<column elemId="639802" childColumn="CUSTOMERID" parentColumn="ID"/>
				</foreignKey>
			</relationship>
			<relationship childTable="party_contract" elemId="26378142" name="contract_has_party" parentTable="contract">
				<foreignKey>
					<column elemId="26378294" childColumn="CONTRACTID" parentColumn="ID"/>
				</foreignKey>
			</relationship>
		</relationships>
		<tables>
			<table elemId="344776" name="party">
				<description>Customers stored in Life Insurance ERP system</description>
				<columns>
					<column elemId="344886" name="ID" description="" type="NUMBER"/>
					<column elemId="344887" name="FIRSTNAME" description="" type="VARCHAR(100)"/>
					<column elemId="109772" name="LASTNAME" description="" type="VARCHAR(100)"/>
					<column elemId="344888" name="BIRTHDATE" description="" type="DATE"/>
					<column elemId="344889" name="ID_CARD" description="" type="VARCHAR(20)"/>
					<column elemId="344890" name="SIN" description="" type="VARCHAR(20)"/>
					<column elemId="110778" name="COMPANYNAME" description="" type="VARCHAR(100)"/>
					<column elemId="110779" name="BUSINESSNUMBER" description="" type="VARCHAR(20)"/>
					<column elemId="116936" name="PHONE" description="" type="VARCHAR(20)"/>
					<column elemId="116937" name="EMAIL" description="" type="VARCHAR(100)"/>
					<column elemId="117356" name="ADDRESSLINE1" description="" type="VARCHAR(100)"/>
					<column elemId="117357" name="ADDRESSLINE2" description="" type="VARCHAR(100)"/>
					<column elemId="117358" name="ADDRESSLINE3" description="" type="VARCHAR(100)"/>
					<column elemId="117359" name="ADDRESSLINE4" description="" type="VARCHAR(100)"/>
					<column elemId="9959" name="CONSENT" description="" type="VARCHAR(10)"/>
					<column elemId="9960" name="CONSENT_DATE" description="" type="DATE"/>
				</columns>
			</table>
			<table elemId="34832" name="contract">
				<description>Life insurance contracts stored in the ERP system</description>
				<columns>
					<column elemId="34925" name="ID" description="" type="NUMBER"/>
					<column elemId="34926" name="CUSTOMERID" description="" type="NUMBER"/>
					<column elemId="37432" name="SALEPOINT" description="" type="VARCHAR(80)"/>
					<column elemId="34927" name="STATUS" description="" type="VARCHAR(30)"/>
					<column elemId="34928" name="TYPE" description="" type="VARCHAR(30)"/>
					<column elemId="34930" name="DATESTART" description="" type="DATE"/>
					<column elemId="34929" name="DATETO" description="" type="DATE"/>
					<column elemId="26377228" name="PRODUCT" description="" type="VARCHAR(50)"/>
					<column elemId="26377229" name="VARIANT" description="" type="VARCHAR(50)"/>
				</columns>
			</table>
			<table elemId="37383" name="product">
				<description>Life Insurance products stored in the ERP system</description>
				<columns>
					<column elemId="34925" name="ID" description="" type="NUMBER"/>
					<column elemId="37458" name="CONTRACTID" description="" type="NUMBER"/>
					<column elemId="37459" name="NAME" description="" type="VARCHAR(100)"/>
					<column elemId="37409" name="PRICE" description="" type="NUMBER"/>
					<column elemId="111408" name="CURRENCY" description="" type="VARCHAR(20)"/>
					<column elemId="111409" name="DESCRIPTION" description="" type="VARCHAR(2000)"/>
					<column elemId="111410" name="PARENTID" description="" type="NUMBER"/>
				</columns>
			</table>
			<table elemId="109406" name="party_contract">
				<description>Customer roles on contracts in the ERP system</description>
				<columns>
					<column elemId="26377684" name="ID" description="" type="NUMBER"/>
					<column elemId="26377685" name="CONTRACTID" description="" type="NUMBER"/>
					<column elemId="26377838" name="CUSTOMERID" description="" type="NUMBER"/>
					<column elemId="26377839" name="RELTYPE" description="" type="VARCHAR(30)"/>
				</columns>
			</table>
		</tables>
	</model>
	<sourceMappings>
		<mapping elemId="277153" customOrigin="" entity="contract" table="contract">
			<description></description>
		</mapping>
		<mapping elemId="277155" customOrigin="" entity="product" table="product">
			<description></description>
		</mapping>
		<mapping elemId="116725" customOrigin="" entity="party" table="party">
			<description></description>
		</mapping>
		<mapping elemId="117990" customOrigin="" entity="contact" table="party">
			<description></description>
		</mapping>
		<mapping elemId="117991" customOrigin="" entity="address" table="party">
			<description></description>
		</mapping>
		<mapping elemId="26375924" customOrigin="" entity="id_document" table="party">
			<description></description>
		</mapping>
		<mapping elemId="307381" customOrigin="" entity="rel_party2contract" table="party_contract">
			<description></description>
		</mapping>
		<mapping elemId="17780" customOrigin="" entity="rel_prod2prod" table="product">
			<description></description>
		</mapping>
		<mapping elemId="18225" customOrigin="" entity="rel_prod2contract" table="contract">
			<description></description>
		</mapping>
		<mapping elemId="14783" customOrigin="" entity="consent" table="party">
			<description></description>
		</mapping>
	</sourceMappings>
	<loadOperations>
		<fullLoad elemId="277263" nameSuffix="full" allTables="false">
			<selectedTables>
				<table elemId="348637" name="contract"/>
				<table elemId="348638" name="product"/>
				<table elemId="18396" name="rel_party2contract"/>
				<table elemId="350211" name="party"/>
				<table elemId="118204" name="contact"/>
				<table elemId="348639" name="address"/>
				<table elemId="26378230" name="id_document"/>
				<table elemId="18666" name="rel_prod2prod"/>
				<table elemId="18667" name="rel_prod2contract"/>
				<table elemId="15831" name="consent"/>
			</selectedTables>
			<advanced deletionStrategy="use global setting (from Preferences)">
				<ignoredComparisonColumns/>
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</fullLoad>
		<initialLoad elemId="9314" ignorePersistence="" allowLoad="true" nameSuffix="initial">
			<advanced>
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</initialLoad>
	</loadOperations>
</system>