<?xml version='1.0' encoding='UTF-8'?>
<system elemId="22688" name="crm">
	<description>CRM system</description>
	<model>
		<relationships>
			<relationship childTable="address" elemId="22785" name="fk_address_customer" parentTable="customer">
				<foreignKey>
					<column elemId="639797" childColumn="cust_id" parentColumn="id"/>
				</foreignKey>
			</relationship>
			<relationship childTable="contact" elemId="37195" name="fk_contact_customer" parentTable="customer">
				<foreignKey>
					<column elemId="639798" childColumn="cust_id" parentColumn="id"/>
				</foreignKey>
			</relationship>
			<relationship childTable="customer_relation" elemId="37290" name="fk_cust_rel1" parentTable="customer">
				<foreignKey>
					<column elemId="639799" childColumn="customer1_id" parentColumn="id"/>
				</foreignKey>
			</relationship>
			<relationship childTable="customer_relation" elemId="37336" name="fk_cust_rel2" parentTable="customer">
				<foreignKey>
					<column elemId="639800" childColumn="customer2_id" parentColumn="id"/>
				</foreignKey>
			</relationship>
		</relationships>
		<tables>
			<table elemId="22630" name="customer">
				<description>Customers in the CRM system</description>
				<columns>
					<column elemId="22784" name="id" description="" type="number"/>
					<column elemId="22772" name="first_name" description="" type="varchar2(100)"/>
					<column elemId="109898" name="last_name" description="" type="varchar2(100)"/>
					<column elemId="37058" name="birth_date" description="" type="date"/>
					<column elemId="107204" name="type" description="" type="varchar2(20)"/>
					<column elemId="107205" name="gender" description="" type="varchar2(6)"/>
					<column elemId="38444" name="id_card" description="" type="varchar2(20)"/>
					<column elemId="22773" name="sin" description="" type="varchar2(20)"/>
					<column elemId="107331" name="company_name" description="" type="varchar2(100)"/>
					<column elemId="107332" name="business_number" description="" type="varchar2(20)"/>
					<column elemId="107333" name="established_date" description="" type="date"/>
				</columns>
			</table>
			<table elemId="22774" name="address">
				<description>Postal addresses stored in the CRM system</description>
				<columns>
					<column elemId="22776" name="id" description="" type="number"/>
					<column elemId="22777" name="cust_id" description="" type="number"/>
					<column elemId="107837" name="type" description="" type="varchar2(2)"/>
					<column elemId="22778" name="street" description="" type="varchar2(100)"/>
					<column elemId="22779" name="city" description="" type="varchar2(100)"/>
					<column elemId="22781" name="state" description="" type="varchar2(2)"/>
					<column elemId="22780" name="zip" description="" type="varchar2(20)"/>
					<column elemId="13748" name="origin_date" description="" type="datetime"/>
					<column elemId="14266" name="expiration_date" description="" type="datetime"/>
				</columns>
			</table>
			<table elemId="37127" name="contact">
				<description>Customers&#39; contacts stored in the CRM systems. Typical contacts are telephone numbers and emails</description>
				<columns>
					<column elemId="22776" name="id" description="" type="number"/>
					<column elemId="22777" name="cust_id" description="" type="number"/>
					<column elemId="22780" name="type" description="" type="varchar2(30)"/>
					<column elemId="22779" name="value" description="" type="varchar2(100)"/>
					<column elemId="4416" name="indicator_status" description="" type="varchar2(1)"/>
					<column elemId="4417" name="origin_date" description="" type="datetime"/>
					<column elemId="31731" name="expiration_date" description="" type="datetime"/>
				</columns>
			</table>
			<table elemId="37241" name="customer_relation">
				<description>Relationships between two customers</description>
				<columns>
					<column elemId="37264" name="id" description="" type="number"/>
					<column elemId="37265" name="customer1_id" description="" type="number"/>
					<column elemId="37266" name="customer2_id" description="" type="number"/>
					<column elemId="37267" name="relation_type" description="" type="varchar(100)"/>
				</columns>
			</table>
		</tables>
	</model>
	<sourceMappings>
		<mapping elemId="149971" customOrigin="" entity="party" table="customer">
			<description>Customers stored in CRM system
</description>
		</mapping>
		<mapping elemId="149972" customOrigin="" entity="address" table="address">
			<description>Address entries for the customers.</description>
		</mapping>
		<mapping elemId="149973" customOrigin="" entity="contact" table="contact">
			<description>Contact entries for the customers.</description>
		</mapping>
		<mapping elemId="149974" customOrigin="" entity="rel_party2party" table="customer_relation">
			<description>Relation between the customers.</description>
		</mapping>
		<mapping elemId="26376307" customOrigin="" entity="id_document" table="customer">
			<description>Customers&#39; identification documents.</description>
		</mapping>
		<mapping elemId="5871" customOrigin="" entity="consent" table="contact">
			<description>Customer&#39;s approval for use of individual contact channels</description>
		</mapping>
	</sourceMappings>
	<loadOperations>
		<fullLoad elemId="118392" nameSuffix="full" allTables="false">
			<selectedTables>
				<table elemId="154025" name="party"/>
				<table elemId="154026" name="address"/>
				<table elemId="154027" name="contact"/>
				<table elemId="110149" name="rel_party2party"/>
				<table elemId="26374538" name="id_document"/>
				<table elemId="6378" name="consent"/>
			</selectedTables>
			<advanced deletionStrategy="use global setting (from Preferences)">
				<ignoredComparisonColumns/>
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</fullLoad>
		<complexDeltaLoad elemId="17790350" nameSuffix="delta">
			<tableSettings>
				<table elemId="17790351" mode="central" keyColumn="source_id" name="party">
					<columns/>
				</table>
				<table elemId="17790352" mode="party" keyColumn="party_source_id" name="address">
					<columns/>
				</table>
				<table elemId="17790353" mode="party" keyColumn="party_source_id" name="contact">
					<columns/>
				</table>
				<table elemId="17790354" mode="party" keyColumn="parent_source_id" name="rel_party2party">
					<columns/>
				</table>
				<table elemId="17790355" mode="party" keyColumn="party_source_id" name="id_document">
					<columns/>
				</table>
			</tableSettings>
			<advanced deletionStrategy="use global setting (from Preferences)">
				<partitionedEntities/>
				<ignoredComparisonColumns/>
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</complexDeltaLoad>
		<initialLoad elemId="8939" ignorePersistence="" allowLoad="true" nameSuffix="initial">
			<advanced>
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</initialLoad>
	</loadOperations>
</system>