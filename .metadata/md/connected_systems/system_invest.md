<?xml version='1.0' encoding='UTF-8'?>
<system elemId="13231" name="invest">
	<description></description>
	<model>
		<relationships>
			<relationship childTable="product_hierarchy" elemId="14149" name="product_is_part_of" parentTable="product">
				<foreignKey/>
			</relationship>
		</relationships>
		<tables>
			<table elemId="14141" name="product_hierarchy">
				<description></description>
				<columns/>
			</table>
			<table elemId="14145" name="product">
				<description></description>
				<columns/>
			</table>
		</tables>
	</model>
	<sourceMappings>
		<mapping elemId="14584" customOrigin="" entity="product" table="product">
			<description></description>
		</mapping>
		<mapping elemId="14585" customOrigin="" entity="rel_prod2prod" table="product_hierarchy">
			<description></description>
		</mapping>
	</sourceMappings>
	<loadOperations>
		<fullLoad elemId="13708" nameSuffix="full" allTables="true">
			<selectedTables/>
			<advanced deletionStrategy="use global setting (from Preferences)">
				<ignoredComparisonColumns/>
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</fullLoad>
		<initialLoad elemId="15020" ignorePersistence="" allowLoad="true" nameSuffix="initial">
			<advanced>
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</initialLoad>
	</loadOperations>
</system>