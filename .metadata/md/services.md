<?xml version='1.0' encoding='UTF-8'?>
<nativeServices>
	<consolidationServices>
		<basicServices processPurge="true" getModel="true" listInstance="true" processDelta="true" rwControl="true" batchControl="true" getOverride="true" genTraversal="true" listMaster="true" modelStat="true" getInstance="true" getMaster="true" processMatch="true"/>
		<configurableServices>
			<identify elemId="27959941" enable="true" name="identifyParty" masterLayer="masters" entity="party" desc=""/>
			<processDeltaAdv elemId="27959942" enable="false" name="example" desc="Complex example of processDelta service where both Partial Mode and Delta Dependecy Mode is configured">
				<partialTables>
					<partialTable elemId="27959947" name="contact">
						<partialColumns>
							<partialColumn elemId="27959948" name="src_type"/>
							<partialColumn elemId="27959949" name="src_value"/>
						</partialColumns>
					</partialTable>
				</partialTables>
				<dependencySettings deletionStrategy="use global setting (from Preferences)">
					<tableSettings>
						<table elemId="27959950" mode="central" keyColumn="source_id" name="party">
							<partialColumns/>
						</table>
						<table elemId="27959951" mode="party" keyColumn="party_source_id" name="address">
							<partialColumns>
								<partialColumn elemId="27959952" name="src_city"/>
							</partialColumns>
						</table>
						<table elemId="27959953" mode="autonomous" keyColumn="" name="id_document">
							<partialColumns>
								<partialColumn elemId="27959954" name="src_type"/>
							</partialColumns>
						</table>
					</tableSettings>
				</dependencySettings>
			</processDeltaAdv>
			<cleanseService elemId="27959943" enable="true" name="dqf_party" entity="party" desc=""/>
			<cleanseService elemId="27959944" enable="true" name="dqf_address" entity="address" desc=""/>
			<cleanseService elemId="27959945" enable="true" name="dqf_contact" entity="contact" desc=""/>
			<cleanseService elemId="27959946" enable="true" name="dqf_p2p" entity="rel_party2party" desc=""/>
		</configurableServices>
	</consolidationServices>
	<sorServiceTab listSor="true" getSor="true">
		<sorServices>
			<processUpsert elemId="27638732" enable="true" entity="party" desc=""/>
			<processUpsert elemId="27638733" enable="true" entity="rd_party_type" desc=""/>
			<processUpsert elemId="27888170" enable="true" entity="rel_party2party" desc=""/>
		</sorServices>
	</sorServiceTab>
	<endpointsDefinition>
		<httpEndpoint elemId="271980" enable="true" format="SOAP" listenerNames="all" pathPrefix="soapOverHttp"/>
		<httpEndpoint elemId="271981" enable="true" format="XML RPC" listenerNames="all" pathPrefix="xmlRpcOverHttp"/>
		<jmsEndpoint elemId="271982" inputDestination="in_queue" activityByReadWriteMode="false" enable="false" format="SOAP" listenerNames="all" connectionName="jms" outputDestination="out_queue" pathPrefix="soapOverJms">
			<outputParameters/>
		</jmsEndpoint>
	</endpointsDefinition>
</nativeServices>