<?xml version='1.0' encoding='UTF-8'?>
<settings deletionStrategy="deactivate" enableRC="false" scope="active" batchSizeRC="100000">
	<lengthValidation databaseType="Apache Derby 10.6+" nmePrefixLength="2"/>
	<advancedSettings alternativeKeys="true" expStringLength="500" matchingCompatibility="false" srcStringLength="100">
		<ignoredComparisonColumns/>
	</advancedSettings>
</settings>