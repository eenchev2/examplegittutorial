<?xml version='1.0' encoding='UTF-8'?>
<masterModel elemId="12690" name="masters" label="Full master view">
	<description>Complete master data view</description>
	<relationships>
		<relationship childRole="addresses" childTable="address" elemId="22894" name="party_has_address" parentRole="party" label="Party has Address" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109192" childColumn="party_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances" childTable="party_instance" elemId="22897" name="party_has_instance" parentRole="master" label="Party has Instance Details" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109194" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="contacts" childTable="contact" elemId="36452" name="party_has_contact" parentRole="" label="Party has Contact" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109196" childColumn="party_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="relparent" childTable="rel_party_party" elemId="36626" name="party_has_parent_party" parentRole="parent" label="Party has Parent Party" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109198" childColumn="parent_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="relchild" childTable="rel_party_party" elemId="36673" name="party_has_child_party" parentRole="child" label="Party has Child Party" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109200" childColumn="child_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances" childTable="contact_instance" elemId="36877" name="contact_has_instance" parentRole="master" label="Contact has Instance Details" parentTable="contact" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109202" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances" childTable="address_instance" elemId="36967" name="address_has_instance" parentRole="master" label="Address has Instance Details" parentTable="address" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18108677" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="contract" childTable="rel_party2contract_instance" elemId="38949" name="party_has_contract" parentRole="party" label="Party has Contract" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109205" childColumn="pty_master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="party" childTable="rel_party2contract_instance" elemId="49427" name="contract_has_party" parentRole="contract" label="Contract is Related to Party" parentTable="contract_instance" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109207" childColumn="contract_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances" childTable="rel_party_party_instance" elemId="78629" name="relation_party_has_instance" parentRole="master" label="Relation has Instance Details" parentTable="rel_party_party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109209" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="id_documents" childTable="id_document" elemId="26377853" name="party_has_id_document" parentRole="party" label="Party has ID Document" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109211" childColumn="party_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="id_document_instance" elemId="156412" name="id_document_has_instance" parentRole="" label="ID Document has Instance Details" parentTable="id_document" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="156744" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="rel_prod2prod_instance" elemId="20329" name="rel_prod2prod_has_instance" parentRole="" label="" parentTable="rel_prod2prod" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="31363" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="rel_prod2prod" elemId="20337" name="product_has_child" parentRole="" label="" parentTable="product" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="14277" childColumn="child_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="rel_prod2prod" elemId="20345" name="product_has_parent" parentRole="" label="" parentTable="product" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="13393" childColumn="parent_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="product_instance" elemId="20353" name="product_has_instance" parentRole="" label="" parentTable="product" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="20826" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="rel_prod2contract_instance" elemId="20361" name="product_has_contract" parentRole="" label="" parentTable="product" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="5535" childColumn="prd_master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="rel_prod2contract_instance" elemId="21440" name="contract_has_product" parentRole="" label="" parentTable="contract_instance" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="4086" childColumn="ctr_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="consents" childTable="consent" elemId="24809" name="party_has_consent" parentRole="party" label="Party has consent" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="26393" childColumn="party_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="consent_instance" elemId="19913" name="consent_has_instance" parentRole="" label="" parentTable="consent" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="20977" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instance_documents" childTable="id_document_instance" elemId="27524151" name="inst_party_has_inst_documents" parentRole="instance_party" label="Instance Party has Instance Address" parentTable="party_instance" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="27524686" childColumn="party_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
	</relationships>
	<masterTables>
		<masterTable elemId="22867" entityRole="golden" topLevel="true" name="party" label="Master Party" instanceTable="party">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="Party Id" type="long_int" isExp="false" elemId="22887" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="party_type" isCmo="true" isSco="false" description="" label="Customer Type" type="string" isExp="false" elemId="128185" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="First Name" type="string" isExp="false" elemId="22868" isPk="false" size="100" enableValidation="true" name="first_name" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27962520" severity="WARNING" enable="true" name="NAME_FIRST_NOT_FOUND" message="First name missing">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27962521" severity="ERROR" enable="true" name="NAME_EMPTY" message="First name missing">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="sco_full_name" infoWarningThreshold="" customExpColumn="exp_full_name"/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Last Name" type="string" isExp="false" elemId="29082" isPk="false" size="100" enableValidation="true" name="last_name" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27962522" severity="WARNING" enable="true" name="NAME_LAST_NOT_VERIFIED" message="Last name missing">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27962523" severity="ERROR" enable="true" name="NAME_EMPTY" message="Last name missing">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="sco_full_name" infoWarningThreshold="" customExpColumn="exp_full_name"/>
					</validations>
				</column>
				<column refData="gender" isCmo="true" isSco="false" description="" label="Gender" type="string" isExp="true" elemId="36720" isPk="false" size="10" enableValidation="true" name="gender" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27848870" severity="ERROR" enable="true" name="GENDER_MISSING_INVALID" message="Missing gender">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="true" description="" label="Birth Date" type="day" isExp="true" elemId="36721" isPk="false" size="" enableValidation="true" name="birth_date" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27844900" severity="INFO" enable="true" name="DATE_FAR_IN_PAST" message="Birth date too far in past">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27844901" severity="WARNING" enable="true" name="DATE_IN_FUTURE" message="Birth date in the future">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27844902" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing birth date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27844903" severity="ERROR" enable="true" name="DATE_INVALID" message="Invalid birth date">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="100" customScoColumn="" infoWarningThreshold="10" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="true" description="" label="Social Insurance Number" type="string" isExp="true" elemId="128186" isPk="false" size="30" enableValidation="true" name="sin" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27962524" severity="ERROR" enable="true" name="SIN_EMPTY" message="">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27962525" severity="ERROR" enable="true" name="SIN_INVALID_CHECK" message="">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27962526" severity="WARNING" enable="true" name="SIN_INVALID_DIGIT_COUNT" message="">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27962527" severity="INFO" enable="true" name="SIN_NOT_PARSED" message="">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="true" description="" label="Company Name" type="string" isExp="true" elemId="128187" isPk="false" size="200" enableValidation="true" name="company_name" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="true" description="" label="Business Number" type="string" isExp="true" elemId="128188" isPk="false" size="30" enableValidation="true" name="business_number" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27851852" severity="WARNING" enable="true" name="BN_INVALID" message="Invalid business number">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27851853" severity="ERROR" enable="true" name="BN_EMPTY" message="Empty business number">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn="exp_business_number"/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="true" description="" label="Established Date" type="day" isExp="true" elemId="128189" isPk="false" size="" enableValidation="true" name="established_date" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27855345" severity="WARNING" enable="true" name="DATE_IN_FUTURE" message="Birth date in the future">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27855346" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing birth date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27855347" severity="WARNING" enable="true" name="DATE_INVALID" message="Invalid birth date">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="legal_form" isCmo="true" isSco="false" description="" label="Business Form" type="string" isExp="false" elemId="26381948" isPk="false" size="30" enableValidation="true" name="legal_form" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27868561" severity="INFO" enable="true" name="CN_LEGAL_FORM_NOT_FOUND" message="Legal name not found">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn="exp_company_name"/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="107789">
						<groups>
							<labeledGroup elemId="6546" name="par_basic" label="Basic Party Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="11473299" condition="" format="" name="id" lookupType=""/>
									<column elemId="87507" condition="" format="" name="cmo_type" lookupType="COMBO"/>
									<column elemId="21463" condition="cmo_type_P" format="" name="cmo_first_name" lookupType=""/>
									<column elemId="21464" condition="cmo_type_P" format="" name="cmo_last_name" lookupType=""/>
									<column elemId="21465" condition="cmo_type_P" format="" name="cmo_gender" lookupType="WINDOW"/>
									<column elemId="21466" condition="cmo_type_P" format="" name="cmo_birth_date" lookupType=""/>
									<column elemId="21467" condition="cmo_type_P" format="" name="cmo_sin" lookupType=""/>
									<column elemId="21468" condition="cmo_type_P" format="" name="age" lookupType=""/>
									<column elemId="93294" condition="cmo_type_C" format="" name="cmo_company_name" lookupType=""/>
									<column elemId="93295" condition="cmo_type_C" format="" name="cmo_business_number" lookupType=""/>
									<column elemId="93296" condition="cmo_type_C" format="" name="cmo_established_date" lookupType=""/>
									<column elemId="93297" condition="cmo_type_C" format="" name="cmo_legal_form" lookupType="WINDOW"/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="10848" name="par_additional" label="Additional Party Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="10851" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="10849" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="10850" condition="" format="" name="published_by" lookupType=""/>
									<column elemId="10853" condition="" format="" name="group_size" lookupType=""/>
									<column elemId="10852" condition="" format="###,###.######" name="group_matching_quality" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledRelatedList elemId="22454" previewLimit="" name="consent" label="Consent" templatePosition="right" relationship="party_has_consent" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cmo_category} ${cmo_purpose}</firstColumnFormat>
									<secondColumnFormat>${cmo_system}${cmo_consumer}${cmo_channel} (${cmo_status})</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedList elemId="158834" previewLimit="" name="address" label="Address" templatePosition="right" relationship="party_has_address" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cmo_type}</firstColumnFormat>
									<secondColumnFormat>${cmo_street} ${cmo_city}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedList elemId="111272" previewLimit="" name="id_doc" label="ID Document" templatePosition="right" relationship="party_has_id_document" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cmo_type}</firstColumnFormat>
									<secondColumnFormat>${cmo_value}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedList elemId="24847" previewLimit="" name="contact" label="Contact" templatePosition="right" relationship="party_has_contact" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cmo_type}</firstColumnFormat>
									<secondColumnFormat>${cmo_value}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedMNList elemId="160124" entity_out="party" previewLimit="" name="p2p_parent" label="Parent Party" templatePosition="left" relationship="party_has_child_party" relationship_out="party_has_parent_party" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Parent Id: ${parent_id} | Relationship Type: ${cmo_p2p_rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Party</firstColumnFormat>
									<secondColumnFormat>${cmo_first_name} ${cmo_last_name} ${cmo_company_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="122759" entity_out="party" previewLimit="" name="p2p_child" label="Child Party" templatePosition="left" relationship="party_has_parent_party" relationship_out="party_has_child_party" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Child Id: ${child_id} | Relationship Type: ${cmo_p2p_rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Party</firstColumnFormat>
									<secondColumnFormat>${cmo_first_name} ${cmo_last_name} ${cmo_company_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="130114" entity_out="contract_instance" previewLimit="" name="p2c" label="Contract" templatePosition="right" relationship="party_has_contract" relationship_out="contract_has_party" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Contract Id: ${contract_source_id} | Relationship Type: ${src_rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Contract Type</firstColumnFormat>
									<secondColumnFormat>${src_type}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<listGrid elemId="31251" name="list_P" label="Instances" templatePosition="bottom" relationship="party_has_instance" viewCondition="cmo_type_P">
								<lists>
									<list elemId="45892" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
											<column elemId="28483227" condition="" name="eng_last_update_date"/>
											<column elemId="28483228" condition="" name="eng_active"/>
										</columns>
									</list>
									<list elemId="45894" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="45895" condition="" name="uni" label="Instances: matching results" columnMask="">
										<columns>
											<column elemId="48665" condition="" name="eng_source_system"/>
											<column elemId="48666" condition="" name="source_id"/>
											<column elemId="48668" condition="" name="master_id"/>
											<column elemId="48669" condition="" name="match_rule_name"/>
											<column elemId="17415" condition="" name="match_quality"/>
											<column elemId="17416" condition="" name="match_related_id"/>
											<column elemId="31252" condition="" name="mat_party_type"/>
											<column elemId="31253" condition="" name="mat_gender"/>
											<column elemId="31254" condition="" name="mat_first_name"/>
											<column elemId="31255" condition="" name="mat_middle_name"/>
											<column elemId="31256" condition="" name="mat_last_name"/>
											<column elemId="31257" condition="" name="mat_full_name"/>
											<column elemId="31258" condition="" name="mat_initials"/>
											<column elemId="31259" condition="" name="mat_person_id"/>
											<column elemId="31260" condition="" name="mat_birth_date"/>
											<column elemId="31261" condition="" name="mat_address_set"/>
											<column elemId="31262" condition="" name="mat_contact_set"/>
											<column elemId="31263" condition="" name="mat_id_doc_set"/>
										</columns>
									</list>
									<list elemId="45896" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
							<listGrid elemId="97076" name="list_C" label="Instances" templatePosition="bottom" relationship="party_has_instance" viewCondition="cmo_type_C">
								<lists>
									<list elemId="45892" condition="" name="src" label="Instances: source columns" columnMask="^src_comp*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
											<column elemId="31739" condition="" name="src_type"/>
										</columns>
									</list>
									<list elemId="45894" condition="" name="std" label="Instances: standardized columns" columnMask="^std_comp*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="45895" condition="" name="uni" label="Instances: matching results" columnMask="">
										<columns>
											<column elemId="48665" condition="" name="eng_source_system"/>
											<column elemId="48666" condition="" name="source_id"/>
											<column elemId="48668" condition="" name="master_id"/>
											<column elemId="48669" condition="" name="match_rule_name"/>
											<column elemId="17415" condition="" name="match_quality"/>
											<column elemId="17416" condition="" name="match_related_id"/>
											<column elemId="31264" condition="" name="mat_party_type"/>
											<column elemId="31265" condition="" name="mat_company_name"/>
											<column elemId="31266" condition="" name="mat_company_id"/>
											<column elemId="31267" condition="" name="mat_address_set"/>
											<column elemId="31268" condition="" name="mat_contact_set"/>
											<column elemId="31269" condition="" name="mat_id_doc_set"/>
										</columns>
									</list>
									<list elemId="45896" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions>
							<guiDetailCondition elemId="12832" name="cmo_type_P">
								<condition>cmo_type = &#39;P&#39;</condition>
							</guiDetailCondition>
							<guiDetailCondition elemId="12833" name="cmo_type_C">
								<condition>cmo_type = &#39;C&#39;</condition>
							</guiDetailCondition>
							<guiDetailCondition elemId="12834" name="cmo_type_Unknown">
								<condition>cmo_type in {&#39;XNA&#39;,&#39;XER&#39;} or cmo_type is null</condition>
							</guiDetailCondition>
						</guiDetailConditions>
					</detailView>
					<breadcrumbView elemId="107790" allColumns="false">
						<columns>
							<name>${cmo_first_name} ${cmo_last_name} ${cmo_company_name}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns>
					<column elemId="657533" indexType="BOTH" dataType="string" name="age" description="Computed age" label="Age">
						<producer>iif(getMdaValString(&#39;cmo_type&#39;)=&#39;P&#39;, 
iif (floor(dateDiff(getMdaValDatetime(&#39;cmo_birth_date&#39;), now(), &#39;DAY&#39;)/365) &gt;= 0,
	toString(floor((dateDiff(getMdaValDatetime(&#39;cmo_birth_date&#39;), now(), &#39;DAY&#39;)-(floor(ceil(dateDiff(getMdaValDatetime(&#39;cmo_birth_date&#39;), now(), &#39;YEAR&#39;)/4))))/365)),
	&#39;N/A&#39;)
, &#39;&#39;)</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="true">
					<validationScoColumns>
						<validationScoColumn elemId="27884608" columns="sco_full_name"/>
						<validationScoColumn elemId="27884609" columns="sco_sin"/>
						<validationScoColumn elemId="27884610" columns="sco_birth_date"/>
						<validationScoColumn elemId="27884611" columns="sco_business_number"/>
						<validationScoColumn elemId="27884612" columns="sco_company_name"/>
						<validationScoColumn elemId="27884613" columns="sco_established_date"/>
					</validationScoColumns>
					<validationExpColumns>
						<validationExpColumn elemId="27961933" name="exp_full_name">
							<validationTabKeys>
								<validationTabKey elemId="27961934" enable="true" name="NAME_LAST_NOT_VERIFIED" message="" quality=""/>
								<validationTabKey elemId="27961935" enable="true" name="NAME_FIRST_NOT_FOUND" message="" quality=""/>
								<validationTabKey elemId="27961936" enable="true" name="NAME_EMPTY" message="" quality="Low"/>
							</validationTabKeys>
						</validationExpColumn>
					</validationExpColumns>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns>
					<column elemId="6284" size="" enableValidation="false" name="group_size" createInto="Merge" label="Group Size" type="integer">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="10833" size="1000" enableValidation="false" name="published_by" createInto="Merge" label="Published By" type="string">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="9905" size="" enableValidation="false" name="group_matching_quality" createInto="Merge" label="Group Matching Quality" type="float">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27550490" size="500" enableValidation="false" name="exp_full_name" createInto="Validation" label="" type="string">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27550491" size="" enableValidation="false" name="sco_full_name" createInto="Validation" label="" type="integer">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
			</advanced>
		</masterTable>
		<masterTable elemId="22881" entityRole="silver" topLevel="false" name="address" label="Master Address" instanceTable="address">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="Address Id" type="long_int" isExp="false" elemId="22885" isPk="true" size="30" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="Party Id" type="long_int" isExp="false" elemId="22891" isPk="false" size="30" enableValidation="false" name="party_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="address_type" isCmo="true" isSco="false" description="" label="Type" type="integer" isExp="false" elemId="129734" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Street" type="string" isExp="false" elemId="35288" isPk="false" size="100" enableValidation="false" name="street" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="City" type="string" isExp="false" elemId="35289" isPk="false" size="100" enableValidation="false" name="city" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="State" type="string" isExp="false" elemId="35290" isPk="false" size="100" enableValidation="false" name="state" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Postal Code" type="string" isExp="false" elemId="35291" isPk="false" size="100" enableValidation="false" name="zip" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="135960">
						<groups>
							<labeledGroup elemId="27864238" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26380488" condition="" format="" name="id" lookupType=""/>
									<column elemId="130397" condition="" format="" name="cmo_type" lookupType=""/>
									<column elemId="511929" condition="" format="" name="cmo_street" lookupType=""/>
									<column elemId="515447" condition="" format="" name="cmo_city" lookupType=""/>
									<column elemId="515449" condition="" format="" name="cmo_zip" lookupType=""/>
									<column elemId="515448" condition="" format="" name="cmo_state" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="198134" name="additional" label="Additional Information" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="11454524" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="11478848" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="15214" condition="" format="" name="published_by" lookupType=""/>
									<column elemId="515446" condition="" format="" name="party_id" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="515582" name="instances" label="Instances" templatePosition="bottom" relationship="address_has_instance" viewCondition="">
								<lists>
									<list elemId="50293" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="50294" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="50295" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="97510" allColumns="false">
						<columns>
							<name>${cmo_street} ${cmo_city}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns>
					<column elemId="6706" size="" enableValidation="false" name="group_size" createInto="" label="Group Size" type="integer">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="11354" size="100" enableValidation="false" name="published_by" createInto="" label="Published By" type="string">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
			</advanced>
		</masterTable>
		<masterTable elemId="36374" entityRole="silver" topLevel="false" name="contact" label="Master Contact" instanceTable="contact">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="Contact Id" type="long_int" isExp="false" elemId="22885" isPk="true" size="30" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="Party Id" type="long_int" isExp="false" elemId="22891" isPk="false" size="30" enableValidation="false" name="party_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="contact_type" isCmo="true" isSco="true" description="" label="Contact Type" type="string" isExp="true" elemId="35288" isPk="false" size="100" enableValidation="true" name="type" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27860641" severity="ERROR" enable="true" name="TYPE_MISSING_INVALID" message="Contact type is missing">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="sco_value" infoWarningThreshold="" customExpColumn="exp_type"/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="true" description="" label="Contact Value" type="string" isExp="true" elemId="35289" isPk="false" size="100" enableValidation="true" name="value" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27861869" severity="" enable="true" name="PHONE_NUMBER_MISSING_IDC" message="Phone is missing IDC">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27861870" severity="" enable="true" name="PHONE_FICTIVE_NUMBER" message="Fictive phone number">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27861871" severity="" enable="true" name="PHONE_EMPTY" message="Phone is missing">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27861872" severity="" enable="true" name="PHONE_WRONG_PATTERN" message="Wrong phone pattern">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27861873" severity="" enable="true" name="PHONE_TOO_LONG" message="Phone too long">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27861874" severity="" enable="true" name="PHONE_TOO_SHORT" message="Phone too short">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27859205" severity="INFO" enable="true" name="PHONE_AREA_OR_CO_CODE_NOT_VERIFIED" message="Phone area or country code not verified">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="100" customScoColumn="sco_value" infoWarningThreshold="10" customExpColumn="exp_value"/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26386301">
						<groups>
							<labeledGroup elemId="27864887" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26376694" condition="" format="" name="id" lookupType=""/>
									<column elemId="26376696" condition="" format="" name="cmo_type" lookupType=""/>
									<column elemId="26376697" condition="" format="" name="cmo_value" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26386302" name="additional" label="Additional Information" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="11461721" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="27864888" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="17562" condition="" format="" name="published_by" lookupType=""/>
									<column elemId="26376695" condition="" format="" name="party_id" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="26380322" name="instances" label="Instances" templatePosition="bottom" relationship="contact_has_instance" viewCondition="">
								<lists>
									<list elemId="50731" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="50732" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="50733" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="86327" allColumns="false">
						<columns>
							<name>${cmo_type} ${cmo_value}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns>
					<column elemId="26380143" indexType="BOTH" dataType="string" name="contact_label" description="" label="Contact Label">
						<producer>getMdaValString(&#39;cmo_type&#39;) + &#39; contact&#39;</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="true">
					<validationScoColumns>
						<validationScoColumn elemId="27960608" columns="sco_type"/>
						<validationScoColumn elemId="27960609" columns="sco_value"/>
					</validationScoColumns>
					<validationExpColumns>
						<validationExpColumn elemId="27960610" name="exp_type">
							<validationTabKeys/>
						</validationExpColumn>
						<validationExpColumn elemId="27960611" name="exp_value">
							<validationTabKeys/>
						</validationExpColumn>
					</validationExpColumns>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns>
					<column elemId="7102" size="" enableValidation="false" name="group_size" createInto="" label="Group Size" type="integer">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="11852" size="100" enableValidation="false" name="published_by" createInto="" label="Published By" type="string">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
			</advanced>
		</masterTable>
		<masterTable elemId="36496" entityRole="silver" topLevel="false" name="rel_party_party" label="Master Party to Party Relations" instanceTable="rel_party2party">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="Relationship Id" type="long_int" isExp="false" elemId="36520" isPk="true" size="30" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="Parent Party Id" type="long_int" isExp="false" elemId="36592" isPk="false" size="30" enableValidation="false" name="parent_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="Child Party Id" type="long_int" isExp="false" elemId="36590" isPk="false" size="30" enableValidation="false" name="child_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="rel_pty2pty_type" isCmo="true" isSco="false" description="" label="Relationship Type" type="string" isExp="false" elemId="36593" isPk="false" size="100" enableValidation="false" name="p2p_rel_type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="rel_pty2pty_ctg" isCmo="true" isSco="false" description="" label="Relationship Category" type="string" isExp="false" elemId="11441674" isPk="false" size="100" enableValidation="false" name="rel_category" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26375491">
						<groups>
							<labeledGroup elemId="26375492" name="basic" label="Party to Party relation" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26375493" condition="" format="" name="id" lookupType=""/>
									<column elemId="26375494" condition="" format="" name="parent_id" lookupType=""/>
									<column elemId="26375495" condition="" format="" name="child_id" lookupType=""/>
									<column elemId="26375496" condition="" format="" name="cmo_p2p_rel_type" lookupType=""/>
									<column elemId="11458928" condition="" format="" name="cmo_rel_category" lookupType=""/>
									<column elemId="109012" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="16153" condition="" format="" name="published_by" lookupType=""/>
									<column elemId="109013" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="109416" name="list" label="Instances " templatePosition="bottom" relationship="relation_party_has_instance" viewCondition="">
								<lists>
									<list elemId="51588" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="51589" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="51590" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns>
					<column elemId="7867" size="" enableValidation="false" name="group_size" createInto="" label="Group Size" type="integer">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="12816" size="100" enableValidation="false" name="published_by" createInto="" label="Published By" type="string">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
			</advanced>
		</masterTable>
		<masterTable elemId="26377244" entityRole="silver" topLevel="false" name="id_document" label="Master ID Document" instanceTable="id_document">
			<description>Identification Document - usually driving licence, ID card, or Passport</description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="Document Id" type="long_int" isExp="false" elemId="26373988" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="Party Id" type="long_int" isExp="false" elemId="26374577" isPk="false" size="" enableValidation="false" name="party_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="id_document_type" isCmo="true" isSco="false" description="" label="Document Type" type="string" isExp="false" elemId="26373989" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Document Value" type="string" isExp="false" elemId="26373990" isPk="false" size="100" enableValidation="false" name="value" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26374207">
						<groups>
							<labeledGroup elemId="26374208" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26374209" condition="" format="" name="id" lookupType=""/>
									<column elemId="26374210" condition="" format="" name="cmo_type" lookupType=""/>
									<column elemId="26374211" condition="" format="" name="cmo_value" lookupType=""/>
									<column elemId="11460928" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="16622" condition="" format="" name="published_by" lookupType=""/>
									<column elemId="26374212" condition="" format="" name="party_id" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="154687" name="list" label="Instances" templatePosition="bottom" relationship="id_document_has_instance" viewCondition="">
								<lists>
									<list elemId="51160" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="51161" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="51162" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="118349" allColumns="false">
						<columns>
							<name>${cmo_value}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns>
					<column elemId="26375307" indexType="BOTH" dataType="string" name="id_document_label" description="" label="ID Document Label">
						<producer>getMdaValString(&#39;cmo_value&#39;)</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns>
					<column elemId="7495" size="" enableValidation="false" name="group_size" createInto="" label="Group Size" type="integer">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="13311" size="100" enableValidation="false" name="published_by" createInto="" label="Published By" type="string">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
			</advanced>
		</masterTable>
		<masterTable elemId="20259" entityRole="silver" topLevel="false" name="rel_prod2prod" label="Master Product to Product Relation" instanceTable="rel_prod2prod">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="long_int" isExp="false" elemId="12220" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="long_int" isExp="false" elemId="13685" isPk="false" size="200" enableValidation="false" name="parent_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="long_int" isExp="false" elemId="13686" isPk="false" size="200" enableValidation="false" name="child_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="" type="string" isExp="false" elemId="13687" isPk="false" size="100" enableValidation="false" name="rel_type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="" type="string" isExp="false" elemId="13688" isPk="false" size="100" enableValidation="false" name="rel_category" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<breadcrumbView elemId="24389" allColumns="false">
						<columns>
							<name>${id} ${cmo_rel_type} $[cmo_rel_category}</name>
						</columns>
					</breadcrumbView>
					<detailView elemId="25276">
						<groups>
							<labeledGroup elemId="25277" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="25278" condition="" format="" name="id" lookupType=""/>
									<column elemId="25279" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="25280" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="25281" condition="" format="" name="parent_id" lookupType=""/>
									<column elemId="25282" condition="" format="" name="child_id" lookupType=""/>
									<column elemId="25283" condition="" format="" name="cmo_rel_type" lookupType=""/>
									<column elemId="25284" condition="" format="" name="cmo_rel_category" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="29782" name="instances" label="Instances" templatePosition="bottom" relationship="rel_prod2prod_has_instance" viewCondition="">
								<lists>
									<list elemId="30519" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="30520" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="30521" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="20279" entityRole="golden" topLevel="true" name="product" label="Master Product" instanceTable="product">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="Product ID" type="long_int" isExp="false" elemId="3498" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Product Name" type="string" isExp="false" elemId="7295" isPk="false" size="100" enableValidation="false" name="name" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Description" type="string" isExp="false" elemId="7296" isPk="false" size="1000" enableValidation="false" name="description" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="product_type" isCmo="true" isSco="false" description="" label="Product Type" type="string" isExp="false" elemId="7297" isPk="false" size="100" enableValidation="false" name="type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Status" type="string" isExp="false" elemId="7298" isPk="false" size="100" enableValidation="false" name="status" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Valid From" type="day" isExp="true" elemId="6800" isPk="false" size="" enableValidation="true" name="valid_from" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27853980" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing valid to date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27853981" severity="ERROR" enable="true" name="DATE_INVALID" message="Invalid valid to date">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Valid To" type="day" isExp="true" elemId="6799" isPk="false" size="" enableValidation="true" name="valid_to" isFk="false">
					<validations>
						<validationKeys>
							<validationKey elemId="27853384" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing valid from date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27853385" severity="ERROR" enable="true" name="DATE_INVALID" message="Invalid valid from">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Package" type="string" isExp="false" elemId="7299" isPk="false" size="10" enableValidation="false" name="package_flag" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="23217">
						<groups>
							<labeledGroup elemId="23218" name="basic" label="Basic Product Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="23219" condition="" format="" name="id" lookupType=""/>
									<column elemId="23220" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="23221" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="23222" condition="" format="" name="cmo_name" lookupType=""/>
									<column elemId="23223" condition="" format="" name="cmo_description" lookupType=""/>
									<column elemId="23224" condition="" format="" name="cmo_type" lookupType="COMBO"/>
									<column elemId="23225" condition="" format="" name="cmo_status" lookupType=""/>
									<column elemId="23226" condition="" format="" name="cmo_package_flag" lookupType=""/>
									<column elemId="18040" condition="" format="" name="cmo_valid_to" lookupType=""/>
									<column elemId="18041" condition="" format="" name="cmo_valid_from" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledRelatedMNList elemId="14726" entity_out="product" previewLimit="" name="prod2prod_parent" label="Product has Parent" templatePosition="right" relationship="product_has_child" relationship_out="product_has_parent" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Parent Id: ${parent_id} | Relationship Type: ${cmo_rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Product</firstColumnFormat>
									<secondColumnFormat>${cmo_name} ${cmo_type}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="8672" entity_out="product" previewLimit="" name="prod2prod_child" label="Product has Child" templatePosition="right" relationship="product_has_parent" relationship_out="product_has_child" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Child Id: ${child_id} | Relationship Type: ${cmo_rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Product</firstColumnFormat>
									<secondColumnFormat>${cmo_name} ${cmo_type}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="12909" entity_out="contract_instance" previewLimit="" name="contract" label="Product has Contract" templatePosition="left" relationship="product_has_contract" relationship_out="contract_has_product" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Contract Id: ${ctr_source_id} | Variant Type: ${src_variant}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Contract</firstColumnFormat>
									<secondColumnFormat>${cio_sale_point} ${cio_type} ${source_id}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<listGrid elemId="19246" name="instances" label="Instances" templatePosition="bottom" relationship="product_has_instance" viewCondition="">
								<lists>
									<list elemId="32926" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="32934" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="34484" condition="" name="cio" label="Instances: cleansed columns" columnMask="^cio_*">
										<columns>
											<column elemId="34485" condition="" name="source_id"/>
											<column elemId="34486" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="32935" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="22912" allColumns="false">
						<columns>
							<name>${cmo_name} ${cmo_type}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="23676" entityRole="silver" topLevel="false" name="consent" label="Master Consent" instanceTable="consent">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="Consent Id" type="long_int" isExp="false" elemId="24260" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="Party Id" type="long_int" isExp="false" elemId="27449" isPk="false" size="" enableValidation="false" name="party_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_category" isCmo="true" isSco="false" description="" label="Category" type="string" isExp="false" elemId="27840104" isPk="false" size="30" enableValidation="false" name="category" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_purpose" isCmo="true" isSco="false" description="" label="Purpose" type="string" isExp="false" elemId="27840105" isPk="false" size="30" enableValidation="false" name="purpose" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_system" isCmo="true" isSco="false" description="" label="System" type="string" isExp="false" elemId="27840106" isPk="false" size="30" enableValidation="false" name="system" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_consumer" isCmo="true" isSco="false" description="" label="Consumer" type="string" isExp="false" elemId="27840107" isPk="false" size="30" enableValidation="false" name="consumer" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_channel" isCmo="true" isSco="false" description="" label="Channel" type="string" isExp="false" elemId="23122" isPk="false" size="30" enableValidation="false" name="channel" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_status" isCmo="true" isSco="false" description="" label="Status" type="string" isExp="false" elemId="23124" isPk="false" size="30" enableValidation="false" name="status" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Origin Date" type="datetime" isExp="false" elemId="28560" isPk="false" size="" enableValidation="false" name="origin_date" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Expiration Date" type="datetime" isExp="false" elemId="23125" isPk="false" size="" enableValidation="false" name="expiration_date" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="20227">
						<groups>
							<labeledGroup elemId="31202" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="27846748" condition="" format="" name="cmo_category" lookupType=""/>
									<column elemId="27846749" condition="" format="" name="cmo_purpose" lookupType=""/>
									<column elemId="27846750" condition="CONS_S" format="" name="cmo_system" lookupType=""/>
									<column elemId="27846751" condition="CONS_A" format="" name="cmo_consumer" lookupType=""/>
									<column elemId="21309" condition="CONS_C" format="" name="cmo_channel" lookupType=""/>
									<column elemId="21310" condition="" format="" name="cmo_status" lookupType=""/>
									<column elemId="28561" condition="" format="" name="cmo_origin_date" lookupType=""/>
									<column elemId="21311" condition="" format="" name="cmo_expiration_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="20228" name="additional" label="Additional Information" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="21305" condition="" format="" name="id" lookupType=""/>
									<column elemId="21306" condition="" format="" name="party_id" lookupType=""/>
									<column elemId="21307" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="21312" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="20768" name="instances" label="Instances" templatePosition="bottom" relationship="consent_has_instance" viewCondition="">
								<lists>
									<list elemId="24796" condition="" name="key" label="Instances: key columns" columnMask="">
										<columns>
											<column elemId="24797" condition="" name="id"/>
											<column elemId="24798" condition="" name="source_id"/>
											<column elemId="24799" condition="" name="master_id"/>
											<column elemId="24800" condition="" name="eng_source_system"/>
											<column elemId="27847937" condition="" name="std_category"/>
											<column elemId="27847938" condition="" name="std_purpose"/>
											<column elemId="27847939" condition="CONS_S" name="std_system"/>
											<column elemId="27847940" condition="CONS_A" name="std_consumer"/>
											<column elemId="24801" condition="CONS_C" name="std_channel"/>
											<column elemId="24804" condition="" name="cio_status"/>
											<column elemId="30143" condition="" name="cio_expiration_date"/>
											<column elemId="24805" condition="" name="cio_origin_date"/>
										</columns>
									</list>
									<list elemId="24269" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions>
							<guiDetailCondition elemId="27846752" name="CONS_A">
								<condition>cmo_category = &quot;access&quot;</condition>
							</guiDetailCondition>
							<guiDetailCondition elemId="27846753" name="CONS_S">
								<condition>cmo_category = &quot;storing&quot;</condition>
							</guiDetailCondition>
							<guiDetailCondition elemId="27846754" name="CONS_C">
								<condition>cmo_category = &quot;contact&quot;</condition>
							</guiDetailCondition>
						</guiDetailConditions>
					</detailView>
					<breadcrumbView elemId="18632" allColumns="false">
						<columns>
							<name>${cmo_category} - ${cmo_purpose} - ${cmo_system}${cmo_consumer}${cmo_channel} : ${cmo_status}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
	</masterTables>
	<instanceTables>
		<instanceTable elemId="22862" topLevel="false" name="party_instance" label="Instance Party" instanceTable="party">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26374600">
						<groups>
							<labeledGroup elemId="26374818" name="source" label="Source Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="18081162" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="26375022" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="26375023" condition="" format="" name="src_first_name" lookupType=""/>
									<column elemId="26375024" condition="" format="" name="src_last_name" lookupType=""/>
									<column elemId="26375025" condition="" format="" name="src_sin" lookupType=""/>
									<column elemId="26375026" condition="" format="" name="src_company_name" lookupType=""/>
									<column elemId="26375027" condition="" format="" name="src_business_number" lookupType=""/>
									<column elemId="26375028" condition="" format="" name="src_birth_date" lookupType=""/>
									<column elemId="26375029" condition="" format="" name="src_gender" lookupType=""/>
									<column elemId="26375031" condition="" format="" name="src_legal_form" lookupType=""/>
									<column elemId="26375032" condition="" format="" name="src_established_date" lookupType=""/>
									<column elemId="18081305" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26374601" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26374604" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="26374602" condition="" format="" name="id" lookupType=""/>
									<column elemId="178085" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="26382361" condition="" format="" name="cio_first_name" lookupType=""/>
									<column elemId="178086" condition="" format="" name="cio_middle_name" lookupType=""/>
									<column elemId="26382362" condition="" format="" name="cio_last_name" lookupType=""/>
									<column elemId="26382363" condition="" format="" name="cio_sin" lookupType=""/>
									<column elemId="26373214" condition="" format="" name="cio_gender" lookupType=""/>
									<column elemId="26382364" condition="" format="" name="cio_company_name" lookupType=""/>
									<column elemId="26382365" condition="" format="" name="cio_business_number" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledRelatedList elemId="27858340" previewLimit="" name="documents" label="ID Document " templatePosition="right" relationship="inst_party_has_inst_documents" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cio_type}</firstColumnFormat>
									<secondColumnFormat>${cio_value}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledGroup elemId="26375238" name="cleansed" label="Cleansed Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="26376064" condition="" format="" name="cio_first_name" lookupType=""/>
									<column elemId="26376065" condition="" format="" name="cio_last_name" lookupType=""/>
									<column elemId="26375798" condition="" format="" name="exp_full_name" lookupType=""/>
									<column elemId="26376066" condition="" format="" name="cio_birth_date" lookupType=""/>
									<column elemId="26375799" condition="" format="" name="exp_birth_date" lookupType=""/>
									<column elemId="26376067" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="26375800" condition="" format="" name="exp_type" lookupType=""/>
									<column elemId="26376068" condition="" format="" name="cio_gender" lookupType=""/>
									<column elemId="26376070" condition="" format="" name="cio_sin" lookupType=""/>
									<column elemId="26375801" condition="" format="" name="exp_sin" lookupType=""/>
									<column elemId="26376071" condition="" format="" name="cio_company_name" lookupType=""/>
									<column elemId="26375802" condition="" format="" name="exp_company_name" lookupType=""/>
									<column elemId="26376072" condition="" format="" name="cio_business_number" lookupType=""/>
									<column elemId="26375803" condition="" format="" name="exp_business_number" lookupType=""/>
									<column elemId="26376073" condition="" format="" name="cio_legal_form" lookupType=""/>
									<column elemId="26376074" condition="" format="" name="cio_established_date" lookupType=""/>
									<column elemId="26375804" condition="" format="" name="exp_established_date" lookupType=""/>
									<column elemId="26376075" condition="" format="" name="cio_address_comp_set" lookupType=""/>
									<column elemId="26376076" condition="" format="" name="cio_contact_comp_set" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26376722" name="matching" label="Matching Details" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="178087" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="26376724" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="14334" condition="" format="" name="match_related_id" lookupType=""/>
									<column elemId="12728" condition="" format="" name="match_quality" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27520419" allColumns="false">
						<columns>
							<name>${cio_first_name} ${cio_last_name} ${cio_company_name}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="211582" name="id" customLabel="Party Id"/>
				<column elemId="657845" name="match_rule_name" customLabel="Matching Rule Name"/>
				<column elemId="211583" name="source_id" customLabel="Data source_id"/>
				<column elemId="211584" name="master_id" customLabel="Master Party Id"/>
				<column elemId="27536475" name="isolate_flag" customLabel="Isolate Flag (master_id)"/>
			</columns>
		</instanceTable>
		<instanceTable elemId="36790" topLevel="false" name="contact_instance" label="Instance Contact" instanceTable="contact">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26380093">
						<groups>
							<labeledGroup elemId="27532596" name="source" label="Source Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="27532597" condition="" format="" name="eng_source_timestamp" lookupType=""/>
									<column elemId="27532598" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="27532599" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="27532600" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="27532601" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="27532602" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="27532603" condition="" format="" name="src_value" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26380094" name="basic" label="Basic Information" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="26380095" condition="" format="" name="id" lookupType=""/>
									<column elemId="26380096" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="18081018" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="26380097" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="26380098" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="26380099" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="26380100" condition="" format="" name="std_type" lookupType=""/>
									<column elemId="26380101" condition="" format="" name="dic_type" lookupType=""/>
									<column elemId="26380102" condition="" format="" name="src_value" lookupType=""/>
									<column elemId="26380103" condition="" format="" name="std_value" lookupType=""/>
									<column elemId="26380104" condition="" format="" name="cio_value" lookupType=""/>
									<column elemId="26380105" condition="" format="" name="exp_value" lookupType=""/>
									<column elemId="26380106" condition="" format="" name="sco_value" lookupType=""/>
									<column elemId="26380107" condition="" format="" name="cio_contact_comp" lookupType=""/>
									<column elemId="26380109" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="26380111" condition="" format="" name="party_master_id" lookupType=""/>
									<column elemId="18081019" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27523080" allColumns="false">
						<columns>
							<name>${cio_type} ${cio_value}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="510184" name="id" customLabel="Contact Id"/>
				<column elemId="510185" name="source_id" customLabel="source_id"/>
				<column elemId="510186" name="master_id" customLabel="Master Contact Id"/>
				<column elemId="658070" name="party_master_id" customLabel="Master Party Id"/>
			</columns>
		</instanceTable>
		<instanceTable elemId="36923" topLevel="false" name="address_instance" label="Instance Address" instanceTable="address">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26373348">
						<groups>
							<labeledGroup elemId="26373349" name="source" label="Source Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="18081449" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="26373767" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="26373768" condition="" format="" name="src_street" lookupType=""/>
									<column elemId="26373769" condition="" format="" name="src_city" lookupType=""/>
									<column elemId="26373771" condition="" format="" name="src_zip" lookupType=""/>
									<column elemId="26373770" condition="" format="" name="src_state" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26373554" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26373555" condition="" format="" name="id" lookupType=""/>
									<column elemId="26373556" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="26373557" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="26373558" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="26373560" condition="" format="" name="party_master_id" lookupType=""/>
									<column elemId="11476866" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="11476867" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26373350" name="cleansed" label="Cleansed Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="26374180" condition="" format="" name="cio_address_comp" lookupType=""/>
									<column elemId="26374181" condition="" format="" name="dic_type" lookupType=""/>
									<column elemId="26374182" condition="" format="" name="cio_street" lookupType=""/>
									<column elemId="26374183" condition="" format="" name="cio_city" lookupType=""/>
									<column elemId="26374184" condition="" format="" name="cio_zip" lookupType=""/>
									<column elemId="26374185" condition="" format="" name="cio_state" lookupType=""/>
									<column elemId="26374594" condition="" format="" name="exp_address" lookupType=""/>
									<column elemId="26374595" condition="" format="" name="sco_address" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27520950" allColumns="false">
						<columns>
							<name>${cio_street} ${cio_city}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="658295" name="id" customLabel="Address Id"/>
				<column elemId="658296" name="source_id" customLabel="Address source_id"/>
				<column elemId="658298" name="party_source_id" customLabel="Party source_id"/>
				<column elemId="658302" name="exp_address" customLabel="Cleansing Explanation"/>
				<column elemId="658303" name="sco_address" customLabel="Cleansing Score"/>
				<column elemId="658304" name="party_master_id" customLabel="Master Party Id"/>
				<column elemId="26375003" name="cio_address_comp" customLabel="Address Component"/>
				<column elemId="26375638" name="dic_type" customLabel="Verified Address Type"/>
				<column elemId="172557" name="master_id" customLabel="Master Address Id"/>
				<column elemId="172897" name="src_type" customLabel="Type"/>
			</columns>
		</instanceTable>
		<instanceTable elemId="38877" topLevel="false" name="rel_party2contract_instance" label="Instance Party to Contract Role" instanceTable="rel_party2contract">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="111806">
						<groups>
							<labeledGroup elemId="110993" name="grp" label="Details" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="111807" condition="" format="" name="id" lookupType=""/>
									<column elemId="111808" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="111809" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="111810" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="111811" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="111812" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="111813" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="111814" condition="" format="" name="contract_source_id" lookupType=""/>
									<column elemId="111815" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="111816" condition="" format="" name="src_rel_type" lookupType=""/>
									<column elemId="111817" condition="" format="" name="std_rel_type" lookupType=""/>
									<column elemId="111818" condition="" format="" name="exp_rel_type" lookupType=""/>
									<column elemId="111819" condition="" format="" name="sco_rel_type" lookupType=""/>
									<column elemId="111825" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="111827" condition="" format="" name="pty_master_id" lookupType=""/>
									<column elemId="111828" condition="" format="" name="party_parent_cio_type" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="212415" name="id" customLabel="Party to Contract Relationship Id"/>
				<column elemId="212416" name="source_id" customLabel="Party to Contract Relationship source_id"/>
				<column elemId="212417" name="contract_source_id" customLabel="Contract source_id"/>
				<column elemId="212423" name="pty_master_id" customLabel="Party Master Id"/>
				<column elemId="132918" name="party_source_id" customLabel="Party source_id"/>
				<column elemId="132919" name="src_rel_type" customLabel="Relationship Type"/>
				<column elemId="132920" name="std_rel_type" customLabel="Relationship Type"/>
			</columns>
		</instanceTable>
		<instanceTable elemId="49273" topLevel="true" name="contract_instance" label="Instance Contract" instanceTable="contract">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="113030">
						<groups>
							<labeledGroup elemId="11489164" name="source" label="Source Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="11505850" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="11491153" condition="" format="" name="src_sale_point" lookupType=""/>
									<column elemId="11491154" condition="" format="" name="src_status" lookupType=""/>
									<column elemId="11491155" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="11491156" condition="" format="" name="src_variant" lookupType=""/>
									<column elemId="11491157" condition="" format="" name="src_valid_from" lookupType=""/>
									<column elemId="11491158" condition="" format="" name="src_valid_to" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="11489160" name="basic" label="Contract Instance Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26374602" condition="" format="" name="id" lookupType=""/>
									<column elemId="26374604" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="11489161" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="11489162" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="11489163" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledRelatedMNList elemId="123614" entity_out="party" previewLimit="" name="c2p" label="Contract to Party Relation" templatePosition="right" relationship="contract_has_party" relationship_out="party_has_contract" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Party Id: ${id}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Party</firstColumnFormat>
									<secondColumnFormat>${cmo_first_name} ${cmo_last_name} ${cmo_company_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="27829213" entity_out="product" previewLimit="" name="c2prod" label="Contract to Product Relation" templatePosition="right" relationship="contract_has_product" relationship_out="product_has_contract" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Product Id: ${id}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt} </secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Product</firstColumnFormat>
									<secondColumnFormat>${cmo_type}: ${cmo_name} - ${cmo_status}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledGroup elemId="11489165" name="cleansed" label="Cleansed Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="11508652" condition="" format="" name="cio_sale_point" lookupType=""/>
									<column elemId="11508653" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="11493537" condition="" format="" name="std_valid_from" lookupType=""/>
									<column elemId="11493538" condition="" format="" name="cio_valid_from" lookupType=""/>
									<column elemId="11493539" condition="" format="" name="exp_valid_from" lookupType=""/>
									<column elemId="11493540" condition="" format="" name="std_valid_to" lookupType=""/>
									<column elemId="11493541" condition="" format="" name="cio_valid_to" lookupType=""/>
									<column elemId="11493542" condition="" format="" name="exp_valid_to" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="113031" allColumns="false">
						<columns>
							<name>${src_type} ${src_variant}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns>
					<column elemId="26386514" indexType="BOTH" dataType="string" name="contract_single_line" description="" label="Contract Label">
						<producer>getMdaValString(&#39;source_id&#39;) + &#39;@&#39; + nvl(getMdaValString(&#39;dic_sale_point&#39;), getMdaValString(&#39;src_sale_point&#39;))</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="132473" name="id" customLabel="Contract Id"/>
				<column elemId="132474" name="source_id" customLabel="Contract Number"/>
				<column elemId="132475" name="src_sale_point" customLabel="Contract Sale Point"/>
				<column elemId="132480" name="src_status" customLabel="Contract Status"/>
				<column elemId="132484" name="src_type" customLabel="Contract Type"/>
				<column elemId="132489" name="src_valid_from" customLabel="Valid From"/>
				<column elemId="132490" name="cio_valid_from" customLabel="Valid From"/>
				<column elemId="132491" name="src_valid_to" customLabel="Valid To"/>
				<column elemId="132492" name="cio_valid_to" customLabel="Valid To"/>
				<column elemId="170476" name="product_source_id" customLabel="Product Family"/>
				<column elemId="11508654" name="cio_sale_point" customLabel="Point of Sale"/>
				<column elemId="11508655" name="cio_type" customLabel="Contract Type"/>
			</columns>
		</instanceTable>
		<instanceTable elemId="78553" topLevel="false" name="rel_party_party_instance" label="Instance Party to Party Relations" instanceTable="rel_party2party">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="110609">
						<groups>
							<labeledGroup elemId="110192" name="grp" label="Attributes" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="110193" condition="" format="" name="id" lookupType=""/>
									<column elemId="110214" condition="" format="" name="dic_rel_type" lookupType=""/>
									<column elemId="110222" condition="" format="" name="party_parent_cio_type" lookupType=""/>
									<column elemId="110194" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="11483602" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="110197" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="110198" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="110199" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="110200" condition="" format="" name="parent_source_id" lookupType=""/>
									<column elemId="110201" condition="" format="" name="child_source_id" lookupType=""/>
									<column elemId="110202" condition="" format="" name="src_rel_type" lookupType=""/>
									<column elemId="110217" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="110219" condition="" format="" name="party_child_master_id" lookupType=""/>
									<column elemId="110220" condition="" format="" name="party_child_cio_type" lookupType=""/>
									<column elemId="110221" condition="" format="" name="party_parent_master_id" lookupType=""/>
									<column elemId="110204" condition="" format="" name="std_p2p_valid_from" lookupType=""/>
									<column elemId="110205" condition="" format="" name="cio_p2p_valid_from" lookupType=""/>
									<column elemId="11484792" condition="" format="" name="exp_p2p_valid_from" lookupType=""/>
									<column elemId="110209" condition="" format="" name="std_p2p_valid_to" lookupType=""/>
									<column elemId="110210" condition="" format="" name="cio_p2p_valid_to" lookupType=""/>
									<column elemId="11484793" condition="" format="" name="exp_p2p_valid_to" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27522547" allColumns="false">
						<columns>
							<name>${dic_rel_type} ${dic_rel_category}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="127730" name="id" customLabel="Party to Party Relation Id"/>
				<column elemId="127731" name="master_id" customLabel="Party to Party Relation Master Id"/>
				<column elemId="127745" name="parent_source_id" customLabel="Parent Party source_id"/>
				<column elemId="127746" name="child_source_id" customLabel="Child Party source_id"/>
				<column elemId="127755" name="party_child_master_id" customLabel="Child Party master_id"/>
				<column elemId="127756" name="party_parent_master_id" customLabel="Parent Party master_id"/>
			</columns>
		</instanceTable>
		<instanceTable elemId="155032" topLevel="false" name="id_document_instance" label="Instance ID Document" instanceTable="id_document">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="155364">
						<groups>
							<labeledGroup elemId="155365" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="155366" condition="" format="" name="id" lookupType=""/>
									<column elemId="155367" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="155368" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="155369" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="155370" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="155371" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="155372" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="155373" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="155374" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="155375" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="155376" condition="" format="" name="dic_type" lookupType=""/>
									<column elemId="155377" condition="" format="" name="src_value" lookupType=""/>
									<column elemId="155378" condition="" format="" name="cio_value" lookupType=""/>
									<column elemId="155379" condition="" format="" name="cio_id_document_comp" lookupType=""/>
									<column elemId="155381" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="155383" condition="" format="" name="party_master_id" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27523615" allColumns="false">
						<columns>
							<name>${cio_type} ${cio_value}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns/>
		</instanceTable>
		<instanceTable elemId="20249" topLevel="false" name="rel_prod2prod_instance" label="Product to Product Relation" instanceTable="rel_prod2prod">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<breadcrumbView elemId="28544" allColumns="false">
						<columns>
							<name>${cio_rel_type} ${cio_rel_category}</name>
						</columns>
					</breadcrumbView>
					<detailView elemId="29163">
						<groups>
							<labeledGroup elemId="29164" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="29165" condition="" format="" name="id" lookupType=""/>
									<column elemId="29166" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="29167" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="29168" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="29169" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="29170" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="29171" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="29172" condition="" format="" name="parent_source_id" lookupType=""/>
									<column elemId="29173" condition="" format="" name="child_source_id" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="29797" name="data" label="Relationship Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="29798" condition="" format="" name="parent_source_id" lookupType=""/>
									<column elemId="29799" condition="" format="" name="child_source_id" lookupType=""/>
									<column elemId="29800" condition="" format="" name="src_rel_type" lookupType=""/>
									<column elemId="29801" condition="" format="" name="cio_rel_type" lookupType=""/>
									<column elemId="29802" condition="" format="" name="src_rel_category" lookupType=""/>
									<column elemId="29803" condition="" format="" name="cio_rel_category" lookupType=""/>
									<column elemId="29804" condition="" format="" name="product_child_master_id" lookupType=""/>
									<column elemId="29805" condition="" format="" name="product_parent_master_id" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns/>
		</instanceTable>
		<instanceTable elemId="20299" topLevel="false" name="product_instance" label="Instance Product" instanceTable="product">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<breadcrumbView elemId="23572" allColumns="false">
						<columns>
							<name>${cio_name} ${cio_type}</name>
						</columns>
					</breadcrumbView>
					<detailView elemId="20156">
						<groups>
							<labeledGroup elemId="20157" name="basic" label="Basic Product Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="20158" condition="" format="" name="id" lookupType=""/>
									<column elemId="24493" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="24494" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="24495" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="24496" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="20159" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="24498" name="source" label="Source Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="25117" condition="" format="" name="src_name" lookupType=""/>
									<column elemId="25118" condition="" format="" name="src_description" lookupType=""/>
									<column elemId="25119" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="25120" condition="" format="" name="src_status" lookupType=""/>
									<column elemId="25121" condition="" format="" name="src_package_flag" lookupType=""/>
									<column elemId="25122" condition="" format="" name="src_valid_to" lookupType=""/>
									<column elemId="25123" condition="" format="" name="src_valid_from" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="24499" name="cleansed" label="Cleansed Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="25736" condition="" format="" name="cio_name" lookupType=""/>
									<column elemId="25737" condition="" format="" name="cio_description" lookupType=""/>
									<column elemId="25738" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="25739" condition="" format="" name="cio_status" lookupType=""/>
									<column elemId="25740" condition="" format="" name="cio_package_flag" lookupType=""/>
									<column elemId="25741" condition="" format="" name="cio_valid_to" lookupType=""/>
									<column elemId="25742" condition="" format="" name="cio_valid_from" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns/>
		</instanceTable>
		<instanceTable elemId="20309" topLevel="false" name="rel_prod2contract_instance" label="Instance Contract 2 Product Role" instanceTable="rel_prod2contract">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<breadcrumbView elemId="26898" allColumns="false">
						<columns>
							<name>${src_variant}</name>
						</columns>
					</breadcrumbView>
					<detailView elemId="27966">
						<groups>
							<labeledGroup elemId="27967" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="27968" condition="" format="" name="id" lookupType=""/>
									<column elemId="27969" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="27970" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="27971" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="27972" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="27973" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="27974" condition="" format="" name="prd_source_id" lookupType=""/>
									<column elemId="27975" condition="" format="" name="ctr_source_id" lookupType=""/>
									<column elemId="27976" condition="" format="" name="src_variant" lookupType=""/>
									<column elemId="27977" condition="" format="" name="prd_master_id" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns/>
		</instanceTable>
		<instanceTable elemId="18315" topLevel="false" name="consent_instance" label="Instance Consent" instanceTable="consent">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="21846">
						<groups>
							<labeledGroup elemId="27533693" name="source" label="Source Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="27533694" condition="" format="" name="id" lookupType=""/>
									<column elemId="27533695" condition="" format="" name="eng_source_timestamp" lookupType=""/>
									<column elemId="27533696" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="27533697" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="27533698" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="27533699" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="27852046" condition="" format="" name="src_category" lookupType=""/>
									<column elemId="27852047" condition="" format="" name="src_purpose" lookupType=""/>
									<column elemId="27852048" condition="" format="" name="src_system" lookupType=""/>
									<column elemId="27852049" condition="" format="" name="src_consumer" lookupType=""/>
									<column elemId="27533700" condition="" format="" name="src_channel" lookupType=""/>
									<column elemId="27533702" condition="" format="" name="src_status" lookupType=""/>
									<column elemId="27533703" condition="" format="" name="src_origin_date" lookupType=""/>
									<column elemId="27533704" condition="" format="" name="src_expiration_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="21847" name="basic" label="Standardized Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="27852050" condition="" format="" name="std_category" lookupType=""/>
									<column elemId="27852051" condition="" format="" name="exp_category" lookupType=""/>
									<column elemId="27852052" condition="" format="" name="sco_category" lookupType=""/>
									<column elemId="27852053" condition="" format="" name="std_purpose" lookupType=""/>
									<column elemId="27852054" condition="" format="" name="exp_purpose" lookupType=""/>
									<column elemId="27852055" condition="" format="" name="sco_purpose" lookupType=""/>
									<column elemId="27852056" condition="" format="" name="std_system" lookupType=""/>
									<column elemId="27852057" condition="" format="" name="exp_system" lookupType=""/>
									<column elemId="27852058" condition="" format="" name="sco_system" lookupType=""/>
									<column elemId="27852059" condition="" format="" name="std_consumer" lookupType=""/>
									<column elemId="27852060" condition="" format="" name="exp_consumer" lookupType=""/>
									<column elemId="27852061" condition="" format="" name="sco_consumer" lookupType=""/>
									<column elemId="27533705" condition="" format="" name="std_channel" lookupType=""/>
									<column elemId="27533706" condition="" format="" name="exp_channel" lookupType=""/>
									<column elemId="27533707" condition="" format="" name="sco_channel" lookupType=""/>
									<column elemId="27533711" condition="" format="" name="std_status" lookupType=""/>
									<column elemId="27533712" condition="" format="" name="cio_status" lookupType=""/>
									<column elemId="27533713" condition="" format="" name="exp_status" lookupType=""/>
									<column elemId="27533714" condition="" format="" name="sco_status" lookupType=""/>
									<column elemId="27533715" condition="" format="" name="std_origin_date" lookupType=""/>
									<column elemId="27533716" condition="" format="" name="cio_origin_date" lookupType=""/>
									<column elemId="27533717" condition="" format="" name="exp_origin_date" lookupType=""/>
									<column elemId="27533718" condition="" format="" name="sco_origin_date" lookupType=""/>
									<column elemId="27533719" condition="" format="" name="std_expiration_date" lookupType=""/>
									<column elemId="27533720" condition="" format="" name="cio_expiration_date" lookupType=""/>
									<column elemId="27533721" condition="" format="" name="exp_expiration_date" lookupType=""/>
									<column elemId="27533722" condition="" format="" name="sco_expiration_date" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27521482" allColumns="false">
						<columns>
							<name>${std_category} - ${std_purpose} : ${cio_status}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns/>
		</instanceTable>
	</instanceTables>
</masterModel>