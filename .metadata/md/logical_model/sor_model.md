<?xml version='1.0' encoding='UTF-8'?>
<sorModel>
	<relationships>
		<relationship childRole="addresses" childTable="address" elemId="27561015" name="party_has_address" parentRole="party" label="Party Has Address" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<sorForeignKey>
				<column elemId="27179683" childColumn="party_id" parentColumn="id"/>
			</sorForeignKey>
		</relationship>
		<relationship childRole="consents" childTable="consent" elemId="27846976" name="party_has_consent" parentRole="party" label="Party Has Consent" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<sorForeignKey>
				<column elemId="27846977" childColumn="party_id" parentColumn="id"/>
			</sorForeignKey>
		</relationship>
		<relationship childRole="relparent" childTable="rel_party2party" elemId="27836210" name="party_has_parent_party" parentRole="parent" label="Party has Parent Party" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<sorForeignKey>
				<column elemId="27837418" childColumn="parent_id" parentColumn="id"/>
			</sorForeignKey>
		</relationship>
		<relationship childRole="relchild" childTable="rel_party2party" elemId="27836211" name="party_has_child_party" parentRole="child" label="Party has Child Party" parentTable="party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<sorForeignKey>
				<column elemId="27836817" childColumn="child_id" parentColumn="id"/>
			</sorForeignKey>
		</relationship>
	</relationships>
	<tables>
		<table elemId="27559465" topLevel="false" name="party" label="SoR Party">
			<description></description>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="27522495">
						<groups>
							<labeledGroup elemId="6546" name="par_basic" label="Basic Party Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="11473299" condition="" format="" name="id" lookupType=""/>
									<column elemId="87507" condition="" format="" name="type" lookupType="COMBO"/>
									<column elemId="21463" condition="type_P" format="" name="first_name" lookupType=""/>
									<column elemId="21464" condition="type_P" format="" name="last_name" lookupType=""/>
									<column elemId="21465" condition="type_P" format="" name="gender" lookupType="WINDOW"/>
									<column elemId="21466" condition="type_P" format="" name="birth_date" lookupType=""/>
									<column elemId="21467" condition="type_P" format="" name="sin" lookupType=""/>
									<column elemId="93294" condition="type_C" format="" name="company_name" lookupType=""/>
									<column elemId="93295" condition="type_C" format="" name="business_number" lookupType=""/>
									<column elemId="93296" condition="type_C" format="" name="established_date" lookupType=""/>
									<column elemId="93297" condition="type_C" format="" name="legal_form" lookupType="WINDOW"/>
									<column elemId="27876003" condition="" format="" name="last_call_timestamp" lookupType=""/>
									<column elemId="27876004" condition="" format="" name="satisfaction_ration" lookupType=""/>
									<column elemId="27902161" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="27902162" condition="" format="" name="eng_modified_by" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledRelatedList elemId="158834" previewLimit="" name="address" label="Address" templatePosition="right" relationship="party_has_address" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${type}</firstColumnFormat>
									<secondColumnFormat>${street} ${city}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedList elemId="27846393" previewLimit="" name="consent" label="Consent" templatePosition="right" relationship="party_has_consent" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${category} ${purpose}</firstColumnFormat>
									<secondColumnFormat>${system}${consumer}${channel} (${status})</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedMNList elemId="27885679" entity_out="party" previewLimit="" name="p2p_parent" label="Parent Party" templatePosition="left" relationship="party_has_child_party" relationship_out="party_has_parent_party" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Parent Id: ${parent_id} | Relationship Type: ${rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Party</firstColumnFormat>
									<secondColumnFormat>${first_name} ${last_name} ${company_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="27885680" entity_out="party" previewLimit="" name="p2p_child" label="Child Party" templatePosition="left" relationship="party_has_parent_party" relationship_out="party_has_child_party" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Child Id: ${child_id} | Relationship Type: ${rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Party</firstColumnFormat>
									<secondColumnFormat>${first_name} ${last_name} ${company_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledGroup elemId="10848" name="par_additional" label="Additional Party Information" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="10851" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="10849" condition="" format="" name="eng_active" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions>
							<guiDetailCondition elemId="12832" name="type_P">
								<condition>type = &#39;P&#39;</condition>
							</guiDetailCondition>
							<guiDetailCondition elemId="12833" name="type_C">
								<condition>type = &#39;C&#39;</condition>
							</guiDetailCondition>
							<guiDetailCondition elemId="12834" name="type_Unknown">
								<condition>type in {&#39;XNA&#39;,&#39;XER&#39;} or type is null</condition>
							</guiDetailCondition>
						</guiDetailConditions>
					</detailView>
					<breadcrumbView elemId="27522496" allColumns="false">
						<columns>
							<name>${first_name} ${last_name} ${company_name}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns/>
			</advanced>
			<columns>
				<column elemId="27094234" refData="" isPk="true" size="" isCmo="false" name="id" description="" label="Id" type="long_int" isFk="false"/>
				<column elemId="27612451" refData="party_type" isPk="false" size="30" isCmo="false" name="type" description="" label="Party Type" type="string" isFk="false"/>
				<column elemId="27612452" refData="" isPk="false" size="100" isCmo="false" name="first_name" description="" label="First Name" type="string" isFk="false"/>
				<column elemId="27612453" refData="" isPk="false" size="100" isCmo="false" name="last_name" description="" label="Last Name" type="string" isFk="false"/>
				<column elemId="27612454" refData="gender" isPk="false" size="10" isCmo="false" name="gender" description="" label="Gender" type="string" isFk="false"/>
				<column elemId="27612455" refData="" isPk="false" size="30" isCmo="false" name="birth_date" description="" label="Birth Date" type="day" isFk="false"/>
				<column elemId="27612456" refData="" isPk="false" size="30" isCmo="false" name="sin" description="" label="SIN" type="integer" isFk="false"/>
				<column elemId="27612457" refData="" isPk="false" size="200" isCmo="false" name="company_name" description="" label="Company Name" type="string" isFk="false"/>
				<column elemId="27612458" refData="" isPk="false" size="30" isCmo="false" name="business_number" description="" label="Business Number" type="string" isFk="false"/>
				<column elemId="27612459" refData="" isPk="false" size="10" isCmo="false" name="established_date" description="" label="Established Date" type="day" isFk="false"/>
				<column elemId="27612460" refData="legal_form" isPk="false" size="30" isCmo="false" name="legal_form" description="" label="Legal Form" type="string" isFk="false"/>
				<column elemId="27875413" refData="" isPk="false" size="" isCmo="false" name="satisfaction_ration" description="" label="Satisfaction Ratio" type="float" isFk="false"/>
				<column elemId="27875414" refData="" isPk="false" size="" isCmo="false" name="last_call_timestamp" description="" label="Last Call " type="datetime" isFk="false"/>
			</columns>
		</table>
		<table elemId="27560482" topLevel="false" name="address" label="SoR Address">
			<description></description>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="27524722">
						<groups>
							<labeledGroup elemId="198134" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26380488" condition="" format="" name="id" lookupType=""/>
									<column elemId="130397" condition="" format="" name="type" lookupType=""/>
									<column elemId="511929" condition="" format="" name="street" lookupType=""/>
									<column elemId="515447" condition="" format="" name="city" lookupType=""/>
									<column elemId="515449" condition="" format="" name="zip" lookupType=""/>
									<column elemId="515448" condition="" format="" name="state" lookupType=""/>
									<column elemId="11454524" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="515446" condition="" format="" name="party_id" lookupType=""/>
									<column elemId="11478848" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="27901524" condition="" format="" name="eng_modified_by" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27524723" allColumns="false">
						<columns>
							<name>${street} ${city}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="Basic Strategy" useSampleThreshold="" enable="true" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns/>
			</advanced>
			<columns>
				<column elemId="27526921" refData="" isPk="true" size="" isCmo="false" name="id" description="" label="Id" type="long_int" isFk="false"/>
				<column elemId="27613530" refData="" isPk="false" size="" isCmo="false" name="party_id" description="" label="Party Id" type="long_int" isFk="true"/>
				<column elemId="27613531" refData="address_type" isPk="false" size="" isCmo="false" name="type" description="" label="Address Type" type="integer" isFk="false"/>
				<column elemId="27613532" refData="" isPk="false" size="100" isCmo="false" name="street" description="" label="Street" type="string" isFk="false"/>
				<column elemId="27613533" refData="" isPk="false" size="100" isCmo="false" name="city" description="" label="City" type="string" isFk="false"/>
				<column elemId="27613534" refData="" isPk="false" size="100" isCmo="false" name="state" description="" label="State" type="string" isFk="false"/>
				<column elemId="27613535" refData="" isPk="false" size="100" isCmo="false" name="zip" description="" label="ZIP" type="string" isFk="false"/>
			</columns>
		</table>
		<table elemId="27840466" topLevel="false" name="consent" label="SoR Consent">
			<description></description>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="27845226">
						<groups>
							<labeledGroup elemId="31202" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="27846748" condition="" format="" name="category" lookupType=""/>
									<column elemId="27846749" condition="" format="" name="purpose" lookupType=""/>
									<column elemId="27846750" condition="CONS_S" format="" name="system" lookupType=""/>
									<column elemId="27846751" condition="CONS_A" format="" name="consumer" lookupType=""/>
									<column elemId="21309" condition="CONS_C" format="" name="channel" lookupType=""/>
									<column elemId="21310" condition="" format="" name="status" lookupType=""/>
									<column elemId="28561" condition="" format="" name="origin_date" lookupType=""/>
									<column elemId="21311" condition="" format="" name="expiration_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="20228" name="additional" label="Additional Information" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="21305" condition="" format="" name="id" lookupType=""/>
									<column elemId="21306" condition="" format="" name="party_id" lookupType=""/>
									<column elemId="21307" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="21312" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions>
							<guiDetailCondition elemId="27846752" name="CONS_A">
								<condition>category = &quot;access&quot;</condition>
							</guiDetailCondition>
							<guiDetailCondition elemId="27846753" name="CONS_S">
								<condition>category = &quot;storing&quot;</condition>
							</guiDetailCondition>
							<guiDetailCondition elemId="27846754" name="CONS_C">
								<condition>category = &quot;contact&quot;</condition>
							</guiDetailCondition>
						</guiDetailConditions>
					</detailView>
					<breadcrumbView elemId="27845227" allColumns="false">
						<columns>
							<name>${category} - ${purpose} - ${system}${consumer}${channel} : ${status}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns/>
			</advanced>
			<columns>
				<column elemId="27840467" refData="" isPk="true" size="" isCmo="false" name="id" description="" label="Id" type="long_int" isFk="false"/>
				<column elemId="27840468" refData="" isPk="false" size="" isCmo="false" name="party_id" description="" label="Party Id" type="long_int" isFk="true"/>
				<column elemId="27840469" refData="consent_category" isPk="false" size="30" isCmo="false" name="category" description="" label="Category" type="string" isFk="false"/>
				<column elemId="27840470" refData="consent_purpose" isPk="false" size="30" isCmo="false" name="purpose" description="" label="Purpose" type="string" isFk="false"/>
				<column elemId="27840471" refData="consent_system" isPk="false" size="30" isCmo="false" name="system" description="" label="System" type="string" isFk="false"/>
				<column elemId="27840472" refData="consent_consumer" isPk="false" size="30" isCmo="false" name="consumer" description="" label="Consumer" type="string" isFk="false"/>
				<column elemId="27840473" refData="consent_channel" isPk="false" size="30" isCmo="false" name="channel" description="" label="Channel" type="string" isFk="false"/>
				<column elemId="27840474" refData="consent_status" isPk="false" size="30" isCmo="false" name="status" description="" label="Status" type="string" isFk="false"/>
				<column elemId="27840475" refData="" isPk="false" size="" isCmo="false" name="origin_date" description="" label="Origin Date" type="datetime" isFk="false"/>
				<column elemId="27840476" refData="" isPk="false" size="" isCmo="false" name="expiration_date" description="" label="Expiration Date" type="datetime" isFk="false"/>
			</columns>
		</table>
		<table elemId="27830731" topLevel="false" name="rel_party2party" label="SoR Party to Party Relations">
			<description></description>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="27835609">
						<groups>
							<labeledGroup elemId="27835610" name="basic" label="Party to Party relation" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26375494" condition="" format="" name="parent_id" lookupType=""/>
									<column elemId="26375495" condition="" format="" name="child_id" lookupType=""/>
									<column elemId="26375496" condition="" format="" name="rel_type" lookupType=""/>
									<column elemId="11458928" condition="" format="" name="rel_category" lookupType=""/>
									<column elemId="27835606" condition="" format="" name="p2p_valid_from" lookupType=""/>
									<column elemId="27835607" condition="" format="" name="p2p_valid_to" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26375492" name="additional" label="Additional information" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="26375494" condition="" format="" name="id" lookupType=""/>
									<column elemId="27835611" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="27835612" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression></inputFilterExpression>
				<specialColumns/>
			</advanced>
			<columns>
				<column elemId="27830732" refData="" isPk="true" size="" isCmo="false" name="id" description="" label="Id" type="long_int" isFk="false"/>
				<column elemId="27830733" refData="" isPk="false" size="" isCmo="false" name="parent_id" description="" label="Parent Id" type="long_int" isFk="true"/>
				<column elemId="27830734" refData="" isPk="false" size="" isCmo="false" name="child_id" description="" label="Child Id" type="long_int" isFk="true"/>
				<column elemId="27830735" refData="rel_pty2pty_type" isPk="false" size="100" isCmo="false" name="rel_type" description="" label="Relation Type" type="string" isFk="false"/>
				<column elemId="27830736" refData="rel_pty2pty_ctg" isPk="false" size="100" isCmo="false" name="rel_category" description="" label="Relation Category" type="string" isFk="false"/>
				<column elemId="27830737" refData="" isPk="false" size="" isCmo="false" name="p2p_valid_from" description="" label="Valid From" type="day" isFk="false"/>
				<column elemId="27830738" refData="" isPk="false" size="" isCmo="false" name="p2p_valid_to" description="" label="Valid To" type="day" isFk="false"/>
			</columns>
		</table>
	</tables>
</sorModel>