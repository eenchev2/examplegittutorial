<?xml version='1.0' encoding='UTF-8'?>
<instanceModel label="">
	<relationships>
		<relationship childRole="addresses" childTable="address" elemId="22725" name="party_has_address" parentRole="party" parentTable="party" type="Same System">
			<description></description>
			<childToParent>
				<column elemId="111566" method="concatenateDistinct" size="40000" name="cio_address_comp_set" createInto="party_match" label="" source="cio_address_comp" type="string" separator="^~">
					<filterExpression></filterExpression>
				</column>
			</childToParent>
			<parentToChild>
				<column elemId="29326" size="" name="party_master_id" createInto="address_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="435514" childColumn="party_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="contacts" childTable="contact" elemId="35423" name="party_has_contact" parentRole="party" parentTable="party" type="Same System">
			<description>Relationship between Party and Contact. Party can have have multiple Contacts (even same type and value), and each Contact belongs exactly to one Party</description>
			<childToParent>
				<column elemId="105669" method="concatenateDistinct" size="2000" name="cio_contact_comp_set" createInto="party_match" label="" source="cio_contact_comp" type="string" separator="^~">
					<filterExpression>source.cio_contact_comp is not null</filterExpression>
				</column>
			</childToParent>
			<parentToChild>
				<column elemId="37575" size="" name="party_master_id" createInto="contact_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="455507" childColumn="party_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="rel_party_contract" childTable="rel_party2contract" elemId="35618" name="party_has_contract" parentRole="party" parentTable="party" type="Same System">
			<description></description>
			<childToParent/>
			<parentToChild>
				<column elemId="39002" size="" name="pty_master_id" createInto="rel_party2contract_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
				<column elemId="11885" size="30" name="party_parent_cio_type" createInto="rel_party2contract_match" label="" source="cio_type" type="string">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="455653" childColumn="party_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="party_contract" childTable="rel_party2contract" elemId="35634" name="contract_has_party" parentRole="ctr" parentTable="contract" type="Same System">
			<description></description>
			<childToParent/>
			<parentToChild/>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="455799" childColumn="contract_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="rel_party_party_parent" childTable="rel_party2party" elemId="35827" name="party_has_child_party" parentRole="party_child" parentTable="party" type="Same System">
			<description></description>
			<childToParent/>
			<parentToChild>
				<column elemId="37694" size="" name="party_child_master_id" createInto="rel_party2party_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
				<column elemId="20870" size="30" name="party_child_cio_type" createInto="rel_party2party_match" label="" source="cio_type" type="string">
					<filterExpression></filterExpression>
				</column>
				<column elemId="185578" size="" name="party_child_match_isolate" createInto="rel_party2party_match" label="" source="isolate_flag" type="boolean">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="455945" childColumn="child_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="rel_party_party_child" childTable="rel_party2party" elemId="35885" name="party_has_parent_party" parentRole="party_parent" parentTable="party" type="Same System">
			<description></description>
			<childToParent/>
			<parentToChild>
				<column elemId="37993" size="" name="party_parent_master_id" createInto="rel_party2party_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
				<column elemId="21149" size="30" name="party_parent_cio_type" createInto="rel_party2party_match" label="" source="cio_type" type="string">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="456091" childColumn="parent_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="id_documents" childTable="id_document" elemId="26400708" name="party_has_id_document" parentRole="party" parentTable="party" type="Same System">
			<description></description>
			<childToParent>
				<column elemId="26401859" method="concatenateDistinct" size="2000" name="cio_id_document_set" createInto="party_match" label="" source="cio_id_document_comp" type="string" separator="^~">
					<filterExpression>source.cio_id_document_comp is not null</filterExpression>
				</column>
			</childToParent>
			<parentToChild>
				<column elemId="26401858" size="" name="party_master_id" createInto="id_document_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="26403943" childColumn="party_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="rel_prod2prod" elemId="14735" name="product_has_parent" parentRole="" parentTable="product" type="Same System">
			<description></description>
			<childToParent/>
			<parentToChild>
				<column elemId="16637" size="" name="product_parent_master_id" createInto="rel_prod2prod_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="20728" childColumn="parent_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="rel_prod2contract" elemId="14749" name="contract_has_product" parentRole="" parentTable="contract" type="Same System">
			<description></description>
			<childToParent/>
			<parentToChild/>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="19950" childColumn="ctr_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="rel_prod2contract" elemId="14763" name="product_has_contract" parentRole="" parentTable="product" type="Same System">
			<description></description>
			<childToParent/>
			<parentToChild>
				<column elemId="16049" size="" name="prd_master_id" createInto="rel_prod2contract_clean" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="true" childColumn="" parentColumn="meta_system_override"/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="20339" childColumn="prd_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="rel_prod2prod" elemId="14777" name="product_has_child" parentRole="" parentTable="product" type="Same System">
			<description></description>
			<childToParent/>
			<parentToChild>
				<column elemId="16935" size="" name="product_child_master_id" createInto="rel_prod2prod_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="21117" childColumn="child_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="consents" childTable="consent" elemId="5872" name="party_has_consent" parentRole="party" parentTable="party" type="Same System">
			<description></description>
			<childToParent/>
			<parentToChild>
				<column elemId="20546" size="" name="party_master_id" createInto="consent_match" label="" source="master_id" type="long_int">
					<filterExpression></filterExpression>
				</column>
				<column elemId="25331" size="" name="party_match_isolate" createInto="consent_match" label="" source="isolate_flag" type="boolean">
					<filterExpression></filterExpression>
				</column>
			</parentToChild>
			<advanced>
				<extendedSameSystem enable="false" childColumn="" parentColumn=""/>
				<alternativeKey alternativePk="" alternativeFk=""/>
			</advanced>
			<foreignKey>
				<column elemId="6376" childColumn="party_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
	</relationships>
	<tables>
		<table elemId="22660" topLevel="true" name="party" label="Instance Party" type="instance">
			<description>Entity represents all company customers - persons and companies</description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="22662" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description>This ID serves as Primary Key in the external system providing the data. Combination of source_id and origin is unique in the MDM hub</description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="First Name" type="string" isExp="false" elemId="22754" isPk="false" size="100" enableValidation="true" name="first_name" isFk="false">
					<description>First name</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27960012" severity="INFO" enable="true" name="NAME_FIRST_NOT_FOUND" message="First name not found">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27960013" severity="ERROR" enable="true" name="NAME_EMPTY" message="First name missing">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn="exp_full_name"/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="Middle Name" type="string" isExp="false" elemId="144664" isPk="false" size="100" enableValidation="true" name="middle_name" isFk="false">
					<description>Middle name</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27849044" severity="INFO" enable="true" name="NAME_MIDDLE_EMPTY" message="Middle name is missing">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn="exp_full_name"/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="Last Name" type="string" isExp="false" elemId="22755" isPk="false" size="100" enableValidation="true" name="last_name" isFk="false">
					<description>Last name</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27846679" severity="INFO" enable="true" name="NAME_LAST_NOT_VERIFIED" message="Last name not verified">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27846680" severity="ERROR" enable="true" name="NAME_EMPTY" message="Last name missing">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn="exp_full_name"/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Birth Date" type="day" isExp="true" elemId="35355" isPk="false" size="" enableValidation="true" name="birth_date" isFk="false">
					<description>Date of birth</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27960018" severity="INFO" enable="true" name="DATE_FAR_IN_PAST" message="Birth date too far in past">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27844303" severity="WARNING" enable="true" name="DATE_IN_FUTURE" message="Birth date in the future">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27844304" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing birth date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27844305" severity="ERROR" enable="true" name="DATE_INVALID" message="Invalid birth date">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="party_type" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Type" type="string" isExp="true" elemId="35356" isPk="false" size="30" enableValidation="true" name="type" isFk="false">
					<description>Type of customer - either Person or Company</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27850229" severity="ERROR" enable="true" name="TYPE_MISSING_INVALID" message="Party type is missing">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="gender" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Gender" type="string" isExp="true" elemId="22756" isPk="false" size="10" enableValidation="true" name="gender" isFk="false">
					<description>Gender</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27846086" severity="ERROR" enable="true" name="GENDER_MISSING_INVALID" message="Missing gender">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="SIN" type="string" isExp="true" elemId="626" isPk="false" size="30" enableValidation="true" name="sin" isFk="false">
					<description>Social insurance number</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27959423" severity="ERROR" enable="true" name="SIN_EMPTY" message="">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27959424" severity="ERROR" enable="true" name="SIN_INVALID_CHECK" message="">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27959427" severity="WARNING" enable="true" name="SIN_INVALID_DIGIT_COUNT" message="">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27959428" severity="INFO" enable="true" name="SIN_NOT_PARSED" message="">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Company Name" type="string" isExp="true" elemId="627" isPk="false" size="200" enableValidation="true" name="company_name" isFk="false">
					<description>Company name</description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Business Number" type="string" isExp="true" elemId="628" isPk="false" size="30" enableValidation="true" name="business_number" isFk="false">
					<description>Business number</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27842523" severity="WARNING" enable="true" name="BN_INVALID" message="Invalid business number">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27842524" severity="ERROR" enable="true" name="BN_EMPTY" message="Empty business number">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="legal_form" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="Legal Form" type="string" isExp="false" elemId="107186" isPk="false" size="30" enableValidation="true" name="legal_form" isFk="false">
					<description>Company legal form</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27840614" severity="INFO" enable="true" name="CN_LEGAL_FORM_NOT_FOUND" message="Legal name not found">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn="exp_company_name"/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Established Date" type="day" isExp="true" elemId="629" isPk="false" size="" enableValidation="true" name="established_date" isFk="false">
					<description>Company established date</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27841208" severity="WARNING" enable="true" name="DATE_IN_FUTURE" message="Birth date in the future">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27841209" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing birth date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27841210" severity="WARNING" enable="true" name="DATE_INVALID" message="Invalid birth date">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="SIN" type="string" isExp="true" elemId="27868267" isPk="false" size="30" enableValidation="true" name="not_xin" isFk="false">
					<description>Social insurance number</description>
					<validations>
						<validationKeys>
							<validationKey elemId="27959423" severity="ERROR" enable="true" name="SIN_EMPTY" message="">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27959424" severity="ERROR" enable="true" name="SIN_INVALID_CHECK" message="">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27959427" severity="WARNING" enable="true" name="SIN_INVALID_DIGIT_COUNT" message="">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27959428" severity="INFO" enable="true" name="SIN_NOT_PARSED" message="">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="true" matching="true">
				<matchingProposals enable="true"/>
				<matchingTabColumns>
					<column elemId="20413" size="1000" enableValidation="false" name="mat_party_type" label="Type (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20414" size="1000" enableValidation="false" name="mat_gender" label="Gender (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20415" size="1000" enableValidation="false" name="mat_first_name" label="First Name (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20416" size="1000" enableValidation="false" name="mat_middle_name" label="Middle Name (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20417" size="1000" enableValidation="false" name="mat_last_name" label="Last Name (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20418" size="1000" enableValidation="false" name="mat_full_name" label="Full Name (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20419" size="1000" enableValidation="false" name="mat_initials" label="Initials (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20420" size="1000" enableValidation="false" name="mat_person_id" label="SIN (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20421" size="" enableValidation="false" name="mat_birth_date" label="Birth Date (mat)" type="day" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20422" size="1000" enableValidation="false" name="mat_company_name" label="Company Name (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20423" size="1000" enableValidation="false" name="mat_company_id" label="Business Number (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20424" size="1000" enableValidation="false" name="mat_address_set" label="Addresses set (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20425" size="1000" enableValidation="false" name="mat_contact_set" label="Contacts set (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="20426" size="1000" enableValidation="false" name="mat_id_doc_set" label="Documents set (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</matchingTabColumns>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="master_id" aggregation="true">
				<aggregationTabColumns>
					<aggregationTabColumn elemId="16123" size="" name="agg_group_size" label="Matching Group Size" type="integer" isFk="false">
						<description>Number of records in the matching group</description>
					</aggregationTabColumn>
				</aggregationTabColumns>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26374600">
						<groups>
							<labeledGroup elemId="26374818" name="source" label="Source Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="18081162" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="26375022" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="26375023" condition="" format="" name="src_first_name" lookupType=""/>
									<column elemId="26375024" condition="" format="" name="src_last_name" lookupType=""/>
									<column elemId="26375025" condition="" format="" name="src_sin" lookupType=""/>
									<column elemId="26375026" condition="" format="" name="src_company_name" lookupType=""/>
									<column elemId="26375027" condition="" format="" name="src_business_number" lookupType=""/>
									<column elemId="26375028" condition="" format="" name="src_birth_date" lookupType=""/>
									<column elemId="26375029" condition="" format="" name="src_gender" lookupType=""/>
									<column elemId="26375031" condition="" format="" name="src_legal_form" lookupType=""/>
									<column elemId="26375032" condition="" format="" name="src_established_date" lookupType=""/>
									<column elemId="18081305" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26374601" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26374604" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="26374602" condition="" format="" name="id" lookupType=""/>
									<column elemId="178085" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="26382361" condition="" format="" name="cio_first_name" lookupType=""/>
									<column elemId="178086" condition="" format="" name="cio_middle_name" lookupType=""/>
									<column elemId="26382362" condition="" format="" name="cio_last_name" lookupType=""/>
									<column elemId="26382363" condition="" format="" name="cio_sin" lookupType=""/>
									<column elemId="26373214" condition="" format="" name="cio_gender" lookupType=""/>
									<column elemId="26382364" condition="" format="" name="cio_company_name" lookupType=""/>
									<column elemId="26382365" condition="" format="" name="cio_business_number" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledRelatedList elemId="27869544" previewLimit="" name="address_inst" label="Instance Addresses" templatePosition="right" relationship="party_has_address" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${std_type}</firstColumnFormat>
									<secondColumnFormat>${cio_street} ${cio_city}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedList elemId="27865141" previewLimit="" name="contact_inst" label="Instance Contacts" templatePosition="right" relationship="party_has_contact" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cio_type}</firstColumnFormat>
									<secondColumnFormat>${cio_value}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledGroup elemId="26375238" name="cleansed" label="Cleansed Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="26376067" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="26376064" condition="" format="" name="cio_first_name" lookupType=""/>
									<column elemId="27866872" condition="" format="" name="cio_middle_name" lookupType=""/>
									<column elemId="26376065" condition="" format="" name="cio_last_name" lookupType=""/>
									<column elemId="26376070" condition="" format="" name="cio_sin" lookupType=""/>
									<column elemId="27866873" condition="" format="" name="cio_birth_date" lookupType=""/>
									<column elemId="27866874" condition="" format="" name="cio_gender" lookupType=""/>
									<column elemId="26376071" condition="" format="" name="cio_company_name" lookupType=""/>
									<column elemId="26376072" condition="" format="" name="cio_business_number" lookupType=""/>
									<column elemId="26376073" condition="" format="" name="cio_legal_form" lookupType=""/>
									<column elemId="26376074" condition="" format="" name="cio_established_date" lookupType=""/>
									<column elemId="26376075" condition="" format="" name="cio_address_comp_set" lookupType=""/>
									<column elemId="26376076" condition="" format="" name="cio_contact_comp_set" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26376722" name="matching" label="Matching Details" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="178087" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="26376724" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="14334" condition="" format="" name="match_related_id" lookupType=""/>
									<column elemId="12728" condition="" format="" name="match_quality" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="27873858" name="contact" label="Instance Contacts " templatePosition="bottom" relationship="party_has_contact" viewCondition="">
								<lists>
									<list elemId="27874436" condition="" name="All" label="All Columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27520419" allColumns="false">
						<columns>
							<name>${cio_first_name} ${cio_last_name} ${cio_company_name}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="true">
					<validationScoColumns>
						<validationScoColumn elemId="27957495" columns="sco_instance"/>
					</validationScoColumns>
					<validationExpColumns>
						<validationExpColumn elemId="27957496" name="exp_instance">
							<validationTabKeys>
								<validationTabKey elemId="27958830" enable="true" name="SIN_EMPTY" message="" quality=""/>
								<validationTabKey elemId="27958831" enable="true" name="SIN_INVALID_CHECK" message="" quality="Low"/>
								<validationTabKey elemId="27958832" enable="true" name="SIN_INVALID_DIGIT_COUNT" message="" quality="Medium"/>
								<validationTabKey elemId="27958833" enable="true" name="SIN_NOT_PARSED" message="" quality="High"/>
							</validationTabKeys>
						</validationExpColumn>
					</validationExpColumns>
					<validationTabOverrideDefaultSettings highDQThreshold="1000" lowDQThreshold="10000000"/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns>
					<column elemId="105256" size="" enableValidation="false" name="sco_full_name" createInto="party_clean" label="Full Name (sco)" type="integer" isFk="false">
						<description>Full name cleansing Score column</description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="105257" size="2000" enableValidation="false" name="exp_full_name" createInto="party_clean" label="Full Name (exp)" type="string" isFk="false">
						<description>Full name cleansing Explanation column</description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="166560" size="" enableValidation="false" name="sco_instance" createInto="party_clean" label="Instance Score" type="integer" isFk="false">
						<description>Concatenated score across the whole party entity</description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="27957497" size="2000" enableValidation="false" name="exp_instance" createInto="party_clean" label="Instance Explanation" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="145001" size="20" enableValidation="false" name="pat_name" createInto="party_clean" label="Name Parsing Pattern" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="6481" size="1000" enableValidation="false" name="published_by" createInto="party_clean" label="Published By" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="22721" topLevel="false" name="address" label="Instance Address" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="22723" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Party Source ID" type="string" isExp="false" elemId="22734" isPk="false" size="200" enableValidation="false" name="party_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="address_type" isCio="false" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Type" type="integer" isExp="true" elemId="114595" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="Street" type="string" isExp="false" elemId="22759" isPk="false" size="100" enableValidation="false" name="street" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="City" type="string" isExp="false" elemId="22760" isPk="false" size="100" enableValidation="false" name="city" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="State" type="string" isExp="false" elemId="22761" isPk="false" size="100" enableValidation="false" name="state" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="ZIP" type="string" isExp="false" elemId="22762" isPk="false" size="100" enableValidation="false" name="zip" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="true" label="Address" type="string" isExp="true" elemId="22763" isPk="false" size="100" enableValidation="false" name="address" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="true">
				<matchingProposals enable="false"/>
				<matchingTabColumns/>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26373348">
						<groups>
							<labeledGroup elemId="26373349" name="source" label="Source Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="18081449" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="26373767" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="26373768" condition="" format="" name="src_street" lookupType=""/>
									<column elemId="26373769" condition="" format="" name="src_city" lookupType=""/>
									<column elemId="26373771" condition="" format="" name="src_zip" lookupType=""/>
									<column elemId="26373770" condition="" format="" name="src_state" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26373554" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26373555" condition="" format="" name="id" lookupType=""/>
									<column elemId="26373556" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="26373557" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="26373558" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="26373560" condition="" format="" name="party_master_id" lookupType=""/>
									<column elemId="11476866" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="11476867" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26373350" name="cleansed" label="Cleansed Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="26374180" condition="" format="" name="cio_address_comp" lookupType=""/>
									<column elemId="26374181" condition="" format="" name="dic_type" lookupType=""/>
									<column elemId="26374182" condition="" format="" name="cio_street" lookupType=""/>
									<column elemId="26374183" condition="" format="" name="cio_city" lookupType=""/>
									<column elemId="26374184" condition="" format="" name="cio_zip" lookupType=""/>
									<column elemId="26374185" condition="" format="" name="cio_state" lookupType=""/>
									<column elemId="26374594" condition="" format="" name="exp_address" lookupType=""/>
									<column elemId="26374595" condition="" format="" name="sco_address" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27520950" allColumns="false">
						<columns>
							<name>${cio_street} ${cio_city}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns>
					<column elemId="111107" size="1000" enableValidation="false" name="cio_address_comp" createInto="address_clean" label="Address set" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="149960" size="100" enableValidation="false" name="meta_address_status" createInto="address_clean" label="Quality Staus" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="149961" size="100" enableValidation="false" name="meta_address_validity_level" createInto="address_clean" label="Validity Level" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="26965426" size="100" enableValidation="false" name="meta_label" createInto="address_clean" label="Address Label" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="8833" size="100" enableValidation="false" name="published_by" createInto="address_clean" label="Published By" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="35382" topLevel="false" name="contact" label="Instance Contact" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="35452" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Party Source ID" type="string" isExp="false" elemId="35384" isPk="false" size="200" enableValidation="false" name="party_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="contact_type" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Type" type="string" isExp="true" elemId="35399" isPk="false" size="20" enableValidation="true" name="type" isFk="false">
					<description></description>
					<validations>
						<validationKeys>
							<validationKey elemId="27860641" severity="ERROR" enable="true" name="TYPE_MISSING_INVALID" message="Contact type is missing">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Value" type="string" isExp="true" elemId="35400" isPk="false" size="100" enableValidation="true" name="value" isFk="false">
					<description></description>
					<validations>
						<validationKeys>
							<validationKey elemId="27861869" severity="" enable="true" name="PHONE_NUMBER_MISSING_IDC" message="Phone is missing IDC">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27861870" severity="" enable="true" name="PHONE_FICTIVE_NUMBER" message="Fictive phone number">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27861871" severity="" enable="true" name="PHONE_EMPTY" message="Phone is missing">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27861872" severity="" enable="true" name="PHONE_WRONG_PATTERN" message="Wrong phone pattern">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27861873" severity="" enable="true" name="PHONE_TOO_LONG" message="Phone too long">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27861874" severity="" enable="true" name="PHONE_TOO_SHORT" message="Phone too short">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="100" customScoColumn="" infoWarningThreshold="10" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="true">
				<matchingProposals enable="false"/>
				<matchingTabColumns>
					<column elemId="18781" size="1000" enableValidation="false" name="mat_value" label="Value (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</matchingTabColumns>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26380093">
						<groups>
							<labeledGroup elemId="27532596" name="source" label="Source Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="27532597" condition="" format="" name="eng_source_timestamp" lookupType=""/>
									<column elemId="27532598" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="27532599" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="27532600" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="27532601" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="27532602" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="27532603" condition="" format="" name="src_value" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26380094" name="basic" label="Basic Information" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="26380095" condition="" format="" name="id" lookupType=""/>
									<column elemId="26380096" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="18081018" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="26380097" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="26380098" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="26380099" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="26380100" condition="" format="" name="std_type" lookupType=""/>
									<column elemId="26380101" condition="" format="" name="dic_type" lookupType=""/>
									<column elemId="26380102" condition="" format="" name="src_value" lookupType=""/>
									<column elemId="26380103" condition="" format="" name="std_value" lookupType=""/>
									<column elemId="26380104" condition="" format="" name="cio_value" lookupType=""/>
									<column elemId="26380105" condition="" format="" name="exp_value" lookupType=""/>
									<column elemId="26380106" condition="" format="" name="sco_value" lookupType=""/>
									<column elemId="26380107" condition="" format="" name="cio_contact_comp" lookupType=""/>
									<column elemId="26380109" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="26380111" condition="" format="" name="party_master_id" lookupType=""/>
									<column elemId="18081019" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27523080" allColumns="false">
						<columns>
							<name>${cio_type} ${cio_value}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns>
					<column elemId="112223" size="1000" enableValidation="false" name="cio_contact_comp" createInto="contact_clean" label="Contact set" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="9336" size="100" enableValidation="false" name="published_by" createInto="contact_clean" label="Published By" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="35725" topLevel="false" name="rel_party2party" label="Instance Party2Pary Relation" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="35727" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Parent Source ID" type="string" isExp="false" elemId="35749" isPk="false" size="200" enableValidation="false" name="parent_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Child Source ID" type="string" isExp="false" elemId="35747" isPk="false" size="200" enableValidation="false" name="child_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="rel_pty2pty_type" isCio="false" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="Relation Type" type="string" isExp="false" elemId="35750" isPk="false" size="100" enableValidation="false" name="rel_type" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="rel_pty2pty_ctg" isCio="false" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="Relation Category" type="string" isExp="false" elemId="11440487" isPk="false" size="100" enableValidation="false" name="rel_category" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Valid From" type="day" isExp="true" elemId="26373496" isPk="false" size="" enableValidation="true" name="p2p_valid_from" isFk="false">
					<description></description>
					<validations>
						<validationKeys>
							<validationKey elemId="27853384" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing valid from date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27853385" severity="ERROR" enable="true" name="DATE_INVALID" message="Invalid valid from">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Valid To" type="day" isExp="true" elemId="26373497" isPk="false" size="" enableValidation="true" name="p2p_valid_to" isFk="false">
					<description></description>
					<validations>
						<validationKeys>
							<validationKey elemId="27853980" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing valid to date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27853981" severity="ERROR" enable="true" name="DATE_INVALID" message="Invalid valid to date">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="true">
				<matchingProposals enable="false"/>
				<matchingTabColumns/>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="110609">
						<groups>
							<labeledGroup elemId="110192" name="grp" label="Attributes" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="110193" condition="" format="" name="id" lookupType=""/>
									<column elemId="110214" condition="" format="" name="dic_rel_type" lookupType=""/>
									<column elemId="110222" condition="" format="" name="party_parent_cio_type" lookupType=""/>
									<column elemId="110194" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="11483602" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="110197" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="110198" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="110199" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="110200" condition="" format="" name="parent_source_id" lookupType=""/>
									<column elemId="110201" condition="" format="" name="child_source_id" lookupType=""/>
									<column elemId="110202" condition="" format="" name="src_rel_type" lookupType=""/>
									<column elemId="110217" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="110219" condition="" format="" name="party_child_master_id" lookupType=""/>
									<column elemId="110220" condition="" format="" name="party_child_cio_type" lookupType=""/>
									<column elemId="110221" condition="" format="" name="party_parent_master_id" lookupType=""/>
									<column elemId="110204" condition="" format="" name="std_p2p_valid_from" lookupType=""/>
									<column elemId="110205" condition="" format="" name="cio_p2p_valid_from" lookupType=""/>
									<column elemId="11484792" condition="" format="" name="exp_p2p_valid_from" lookupType=""/>
									<column elemId="110209" condition="" format="" name="std_p2p_valid_to" lookupType=""/>
									<column elemId="110210" condition="" format="" name="cio_p2p_valid_to" lookupType=""/>
									<column elemId="11484793" condition="" format="" name="exp_p2p_valid_to" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27522547" allColumns="false">
						<columns>
							<name>${dic_rel_type} ${dic_rel_category}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns>
					<column elemId="10333" size="100" enableValidation="false" name="published_by" createInto="rel_party2party_clean" label="Published By" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="35554" topLevel="false" name="rel_party2contract" label="Instance Party2Contract Relation" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="35556" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Contract Source ID" type="string" isExp="false" elemId="35593" isPk="false" size="200" enableValidation="false" name="contract_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Party Source ID" type="string" isExp="false" elemId="35594" isPk="false" size="200" enableValidation="false" name="party_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="rel_pty2ctr_type" isCio="false" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Relation Type" type="string" isExp="true" elemId="35595" isPk="false" size="100" enableValidation="false" name="rel_type" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="true">
				<matchingProposals enable="false"/>
				<matchingTabColumns/>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="111806">
						<groups>
							<labeledGroup elemId="110993" name="grp" label="Details" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="111807" condition="" format="" name="id" lookupType=""/>
									<column elemId="111808" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="111809" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="111810" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="111811" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="111812" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="111813" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="111814" condition="" format="" name="contract_source_id" lookupType=""/>
									<column elemId="111815" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="111816" condition="" format="" name="src_rel_type" lookupType=""/>
									<column elemId="111817" condition="" format="" name="std_rel_type" lookupType=""/>
									<column elemId="111818" condition="" format="" name="exp_rel_type" lookupType=""/>
									<column elemId="111819" condition="" format="" name="sco_rel_type" lookupType=""/>
									<column elemId="111825" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="111827" condition="" format="" name="pty_master_id" lookupType=""/>
									<column elemId="111828" condition="" format="" name="party_parent_cio_type" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns/>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="35489" topLevel="true" name="contract" label="Instance Contract" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="35491" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="sale_point" isCio="true" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="Sale Point" type="string" isExp="false" elemId="35526" isPk="false" size="100" enableValidation="false" name="sale_point" isFk="false">
					<description>ID of the Branch where the contract has been signed - works just as a reference check. List of Branches is maintained elsewhere and is not mastered in Party entity</description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="Status" type="string" isExp="false" elemId="115661" isPk="false" size="30" enableValidation="false" name="status" isFk="false">
					<description>Describes Contract life-cycle states (e.g. &quot;Proposed&quot;, &quot;Active&quot;, &quot;Terminated&quot;, ...)</description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="contract_type" isCio="true" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="Type" type="string" isExp="false" elemId="115662" isPk="false" size="100" enableValidation="false" name="type" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="Variant" type="string" isExp="false" elemId="26387343" isPk="false" size="100" enableValidation="false" name="variant" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Valid From" type="datetime" isExp="true" elemId="35524" isPk="false" size="" enableValidation="true" name="valid_from" isFk="false">
					<description></description>
					<validations>
						<validationKeys>
							<validationKey elemId="27853384" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing valid from date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27853385" severity="ERROR" enable="true" name="DATE_INVALID" message="Invalid valid from">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Valid To" type="datetime" isExp="true" elemId="35525" isPk="false" size="" enableValidation="true" name="valid_to" isFk="false">
					<description></description>
					<validations>
						<validationKeys>
							<validationKey elemId="27853980" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing valid to date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27853981" severity="ERROR" enable="true" name="DATE_INVALID" message="Invalid valid to date">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Product Source ID" type="string" isExp="false" elemId="26376215" isPk="false" size="200" enableValidation="false" name="product_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="false">
				<matchingProposals enable="false"/>
				<matchingTabColumns/>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="113030">
						<groups>
							<labeledGroup elemId="11489164" name="source" label="Source Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="11505850" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="11491153" condition="" format="" name="src_sale_point" lookupType=""/>
									<column elemId="11491154" condition="" format="" name="src_status" lookupType=""/>
									<column elemId="11491155" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="11491156" condition="" format="" name="src_variant" lookupType=""/>
									<column elemId="11491157" condition="" format="" name="src_valid_from" lookupType=""/>
									<column elemId="11491158" condition="" format="" name="src_valid_to" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="11489160" name="basic" label="Contract Instance Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26374602" condition="" format="" name="id" lookupType=""/>
									<column elemId="26374604" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="11489161" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="11489162" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="11489163" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledRelatedMNList elemId="123614" entity_out="party" previewLimit="" name="c2p" label="Contract to Party Relation" templatePosition="right" relationship="contract_has_party" relationship_out="party_has_contract" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Party Id: ${id}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Party</firstColumnFormat>
									<secondColumnFormat>${cmo_first_name} ${cmo_last_name} ${cmo_company_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledGroup elemId="11489165" name="cleansed" label="Cleansed Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="11508652" condition="" format="" name="cio_sale_point" lookupType=""/>
									<column elemId="11508653" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="11493537" condition="" format="" name="std_valid_from" lookupType=""/>
									<column elemId="11493538" condition="" format="" name="cio_valid_from" lookupType=""/>
									<column elemId="11493539" condition="" format="" name="exp_valid_from" lookupType=""/>
									<column elemId="11493540" condition="" format="" name="std_valid_to" lookupType=""/>
									<column elemId="11493541" condition="" format="" name="cio_valid_to" lookupType=""/>
									<column elemId="11493542" condition="" format="" name="exp_valid_to" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="113031" allColumns="false">
						<columns>
							<name>${src_type} ${src_variant}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns>
					<column elemId="26386514" indexType="BOTH" dataType="string" name="contract_single_line" description="" label="Contract Label">
						<producer>getMdaValString(&#39;source_id&#39;) + &#39;@&#39; + nvl(getMdaValString(&#39;dic_sale_point&#39;), getMdaValString(&#39;src_sale_point&#39;))</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns/>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="26399563" topLevel="false" name="id_document" label="Instance Document" type="instance">
			<description>Identification Document - usually driving licence, ID card, or Passport</description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="26400158" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Party Source ID" type="string" isExp="false" elemId="26400524" isPk="false" size="200" enableValidation="false" name="party_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="id_document_type" isCio="true" isStd="false" sizeOverride="" isSrc="true" isSco="true" label="Type" type="string" isExp="true" elemId="26401077" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="Value" type="string" isExp="false" elemId="26401078" isPk="false" size="100" enableValidation="false" name="value" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="true">
				<matchingProposals enable="false"/>
				<matchingTabColumns>
					<column elemId="19565" size="1000" enableValidation="false" name="mat_value" label="Value (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</matchingTabColumns>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="155364">
						<groups>
							<labeledGroup elemId="155365" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="155366" condition="" format="" name="id" lookupType=""/>
									<column elemId="155367" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="155368" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="155369" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="155370" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="155371" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="155372" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="155373" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="155374" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="155375" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="155376" condition="" format="" name="dic_type" lookupType=""/>
									<column elemId="155377" condition="" format="" name="src_value" lookupType=""/>
									<column elemId="155378" condition="" format="" name="cio_value" lookupType=""/>
									<column elemId="155379" condition="" format="" name="cio_id_document_comp" lookupType=""/>
									<column elemId="155381" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="155383" condition="" format="" name="party_master_id" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27523615" allColumns="false">
						<columns>
							<name>${cio_type} ${cio_value}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns>
					<column elemId="26402046" size="1000" enableValidation="false" name="cio_id_document_comp" createInto="id_document_clean" label="Document set" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="9834" size="100" enableValidation="false" name="published_by" createInto="id_document_clean" label="Published By" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</specialColumns>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="14675" topLevel="true" name="product" label="Instance Product" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="13497" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Name" type="string" isExp="true" elemId="13498" isPk="false" size="100" enableValidation="false" name="name" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="Description" type="string" isExp="false" elemId="13499" isPk="false" size="1000" enableValidation="false" name="description" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="product_type" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Type" type="string" isExp="true" elemId="13500" isPk="false" size="100" enableValidation="false" name="type" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Status" type="string" isExp="true" elemId="13501" isPk="false" size="100" enableValidation="false" name="status" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="Package" type="string" isExp="false" elemId="13502" isPk="false" size="10" enableValidation="false" name="package_flag" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Valid To" type="day" isExp="true" elemId="4950" isPk="false" size="" enableValidation="true" name="valid_to" isFk="false">
					<description></description>
					<validations>
						<validationKeys>
							<validationKey elemId="27853384" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing valid from date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27853385" severity="ERROR" enable="true" name="DATE_INVALID" message="Invalid valid from">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Valid From" type="day" isExp="true" elemId="4951" isPk="false" size="" enableValidation="true" name="valid_from" isFk="false">
					<description></description>
					<validations>
						<validationKeys>
							<validationKey elemId="27853980" severity="ERROR" enable="true" name="DATE_EMPTY_INPUT" message="Missing valid to date">
								<validationColumns/>
							</validationKey>
							<validationKey elemId="27853981" severity="ERROR" enable="true" name="DATE_INVALID" message="Invalid valid to date">
								<validationColumns/>
							</validationKey>
						</validationKeys>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="true">
				<matchingProposals enable="false"/>
				<matchingTabColumns>
					<column elemId="32605" size="100" enableValidation="false" name="mat_name" label="Name (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
					<column elemId="32606" size="100" enableValidation="false" name="mat_type" label="Type (mat)" type="string" isFk="false">
						<description></description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</matchingTabColumns>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<breadcrumbView elemId="23572" allColumns="false">
						<columns>
							<name>${cio_name} ${cio_type}</name>
						</columns>
					</breadcrumbView>
					<detailView elemId="20156">
						<groups>
							<labeledGroup elemId="20157" name="basic" label="Basic Product Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="20158" condition="" format="" name="id" lookupType=""/>
									<column elemId="24493" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="24494" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="24495" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="24496" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="20159" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="24498" name="source" label="Source Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="25117" condition="" format="" name="src_name" lookupType=""/>
									<column elemId="25118" condition="" format="" name="src_description" lookupType=""/>
									<column elemId="25119" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="25120" condition="" format="" name="src_status" lookupType=""/>
									<column elemId="25121" condition="" format="" name="src_package_flag" lookupType=""/>
									<column elemId="25122" condition="" format="" name="src_valid_to" lookupType=""/>
									<column elemId="25123" condition="" format="" name="src_valid_from" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="24499" name="cleansed" label="Cleansed Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="25736" condition="" format="" name="cio_name" lookupType=""/>
									<column elemId="25737" condition="" format="" name="cio_description" lookupType=""/>
									<column elemId="25738" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="25739" condition="" format="" name="cio_status" lookupType=""/>
									<column elemId="25740" condition="" format="" name="cio_package_flag" lookupType=""/>
									<column elemId="25741" condition="" format="" name="cio_valid_to" lookupType=""/>
									<column elemId="25742" condition="" format="" name="cio_valid_from" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns/>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="14695" topLevel="false" name="rel_prod2prod" label="Instance Product2Product Relation" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="15169" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Product Parent ID" type="string" isExp="false" elemId="15170" isPk="false" size="200" enableValidation="false" name="parent_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Product Child ID" type="string" isExp="false" elemId="15171" isPk="false" size="200" enableValidation="false" name="child_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="Type" type="string" isExp="false" elemId="17162" isPk="false" size="100" enableValidation="false" name="rel_type" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="false" label="Category" type="string" isExp="false" elemId="17163" isPk="false" size="100" enableValidation="false" name="rel_category" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="true">
				<matchingProposals enable="false"/>
				<matchingTabColumns/>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<breadcrumbView elemId="28544" allColumns="false">
						<columns>
							<name>${cio_rel_type} ${cio_rel_category}</name>
						</columns>
					</breadcrumbView>
					<detailView elemId="29163">
						<groups>
							<labeledGroup elemId="29164" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="29165" condition="" format="" name="id" lookupType=""/>
									<column elemId="29166" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="29167" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="29168" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="29169" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="29170" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="29171" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="29172" condition="" format="" name="parent_source_id" lookupType=""/>
									<column elemId="29173" condition="" format="" name="child_source_id" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="29797" name="data" label="Relationship Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="29798" condition="" format="" name="parent_source_id" lookupType=""/>
									<column elemId="29799" condition="" format="" name="child_source_id" lookupType=""/>
									<column elemId="29800" condition="" format="" name="src_rel_type" lookupType=""/>
									<column elemId="29801" condition="" format="" name="cio_rel_type" lookupType=""/>
									<column elemId="29802" condition="" format="" name="src_rel_category" lookupType=""/>
									<column elemId="29803" condition="" format="" name="cio_rel_category" lookupType=""/>
									<column elemId="29804" condition="" format="" name="product_child_master_id" lookupType=""/>
									<column elemId="29805" condition="" format="" name="product_parent_master_id" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns/>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="14715" topLevel="false" name="rel_prod2contract" label="Instance Product2Contract Relation" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="13099" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Product Source ID" type="string" isExp="false" elemId="13100" isPk="false" size="200" enableValidation="false" name="prd_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Contract Source ID" type="string" isExp="false" elemId="13101" isPk="false" size="200" enableValidation="false" name="ctr_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="true" isSco="false" label="Variant" type="string" isExp="false" elemId="27071" isPk="false" size="100" enableValidation="false" name="variant" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="false">
				<matchingProposals enable="false"/>
				<matchingTabColumns/>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="" aggregation="false">
				<aggregationTabColumns/>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<breadcrumbView elemId="26898" allColumns="false">
						<columns>
							<name>${src_variant}</name>
						</columns>
					</breadcrumbView>
					<detailView elemId="27966">
						<groups>
							<labeledGroup elemId="27967" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="27968" condition="" format="" name="id" lookupType=""/>
									<column elemId="27969" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="27970" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="27971" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="27972" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="27973" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="27974" condition="" format="" name="prd_source_id" lookupType=""/>
									<column elemId="27975" condition="" format="" name="ctr_source_id" lookupType=""/>
									<column elemId="27976" condition="" format="" name="src_variant" lookupType=""/>
									<column elemId="27977" condition="" format="" name="prd_master_id" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns/>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
		<table elemId="4290" topLevel="false" name="consent" label="Instance Consent" type="instance">
			<description></description>
			<columns>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Source ID" type="string" isExp="false" elemId="5295" isPk="true" size="200" enableValidation="false" name="source_id" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="false" isStd="false" sizeOverride="" isSrc="false" isSco="false" label="Party Source ID" type="string" isExp="false" elemId="6885" isPk="false" size="200" enableValidation="false" name="party_source_id" isFk="true">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_category" isCio="false" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Category" type="string" isExp="true" elemId="27834425" isPk="false" size="30" enableValidation="false" name="category" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_purpose" isCio="false" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Purpose" type="string" isExp="true" elemId="27834426" isPk="false" size="30" enableValidation="false" name="purpose" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_system" isCio="false" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="System" type="string" isExp="true" elemId="27834427" isPk="false" size="30" enableValidation="false" name="system" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_consumer" isCio="false" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Consumer" type="string" isExp="true" elemId="27834428" isPk="false" size="30" enableValidation="false" name="consumer" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_channel" isCio="false" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Channel" type="string" isExp="true" elemId="5296" isPk="false" size="30" enableValidation="false" name="channel" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="consent_status" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Status" type="string" isExp="true" elemId="5298" isPk="false" size="30" enableValidation="false" name="status" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Origin Date" type="datetime" isExp="true" elemId="5299" isPk="false" size="" enableValidation="false" name="origin_date" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCio="true" isStd="true" sizeOverride="" isSrc="true" isSco="true" label="Expiration Date" type="datetime" isExp="true" elemId="26946" isPk="false" size="" enableValidation="false" name="expiration_date" isFk="false">
					<description></description>
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<matchingTab enableIdentify="false" matching="true">
				<matchingProposals enable="false"/>
				<matchingTabColumns>
					<column elemId="27842596" size="100" enableValidation="false" name="mat_qualifier" label="Category Qualifier" type="string" isFk="false">
						<description>Category specific value chosen for matching</description>
						<validations>
							<validationKeys/>
							<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
						</validations>
					</column>
				</matchingTabColumns>
				<multipleMatching disableDefault="false">
					<matchingDefinitions/>
					<rematchIfChangedSectionColumns/>
				</multipleMatching>
			</matchingTab>
			<aggregationTab groupingColumn="party_master_id" aggregation="true">
				<aggregationTabColumns>
					<aggregationTabColumn elemId="21595" size="30" name="agg_channel_status" label="Channel Status (agg)" type="string" isFk="false">
						<description></description>
					</aggregationTabColumn>
					<aggregationTabColumn elemId="27489" size="" name="agg_expiration_date" label="Expiration Date (agg)" type="datetime" isFk="false">
						<description></description>
					</aggregationTabColumn>
					<aggregationTabColumn elemId="30671" size="" name="agg_origin_date" label="Origin Date (agg)" type="datetime" isFk="false">
						<description></description>
					</aggregationTabColumn>
				</aggregationTabColumns>
			</aggregationTab>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="27852656">
						<groups>
							<labeledGroup elemId="27533693" name="source" label="Source Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="27533694" condition="" format="" name="id" lookupType=""/>
									<column elemId="27533695" condition="" format="" name="eng_source_timestamp" lookupType=""/>
									<column elemId="27533696" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="27533697" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="27533698" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="27533699" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="27852046" condition="" format="" name="src_category" lookupType=""/>
									<column elemId="27852047" condition="" format="" name="src_purpose" lookupType=""/>
									<column elemId="27852048" condition="" format="" name="src_system" lookupType=""/>
									<column elemId="27852049" condition="" format="" name="src_consumer" lookupType=""/>
									<column elemId="27533700" condition="" format="" name="src_channel" lookupType=""/>
									<column elemId="27533702" condition="" format="" name="src_status" lookupType=""/>
									<column elemId="27533703" condition="" format="" name="src_origin_date" lookupType=""/>
									<column elemId="27533704" condition="" format="" name="src_expiration_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="21847" name="basic" label="Standardized Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="27852050" condition="" format="" name="std_category" lookupType=""/>
									<column elemId="27852051" condition="" format="" name="exp_category" lookupType=""/>
									<column elemId="27852052" condition="" format="" name="sco_category" lookupType=""/>
									<column elemId="27852053" condition="" format="" name="std_purpose" lookupType=""/>
									<column elemId="27852054" condition="" format="" name="exp_purpose" lookupType=""/>
									<column elemId="27852055" condition="" format="" name="sco_purpose" lookupType=""/>
									<column elemId="27852056" condition="" format="" name="std_system" lookupType=""/>
									<column elemId="27852057" condition="" format="" name="exp_system" lookupType=""/>
									<column elemId="27852058" condition="" format="" name="sco_system" lookupType=""/>
									<column elemId="27852059" condition="" format="" name="std_consumer" lookupType=""/>
									<column elemId="27852060" condition="" format="" name="exp_consumer" lookupType=""/>
									<column elemId="27852061" condition="" format="" name="sco_consumer" lookupType=""/>
									<column elemId="27533705" condition="" format="" name="std_channel" lookupType=""/>
									<column elemId="27533706" condition="" format="" name="exp_channel" lookupType=""/>
									<column elemId="27533707" condition="" format="" name="sco_channel" lookupType=""/>
									<column elemId="27533711" condition="" format="" name="std_status" lookupType=""/>
									<column elemId="27533712" condition="" format="" name="cio_status" lookupType=""/>
									<column elemId="27533713" condition="" format="" name="exp_status" lookupType=""/>
									<column elemId="27533714" condition="" format="" name="sco_status" lookupType=""/>
									<column elemId="27533715" condition="" format="" name="std_origin_date" lookupType=""/>
									<column elemId="27533716" condition="" format="" name="cio_origin_date" lookupType=""/>
									<column elemId="27533717" condition="" format="" name="exp_origin_date" lookupType=""/>
									<column elemId="27533718" condition="" format="" name="sco_origin_date" lookupType=""/>
									<column elemId="27533719" condition="" format="" name="std_expiration_date" lookupType=""/>
									<column elemId="27533720" condition="" format="" name="cio_expiration_date" lookupType=""/>
									<column elemId="27533721" condition="" format="" name="exp_expiration_date" lookupType=""/>
									<column elemId="27533722" condition="" format="" name="sco_expiration_date" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="27852657" allColumns="false">
						<columns>
							<name>${std_category} - ${std_purpose} : ${cio_status}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced>
				<specialColumns/>
				<historicalColumns/>
				<oldValueColumns/>
			</advanced>
		</table>
	</tables>
	<dicTables/>
</instanceModel>