<?xml version='1.0' encoding='UTF-8'?>
<masterModel elemId="22817" name="norm" label="Normalized view">
	<description>Normilized example</description>
	<relationships>
		<relationship childRole="addresses_per" childTable="address" elemId="22894" name="party_has_address" parentRole="person" label="Party has Address" parentTable="person" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109192" childColumn="party_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances" childTable="party_instance" elemId="22897" name="party_has_instance" parentRole="master_per" label="Party has Instance Details" parentTable="person" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109194" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="contacts_per" childTable="contact" elemId="36452" name="party_has_contact" parentRole="person" label="Party has Contact" parentTable="person" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109196" childColumn="party_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="relparent_person" childTable="rel_per_per" elemId="36626" name="party_has_parent_party_per" parentRole="parent_person" label="Party has Parent Party" parentTable="person" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109198" childColumn="parent_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="relchild" childTable="rel_per_per" elemId="36673" name="party_has_child_party_per" parentRole="child" label="Party has Child Party" parentTable="person" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109200" childColumn="child_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances" childTable="contact_instance" elemId="36877" name="contact_has_instance" parentRole="master" label="Contact has Instance Details" parentTable="contact" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109202" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances" childTable="address_instance" elemId="36967" name="address_has_instance" parentRole="master" label="Address has Instance Details" parentTable="address" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18108677" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances_per" childTable="rel_party_party_instance" elemId="78629" name="relation_party_has_instance_per" parentRole="master_per" label="Relation has Instance Details" parentTable="rel_per_per" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109209" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="id_documents_per" childTable="id_document" elemId="26377853" name="party_has_id_document" parentRole="person" label="Party has ID Document" parentTable="person" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109211" childColumn="party_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="" childTable="id_document_instance" elemId="156412" name="id_document_has_instance" parentRole="" label="" parentTable="id_document" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="156744" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="relparent_org" childTable="rel_org_org" elemId="13243" name="party_has_parent_party_org" parentRole="parent_org" label="Party has Parent Party" parentTable="organisation" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109198" childColumn="parent_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="relchild_org" childTable="rel_org_org" elemId="13249" name="party_has_child_party_org" parentRole="child_org" label="Party has Child Party" parentTable="organisation" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109200" childColumn="child_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances" childTable="party_instance" elemId="16006" name="party_has_instance_2" parentRole="master_org" label="Party has Instance Details" parentTable="organisation" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109194" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="relparent_per" childTable="rel_party_party" elemId="17353" name="person_has_organisation" parentRole="parent_per" label="Party has Parent Party" parentTable="person" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109198" childColumn="parent_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="relchild" childTable="rel_party_party" elemId="17359" name="organisation_has_person" parentRole="child" label="Party has Child Party" parentTable="organisation" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109200" childColumn="child_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="addresses" childTable="address" elemId="18713" name="party_has_address_2" parentRole="party" label="Party has Address" parentTable="organisation" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109192" childColumn="party_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="id_documents" childTable="id_document" elemId="19253" name="party_has_id_document_2" parentRole="party" label="Party has ID Document" parentTable="organisation" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109211" childColumn="party_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="contacts" childTable="contact" elemId="19627" name="party_has_contact_2" parentRole="" label="Party has Contact" parentTable="organisation" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109196" childColumn="party_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="person" childTable="rel_per_cntrct_i" elemId="111450" name="contract_has_party_per" parentRole="contract" label="Contract has Person" parentTable="contract_instance" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109207" childColumn="contract_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="org" childTable="rel_org_cntrct_i" elemId="111458" name="contract_has_party_org" parentRole="contract" label="Contract has Organisation" parentTable="contract_instance" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109207" childColumn="contract_source_id" parentColumn="source_id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="contract" childTable="rel_org_cntrct_i" elemId="111466" name="org_has_contract" parentRole="org" label="Organisation has Contract" parentTable="organisation" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109205" childColumn="pty_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="contract" childTable="rel_per_cntrct_i" elemId="111474" name="per_has_contract" parentRole="person" label="Person has Contract" parentTable="person" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109205" childColumn="pty_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances_org" childTable="rel_party_party_instance" elemId="25249" name="relation_party_has_instance_org" parentRole="master_org" label="Relation has Instance Details" parentTable="rel_org_org" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109209" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
		<relationship childRole="instances_pty" childTable="rel_party_party_instance" elemId="26648" name="relation_party_has_instance_party" parentRole="master_pty" label="Relation has Instance Details" parentTable="rel_party_party" type="">
			<description></description>
			<advanced>
				<extendedSameSystem childColumn="" parentColumn=""/>
			</advanced>
			<foreignKey>
				<column elemId="18109209" childColumn="master_id" parentColumn="id"/>
			</foreignKey>
		</relationship>
	</relationships>
	<masterTables>
		<masterTable elemId="12691" entityRole="golden" topLevel="true" name="organisation" label="Master organisation" instanceTable="party">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="ID" type="long_int" isExp="false" elemId="22887" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="party_type" isCmo="true" isSco="false" description="" label="Customer Type" type="string" isExp="false" elemId="128185" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Company Name" type="string" isExp="false" elemId="128187" isPk="false" size="200" enableValidation="false" name="company_name" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Business Number" type="string" isExp="false" elemId="128188" isPk="false" size="30" enableValidation="false" name="business_number" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Established Date" type="day" isExp="false" elemId="128189" isPk="false" size="" enableValidation="false" name="established_date" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="legal_form" isCmo="true" isSco="false" description="" label="Business Form" type="string" isExp="false" elemId="26381948" isPk="false" size="30" enableValidation="false" name="legal_form" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="166199">
						<groups>
							<labeledGroup elemId="87506" name="party_basic" label="Basic personal information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="47828" condition="" format="" name="id" lookupType=""/>
									<column elemId="87507" condition="" format="" name="cmo_type" lookupType=""/>
									<column elemId="47829" condition="" format="" name="cmo_company_name" lookupType=""/>
									<column elemId="47830" condition="" format="" name="cmo_business_number" lookupType=""/>
									<column elemId="47831" condition="" format="" name="cmo_established_date" lookupType=""/>
									<column elemId="47832" condition="" format="" name="cmo_legal_form" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledRelatedList elemId="158834" previewLimit="" name="address" label="Address" templatePosition="right" relationship="party_has_address_2" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cmo_type}</firstColumnFormat>
									<secondColumnFormat>${address_single_line}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedList elemId="111272" previewLimit="" name="id_doc" label="ID document" templatePosition="right" relationship="party_has_id_document_2" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cmo_type}</firstColumnFormat>
									<secondColumnFormat>${cmo_value}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedList elemId="159802" previewLimit="" name="contact" label="Contact" templatePosition="right" relationship="party_has_contact_2" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cmo_type}</firstColumnFormat>
									<secondColumnFormat>${cmo_value}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedMNList elemId="160124" entity_out="organisation" previewLimit="" name="o2o_child" label="Child Organisation" templatePosition="left" relationship="party_has_parent_party_org" relationship_out="party_has_child_party_org" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>ChildId:${child_id} | RelType: ${cmo_p2p_rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Organisation</firstColumnFormat>
									<secondColumnFormat>${cmo_company_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="122759" entity_out="organisation" previewLimit="" name="o2o_parent" label="Organisation Parent" templatePosition="left" relationship="party_has_child_party_org" relationship_out="party_has_parent_party_org" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>ParentId:${parent_id} | RelType: ${cmo_p2p_rel_type}&quot;</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Company</firstColumnFormat>
									<secondColumnFormat>${cmo_company_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="130114" entity_out="person" previewLimit="" name="o2p" label="Person" templatePosition="left" relationship="organisation_has_person" relationship_out="person_has_organisation" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>OrgId:${parent_id} | RelType: ${cmo_p2p_rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Person</firstColumnFormat>
									<secondColumnFormat>${cmo_first_name} ${cmo_last_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="38223" entity_out="contract_instance" previewLimit="" name="o2c" label="Contract" templatePosition="right" relationship="org_has_contract" relationship_out="contract_has_party_org" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Contract Id: ${contract_source_id}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Contract</firstColumnFormat>
									<secondColumnFormat>${src_type}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<listGrid elemId="97076" name="list" label="Instances" templatePosition="bottom" relationship="party_has_instance_2" viewCondition="">
								<lists>
									<list elemId="34112" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="34113" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="34114" condition="" name="uni" label="Instances: matching results" columnMask="">
										<columns>
											<column elemId="48665" condition="" name="eng_source_system"/>
											<column elemId="48666" condition="" name="source_id"/>
											<column elemId="48668" condition="" name="master_id"/>
											<column elemId="48669" condition="" name="match_rule_name"/>
											<column elemId="17415" condition="" name="match_quality"/>
											<column elemId="17416" condition="" name="match_related_id"/>
										</columns>
									</list>
									<list elemId="34115" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="58294" allColumns="false">
						<columns>
							<name>${cmo_company_name}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns>
					<column elemId="26383506" indexType="BOTH" dataType="string" name="customer_single_line" description="" label="Customer in Single Line">
						<producer>nvl(getMdaValString(&#39;cmo_company_name&#39;))</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="true" groupColumn="">
				<inputFilterExpression>eng_active = true and cio_type = &#39;C&#39;</inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="22867" entityRole="golden" topLevel="true" name="person" label="Master person" instanceTable="party">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="ID" type="long_int" isExp="false" elemId="22887" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="party_type" isCmo="true" isSco="false" description="" label="Customer Type" type="string" isExp="false" elemId="128185" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Firstname" type="string" isExp="false" elemId="22868" isPk="false" size="100" enableValidation="false" name="first_name" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Lastname" type="string" isExp="false" elemId="29082" isPk="false" size="100" enableValidation="false" name="last_name" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="gender" isCmo="true" isSco="false" description="" label="Gender" type="string" isExp="false" elemId="36720" isPk="false" size="10" enableValidation="false" name="gender" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Birth Date" type="day" isExp="false" elemId="36721" isPk="false" size="" enableValidation="false" name="birth_date" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Social Insurance Number" type="string" isExp="false" elemId="128186" isPk="false" size="30" enableValidation="false" name="sin" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="87505">
						<groups>
							<labeledGroup elemId="87506" name="party_basic" label="Basic personal information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="87507" condition="" format="" name="cmo_type" lookupType=""/>
									<column elemId="87508" condition="" format="" name="cmo_first_name" lookupType=""/>
									<column elemId="87509" condition="" format="" name="cmo_last_name" lookupType=""/>
									<column elemId="87510" condition="" format="" name="cmo_gender" lookupType=""/>
									<column elemId="87511" condition="" format="" name="cmo_birth_date" lookupType=""/>
									<column elemId="87512" condition="" format="" name="cmo_sin" lookupType=""/>
									<column elemId="87513" condition="" format="" name="age" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledRelatedList elemId="158834" previewLimit="" name="address" label="Address" templatePosition="right" relationship="party_has_address" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cmo_type}</firstColumnFormat>
									<secondColumnFormat>${address_single_line}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedList elemId="111272" previewLimit="" name="id_doc" label="ID document" templatePosition="right" relationship="party_has_id_document" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cmo_type}</firstColumnFormat>
									<secondColumnFormat>${cmo_value}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedList elemId="159802" previewLimit="" name="contact" label="Contact" templatePosition="right" relationship="party_has_contact" viewCondition="">
								<linkedRecordParams>
									<firstColumnFormat>${cmo_type}</firstColumnFormat>
									<secondColumnFormat>${cmo_value}</secondColumnFormat>
								</linkedRecordParams>
							</labeledRelatedList>
							<labeledRelatedMNList elemId="160124" entity_out="person" previewLimit="" name="p2p_parent" label="Parent Person" templatePosition="left" relationship="party_has_child_party_per" relationship_out="party_has_parent_party_per" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>ParentId: ${parent_id} | RelType: ${cmo_p2p_rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Person: </firstColumnFormat>
									<secondColumnFormat>${cmo_first_name} ${cmo_last_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="122759" entity_out="person" previewLimit="" name="p2p_child" label="Child person" templatePosition="left" relationship="party_has_parent_party_per" relationship_out="party_has_child_party_per" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>ChildId:${child_id} | RelType: ${cmo_p2p_rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Person</firstColumnFormat>
									<secondColumnFormat>${cmo_first_name} ${cmo_last_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="36639" entity_out="organisation" previewLimit="" name="p2o" label="Organisation" templatePosition="left" relationship="person_has_organisation" relationship_out="organisation_has_person" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>ParentId:${parent_id} | RelType: ${cmo_p2p_rel_type}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Organisation</firstColumnFormat>
									<secondColumnFormat>${cmo_company_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="130114" entity_out="contract_instance" previewLimit="" name="p2c" label="Contract" templatePosition="right" relationship="per_has_contract" relationship_out="contract_has_party_per" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>Contract Id: ${contract_source_id}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Contract</firstColumnFormat>
									<secondColumnFormat>${src_type}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<listGrid elemId="97076" name="list" label="Instances" templatePosition="bottom" relationship="party_has_instance" viewCondition="">
								<lists>
									<list elemId="33735" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="33736" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="33737" condition="" name="uni" label="Instances: matching results" columnMask="">
										<columns>
											<column elemId="48665" condition="" name="eng_source_system"/>
											<column elemId="48666" condition="" name="source_id"/>
											<column elemId="48668" condition="" name="master_id"/>
											<column elemId="48669" condition="" name="match_rule_name"/>
											<column elemId="17415" condition="" name="match_quality"/>
											<column elemId="17416" condition="" name="match_related_id"/>
										</columns>
									</list>
									<list elemId="33738" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="10691" allColumns="false">
						<columns>
							<name>${cmo_first_name} ${cmo_last_name}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns>
					<column elemId="657533" indexType="BOTH" dataType="string" name="age" description="" label="Age">
						<producer>iif(getMdaValString(&#39;cmo_type&#39;)=&#39;P&#39;, 
iif (floor(dateDiff(getMdaValDatetime(&#39;cmo_birth_date&#39;), now(), &#39;DAY&#39;)/365) &gt;= 0,
	toString(floor((dateDiff(getMdaValDatetime(&#39;cmo_birth_date&#39;), now(), &#39;DAY&#39;)-(floor(ceil(dateDiff(getMdaValDatetime(&#39;cmo_birth_date&#39;), now(), &#39;YEAR&#39;)/4))))/365)),
	&#39;N/A&#39;)
, &#39;&#39;)</producer>
					</column>
					<column elemId="26383506" indexType="BOTH" dataType="string" name="customer_single_line" description="" label="Customer in Single Line">
						<producer>nvl(getMdaValString(&#39;cmo_first_name&#39;) + &#39; &#39; + getMdaValString(&#39;cmo_last_name&#39;))</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="true" groupColumn="">
				<inputFilterExpression>eng_active = true and cio_type = &#39;P&#39;</inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="22881" entityRole="silver" topLevel="false" name="address" label="Master address" instanceTable="address">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="ID" type="long_int" isExp="false" elemId="22885" isPk="true" size="30" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="Party ID" type="long_int" isExp="false" elemId="22891" isPk="false" size="30" enableValidation="false" name="party_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="address_type" isCmo="true" isSco="false" description="" label="Type" type="integer" isExp="false" elemId="129734" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Street" type="string" isExp="false" elemId="35288" isPk="false" size="100" enableValidation="false" name="street" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="City" type="string" isExp="false" elemId="35289" isPk="false" size="100" enableValidation="false" name="city" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="State" type="string" isExp="false" elemId="35290" isPk="false" size="100" enableValidation="false" name="state" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Postal Code" type="string" isExp="false" elemId="35291" isPk="false" size="100" enableValidation="false" name="zip" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="135960">
						<groups>
							<labeledGroup elemId="198134" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26380488" condition="" format="" name="id" lookupType=""/>
									<column elemId="130397" condition="" format="" name="cmo_type" lookupType=""/>
									<column elemId="511929" condition="" format="" name="cmo_street" lookupType=""/>
									<column elemId="515447" condition="" format="" name="cmo_city" lookupType=""/>
									<column elemId="515449" condition="" format="" name="cmo_zip" lookupType=""/>
									<column elemId="515448" condition="" format="" name="cmo_state" lookupType=""/>
									<column elemId="515446" condition="" format="" name="party_id" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="515582" name="instances" label="Instances" templatePosition="bottom" relationship="address_has_instance" viewCondition="">
								<lists>
									<list elemId="34489" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="34490" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="34491" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns>
					<column elemId="26379312" indexType="BOTH" dataType="string" name="address_single_line" description="" label="Address Information">
						<producer>getMdaValString(&#39;cmo_street&#39;) + &#39;, &#39; + getMdaValString(&#39;cmo_city&#39;)</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="true" groupColumn="">
				<inputFilterExpression>eng_active = true</inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="36374" entityRole="silver" topLevel="false" name="contact" label="Master contact" instanceTable="contact">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="ID (Contact PK)" type="long_int" isExp="false" elemId="22885" isPk="true" size="30" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="Party Id" type="long_int" isExp="false" elemId="22891" isPk="false" size="30" enableValidation="false" name="party_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="contact_type" isCmo="true" isSco="false" description="" label="Contact Type" type="string" isExp="false" elemId="35288" isPk="false" size="100" enableValidation="false" name="type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="Contact Value" type="string" isExp="false" elemId="35289" isPk="false" size="100" enableValidation="false" name="value" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<breadcrumbView elemId="34864" allColumns="false">
						<columns>
							<name>${cmo_value}</name>
						</columns>
					</breadcrumbView>
					<detailView elemId="61661">
						<groups>
							<labeledGroup elemId="62617" name="grp" label="Contact" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="62618" condition="" format="" name="id" lookupType=""/>
									<column elemId="62619" condition="" format="" name="party_id" lookupType=""/>
									<column elemId="62620" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="62621" condition="" format="" name="cmo_type" lookupType=""/>
									<column elemId="62622" condition="" format="" name="cmo_value" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="61662" name="list" label="Contact instances" templatePosition="bottom" relationship="contact_has_instance" viewCondition="">
								<lists>
									<list elemId="34866" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="34867" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="34868" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns>
					<column elemId="26380143" indexType="BOTH" dataType="string" name="contact_label" description="" label="Contact Label">
						<producer>getMdaValString(&#39;cmo_type&#39;) + &#39; contact&#39;</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="true" groupColumn="">
				<inputFilterExpression>eng_active = true</inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="12692" entityRole="silver" topLevel="false" name="rel_party_party" label="Master party to party relations" instanceTable="rel_party2party">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="id" type="long_int" isExp="false" elemId="36520" isPk="true" size="30" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="parent_id" type="long_int" isExp="false" elemId="36592" isPk="false" size="30" enableValidation="false" name="parent_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="child_id" type="long_int" isExp="false" elemId="36590" isPk="false" size="30" enableValidation="false" name="child_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="rel_pty2pty_type" isCmo="true" isSco="false" description="" label="p2p_rel_type" type="string" isExp="false" elemId="36593" isPk="false" size="100" enableValidation="false" name="p2p_rel_type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="103869">
						<groups>
							<labeledGroup elemId="103871" name="grp" label="Party to Party relation" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="103872" condition="" format="" name="id" lookupType=""/>
									<column elemId="103873" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="103874" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="103875" condition="" format="" name="parent_id" lookupType=""/>
									<column elemId="103876" condition="" format="" name="child_id" lookupType=""/>
									<column elemId="103877" condition="" format="" name="cmo_p2p_rel_type" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="true" groupColumn="">
				<inputFilterExpression>(party_parent_cio_type = &#39;C&#39; and party_child_cio_type = &#39;P&#39;) or (party_parent_cio_type = &#39;P&#39; and party_child_cio_type = &#39;C&#39;)</inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="12693" entityRole="silver" topLevel="false" name="rel_org_org" label="Master party to party relations" instanceTable="rel_party2party">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="id" type="long_int" isExp="false" elemId="36520" isPk="true" size="30" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="parent_id" type="long_int" isExp="false" elemId="36592" isPk="false" size="30" enableValidation="false" name="parent_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="child_id" type="long_int" isExp="false" elemId="36590" isPk="false" size="30" enableValidation="false" name="child_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="rel_pty2pty_type" isCmo="true" isSco="false" description="" label="p2p_rel_type" type="string" isExp="false" elemId="36593" isPk="false" size="100" enableValidation="false" name="p2p_rel_type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="104280">
						<groups>
							<labeledGroup elemId="104282" name="grp" label="Org to Org relation" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="104283" condition="" format="" name="id" lookupType=""/>
									<column elemId="104284" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="104285" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="104286" condition="" format="" name="parent_id" lookupType=""/>
									<column elemId="104287" condition="" format="" name="child_id" lookupType=""/>
									<column elemId="104288" condition="" format="" name="cmo_p2p_rel_type" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="true" groupColumn="">
				<inputFilterExpression>(party_parent_cio_type = &#39;C&#39; and party_child_cio_type = &#39;C&#39;) or (party_parent_cio_type = &#39;C&#39; and party_child_cio_type = &#39;C&#39;)</inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="36496" entityRole="silver" topLevel="false" name="rel_per_per" label="Master party to party relations" instanceTable="rel_party2party">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="id" type="long_int" isExp="false" elemId="36520" isPk="true" size="30" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="parent_id" type="long_int" isExp="false" elemId="36592" isPk="false" size="30" enableValidation="false" name="parent_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="child_id" type="long_int" isExp="false" elemId="36590" isPk="false" size="30" enableValidation="false" name="child_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="rel_pty2pty_type" isCmo="true" isSco="false" description="" label="p2p_rel_type" type="string" isExp="false" elemId="36593" isPk="false" size="100" enableValidation="false" name="p2p_rel_type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26375491">
						<groups>
							<labeledGroup elemId="26375492" name="basic" label="Person to Person relation" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26375493" condition="" format="" name="id" lookupType=""/>
									<column elemId="26375494" condition="" format="" name="parent_id" lookupType=""/>
									<column elemId="26375495" condition="" format="" name="child_id" lookupType=""/>
									<column elemId="26375496" condition="" format="" name="cmo_p2p_rel_type" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="103483" name="list" label="Instances" templatePosition="bottom" relationship="relation_party_has_instance_per" viewCondition="">
								<lists/>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="true" groupColumn="">
				<inputFilterExpression>(party_parent_cio_type = &#39;P&#39; and party_child_cio_type = &#39;P&#39;) or (party_parent_cio_type = &#39;P&#39; and party_child_cio_type = &#39;P&#39;)</inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="26377244" entityRole="silver" topLevel="false" name="id_document" label="ID Document" instanceTable="id_document">
			<description>Identification Document - usually driving licence, ID card, or Passport</description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="long_int" isExp="false" elemId="26373988" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="long_int" isExp="false" elemId="26374577" isPk="false" size="" enableValidation="false" name="party_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="id_document_type" isCmo="true" isSco="false" description="" label="" type="string" isExp="false" elemId="26373989" isPk="false" size="30" enableValidation="false" name="type" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="true" isSco="false" description="" label="" type="string" isExp="false" elemId="26373990" isPk="false" size="100" enableValidation="false" name="value" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26374207">
						<groups>
							<labeledGroup elemId="26374208" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26374209" condition="" format="" name="id" lookupType=""/>
									<column elemId="26374210" condition="" format="" name="cmo_type" lookupType=""/>
									<column elemId="26374211" condition="" format="" name="cmo_value" lookupType=""/>
									<column elemId="26374212" condition="" format="" name="party_id" lookupType=""/>
								</columns>
							</labeledGroup>
							<listGrid elemId="154687" name="list" label="Instances" templatePosition="bottom" relationship="id_document_has_instance" viewCondition="">
								<lists>
									<list elemId="35244" condition="" name="src" label="Instances: source columns" columnMask="^src_*">
										<columns>
											<column elemId="49892" condition="" name="source_id"/>
											<column elemId="45893" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="35245" condition="" name="std" label="Instances: standardized columns" columnMask="^std_*">
										<columns>
											<column elemId="49494" condition="" name="source_id"/>
											<column elemId="49495" condition="" name="eng_source_system"/>
										</columns>
									</list>
									<list elemId="35246" condition="" name="all" label="Instances: all columns" columnMask="">
										<columns/>
									</list>
								</lists>
							</listGrid>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="35247" allColumns="false">
						<columns>
							<name>${cmo_value}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns>
					<column elemId="26375307" indexType="BOTH" dataType="string" name="id_document_label" description="" label="ID Document Label">
						<producer>getMdaValString(&#39;cmo_value&#39;)</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="true" groupColumn="">
				<inputFilterExpression>eng_active = true</inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="9385" entityRole="silver" topLevel="false" name="rel_per_cntrct_i" label="Instance person to contract roles" instanceTable="rel_party2contract">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="long_int" isExp="false" elemId="12710" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="long_int" isExp="false" elemId="13257" isPk="false" size="" enableValidation="false" name="pty_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="string" isExp="false" elemId="13258" isPk="false" size="1000" enableValidation="false" name="contract_source_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="110992">
						<groups>
							<labeledGroup elemId="110993" name="grp" label="Details" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="110994" condition="" format="" name="id" lookupType=""/>
									<column elemId="110995" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="110996" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="110997" condition="" format="" name="pty_id" lookupType=""/>
									<column elemId="110998" condition="" format="" name="contract_source_id" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression>party_parent_cio_type = &#39;P&#39;</inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
		<masterTable elemId="9666" entityRole="silver" topLevel="false" name="rel_org_cntrct_i" label="Instance organisation to contract roles" instanceTable="rel_party2contract">
			<description></description>
			<columns>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="long_int" isExp="false" elemId="12985" isPk="true" size="" enableValidation="false" name="id" isFk="false">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="long_int" isExp="false" elemId="13539" isPk="false" size="" enableValidation="false" name="pty_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
				<column refData="" isCmo="false" isSco="false" description="" label="" type="string" isExp="false" elemId="13540" isPk="false" size="1000" enableValidation="false" name="contract_source_id" isFk="true">
					<validations>
						<validationKeys/>
						<overrideDefaultSettings warningErrorThreshold="" customScoColumn="" infoWarningThreshold="" customExpColumn=""/>
					</validations>
				</column>
			</columns>
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="111404">
						<groups>
							<labeledGroup elemId="110993" name="grp" label="Details" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="110994" condition="" format="" name="id" lookupType=""/>
									<column elemId="110995" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="110996" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="110997" condition="" format="" name="pty_id" lookupType=""/>
									<column elemId="110998" condition="" format="" name="contract_source_id" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<advanced customActivity="false" groupColumn="">
				<inputFilterExpression>party_parent_cio_type = &#39;C&#39;</inputFilterExpression>
				<specialColumns/>
			</advanced>
		</masterTable>
	</masterTables>
	<instanceTables>
		<instanceTable elemId="22862" topLevel="false" name="party_instance" label="Instance party" instanceTable="party">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26374600">
						<groups>
							<labeledGroup elemId="26374601" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26374604" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="26374602" condition="" format="" name="id" lookupType=""/>
									<column elemId="178085" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="26382361" condition="" format="" name="cio_first_name" lookupType=""/>
									<column elemId="178086" condition="" format="" name="cio_middle_name" lookupType=""/>
									<column elemId="26382362" condition="" format="" name="cio_last_name" lookupType=""/>
									<column elemId="26382363" condition="" format="" name="cio_sin" lookupType=""/>
									<column elemId="26373214" condition="" format="" name="cio_gender" lookupType=""/>
									<column elemId="26382364" condition="" format="" name="cio_company_name" lookupType=""/>
									<column elemId="26382365" condition="" format="" name="cio_business_number" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26374818" name="source" label="Source Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="18081162" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="26375022" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="26375023" condition="" format="" name="src_first_name" lookupType=""/>
									<column elemId="26375024" condition="" format="" name="src_last_name" lookupType=""/>
									<column elemId="26375025" condition="" format="" name="src_sin" lookupType=""/>
									<column elemId="26375026" condition="" format="" name="src_company_name" lookupType=""/>
									<column elemId="26375027" condition="" format="" name="src_business_number" lookupType=""/>
									<column elemId="26375028" condition="" format="" name="src_birth_date" lookupType=""/>
									<column elemId="26375029" condition="" format="" name="src_gender" lookupType=""/>
									<column elemId="26375031" condition="" format="" name="src_legal_form" lookupType=""/>
									<column elemId="26375032" condition="" format="" name="src_established_date" lookupType=""/>
									<column elemId="18081305" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26375238" name="cleansed" label="Cleansed Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="26376064" condition="" format="" name="cio_first_name" lookupType=""/>
									<column elemId="26376065" condition="" format="" name="cio_last_name" lookupType=""/>
									<column elemId="26375798" condition="" format="" name="exp_full_name" lookupType=""/>
									<column elemId="26376066" condition="" format="" name="cio_birth_date" lookupType=""/>
									<column elemId="26375799" condition="" format="" name="exp_birth_date" lookupType=""/>
									<column elemId="26376067" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="26375800" condition="" format="" name="exp_type" lookupType=""/>
									<column elemId="26376068" condition="" format="" name="cio_gender" lookupType=""/>
									<column elemId="26376070" condition="" format="" name="cio_sin" lookupType=""/>
									<column elemId="26375801" condition="" format="" name="exp_sin" lookupType=""/>
									<column elemId="26376071" condition="" format="" name="cio_company_name" lookupType=""/>
									<column elemId="26375802" condition="" format="" name="exp_company_name" lookupType=""/>
									<column elemId="26376072" condition="" format="" name="cio_business_number" lookupType=""/>
									<column elemId="26375803" condition="" format="" name="exp_business_number" lookupType=""/>
									<column elemId="26376073" condition="" format="" name="cio_legal_form" lookupType=""/>
									<column elemId="26376074" condition="" format="" name="cio_established_date" lookupType=""/>
									<column elemId="26375804" condition="" format="" name="exp_established_date" lookupType=""/>
									<column elemId="26376075" condition="" format="" name="cio_address_comp_set" lookupType=""/>
									<column elemId="26376076" condition="" format="" name="cio_contact_comp_set" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26376722" name="matching" label="Matching Details" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="178087" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="26376724" condition="" format="" name="match_rule_name" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="211582" name="id" customLabel="ID"/>
				<column elemId="211585" name="src_first_name" customLabel="Firstname"/>
				<column elemId="211586" name="std_first_name" customLabel="Firstname"/>
				<column elemId="211593" name="src_last_name" customLabel="Lastname"/>
				<column elemId="211594" name="std_last_name" customLabel="Lastname"/>
				<column elemId="26380077" name="src_company_name" customLabel="Company Name"/>
				<column elemId="26380078" name="src_business_number" customLabel=""/>
				<column elemId="26380079" name="src_legal_form" customLabel=""/>
				<column elemId="26380080" name="src_established_date" customLabel=""/>
				<column elemId="211597" name="src_gender" customLabel=""/>
				<column elemId="211601" name="src_birth_date" customLabel=""/>
				<column elemId="211602" name="std_birth_date" customLabel=""/>
				<column elemId="211603" name="exp_birth_date" customLabel=""/>
				<column elemId="211604" name="sco_birth_date" customLabel=""/>
				<column elemId="211605" name="src_sin" customLabel=""/>
				<column elemId="211606" name="std_sin" customLabel=""/>
				<column elemId="211607" name="exp_sin" customLabel=""/>
				<column elemId="211608" name="sco_sin" customLabel=""/>
				<column elemId="657845" name="match_rule_name" customLabel=""/>
				<column elemId="211583" name="source_id" customLabel="Source Data ID"/>
				<column elemId="211584" name="master_id" customLabel="Master Party ID"/>
			</columns>
		</instanceTable>
		<instanceTable elemId="36790" topLevel="false" name="contact_instance" label="Instance contact" instanceTable="contact">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26380093">
						<groups>
							<labeledGroup elemId="26380094" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26380095" condition="" format="" name="id" lookupType=""/>
									<column elemId="26380096" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="18081018" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="26380097" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="26380098" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="26380099" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="26380100" condition="" format="" name="std_type" lookupType=""/>
									<column elemId="26380101" condition="" format="" name="dic_type" lookupType=""/>
									<column elemId="26380102" condition="" format="" name="src_value" lookupType=""/>
									<column elemId="26380103" condition="" format="" name="std_value" lookupType=""/>
									<column elemId="26380104" condition="" format="" name="cio_value" lookupType=""/>
									<column elemId="26380105" condition="" format="" name="exp_value" lookupType=""/>
									<column elemId="26380106" condition="" format="" name="sco_value" lookupType=""/>
									<column elemId="26380107" condition="" format="" name="cio_contact_comp" lookupType=""/>
									<column elemId="26380109" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="26380111" condition="" format="" name="party_master_id" lookupType=""/>
									<column elemId="18081019" condition="" format="" name="eng_last_update_date" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="510184" name="id" customLabel=""/>
				<column elemId="510185" name="source_id" customLabel=""/>
				<column elemId="510186" name="master_id" customLabel=""/>
				<column elemId="509827" name="src_type" customLabel=""/>
				<column elemId="509831" name="src_value" customLabel=""/>
				<column elemId="509832" name="std_value" customLabel=""/>
				<column elemId="509833" name="exp_value" customLabel=""/>
				<column elemId="509834" name="sco_value" customLabel=""/>
				<column elemId="658070" name="party_master_id" customLabel=""/>
			</columns>
		</instanceTable>
		<instanceTable elemId="36923" topLevel="false" name="address_instance" label="Instance address" instanceTable="address">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="26373348">
						<groups>
							<labeledGroup elemId="26373554" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="26373555" condition="" format="" name="id" lookupType=""/>
									<column elemId="26373556" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="26373557" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="26373558" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="26373560" condition="" format="" name="party_master_id" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26373349" name="source" label="Source Data" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="18081449" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="18081914" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="26373767" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="26373768" condition="" format="" name="src_street" lookupType=""/>
									<column elemId="26373769" condition="" format="" name="src_city" lookupType=""/>
									<column elemId="26373771" condition="" format="" name="src_zip" lookupType=""/>
									<column elemId="26373770" condition="" format="" name="src_state" lookupType=""/>
									<column elemId="18081450" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="18081915" condition="" format="" name="eng_active" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledGroup elemId="26373350" name="cleansed" label="Cleansed Data" templatePosition="right" viewCondition="">
								<columns>
									<column elemId="26374180" condition="" format="" name="cio_address_comp" lookupType=""/>
									<column elemId="26374181" condition="" format="" name="dic_type" lookupType=""/>
									<column elemId="26374182" condition="" format="" name="cio_street" lookupType=""/>
									<column elemId="26374183" condition="" format="" name="cio_city" lookupType=""/>
									<column elemId="26374184" condition="" format="" name="cio_zip" lookupType=""/>
									<column elemId="26374185" condition="" format="" name="cio_state" lookupType=""/>
									<column elemId="26374594" condition="" format="" name="exp_address" lookupType=""/>
									<column elemId="26374595" condition="" format="" name="sco_address" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="658295" name="id" customLabel="ID"/>
				<column elemId="658296" name="source_id" customLabel="Address id"/>
				<column elemId="658298" name="party_source_id" customLabel="Party id"/>
				<column elemId="177816" name="src_street" customLabel="Street"/>
				<column elemId="177817" name="cio_street" customLabel="Street"/>
				<column elemId="177818" name="src_city" customLabel="City"/>
				<column elemId="177819" name="cio_city" customLabel="City"/>
				<column elemId="180252" name="src_state" customLabel="State"/>
				<column elemId="658299" name="cio_state" customLabel="State"/>
				<column elemId="658300" name="src_zip" customLabel="ZIP"/>
				<column elemId="658301" name="cio_zip" customLabel="ZIP"/>
				<column elemId="658302" name="exp_address" customLabel="Cleansing Explanation"/>
				<column elemId="658303" name="sco_address" customLabel="Cleansing Score"/>
				<column elemId="658304" name="party_master_id" customLabel="Party master_id"/>
				<column elemId="26375003" name="cio_address_comp" customLabel="Address Component"/>
				<column elemId="26375638" name="dic_type" customLabel="Verified Address Type"/>
				<column elemId="172557" name="master_id" customLabel="Address master_id"/>
				<column elemId="172897" name="src_type" customLabel="Type"/>
			</columns>
		</instanceTable>
		<instanceTable elemId="78553" topLevel="false" name="rel_party_party_instance" label="Instance party to party relations" instanceTable="rel_party2party">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="110191">
						<groups>
							<labeledGroup elemId="110192" name="grp" label="Attributes" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="110193" condition="" format="" name="id" lookupType=""/>
									<column elemId="110194" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="110195" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="110196" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="110197" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="110198" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="110199" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="110200" condition="" format="" name="parent_source_id" lookupType=""/>
									<column elemId="110201" condition="" format="" name="child_source_id" lookupType=""/>
									<column elemId="110202" condition="" format="" name="src_rel_type" lookupType=""/>
									<column elemId="110203" condition="" format="" name="src_p2p_valid_from" lookupType=""/>
									<column elemId="110204" condition="" format="" name="std_p2p_valid_from" lookupType=""/>
									<column elemId="110205" condition="" format="" name="cio_p2p_valid_from" lookupType=""/>
									<column elemId="110206" condition="" format="" name="exp_p2p_valid_from" lookupType=""/>
									<column elemId="110207" condition="" format="" name="sco_p2p_valid_from" lookupType=""/>
									<column elemId="110208" condition="" format="" name="src_p2p_valid_to" lookupType=""/>
									<column elemId="110209" condition="" format="" name="std_p2p_valid_to" lookupType=""/>
									<column elemId="110210" condition="" format="" name="cio_p2p_valid_to" lookupType=""/>
									<column elemId="110211" condition="" format="" name="exp_p2p_valid_to" lookupType=""/>
									<column elemId="110212" condition="" format="" name="sco_p2p_valid_to" lookupType=""/>
									<column elemId="110214" condition="" format="" name="dic_rel_type" lookupType=""/>
									<column elemId="110217" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="110219" condition="" format="" name="party_child_master_id" lookupType=""/>
									<column elemId="110220" condition="" format="" name="party_child_cio_type" lookupType=""/>
									<column elemId="110221" condition="" format="" name="party_parent_master_id" lookupType=""/>
									<column elemId="110222" condition="" format="" name="party_parent_cio_type" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="127730" name="id" customLabel=""/>
				<column elemId="127731" name="master_id" customLabel=""/>
				<column elemId="127745" name="parent_source_id" customLabel=""/>
				<column elemId="127746" name="child_source_id" customLabel=""/>
				<column elemId="127755" name="party_child_master_id" customLabel=""/>
				<column elemId="127756" name="party_parent_master_id" customLabel=""/>
			</columns>
		</instanceTable>
		<instanceTable elemId="155032" topLevel="false" name="id_document_instance" label="ID Document" instanceTable="id_document">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="155364">
						<groups>
							<labeledGroup elemId="155365" name="basic" label="Basic Information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="155366" condition="" format="" name="id" lookupType=""/>
									<column elemId="155367" condition="" format="" name="master_id" lookupType=""/>
									<column elemId="155368" condition="" format="" name="eng_active" lookupType=""/>
									<column elemId="155369" condition="" format="" name="eng_origin" lookupType=""/>
									<column elemId="155370" condition="" format="" name="eng_source_system" lookupType=""/>
									<column elemId="155371" condition="" format="" name="eng_last_update_date" lookupType=""/>
									<column elemId="155372" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="155373" condition="" format="" name="party_source_id" lookupType=""/>
									<column elemId="155374" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="155375" condition="" format="" name="cio_type" lookupType=""/>
									<column elemId="155376" condition="" format="" name="dic_type" lookupType=""/>
									<column elemId="155377" condition="" format="" name="src_value" lookupType=""/>
									<column elemId="155378" condition="" format="" name="cio_value" lookupType=""/>
									<column elemId="155379" condition="" format="" name="cio_id_document_comp" lookupType=""/>
									<column elemId="155381" condition="" format="" name="match_rule_name" lookupType=""/>
									<column elemId="155383" condition="" format="" name="party_master_id" lookupType=""/>
								</columns>
							</labeledGroup>
						</groups>
						<guiDetailConditions/>
					</detailView>
				</views>
				<computedColumns/>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns/>
		</instanceTable>
		<instanceTable elemId="111414" topLevel="true" name="contract_instance" label="Instance contract" instanceTable="contract">
			<guiTab>
				<reduceES allColumns="true">
					<reduceColumns/>
				</reduceES>
				<views>
					<detailView elemId="123613">
						<groups>
							<labeledGroup elemId="87506" name="party_basic" label="Contract instance information" templatePosition="left" viewCondition="">
								<columns>
									<column elemId="19163" condition="" format="" name="id" lookupType=""/>
									<column elemId="19164" condition="" format="" name="source_id" lookupType=""/>
									<column elemId="19166" condition="" format="" name="src_sale_point" lookupType=""/>
									<column elemId="19167" condition="" format="" name="src_status" lookupType=""/>
									<column elemId="19168" condition="" format="" name="src_type" lookupType=""/>
									<column elemId="19169" condition="" format="" name="src_variant" lookupType=""/>
									<column elemId="19170" condition="" format="" name="src_valid_from" lookupType=""/>
									<column elemId="19171" condition="" format="" name="src_valid_to" lookupType=""/>
								</columns>
							</labeledGroup>
							<labeledRelatedMNList elemId="123614" entity_out="organisation" previewLimit="" name="c2o" label="Organisation" templatePosition="left" relationship="contract_has_party_org" relationship_out="org_has_contract" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>ParentId:${pty_id}</firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Organisation: </firstColumnFormat>
									<secondColumnFormat>${cmo_company_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
							<labeledRelatedMNList elemId="130114" entity_out="person" previewLimit="" name="c2p" label="Person" templatePosition="right" relationship="contract_has_party_per" relationship_out="per_has_contract" viewCondition="">
								<MNrelationParams>
									<firstColumnFormat>ParentId:${pty_id} </firstColumnFormat>
									<secondColumnFormat>Active: ${eng_active} | Updated: ${eng_mtdt}</secondColumnFormat>
								</MNrelationParams>
								<MNlinkedRecordsParams>
									<firstColumnFormat>Person: </firstColumnFormat>
									<secondColumnFormat>${cmo_first_name} ${cmo_last_name}</secondColumnFormat>
								</MNlinkedRecordsParams>
							</labeledRelatedMNList>
						</groups>
						<guiDetailConditions/>
					</detailView>
					<breadcrumbView elemId="125898" allColumns="false">
						<columns>
							<name>${src_type} ${src_variant}</name>
						</columns>
					</breadcrumbView>
				</views>
				<computedColumns>
					<column elemId="26386514" indexType="BOTH" dataType="string" name="contract_single_line" description="" label="Contract Label">
						<producer>getMdaValString(&#39;source_id&#39;) + &#39;@&#39; + nvl(getMdaValString(&#39;dic_sale_point&#39;), getMdaValString(&#39;src_sale_point&#39;))</producer>
					</column>
				</computedColumns>
				<sampleSetting sampleStrategy="" useSampleThreshold="" enable="false" sampleSize=""/>
				<guiValidations enable="false">
					<validationScoColumns/>
					<validationExpColumns/>
					<validationTabOverrideDefaultSettings highDQThreshold="" lowDQThreshold=""/>
				</guiValidations>
			</guiTab>
			<columns>
				<column elemId="132473" name="id" customLabel="ID"/>
				<column elemId="132474" name="source_id" customLabel="Contract Number"/>
				<column elemId="132475" name="src_sale_point" customLabel="Contract Sale Point"/>
				<column elemId="132480" name="src_status" customLabel="Contract Status"/>
				<column elemId="132484" name="src_type" customLabel="Contract Type"/>
				<column elemId="132489" name="src_valid_from" customLabel="Valid From"/>
				<column elemId="132490" name="cio_valid_from" customLabel="Valid From"/>
				<column elemId="132491" name="src_valid_to" customLabel="Valid To"/>
				<column elemId="132492" name="cio_valid_to" customLabel="Valid To"/>
				<column elemId="170476" name="product_source_id" customLabel="Product Family"/>
			</columns>
		</instanceTable>
	</instanceTables>
</masterModel>