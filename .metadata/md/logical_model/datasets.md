<?xml version='1.0' encoding='UTF-8'?>
<datasets>
	<datasetsArray>
		<datasetArray elemId="27884053" enable="true" name="party_contact_inst" label="Instance Party Contact  " layerName="dataset">
			<instanceTables>
				<instanceTable elemId="27885186" useAsDetail="true" entityName="party">
					<columns>
						<column elemId="27885187" name="std_first_name" label=""/>
						<column elemId="27885188" name="std_last_name" label=""/>
						<column elemId="27885189" name="cio_sin" label=""/>
					</columns>
				</instanceTable>
				<instanceTable elemId="27885786" useAsDetail="false" entityName="contact">
					<columns>
						<column elemId="27885787" name="cio_type" label=""/>
						<column elemId="27885788" name="cio_value" label=""/>
					</columns>
				</instanceTable>
			</instanceTables>
			<masterTables/>
			<sqlSource>
				<columns/>
				<sqlOver>
					<sql></sql>
				</sqlOver>
			</sqlSource>
			<sorTables/>
			<sampleSetting allowed="false" enable="false" sampleSize=""/>
		</datasetArray>
		<datasetArray elemId="27886380" enable="true" name="party_address_contact_mas" label="Master Party Address Contact" layerName="dataset">
			<instanceTables/>
			<masterTables>
				<masterTable elemId="27886381" useAsDetail="true" entityName="party (masters)">
					<columns>
						<column elemId="27886382" name="cmo_first_name" label=""/>
						<column elemId="27886383" name="cmo_last_name" label=""/>
						<column elemId="27886384" name="cmo_sin" label=""/>
					</columns>
				</masterTable>
				<masterTable elemId="27886385" useAsDetail="false" entityName="address (masters)">
					<columns>
						<column elemId="27886978" name="cmo_type" label=""/>
						<column elemId="27886979" name="cmo_street" label=""/>
						<column elemId="27886980" name="cmo_city" label=""/>
						<column elemId="27886981" name="cmo_zip" label=""/>
					</columns>
				</masterTable>
				<masterTable elemId="27886386" useAsDetail="false" entityName="contact (masters)">
					<columns>
						<column elemId="27886982" name="cmo_type" label=""/>
						<column elemId="27886983" name="cmo_value" label=""/>
					</columns>
				</masterTable>
			</masterTables>
			<sqlSource>
				<columns/>
				<sqlOver>
					<sql></sql>
				</sqlOver>
			</sqlSource>
			<sorTables/>
			<sampleSetting allowed="false" enable="false" sampleSize=""/>
		</datasetArray>
		<datasetArray elemId="27955715" enable="true" name="party_address_sor" label="SoR Party Address Contact" layerName="dataset">
			<instanceTables/>
			<masterTables/>
			<sqlSource>
				<columns/>
				<sqlOver>
					<sql></sql>
				</sqlOver>
			</sqlSource>
			<sorTables>
				<sorTable elemId="27955716" useAsDetail="true" entityName="party">
					<columns>
						<column elemId="27844370" name="first_name" label=""/>
						<column elemId="27844371" name="last_name" label=""/>
						<column elemId="27844372" name="sin" label=""/>
					</columns>
				</sorTable>
				<sorTable elemId="27955720" useAsDetail="false" entityName="address">
					<columns>
						<column elemId="27956307" name="street" label=""/>
						<column elemId="27956308" name="city" label=""/>
						<column elemId="27956309" name="zip" label=""/>
					</columns>
				</sorTable>
			</sorTables>
			<sampleSetting allowed="false" enable="false" sampleSize=""/>
		</datasetArray>
		<datasetArray elemId="27887586" enable="true" name="party_product_mas" label="Master Party Product" layerName="dataset">
			<instanceTables>
				<instanceTable elemId="27887587" useAsDetail="false" entityName="contract">
					<columns/>
				</instanceTable>
				<instanceTable elemId="27892371" useAsDetail="false" entityName="rel_prod2contract">
					<columns/>
				</instanceTable>
				<instanceTable elemId="27894736" useAsDetail="false" entityName="rel_party2contract">
					<columns/>
				</instanceTable>
			</instanceTables>
			<masterTables>
				<masterTable elemId="27887592" useAsDetail="true" entityName="party (masters)">
					<columns/>
				</masterTable>
				<masterTable elemId="27887588" useAsDetail="false" entityName="product (masters)">
					<columns/>
				</masterTable>
			</masterTables>
			<sqlSource>
				<columns>
					<column elemId="27849499" name="cmo_type" label="Party Type" type="string"/>
					<column elemId="27849500" name="cmo_first_name" label="First Name" type="string"/>
					<column elemId="27849501" name="cmo_last_name" label="Last Name" type="string"/>
					<column elemId="27849502" name="cmo_company_name" label="Company Name" type="string"/>
					<column elemId="27849503" name="src_rel_type" label="Contract Relation" type="string"/>
					<column elemId="27849504" name="src_variant" label="Product Variant" type="string"/>
					<column elemId="27849505" name="cmo_name" label="Product Name" type="string"/>
					<column elemId="27849506" name="product_type" label="Product Type" type="string"/>
					<column elemId="27849507" name="cmo_status" label="Product Status" type="string"/>
				</columns>
				<sqlOver>
					<sql>select 
				p.id, p.cmo_type, p.cmo_first_name, p.cmo_last_name, p.cmo_company_name, 
				pac.src_rel_type, 
				pr.cmo_name, pr.cmo_type as product_type, pr.cmo_status,
				prc.src_variant
				from
				(
					(
						($INSTANCE_contract$ c join $INSTANCE_rel_party2contract$ pac on c.source_id=pac.contract_source_id) 
							join $INSTANCE_rel_prod2contract$ prc on c.source_id=prc.ctr_source_id
						)
					join $MASTER_masters_party$ p on p.id=pac.pty_master_id
					)
				join $MASTER_masters_product$ pr on pr.id=prc.prd_master_id
				where c.source_id is not null</sql>
				</sqlOver>
			</sqlSource>
			<sorTables/>
			<sampleSetting allowed="false" enable="true" sampleSize=""/>
		</datasetArray>
		<datasetArray elemId="27891185" enable="true" name="party_consent_access" label="Personal Data Access" layerName="dataset">
			<instanceTables/>
			<masterTables>
				<masterTable elemId="27888790" useAsDetail="true" entityName="party (masters)">
					<columns>
						<column elemId="27889994" name="cmo_type" label=""/>
						<column elemId="27889995" name="cmo_first_name" label=""/>
						<column elemId="27889996" name="cmo_last_name" label=""/>
						<column elemId="27889997" name="cmo_gender" label=""/>
						<column elemId="27843629" name="cmo_birth_date" label=""/>
						<column elemId="27889998" name="cmo_sin" label=""/>
					</columns>
				</masterTable>
				<masterTable elemId="27888791" useAsDetail="false" entityName="consent (masters)">
					<columns>
						<column elemId="27862788" name="cmo_category" label=""/>
						<column elemId="27889383" name="cmo_purpose" label=""/>
						<column elemId="27889384" name="cmo_consumer" label=""/>
						<column elemId="27889385" name="cmo_status" label=""/>
						<column elemId="27889386" name="cmo_origin_date" label=""/>
						<column elemId="27889387" name="cmo_expiration_date" label=""/>
					</columns>
				</masterTable>
			</masterTables>
			<sqlSource>
				<columns/>
				<sqlOver>
					<sql>select 
	party_masters.id, party_masters.cmo_type as party_masters_cmo_type, party_masters.cmo_first_name as party_masters_cmo_first_name, party_masters.cmo_last_name as party_masters_cmo_last_name, 
	party_masters.cmo_gender as party_masters_cmo_gender, party_masters.cmo_birth_date as party_masters_cmo_birth_date, party_masters.cmo_sin as party_masters_cmo_sin, 
	consent_masters.cmo_category as consent_masters_cmo_category, consent_masters.cmo_purpose as consent_masters_cmo_purpose, consent_masters.cmo_consumer as consent_masters_cmo_consumer, 
	consent_masters.cmo_status as consent_masters_cmo_status, consent_masters.cmo_origin_date as consent_masters_cmo_origin_date, consent_masters.cmo_expiration_date as consent_masters_cmo_expiration_date from
			$MASTER_masters_party$ party_masters
			left join $MASTER_masters_consent$ consent_masters
			on party_masters.id = consent_masters.party_id
where consent_masters.cmo_category = &#39;access&#39; and consent_masters.cmo_status in (&#39;granted&#39;, &#39;extended&#39;)</sql>
				</sqlOver>
			</sqlSource>
			<sorTables/>
			<sampleSetting allowed="false" enable="true" sampleSize=""/>
		</datasetArray>
		<datasetArray elemId="27888789" enable="true" name="party_consent_storing" label="Personal Data Storing" layerName="dataset">
			<instanceTables/>
			<masterTables>
				<masterTable elemId="27888790" useAsDetail="true" entityName="party (masters)">
					<columns>
						<column elemId="27889994" name="cmo_type" label=""/>
						<column elemId="27889995" name="cmo_first_name" label=""/>
						<column elemId="27889996" name="cmo_last_name" label=""/>
						<column elemId="27889997" name="cmo_gender" label=""/>
						<column elemId="27844895" name="cmo_birth_date" label=""/>
						<column elemId="27889998" name="cmo_sin" label=""/>
					</columns>
				</masterTable>
				<masterTable elemId="27888791" useAsDetail="false" entityName="consent (masters)">
					<columns>
						<column elemId="27863371" name="cmo_category" label=""/>
						<column elemId="27889383" name="cmo_purpose" label=""/>
						<column elemId="27889384" name="cmo_consumer" label=""/>
						<column elemId="27889385" name="cmo_status" label=""/>
						<column elemId="27889386" name="cmo_origin_date" label=""/>
						<column elemId="27889387" name="cmo_expiration_date" label=""/>
					</columns>
				</masterTable>
			</masterTables>
			<sqlSource>
				<columns/>
				<sqlOver>
					<sql>select 
	party_masters.id, party_masters.cmo_type as party_masters_cmo_type, party_masters.cmo_first_name as party_masters_cmo_first_name, party_masters.cmo_last_name as party_masters_cmo_last_name, 
	party_masters.cmo_gender as party_masters_cmo_gender, party_masters.cmo_birth_date as party_masters_cmo_birth_date, party_masters.cmo_sin as party_masters_cmo_sin, 
	consent_masters.cmo_category as consent_masters_cmo_category, consent_masters.cmo_purpose as consent_masters_cmo_purpose, consent_masters.cmo_consumer as consent_masters_cmo_consumer, 
	consent_masters.cmo_status as consent_masters_cmo_status, consent_masters.cmo_origin_date as consent_masters_cmo_origin_date, consent_masters.cmo_expiration_date as consent_masters_cmo_expiration_date from
			$MASTER_masters_party$ party_masters
			left join $MASTER_masters_consent$ consent_masters
			on party_masters.id = consent_masters.party_id
where consent_masters.cmo_category = &#39;storing&#39; and consent_masters.cmo_status in (&#39;granted&#39;, &#39;extended&#39;)</sql>
				</sqlOver>
			</sqlSource>
			<sorTables/>
			<sampleSetting allowed="false" enable="true" sampleSize=""/>
		</datasetArray>
		<datasetArray elemId="27875792" enable="true" name="party_mat_rules" label="Party Entity Matching Rules" layerName="dataset">
			<instanceTables/>
			<masterTables>
				<masterTable elemId="27875793" useAsDetail="true" entityName="party (masters)">
					<columns/>
				</masterTable>
			</masterTables>
			<sqlSource>
				<columns>
					<column elemId="27878272" name="rule" label="Matching Rule" type="string"/>
					<column elemId="27878273" name="cnt" label="Occurrence" type="integer"/>
					<column elemId="27878274" name="perc" label="Occurrence (%)" type="float"/>
				</columns>
				<sqlOver>
					<sql>select
    sample id, rule, cnt, cast(cast(cnt * 100 as float)/ttl as decimal(5,2)) perc
from
    (select
        min(a.master_id) sample, match_rule_name rule, count(*) cnt, min(b.total) ttl
    from $INSTANCE_party$ a
        join
    (select count(*) total from $INSTANCE_party$ d where d.match_rule_name is not null) b
        on
    1=1
    where a.match_rule_name is not null group by a.match_rule_name
    ) c</sql>
				</sqlOver>
			</sqlSource>
			<sorTables/>
			<sampleSetting allowed="false" enable="true" sampleSize=""/>
		</datasetArray>
	</datasetsArray>
</datasets>