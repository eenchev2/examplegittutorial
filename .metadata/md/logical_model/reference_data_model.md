<?xml version='1.0' encoding='UTF-8'?>
<dictionary>
	<tables>
		<table elemId="332820" loadInst="false" usageValidation="none" name="gender" loadMas="true" masterCodeType="string" label="RD Gender " instUse="cleansing" sourceCodeType="string">
			<description>Maps all different gender values from various Connected Systems and translates them to one mastered value</description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="332948" loadInst="true" usageValidation="none" name="party_type" loadMas="true" masterCodeType="string" label="RD Party Type" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:digit:][:letter:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="109103" loadInst="false" usageValidation="cleansing" name="currency" loadMas="true" masterCodeType="string" label="RD Currency" instUse="none" sourceCodeType="string">
			<description></description>
			<columns>
				<column elemId="26386749" instance="false" size="200" name="country_name" label="Country Name" type="string" master="true" useAsLabel="false">
					<comment></comment>
				</column>
			</columns>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:letter:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="109531" loadInst="false" usageValidation="cleansing" name="contract_type" loadMas="true" masterCodeType="string" label="RD Contract Type" instUse="none" sourceCodeType="string">
			<description></description>
			<columns>
				<column elemId="15682" instance="false" size="1024" name="description_EN" label="" type="string" master="true" useAsLabel="false">
					<comment></comment>
				</column>
			</columns>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="109683" loadInst="false" usageValidation="cleansing" name="sale_point" loadMas="true" masterCodeType="string" label="RD Sale Point" instUse="none" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="109848" loadInst="false" usageValidation="cleansing" name="rel_pty2ctr_type" loadMas="true" masterCodeType="string" label="RD Party 2 Contract Rel Type" instUse="none" sourceCodeType="string">
			<description>Dictionary value check for different relationship types between Party and Contract entities</description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="110021" loadInst="false" usageValidation="cleansing" name="rel_pty2pty_type" loadMas="true" masterCodeType="string" label="RD Party 2 Party Rel Type" instUse="none" sourceCodeType="string">
			<description>Dictionary value check for different relationship types between two Party entities</description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="11437770" loadInst="false" usageValidation="cleansing" name="rel_pty2pty_ctg" loadMas="true" masterCodeType="string" label="RD Party 2 Party Rel Category" instUse="none" sourceCodeType="string">
			<description>Dictionary value check for different relationship categories between two Party entities. One category groups various relationship types</description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="110588" loadInst="true" usageValidation="none" name="address_type" loadMas="true" masterCodeType="integer" label="RD Address Type" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="110793" loadInst="false" usageValidation="none" name="contact_type" loadMas="true" masterCodeType="string" label="RD Contact Type" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="105855" loadInst="false" usageValidation="none" name="legal_form" loadMas="true" masterCodeType="string" label="RD Legal Form" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="26374568" loadInst="false" usageValidation="none" name="id_document_type" loadMas="true" masterCodeType="string" label="RD ID Document Type" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="12252" loadInst="false" usageValidation="none" name="product_type" loadMas="true" masterCodeType="string" label="RD Product Type" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="27835651" loadInst="false" usageValidation="none" name="consent_category" loadMas="true" masterCodeType="string" label="RD Consent Category" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="27835652" loadInst="false" usageValidation="none" name="consent_purpose" loadMas="true" masterCodeType="string" label="RD Consent Purpose" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="27835653" loadInst="false" usageValidation="none" name="consent_system" loadMas="true" masterCodeType="string" label="RD Consent Source System" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="27835654" loadInst="false" usageValidation="none" name="consent_consumer" loadMas="true" masterCodeType="string" label="RD Consent Consumer" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="9430" loadInst="false" usageValidation="none" name="consent_channel" loadMas="true" masterCodeType="string" label="RD Consent Channel" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
		<table elemId="11109" loadInst="false" usageValidation="none" name="consent_status" loadMas="true" masterCodeType="string" label="RD Consent Status" instUse="cleansing" sourceCodeType="string">
			<description></description>
			<columns/>
			<lookupSettings bidirectApproxIndex="false" supportedChars="[:all:]" bestDistanceIndex="false" squeezeWS="true" removeRepeatedChars="false" removeAccents="true" substituteWith="" upperCase="true" duplicities="ACCEPT" approxIndex="false"/>
		</table>
	</tables>
</dictionary>