<?xml version='1.0' encoding='UTF-8'?>
<historyPlugin enable="true">
	<instanceTables>
		<instanceTable elemId="1583" entityName="party" allColumns="true">
			<instanceColumns/>
		</instanceTable>
		<instanceTable elemId="1581" entityName="contract" allColumns="false">
			<instanceColumns>
				<instanceColumn elemId="1582" trace="true" name="cio_valid_to" searchable="false"/>
			</instanceColumns>
		</instanceTable>
		<instanceTable elemId="71236" entityName="product" allColumns="false">
			<instanceColumns>
				<instanceColumn elemId="17419" trace="true" name="cio_name" searchable="false"/>
				<instanceColumn elemId="17420" trace="false" name="cio_type" searchable="false"/>
			</instanceColumns>
		</instanceTable>
	</instanceTables>
	<masterTables>
		<masterTable elemId="155412" entityName="party (masters)" allColumns="true">
			<masterColumns/>
		</masterTable>
		<masterTable elemId="90668" entityName="contact (masters)" allColumns="false">
			<masterColumns>
				<masterColumn elemId="90670" trace="true" name="party_id" searchable="false"/>
				<masterColumn elemId="22018" trace="false" name="cmo_type" searchable="false"/>
				<masterColumn elemId="22019" trace="true" name="cmo_value" searchable="false"/>
			</masterColumns>
		</masterTable>
		<masterTable elemId="132057" entityName="address (masters)" allColumns="true">
			<masterColumns/>
		</masterTable>
		<masterTable elemId="6671" entityName="address (norm)" allColumns="true">
			<masterColumns/>
		</masterTable>
	</masterTables>
	<referenceDataTables>
		<referenceDataTable elemId="70616" entityName="rd_address_type" allColumns="true">
			<referenceDataColumns/>
		</referenceDataTable>
		<referenceDataTable elemId="70617" entityName="rd_contact_type" allColumns="false">
			<referenceDataColumns>
				<referenceDataColumn elemId="70618" trace="true" name="master_code" searchable="false"/>
				<referenceDataColumn elemId="70619" trace="true" name="master_name" searchable="false"/>
				<referenceDataColumn elemId="70620" trace="false" name="source_id" searchable="false"/>
			</referenceDataColumns>
		</referenceDataTable>
	</referenceDataTables>
</historyPlugin>