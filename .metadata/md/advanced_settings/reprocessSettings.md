<?xml version='1.0' encoding='UTF-8'?>
<reprocessSettings>
	<reprocess>
		<full elemId="27341" ignorePersistence="true" rematchAll="false" allTables="true" plan="full_reprocess_operation">
			<description></description>
			<entities/>
		</full>
		<full elemId="168930" ignorePersistence="true" rematchAll="true" allTables="true" plan="full_reprocess_operation_rematch">
			<description></description>
			<entities/>
		</full>
		<full elemId="27860972" ignorePersistence="false" rematchAll="false" allTables="false" plan="consent_reprocess_operation">
			<description>Consents re-calculation process</description>
			<entities>
				<entity elemId="27860973" name="consent"/>
			</entities>
		</full>
	</reprocess>
</reprocessSettings>