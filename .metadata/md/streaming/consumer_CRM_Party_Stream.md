<?xml version='1.0' encoding='UTF-8'?>
<jmsStreamSource elemId="23705619" enable="true" name="CRM_Party_Stream">
	<description>Sample Streaming configuration reading JSON message containing Customer data parsed and mapped to Party entity</description>
	<source inputDestination="dynamicQueues/stream.in.queue" connectionName="esbEvents"/>
	<batching seconds="30" count="20"/>
	<processedEntities allEntities="false">
		<entities>
			<entity elemId="27176735" name="party">
				<columns/>
			</entity>
		</entities>
	</processedEntities>
	<messageTransformer>
		<transformers>
			<planTransformer elemId="27176736" plan="com.ataccama.nme.dqc.stream.PlanTransformer"/>
		</transformers>
		<headers>
			<header elemId="27176737" column="meta_jms_timestamp" header="JMSTimestamp"/>
		</headers>
		<properties/>
	</messageTransformer>
</jmsStreamSource>