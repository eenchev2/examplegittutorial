<?xml version='1.0' encoding='UTF-8'?>
<eventHandler>
	<consolidationEventHandler>
		<handlers>
			<handler elemId="26373348" enable="true" name="default" class="EventHandlerAsync" persistenceDir="../storage/eventHandler" processor="BatchingEventProcessor" desc="Example of how the MDC can capture data changes">
				<publishers>
					<stdOutPublisher elemId="26380561" enable="false">
						<description>Standard Output Publisher - simple event publisher useful for demonstration purposes</description>
						<transformers>
							<simpleXmlTransformer elemId="26380562" indent="true" name="Simple transformation of data change events into XML format" includeOldValues="true"/>
						</transformers>
						<advanced>
							<filteringPublisher filter="false">
								<expression></expression>
								<filteredEntities>
									<entity elemId="26385822" name="party (instance)">
										<expression>new.src_first_name != old.src_first_name</expression>
									</entity>
								</filteredEntities>
							</filteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</advanced>
					</stdOutPublisher>
					<eventPlanPublisher elemId="12459" enable="false" layerName="masters" suffix="">
						<description>Sample of Generic Plan Publisher - Party Masters</description>
						<planAdvancedSettings>
							<planFilteringPublisher filter="false">
								<expression></expression>
							</planFilteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</planAdvancedSettings>
						<entityName allColumns="false" entity="party"/>
						<columnsMeta>
							<columnMeta elemId="13009" name="meta_id"/>
							<columnMeta elemId="13010" name="meta_event_type"/>
							<columnMeta elemId="13011" name="meta_entity"/>
							<columnMeta elemId="13012" name="meta_layer"/>
							<columnMeta elemId="13013" name="meta_master_view"/>
							<columnMeta elemId="13014" name="meta_origin"/>
							<columnMeta elemId="13015" name="meta_source_system"/>
							<columnMeta elemId="13016" name="meta_activation_date"/>
							<columnMeta elemId="13017" name="meta_creation_date"/>
							<columnMeta elemId="13018" name="meta_deactivation_date"/>
							<columnMeta elemId="13019" name="meta_deletion_date"/>
							<columnMeta elemId="13020" name="meta_last_update_date"/>
							<columnMeta elemId="13021" name="meta_last_source_update_date"/>
							<columnMeta elemId="13022" name="meta_activation_tid"/>
							<columnMeta elemId="13023" name="meta_creation_tid"/>
							<columnMeta elemId="13024" name="meta_deactivation_tid"/>
							<columnMeta elemId="13025" name="meta_deletion_tid"/>
							<columnMeta elemId="13026" name="meta_last_update_tid"/>
							<columnMeta elemId="13027" name="meta_last_source_update_tid"/>
						</columnsMeta>
						<columns>
							<column elemId="12990" new="true" old="true" name="eng_active"/>
							<column elemId="12991" new="true" old="true" name="eng_last_update_date"/>
							<column elemId="12992" new="true" old="true" name="eng_creation_date"/>
							<column elemId="12993" new="true" old="true" name="eng_deletion_date"/>
							<column elemId="12994" new="true" old="true" name="eng_activation_date"/>
							<column elemId="12995" new="true" old="true" name="eng_deactivation_date"/>
							<column elemId="12996" new="true" old="true" name="cmo_type"/>
							<column elemId="12997" new="true" old="true" name="cmo_first_name"/>
							<column elemId="12998" new="true" old="true" name="cmo_last_name"/>
							<column elemId="12999" new="true" old="true" name="cmo_gender"/>
							<column elemId="13000" new="true" old="true" name="cmo_birth_date"/>
							<column elemId="13001" new="true" old="true" name="cmo_sin"/>
							<column elemId="13002" new="true" old="true" name="cmo_company_name"/>
							<column elemId="13003" new="true" old="true" name="cmo_business_number"/>
							<column elemId="13004" new="true" old="true" name="cmo_established_date"/>
							<column elemId="13005" new="true" old="true" name="cmo_legal_form"/>
						</columns>
					</eventPlanPublisher>
					<eventPlanPublisher elemId="158703" enable="false" layerName="instance" suffix="">
						<description>Sample of Generic Plan Publisher - Party Instance</description>
						<planAdvancedSettings>
							<planFilteringPublisher filter="false">
								<expression></expression>
							</planFilteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</planAdvancedSettings>
						<entityName allColumns="true" entity="party"/>
						<columnsMeta/>
						<columns/>
					</eventPlanPublisher>
					<jmsPublisher elemId="26380846" enable="false" destination="dynamicQueues/esb.event.queue" connectionName="esbEvents" contentType="text/plain">
						<description>Sample of JMS publisher configuration.
	To run this publisher, JMS server component must be configured in the MDC server config!</description>
						<headers/>
						<transformers>
							<expressionTemplateTransformer elemId="26380847" entityName="party (instance)" name="Sample tamplate for Party Instance">
								<template>Instance party src_company_name changed from ${toString(old.src_company_name)} to ${toString(new.src_company_name)}. ID: ${toString(meta.id)} Change type: ${meta.event_type}</template>
							</expressionTemplateTransformer>
							<expressionTemplateTransformer elemId="26380848" entityName="party (masters)" name="Sample tamplate for Party Master">
								<template>Attribute cmo_company_name changed from  ${old.cmo_company_name} to  ${new.cmo_company_name}. ID: ${toString(meta.id)} Change type: ${meta.event_type}</template>
							</expressionTemplateTransformer>
						</transformers>
						<advanced>
							<filteringPublisher filter="true">
								<expression></expression>
								<filteredEntities>
									<entity elemId="23706237" name="party (instance)">
										<expression>new.src_company_name is not null</expression>
									</entity>
									<entity elemId="23707268" name="party (masters)">
										<expression>new.cmo_company_name is not null</expression>
									</entity>
								</filteredEntities>
							</filteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</advanced>
					</jmsPublisher>
					<httpSoapPublisher elemId="26381426" delay="0" enable="false" urlResourceName="soapPublisher" soapAction="SOAP_ACTION" encoding="UTF-8" soapVersion="SOAP_1_1" timeout="5000">
						<description>Sample of SOAP publisher configuration</description>
						<advanced>
							<filteringPublisher filter="false">
								<expression></expression>
								<filteredEntities/>
							</filteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</advanced>
						<transformers>
							<expressionTemplateTransformer elemId="26381427" entityName="party (instance)" name="XML output">
								<template>&lt;![CDATA[&lt;soapenv:Envelope xmlns:soapenv=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot; xmlns:sam=&quot;http://www.example.org/sample/&quot;&gt;
	&lt;soapenv:Header/&gt;
	&lt;soapenv:Body&gt;
		&lt;sam:test&gt;
			&lt;text&gt;XML content&lt;/text&gt;
		&lt;/sam:test&gt;
	&lt;/soapenv:Body&gt;
	&lt;/soapenv:Envelope&gt; ]]&gt;</template>
							</expressionTemplateTransformer>
						</transformers>
					</httpSoapPublisher>
					<eventSqlPublisher elemId="26381711" enable="false" dataSource="esb_db">
						<description>Sample of SQL publisher configuration</description>
						<sqlTemplates>
							<template elemId="26381713" name="party (instance)">
								<template>insert into PARTY (SOURCE_ID, N_SRC_COMP_NAME, O_SRC_COMP_NAME) values (${new.source_id}, ${new.src_company_name}, ${old.src_company_name})</template>
							</template>
							<template elemId="26381712" name="party (masters)">
								<template>insert into PARTY_MAS (N_CMO_COMP_NAME, O_CMO_COMP_NAME) values (${new.cmo_company_name}, ${old.cmo_company_name})</template>
							</template>
						</sqlTemplates>
						<advanced>
							<filteringPublisher filter="true">
								<expression></expression>
								<filteredEntities>
									<entity elemId="23689318" name="party (instance)">
										<expression>new.cio_type = &#39;C&#39; and new.src_company_name is not null</expression>
									</entity>
									<entity elemId="23691255" name="party (masters)">
										<expression>new.cmo_type = &#39;C&#39; and new.cmo_company_name is not null</expression>
									</entity>
								</filteredEntities>
							</filteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</advanced>
					</eventSqlPublisher>
					<eventPlanPublisher elemId="26956731" enable="true" layerName="instance" suffix="_dq_issue">
						<description>DQ issues party instance</description>
						<planAdvancedSettings>
							<planFilteringPublisher filter="true">
								<expression>meta.event_type = &#39;INSERT&#39; or meta.event_type = &#39;UPDATE&#39;</expression>
							</planFilteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</planAdvancedSettings>
						<entityName allColumns="true" entity="party"/>
						<columnsMeta/>
						<columns/>
					</eventPlanPublisher>
					<eventPlanPublisher elemId="26956732" enable="true" layerName="instance" suffix="_dq_issue">
						<description>DQ issues contact instance</description>
						<planAdvancedSettings>
							<planFilteringPublisher filter="true">
								<expression>meta.event_type = &#39;INSERT&#39; or meta.event_type = &#39;UPDATE&#39;</expression>
							</planFilteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</planAdvancedSettings>
						<entityName allColumns="true" entity="contact"/>
						<columnsMeta/>
						<columns/>
					</eventPlanPublisher>
					<eventPlanPublisher elemId="26973460" enable="true" layerName="masters" suffix="_dq_issue">
						<description>Master party issue</description>
						<planAdvancedSettings>
							<planFilteringPublisher filter="true">
								<expression>meta.event_type = &#39;INSERT&#39; or meta.event_type = &#39;UPDATE&#39; and 
case(
		new.cmo_type = &#39;P&#39; and ((new.cmo_sin is null or new.cmo_sin = &#39;SIN&#39;) or (old.cmo_sin is null or old.cmo_sin = &#39;SIN&#39;)),true,
		new.cmo_type = &#39;C&#39; and (new.cmo_business_number is null or old.cmo_business_number is null),true,
		not(new.cmo_type in {&#39;P&#39;, &#39;C&#39;}) and ((new.cmo_sin is null or  new.cmo_sin = &#39;SIN&#39; or new.cmo_business_number is null) or (old.cmo_sin is null or old.cmo_sin = &#39;SIN&#39; or old.cmo_business_number is null)),true,
		false
	)</expression>
							</planFilteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</planAdvancedSettings>
						<entityName allColumns="false" entity="party"/>
						<columnsMeta>
							<columnMeta elemId="26971565" name="meta_id"/>
							<columnMeta elemId="26971566" name="meta_event_type"/>
							<columnMeta elemId="26971567" name="meta_entity"/>
							<columnMeta elemId="26971568" name="meta_layer"/>
							<columnMeta elemId="26971569" name="meta_master_view"/>
							<columnMeta elemId="26971570" name="meta_origin"/>
							<columnMeta elemId="26971571" name="meta_source_system"/>
							<columnMeta elemId="26971572" name="meta_activation_date"/>
							<columnMeta elemId="26971573" name="meta_creation_date"/>
							<columnMeta elemId="26971574" name="meta_deactivation_date"/>
							<columnMeta elemId="26971575" name="meta_deletion_date"/>
							<columnMeta elemId="26971576" name="meta_last_update_date"/>
							<columnMeta elemId="26971577" name="meta_last_source_update_date"/>
							<columnMeta elemId="26971578" name="meta_activation_tid"/>
							<columnMeta elemId="26971579" name="meta_creation_tid"/>
							<columnMeta elemId="26971580" name="meta_deactivation_tid"/>
							<columnMeta elemId="26971581" name="meta_deletion_tid"/>
							<columnMeta elemId="26971582" name="meta_last_update_tid"/>
							<columnMeta elemId="26971583" name="meta_last_source_update_tid"/>
						</columnsMeta>
						<columns>
							<column elemId="26971584" new="true" old="true" name="cmo_sin"/>
							<column elemId="26971585" new="true" old="true" name="cmo_business_number"/>
							<column elemId="26971586" new="true" old="true" name="cmo_type"/>
							<column elemId="26971587" new="true" old="true" name="cmo_first_name"/>
							<column elemId="26971588" new="true" old="true" name="cmo_last_name"/>
							<column elemId="26971589" new="true" old="true" name="cmo_company_name"/>
							<column elemId="27835617" new="true" old="true" name="exp_gender"/>
							<column elemId="27835618" new="true" old="true" name="exp_birth_date"/>
							<column elemId="27835619" new="true" old="true" name="exp_sin"/>
							<column elemId="27835620" new="true" old="true" name="exp_company_name"/>
							<column elemId="27835621" new="true" old="true" name="exp_business_number"/>
							<column elemId="27835622" new="true" old="true" name="exp_established_date"/>
							<column elemId="27835623" new="true" old="true" name="exp_full_name"/>
						</columns>
					</eventPlanPublisher>
					<eventPlanPublisher elemId="26967454" enable="true" layerName="masters" suffix="_dq_issue">
						<description>Master contact issue</description>
						<planAdvancedSettings>
							<planFilteringPublisher filter="true">
								<expression>meta.event_type = &#39;INSERT&#39; or meta.event_type = &#39;UPDATE&#39; and
new.cmo_value is null or new.cmo_value is in {&#39;N/A - fictive number&#39;} or new.cmo_type is null or old.cmo_value is null or old.cmo_value is in {&#39;N/A - fictive number&#39;} or old.cmo_type is null</expression>
							</planFilteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</planAdvancedSettings>
						<entityName allColumns="false" entity="contact"/>
						<columnsMeta>
							<columnMeta elemId="26971565" name="meta_id"/>
							<columnMeta elemId="26971566" name="meta_event_type"/>
							<columnMeta elemId="26971567" name="meta_entity"/>
							<columnMeta elemId="26971568" name="meta_layer"/>
							<columnMeta elemId="26971569" name="meta_master_view"/>
							<columnMeta elemId="26971570" name="meta_origin"/>
							<columnMeta elemId="26971571" name="meta_source_system"/>
							<columnMeta elemId="26971572" name="meta_activation_date"/>
							<columnMeta elemId="26971573" name="meta_creation_date"/>
							<columnMeta elemId="26971574" name="meta_deactivation_date"/>
							<columnMeta elemId="26971575" name="meta_deletion_date"/>
							<columnMeta elemId="26971576" name="meta_last_update_date"/>
							<columnMeta elemId="26971577" name="meta_last_source_update_date"/>
							<columnMeta elemId="26971578" name="meta_activation_tid"/>
							<columnMeta elemId="26971579" name="meta_creation_tid"/>
							<columnMeta elemId="26971580" name="meta_deactivation_tid"/>
							<columnMeta elemId="26971581" name="meta_deletion_tid"/>
							<columnMeta elemId="26971582" name="meta_last_update_tid"/>
							<columnMeta elemId="26971583" name="meta_last_source_update_tid"/>
						</columnsMeta>
						<columns>
							<column elemId="26971586" new="true" old="true" name="cmo_type"/>
							<column elemId="26975584" new="true" old="true" name="cmo_value"/>
							<column elemId="27836849" new="true" old="true" name="exp_type"/>
							<column elemId="27836850" new="true" old="true" name="exp_value"/>
						</columns>
					</eventPlanPublisher>
					<traversingPlanPublisher elemId="27910728" enable="true" name="party_master" allColumns="false" rootEntity="party (masters)">
						<description>Traversing publisher example - one event in a defined tree will retrieve the whole data tree</description>
						<columnsMeta>
							<columnMeta elemId="27912849" name="meta_id"/>
							<columnMeta elemId="27912850" name="meta_master_view"/>
							<columnMeta elemId="27912851" name="meta_activation_date"/>
							<columnMeta elemId="27912852" name="meta_deactivation_date"/>
						</columnsMeta>
						<columnsTraversing>
							<columnTraversing elemId="27912854" old="true" record="true" name="cmo_type"/>
							<columnTraversing elemId="27912855" old="true" record="true" name="cmo_first_name"/>
							<columnTraversing elemId="27912856" old="true" record="true" name="cmo_last_name"/>
							<columnTraversing elemId="27912857" old="true" record="true" name="cmo_gender"/>
							<columnTraversing elemId="27912858" old="true" record="true" name="exp_gender"/>
							<columnTraversing elemId="27912859" old="true" record="true" name="cmo_birth_date"/>
							<columnTraversing elemId="27912860" old="true" record="true" name="sco_birth_date"/>
							<columnTraversing elemId="27912861" old="true" record="true" name="exp_birth_date"/>
							<columnTraversing elemId="27912862" old="true" record="true" name="cmo_sin"/>
							<columnTraversing elemId="27912863" old="true" record="true" name="sco_sin"/>
							<columnTraversing elemId="27912864" old="true" record="true" name="exp_sin"/>
							<columnTraversing elemId="27912865" old="true" record="true" name="group_size"/>
							<columnTraversing elemId="27912866" old="true" record="true" name="published_by"/>
							<columnTraversing elemId="27912867" old="true" record="true" name="group_matching_quality"/>
							<columnTraversing elemId="27912868" old="true" record="true" name="exp_full_name"/>
							<columnTraversing elemId="27912869" old="true" record="true" name="sco_full_name"/>
						</columnsTraversing>
						<childEntitiesTraversing>
							<childEntityTraversing elemId="27912048" name="address (masters)" allColumns="false">
								<columnsMeta>
									<columnMeta elemId="27912049" name="meta_id"/>
									<columnMeta elemId="27912050" name="meta_event_type"/>
									<columnMeta elemId="27912051" name="meta_entity"/>
									<columnMeta elemId="27912052" name="meta_layer"/>
									<columnMeta elemId="27912053" name="meta_master_view"/>
									<columnMeta elemId="27912054" name="meta_origin"/>
									<columnMeta elemId="27912055" name="meta_source_system"/>
									<columnMeta elemId="27912056" name="meta_activation_date"/>
									<columnMeta elemId="27912057" name="meta_last_update_date"/>
								</columnsMeta>
								<columnsTraversing>
									<columnTraversing elemId="27914248" old="true" record="true" name="cmo_type"/>
								</columnsTraversing>
								<childEntitiesTraversing/>
							</childEntityTraversing>
							<childEntityTraversing elemId="27912060" name="contact (masters)" allColumns="false">
								<columnsMeta>
									<columnMeta elemId="27912870" name="meta_id"/>
									<columnMeta elemId="27912871" name="meta_event_type"/>
									<columnMeta elemId="27912872" name="meta_source_system"/>
									<columnMeta elemId="27912873" name="meta_deactivation_date"/>
								</columnsMeta>
								<columnsTraversing>
									<columnTraversing elemId="27912875" old="true" record="true" name="party_id"/>
									<columnTraversing elemId="27912876" old="true" record="true" name="cmo_type"/>
									<columnTraversing elemId="27912877" old="true" record="true" name="sco_type"/>
									<columnTraversing elemId="27912878" old="true" record="true" name="exp_type"/>
									<columnTraversing elemId="27912879" old="true" record="true" name="cmo_value"/>
									<columnTraversing elemId="27912880" old="true" record="true" name="sco_value"/>
									<columnTraversing elemId="27912881" old="true" record="true" name="exp_value"/>
									<columnTraversing elemId="27912882" old="true" record="true" name="group_size"/>
									<columnTraversing elemId="27912883" old="true" record="true" name="published_by"/>
								</columnsTraversing>
								<childEntitiesTraversing/>
							</childEntityTraversing>
							<childEntityTraversing elemId="27912061" name="id_document (masters)" allColumns="true">
								<columnsMeta/>
								<columnsTraversing/>
								<childEntitiesTraversing/>
							</childEntityTraversing>
							<childEntityTraversing elemId="27912062" name="consent (masters)" allColumns="true">
								<columnsMeta/>
								<columnsTraversing/>
								<childEntitiesTraversing/>
							</childEntityTraversing>
							<childEntityTraversing elemId="27912063" name="party (instance)" allColumns="true">
								<columnsMeta/>
								<columnsTraversing/>
								<childEntitiesTraversing/>
							</childEntityTraversing>
						</childEntitiesTraversing>
					</traversingPlanPublisher>
					<eventPlanPublisher elemId="27907469" enable="true" layerName="match_proposal" suffix="_issue">
						<description>Matching proposal issues</description>
						<planAdvancedSettings>
							<planFilteringPublisher filter="false">
								<expression></expression>
							</planFilteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</planAdvancedSettings>
						<entityName allColumns="true" entity="party (k)"/>
						<columnsMeta/>
						<columns/>
					</eventPlanPublisher>
				</publishers>
				<filter>
					<expression></expression>
					<entities>
						<entity elemId="23699621" name="party (instance)">
							<expression></expression>
						</entity>
						<entity elemId="1906" name="party (masters)">
							<expression></expression>
						</entity>
						<entity elemId="26957728" name="contact (masters)">
							<expression></expression>
						</entity>
						<entity elemId="26957729" name="contact (instance)">
							<expression></expression>
						</entity>
						<entity elemId="84398253" name="address (masters)">
							<expression></expression>
						</entity>
						<entity elemId="84398254" name="id_document (masters)">
							<expression></expression>
						</entity>
						<entity elemId="84398255" name="consent (masters)">
							<expression></expression>
						</entity>
						<entity elemId="27910128" name="party_proposal_k (match_proposal)">
							<expression></expression>
						</entity>
					</entities>
				</filter>
				<groupingEventProcessorSetting>
					<masterEntities/>
					<instanceEntities/>
				</groupingEventProcessorSetting>
			</handler>
		</handlers>
	</consolidationEventHandler>
	<sorEventHandler>
		<handlers>
			<handler elemId="28110689" enable="true" name="sor" class="EventHandlerAsync" persistenceDir="../storage/eventHandlerSoR" processor="BatchingEventProcessor" desc="">
				<publishers>
					<stdOutPublisher elemId="28110690" enable="true">
						<description></description>
						<transformers>
							<simpleXmlTransformer elemId="28110691" indent="true" name="" includeOldValues="true"/>
						</transformers>
						<advanced>
							<filteringPublisher filter="false">
								<expression></expression>
								<filteredEntities/>
							</filteringPublisher>
							<retryingPublisher retryDelay="20" maxRetries="30" consecutiveSuccess="10" globalRetries="5" retry="false"/>
						</advanced>
					</stdOutPublisher>
					<flowPublisher elemId="28196380" enable="true">
						<description></description>
						<configuration>&lt;publisher class=&quot;com.ataccama.nme.engine.model.flow.FlowPublisher&quot;&gt;
	&lt;mapping class=&quot;com.ataccama.nme.engine.model.flow.MappingTransformer&quot;&gt;
		&lt;entities&gt;
			&lt;entity from=&quot;party&quot; to=&quot;party&quot;&gt;
				&lt;columns&gt;
					&lt;!-- &lt;column fromMeta=&quot;id&quot; to=&quot;source_id&quot; /&gt;
					&lt;column from=&quot;type&quot; to=&quot;change_type&quot; /&gt;
					&lt;column from=&quot;???&quot; to=&quot;source_timestamp&quot; /&gt; --&gt;
					&lt;column value=&quot;sor#party#party&quot; to=&quot;origin&quot; /&gt;
					&lt;column from=&quot;type&quot; to=&quot;src_type&quot; /&gt;
					&lt;column from=&quot;first_name&quot; to=&quot;src_first_name&quot; /&gt;
					&lt;column from=&quot;last_name&quot; to=&quot;src_last_name&quot; /&gt;
					&lt;column from=&quot;gender&quot; to=&quot;src_gender&quot; /&gt;
					&lt;column from=&quot;toString(birth_date,&#39;yyyy-MM-dd&#39;)&quot; to=&quot;src_birth_date&quot; /&gt;
					&lt;column from=&quot;toString(sin)&quot; to=&quot;src_sin&quot; /&gt;
					&lt;column from=&quot;company_name&quot; to=&quot;src_company_name&quot; /&gt;
					&lt;column from=&quot;business_number&quot; to=&quot;src_business_number&quot; /&gt;					
					&lt;column from=&quot;toString(established_date,&#39;yyyy-MM-dd&#39;)&quot; to=&quot;src_established_date&quot; /&gt;
					&lt;column from=&quot;legal_form&quot; to=&quot;src_legal_form&quot; /&gt;
				&lt;/columns&gt;
			&lt;/entity&gt;
			&lt;entity from=&quot;rel_party2party&quot; to=&quot;rel_party2party&quot;&gt;
				&lt;columns&gt;
					&lt;!-- &lt;column fromMeta=&quot;id&quot; to=&quot;source_id&quot; /&gt;
					&lt;column from=&quot;type&quot; to=&quot;change_type&quot; /&gt;
					&lt;column from=&quot;???&quot; to=&quot;source_timestamp&quot; /&gt; --&gt;
					&lt;column value=&quot;sor#rel_party2party#rel_party2party&quot; to=&quot;origin&quot; /&gt;
					&lt;column from=&quot;toString(parent_id)&quot; to=&quot;parent_source_id&quot; /&gt;
					&lt;column from=&quot;toString(child_id)&quot; to=&quot;child_source_id&quot; /&gt;
					&lt;column from=&quot;rel_type&quot; to=&quot;src_rel_type&quot; /&gt;
					&lt;column from=&quot;rel_category&quot; to=&quot;src_rel_category&quot; /&gt;
					&lt;column from=&quot;toString(p2p_valid_from,&#39;yyyy-MM-dd&#39;)&quot; to=&quot;src_p2p_valid_from&quot; /&gt;
					&lt;column from=&quot;toString(p2p_valid_to,&#39;yyyy-MM-dd&#39;)&quot; to=&quot;src_p2p_valid_to&quot; /&gt;
				&lt;/columns&gt;
			&lt;/entity&gt;
			&lt;entity from=&quot;address&quot; to=&quot;address&quot;&gt;
				&lt;columns&gt;
					&lt;!-- &lt;column fromMeta=&quot;id&quot; to=&quot;source_id&quot; /&gt; 
					&lt;column from=&quot;type&quot; to=&quot;change_type&quot; /&gt;
					&lt;column from=&quot;???&quot; to=&quot;source_timestamp&quot; /&gt; --&gt;
					&lt;column value=&quot;sor#address#address&quot; to=&quot;origin&quot; /&gt;
					&lt;column from=&quot;toString(party_id)&quot; to=&quot;party_source_id&quot; /&gt;
					&lt;column from=&quot;toString(type)&quot; to=&quot;src_type&quot; /&gt;
					&lt;column from=&quot;street&quot; to=&quot;src_street&quot; /&gt;
					&lt;column from=&quot;city&quot; to=&quot;src_city&quot; /&gt;
					&lt;column from=&quot;state&quot; to=&quot;src_state&quot; /&gt;
					&lt;column from=&quot;zip&quot; to=&quot;src_zip&quot; /&gt;
				&lt;/columns&gt;
			&lt;/entity&gt; 
			&lt;entity from=&quot;consent&quot; to=&quot;consent&quot;&gt;
				&lt;columns&gt;
					&lt;!-- &lt;column fromMeta=&quot;id&quot; to=&quot;source_id&quot; /&gt; 
					&lt;column from=&quot;type&quot; to=&quot;change_type&quot; /&gt;
					&lt;column from=&quot;???&quot; to=&quot;source_timestamp&quot; /&gt; --&gt;
					&lt;column value=&quot;sor#consent#consent&quot; to=&quot;origin&quot; /&gt;
					&lt;column from=&quot;toString(party_id)&quot; to=&quot;party_source_id&quot; /&gt;
					&lt;column from=&quot;category&quot; to=&quot;src_category&quot; /&gt;
					&lt;column from=&quot;purpose&quot; to=&quot;src_purpose&quot; /&gt;
					&lt;column from=&quot;system&quot; to=&quot;src_system&quot; /&gt;
					&lt;column from=&quot;consumer&quot; to=&quot;src_consumer&quot; /&gt;
					&lt;column from=&quot;channel&quot; to=&quot;src_channel&quot; /&gt;
					&lt;column from=&quot;status&quot; to=&quot;src_status&quot; /&gt;
					&lt;column from=&quot;toString(origin_date,&#39;yyyy-MM-dd&#39;)&quot; to=&quot;src_origin_date&quot; /&gt;
					&lt;column from=&quot;toString(expiration_date,&#39;yyyy-MM-dd&#39;)&quot; to=&quot;src_expiration_date&quot; /&gt;
				&lt;/columns&gt;
			&lt;/entity&gt; 
		&lt;/entities&gt;
	&lt;/mapping&gt;
&lt;/publisher&gt;</configuration>
					</flowPublisher>
				</publishers>
				<filter>
					<expression></expression>
					<entities/>
				</filter>
				<groupingEventProcessorSetting>
					<masterEntities/>
					<instanceEntities/>
				</groupingEventProcessorSetting>
			</handler>
		</handlers>
	</sorEventHandler>
</eventHandler>