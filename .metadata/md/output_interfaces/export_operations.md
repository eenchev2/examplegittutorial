<?xml version='1.0' encoding='UTF-8'?>
<exportModel>
	<consolidationExportOperations>
		<fullInstanceExport elemId="199915" allEntities="false" sourceSystem="crm" scope="use global scope (from Preferences)" name="full_instance_crm_export">
			<description></description>
			<selectedTables>
				<instanceTable elemId="199916" name="party" allColumns="true">
					<columns/>
				</instanceTable>
				<instanceTable elemId="199917" name="address" allColumns="true">
					<columns/>
				</instanceTable>
				<instanceTable elemId="199918" name="contact" allColumns="false">
					<columns>
						<column elemId="9938" name="src_type"/>
						<column elemId="9939" name="sco_type"/>
						<column elemId="9940" name="cio_value"/>
					</columns>
				</instanceTable>
			</selectedTables>
			<advanced enableTID="false">
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</fullInstanceExport>
		<complexExport elemId="282942" scope="use global scope (from Preferences)" name="complex_export">
			<description></description>
			<dataSources>
				<instanceDataSource elemId="282943" mode="delta" allEntities="false" prefix="instD" sourceSystem="crm" scope="use scope of parent export operation">
					<selectedTables>
						<instanceTable elemId="282944" name="party" allColumns="true">
							<columns/>
						</instanceTable>
						<instanceTable elemId="282945" name="address" allColumns="false">
							<columns>
								<column elemId="161204" name="id"/>
								<column elemId="161205" name="eng_source_timestamp"/>
								<column elemId="161206" name="eng_active"/>
								<column elemId="161207" name="eng_origin"/>
								<column elemId="161208" name="eng_source_system"/>
								<column elemId="161209" name="eng_last_update_date"/>
								<column elemId="161210" name="eng_last_source_update_date"/>
								<column elemId="161211" name="eng_creation_date"/>
								<column elemId="161212" name="eng_deletion_date"/>
								<column elemId="161213" name="eng_activation_date"/>
								<column elemId="161214" name="eng_deactivation_date"/>
								<column elemId="161215" name="eng_change_type"/>
								<column elemId="161216" name="eng_activity_change_type"/>
								<column elemId="161217" name="master_id"/>
								<column elemId="161218" name="source_id"/>
								<column elemId="161219" name="party_source_id"/>
								<column elemId="161220" name="src_type"/>
								<column elemId="161221" name="std_type"/>
								<column elemId="161222" name="exp_type"/>
								<column elemId="161223" name="sco_type"/>
								<column elemId="161224" name="dic_type"/>
								<column elemId="161225" name="src_street"/>
								<column elemId="161226" name="cio_street"/>
								<column elemId="161227" name="src_city"/>
								<column elemId="161228" name="cio_city"/>
								<column elemId="161229" name="src_state"/>
								<column elemId="161230" name="cio_state"/>
								<column elemId="161231" name="src_zip"/>
								<column elemId="161232" name="cio_zip"/>
								<column elemId="161233" name="exp_address"/>
								<column elemId="161234" name="sco_address"/>
								<column elemId="161236" name="cio_address_comp"/>
								<column elemId="26972675" name="meta_label"/>
								<column elemId="161238" name="meta_address_status"/>
								<column elemId="161239" name="meta_address_validity_level"/>
								<column elemId="161241" name="match_rule_name"/>
								<column elemId="161243" name="party_master_id"/>
							</columns>
						</instanceTable>
						<instanceTable elemId="282946" name="contact" allColumns="true">
							<columns/>
						</instanceTable>
					</selectedTables>
				</instanceDataSource>
				<masterDataSource elemId="282947" mode="full" allEntities="false" prefix="masF" scope="use scope of parent export operation" layerName="masters">
					<selectedTables>
						<table elemId="283052" name="party" allColumns="true">
							<columns/>
						</table>
						<table elemId="283053" name="address" allColumns="true">
							<columns/>
						</table>
						<table elemId="283054" name="contact" allColumns="false">
							<columns>
								<column elemId="160410" name="id"/>
								<column elemId="160411" name="eng_active"/>
								<column elemId="160412" name="eng_last_update_date"/>
								<column elemId="160413" name="eng_creation_date"/>
								<column elemId="160414" name="eng_deletion_date"/>
								<column elemId="160415" name="eng_activation_date"/>
								<column elemId="160416" name="eng_deactivation_date"/>
								<column elemId="160417" name="party_id"/>
								<column elemId="160418" name="cmo_type"/>
								<column elemId="160419" name="cmo_value"/>
							</columns>
						</table>
						<table elemId="283055" name="rel_party_party" allColumns="true">
							<columns/>
						</table>
					</selectedTables>
				</masterDataSource>
				<masterDataSource elemId="160804" mode="delta" allEntities="false" prefix="masD" scope="active" layerName="masters">
					<selectedTables>
						<table elemId="160805" name="party" allColumns="true">
							<columns/>
						</table>
						<table elemId="160806" name="address" allColumns="false">
							<columns>
								<column elemId="160807" name="id"/>
								<column elemId="160808" name="eng_active"/>
								<column elemId="160809" name="eng_last_update_date"/>
								<column elemId="160810" name="eng_creation_date"/>
								<column elemId="160811" name="eng_deletion_date"/>
								<column elemId="160812" name="eng_activation_date"/>
								<column elemId="160813" name="eng_deactivation_date"/>
								<column elemId="160814" name="eng_change_type"/>
								<column elemId="160815" name="eng_activity_change_type"/>
								<column elemId="160816" name="party_id"/>
								<column elemId="160817" name="cmo_type"/>
								<column elemId="160818" name="cmo_street"/>
								<column elemId="160819" name="cmo_city"/>
								<column elemId="160820" name="cmo_state"/>
								<column elemId="160821" name="cmo_zip"/>
							</columns>
						</table>
					</selectedTables>
				</masterDataSource>
				<instanceDataSource elemId="161624" mode="full" allEntities="false" prefix="instF" sourceSystem="" scope="use scope of parent export operation">
					<selectedTables>
						<instanceTable elemId="161625" name="party" allColumns="true">
							<columns/>
						</instanceTable>
						<instanceTable elemId="161626" name="address" allColumns="false">
							<columns>
								<column elemId="161627" name="id"/>
								<column elemId="161628" name="eng_source_timestamp"/>
								<column elemId="161629" name="eng_active"/>
								<column elemId="161630" name="eng_origin"/>
								<column elemId="161631" name="eng_source_system"/>
								<column elemId="161632" name="eng_last_update_date"/>
								<column elemId="161633" name="eng_last_source_update_date"/>
								<column elemId="161634" name="eng_creation_date"/>
								<column elemId="161635" name="eng_deletion_date"/>
								<column elemId="161636" name="eng_activation_date"/>
								<column elemId="161637" name="eng_deactivation_date"/>
								<column elemId="161638" name="master_id"/>
								<column elemId="161639" name="source_id"/>
								<column elemId="161640" name="party_source_id"/>
								<column elemId="161641" name="src_type"/>
								<column elemId="161642" name="std_type"/>
								<column elemId="161643" name="exp_type"/>
								<column elemId="161644" name="sco_type"/>
								<column elemId="161645" name="dic_type"/>
								<column elemId="161646" name="src_street"/>
								<column elemId="161647" name="cio_street"/>
								<column elemId="161648" name="src_city"/>
								<column elemId="161649" name="cio_city"/>
								<column elemId="161650" name="src_state"/>
								<column elemId="161651" name="cio_state"/>
								<column elemId="161652" name="src_zip"/>
								<column elemId="161653" name="cio_zip"/>
								<column elemId="161654" name="exp_address"/>
								<column elemId="161655" name="sco_address"/>
								<column elemId="161657" name="cio_address_comp"/>
								<column elemId="26973172" name="meta_label"/>
								<column elemId="26973173" name="meta_address_status"/>
								<column elemId="26973174" name="meta_address_validity_level"/>
								<column elemId="161662" name="match_rule_name"/>
								<column elemId="161664" name="party_master_id"/>
							</columns>
						</instanceTable>
					</selectedTables>
				</instanceDataSource>
				<conditionalInstanceDataSource elemId="23663802" mode="full" entityName="party" prefix="cond_full" sourceSystem="" scope="use scope of parent export operation" allColumns="true">
					<conditions>
						<condition elemId="23670677" column="cio_type" value="P" operator="=">
							<description></description>
						</condition>
					</conditions>
					<columns/>
				</conditionalInstanceDataSource>
				<conditionalMasterDataSource elemId="23666464" mode="delta" entityName="party" prefix="cond_delta_mas" scope="use scope of parent export operation" allColumns="true" layerName="masters">
					<conditions>
						<condition elemId="23671060" column="cmo_type" value="P" operator="=">
							<description></description>
						</condition>
					</conditions>
					<columns/>
				</conditionalMasterDataSource>
			</dataSources>
			<advanced enableTID="false">
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</complexExport>
		<fullMasterExport elemId="367374" allEntities="false" scope="use global scope (from Preferences)" name="full_master_export" layerName="masters">
			<description></description>
			<selectedTables>
				<table elemId="367378" name="party" allColumns="true">
					<columns/>
				</table>
				<table elemId="367379" name="address" allColumns="true">
					<columns/>
				</table>
				<table elemId="367380" name="contact" allColumns="false">
					<columns>
						<column elemId="10715" name="id"/>
						<column elemId="10716" name="party_id"/>
					</columns>
				</table>
				<table elemId="27848147" name="consent" allColumns="false">
					<columns>
						<column elemId="27848148" name="id"/>
						<column elemId="27848149" name="party_id"/>
						<column elemId="27848150" name="cmo_category"/>
						<column elemId="27848151" name="cmo_purpose"/>
						<column elemId="27848152" name="cmo_system"/>
						<column elemId="27848153" name="cmo_consumer"/>
						<column elemId="27848154" name="cmo_channel"/>
						<column elemId="27848155" name="cmo_status"/>
						<column elemId="27848156" name="cmo_origin_date"/>
						<column elemId="27848157" name="cmo_expiration_date"/>
					</columns>
				</table>
			</selectedTables>
			<advanced enableTID="false">
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</fullMasterExport>
		<fullInstanceExport elemId="35808" allEntities="false" sourceSystem="" scope="use global scope (from Preferences)" name="full_instance_export">
			<description></description>
			<selectedTables>
				<instanceTable elemId="35809" name="party" allColumns="true">
					<columns/>
				</instanceTable>
				<instanceTable elemId="35810" name="address" allColumns="true">
					<columns/>
				</instanceTable>
				<instanceTable elemId="35811" name="contact" allColumns="true">
					<columns/>
				</instanceTable>
				<instanceTable elemId="35812" name="rel_party2party" allColumns="true">
					<columns/>
				</instanceTable>
				<instanceTable elemId="35813" name="rel_party2contract" allColumns="true">
					<columns/>
				</instanceTable>
				<instanceTable elemId="35814" name="contract" allColumns="true">
					<columns/>
				</instanceTable>
				<instanceTable elemId="35815" name="id_document" allColumns="true">
					<columns/>
				</instanceTable>
				<instanceTable elemId="35816" name="product" allColumns="true">
					<columns/>
				</instanceTable>
				<instanceTable elemId="27513923" name="consent" allColumns="true">
					<columns/>
				</instanceTable>
			</selectedTables>
			<advanced enableTID="false">
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</fullInstanceExport>
		<complexExport elemId="34744" scope="use global scope (from Preferences)" name="history_export">
			<description></description>
			<dataSources>
				<historyInstanceEntityDataSource elemId="34745" prefix="instHistP" sourceSystem="" allColumns="true" entity="party">
					<historyInstanceColumns/>
				</historyInstanceEntityDataSource>
				<historyInstanceEntityDataSource elemId="40617" prefix="instHist" sourceSystem="life" allColumns="true" entity="contract">
					<historyInstanceColumns/>
				</historyInstanceEntityDataSource>
				<historyMasterEntityDataSource elemId="156182" prefix="masHist" allColumns="true" entity="party (masters)">
					<historyMasterColumns/>
				</historyMasterEntityDataSource>
				<historyMasterEntityDataSource elemId="35558" prefix="masNormHist" allColumns="false" entity="address (norm)">
					<historyMasterColumns>
						<historyMasterColumn elemId="157326" name="eng_valid_from"/>
						<historyMasterColumn elemId="157327" name="eng_valid_to"/>
						<historyMasterColumn elemId="157328" name="id"/>
						<historyMasterColumn elemId="157329" name="eng_active"/>
						<historyMasterColumn elemId="157332" name="cmo_type"/>
						<historyMasterColumn elemId="157711" name="cmo_street"/>
					</historyMasterColumns>
				</historyMasterEntityDataSource>
				<historyMasterEntityDataSource elemId="42594" prefix="mastHist" allColumns="true" entity="contact (masters)">
					<historyMasterColumns/>
				</historyMasterEntityDataSource>
			</dataSources>
			<advanced enableTID="false">
				<pathVariables/>
				<additionalParameters/>
			</advanced>
		</complexExport>
	</consolidationExportOperations>
	<sorExportOperations>
		<fullSorExport elemId="27834575" allEntities="true" scope="use global scope (from Preferences)" name="full_sor">
			<description></description>
			<advanced enableTID="false">
				<pathVariables/>
				<additionalParameters/>
			</advanced>
			<selectedSoRTables/>
		</fullSorExport>
	</sorExportOperations>
</exportModel>