<?xml version='1.0' encoding='UTF-8'?>
<wfConfig>
	<workflowLabels waiting_for_publish="Waiting for Approval" enableCons="true" draft="In Progress" sor="SoR WF" return_draft="Back to In Progress" enableSor="true" consolidation="Consolidation WF" move_publish="Submit for Approval"/>
	<workflows>
		<workflow elemId="28142487" enable="false" name="def" label="Simple confirmation">
			<steps>
				<step elemId="28142488" transitionLabel="Move to publish" name="draft" transitionTarget="waiting_for_publish" transitionName="move"/>
				<step elemId="28142489" transitionLabel="Return" name="waiting_for_publish" transitionTarget="draft" transitionName="return"/>
			</steps>
		</workflow>
		<workflow elemId="28142490" enable="false" name="long" label="Reviewed by supervisor">
			<steps>
				<step elemId="28142491" transitionLabel="Move to supervisor" name="draft" transitionTarget="waiting_for_supervisor" transitionName="move_supervisor"/>
				<step elemId="28142492" transitionLabel="Move to publish" name="waiting_for_supervisor" transitionTarget="waiting_for_publish" transitionName="move_publish"/>
				<step elemId="28142493" transitionLabel="Return" name="waiting_for_supervisor" transitionTarget="draft" transitionName="move_back"/>
				<step elemId="28142494" transitionLabel="Return" name="waiting_for_publish" transitionTarget="draft" transitionName="return"/>
			</steps>
		</workflow>
	</workflows>
	<statuses>
		<status elemId="28142495" enable="false" name="draft" label="Draft" first="true"/>
		<status elemId="28142496" enable="false" name="waiting_for_publish" label="Waiting for publish" first="false"/>
		<status elemId="28142497" enable="false" name="waiting_for_supervisor" label="Waiting for supervisor" first="false"/>
	</statuses>
</wfConfig>