<?xml version='1.0' encoding='UTF-8'?>
<guiPreferences>
	<sampleDefinitions sampleStrategy="Statistic Strategy" useSampleThreshold="50" enable="true" sampleSize="20">
		<sampleLayerSettings>
			<sampleLayerSetting elemId="27885201" sampleStrategy="Basic Strategy" useSampleThreshold="" sampleSize="" layer="dataset"/>
			<sampleLayerSetting elemId="27885202" sampleStrategy="No Strategy" useSampleThreshold="" sampleSize="" layer="sor"/>
		</sampleLayerSettings>
	</sampleDefinitions>
	<recordDetailVisualization previewLimit="5"/>
	<dataTypeFormats datetime="yyyy-MM-dd HH:mm:ss" boolean="" integer="#,###" float="#.###" day="yyyy-MM-dd" long="#,###"/>
</guiPreferences>