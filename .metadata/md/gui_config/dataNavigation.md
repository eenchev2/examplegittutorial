<?xml version='1.0' encoding='UTF-8'?>
<dataNavigation enable="true" name="Business View">
	<dataNavigationItems>
		<dataNavigationItem elemId="27866616" expanded="true" icon="MASTER" label="Personal / Company Data">
			<dataNavigationChildren>
				<dataNavigationChild elemId="27866617" expanded="false" icon="MASTER" name="party (masters)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866618" expanded="false" icon="MASTER" name="address (masters)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866619" expanded="false" icon="MASTER" name="contact (masters)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866620" expanded="false" icon="MASTER" name="id_document (masters)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27901796" expanded="false" icon="DATASET" name="party_address_contact_mas (dataset)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27901797" expanded="false" icon="DATASET" name="party_mat_rules (dataset)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
			</dataNavigationChildren>
		</dataNavigationItem>
		<dataNavigationItem elemId="27866624" expanded="false" icon="DATASET" label="Product Related Data">
			<dataNavigationChildren>
				<dataNavigationChild elemId="27866625" expanded="false" icon="MASTER" name="product (masters)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866626" expanded="false" icon="INSTANCE" name="contract (instance)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866627" expanded="false" icon="DATASET" name="party_product_mas (dataset)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
			</dataNavigationChildren>
		</dataNavigationItem>
		<dataNavigationItem elemId="27866628" expanded="false" icon="INSTANCE" label="Consent Management">
			<dataNavigationChildren>
				<dataNavigationChild elemId="27866629" expanded="false" icon="MASTER" name="consent (masters)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866632" expanded="false" icon="INSTANCE" name="consent (instance)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866635" expanded="false" icon="SOR" name="consent (sor)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866638" expanded="false" icon="DATASET" name="party_consent_access (dataset)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866639" expanded="false" icon="DATASET" name="party_consent_storing (dataset)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
			</dataNavigationChildren>
		</dataNavigationItem>
		<dataNavigationItem elemId="27866640" expanded="false" icon="SOR" label="Data Authoring">
			<dataNavigationChildren>
				<dataNavigationChild elemId="27866641" expanded="false" icon="SOR" name="party (sor)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866642" expanded="false" icon="SOR" name="address (sor)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866643" expanded="false" icon="DATASET" name="party_address_sor (dataset)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27866644" expanded="false" icon="FOLDER" name="" label="Reference Data">
					<dataNavigationChildren/>
				</dataNavigationChild>
			</dataNavigationChildren>
		</dataNavigationItem>
		<dataNavigationItem elemId="27866645" expanded="false" icon="REFERENCE" label="Reference Data">
			<dataNavigationChildren/>
		</dataNavigationItem>
		<dataNavigationItem elemId="27998617" expanded="false" icon="MASTER" label="Top Domains">
			<dataNavigationChildren>
				<dataNavigationChild elemId="27998618" expanded="false" icon="MASTER" name="party (masters)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27998619" expanded="false" icon="MASTER" name="product (masters)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27998620" expanded="false" icon="INSTANCE" name="contract (instance)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
			</dataNavigationChildren>
		</dataNavigationItem>
		<dataNavigationItem elemId="27999235" expanded="false" icon="DATASET" label="Additional Views">
			<dataNavigationChildren>
				<dataNavigationChild elemId="27999236" expanded="false" icon="DATASET" name="party_address_contact_mas (dataset)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27901798" expanded="false" icon="DATASET" name="party_contact_inst (dataset)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27910128" expanded="false" icon="DATASET" name="party_address_sor (dataset)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
				<dataNavigationChild elemId="27910129" expanded="false" icon="DATASET" name="party_product_mas (dataset)" label="">
					<dataNavigationChildren/>
				</dataNavigationChild>
			</dataNavigationChildren>
		</dataNavigationItem>
		<dataNavigationItem elemId="28012611" expanded="false" icon="FOLDER" label="Party / Address / Contact">
			<dataNavigationChildren>
				<dataNavigationChild elemId="28012612" expanded="false" icon="FOLDER" name="" label="Party">
					<dataNavigationChildren>
						<dataNavigationChild elemId="28012613" expanded="false" icon="MASTER" name="party (masters)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
						<dataNavigationChild elemId="28012614" expanded="false" icon="MASTER" name="person (norm)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
						<dataNavigationChild elemId="28013846" expanded="false" icon="INSTANCE" name="party (instance)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
						<dataNavigationChild elemId="28013847" expanded="false" icon="SOR" name="party (sor)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
					</dataNavigationChildren>
				</dataNavigationChild>
				<dataNavigationChild elemId="28012615" expanded="false" icon="FOLDER" name="" label="Address">
					<dataNavigationChildren>
						<dataNavigationChild elemId="28014221" expanded="false" icon="MASTER" name="address (masters)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
						<dataNavigationChild elemId="28014222" expanded="false" icon="MASTER" name="address (norm)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
						<dataNavigationChild elemId="28014223" expanded="false" icon="INSTANCE" name="address (instance)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
						<dataNavigationChild elemId="28014224" expanded="false" icon="SOR" name="address (sor)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
					</dataNavigationChildren>
				</dataNavigationChild>
				<dataNavigationChild elemId="28012616" expanded="false" icon="FOLDER" name="" label="Contact">
					<dataNavigationChildren>
						<dataNavigationChild elemId="28014225" expanded="false" icon="MASTER" name="contact (masters)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
						<dataNavigationChild elemId="28014226" expanded="false" icon="MASTER" name="contact (norm)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
						<dataNavigationChild elemId="28014227" expanded="false" icon="INSTANCE" name="contact (instance)" label="">
							<dataNavigationChildren/>
						</dataNavigationChild>
					</dataNavigationChildren>
				</dataNavigationChild>
			</dataNavigationChildren>
		</dataNavigationItem>
		<dataNavigationItem elemId="27858974" expanded="false" icon="FOLDER" label="">
			<dataNavigationChildren/>
		</dataNavigationItem>
	</dataNavigationItems>
</dataNavigation>