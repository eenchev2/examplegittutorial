<?xml version='1.0' encoding='UTF-8'?>
<searchTab>
	<filters>
		<filter elemId="27996475" enable="true" name="party_mas" description="Search party by Name and SIN" label="Party from Full master view" entity="party (masters)" joinByOr="false">
			<columns>
				<column elemId="27996480" caseSensitive="false" name="cmo_first_name" operator="CONTAINS"/>
				<column elemId="27996481" caseSensitive="false" name="cmo_last_name" operator="CONTAINS"/>
				<column elemId="27996482" caseSensitive="false" name="cmo_sin" operator="="/>
			</columns>
		</filter>
		<filter elemId="27996476" enable="true" name="people2" description="Search for people by their First or Last Name" label="People from SOR" entity="party (sor)" joinByOr="true">
			<columns>
				<column elemId="27996483" caseSensitive="true" name="first_name" operator="="/>
				<column elemId="27996484" caseSensitive="false" name="last_name" operator="="/>
			</columns>
		</filter>
		<filter elemId="27996477" enable="true" name="party_address_contast_mas" description="Search for people by Last Name and City" label="People and address" entity="party_address_contact_mas (dataset)" joinByOr="false">
			<columns>
				<column elemId="27996485" caseSensitive="true" name="party_masters_cmo_first_name" operator="CONTAINS"/>
				<column elemId="27996486" caseSensitive="true" name="party_masters_cmo_last_name" operator="CONTAINS"/>
				<column elemId="27996487" caseSensitive="false" name="party_masters_cmo_sin" operator="CONTAINS"/>
				<column elemId="27996488" caseSensitive="false" name="address_masters_cmo_street" operator="CONTAINS"/>
				<column elemId="27996489" caseSensitive="false" name="address_masters_cmo_city" operator="CONTAINS"/>
				<column elemId="27996490" caseSensitive="false" name="address_masters_cmo_zip" operator="CONTAINS"/>
				<column elemId="27996491" caseSensitive="false" name="contact_masters_cmo_value" operator="CONTAINS"/>
			</columns>
		</filter>
		<filter elemId="27850090" enable="true" name="party_product_mas" description="Search party and product relations" label="Party to Product Hierarchy" entity="party_product_mas (dataset)" joinByOr="true">
			<columns>
				<column elemId="27860935" caseSensitive="false" name="cmo_last_name" operator="="/>
				<column elemId="27860936" caseSensitive="false" name="cmo_company_name" operator="="/>
				<column elemId="27860937" caseSensitive="false" name="cmo_name" operator="CONTAINS"/>
				<column elemId="27860938" caseSensitive="false" name="product_type" operator="="/>
			</columns>
		</filter>
	</filters>
</searchTab>