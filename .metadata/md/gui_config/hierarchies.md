<?xml version='1.0' encoding='UTF-8'?>
<hierarchies>
	<guiHierarchiesInstance allRels="false">
		<guiHierarchiesArray>
			<hierarchy1N elemId="27861560" enable="true" name="Addresses" label="" relationship="party_has_address">
				<startPoint label="Address" position="RIGHT" entity="party"/>
				<endPoint label="Party" position="LEFT" entity="address"/>
				<connection connectionColour="#BABABA" connectionShape="Bezier">
					<connectionLabel></connectionLabel>
				</connection>
			</hierarchy1N>
		</guiHierarchiesArray>
	</guiHierarchiesInstance>
	<guiHierarchiesMaster>
		<layers>
			<layer elemId="27860317" enable="true" allRels="false" layerName="masters">
				<guiHierarchiesArray>
					<hierarchy1N elemId="27860318" enable="true" name="Addresses" label="" relationship="party_has_address">
						<startPoint label="Address" position="RIGHT" entity="party"/>
						<endPoint label="Party" position="LEFT" entity="address"/>
						<connection connectionColour="#BABABA" connectionShape="Bezier">
							<connectionLabel></connectionLabel>
						</connection>
					</hierarchy1N>
					<hierarchy1N elemId="27865264" enable="true" name="Contacts" label="" relationship="party_has_contact">
						<startPoint label="Contact" position="LEFT" entity="party"/>
						<endPoint label="Party" position="RIGHT" entity="contact"/>
						<connection connectionColour="#BABABA" connectionShape="Bezier">
							<connectionLabel></connectionLabel>
						</connection>
					</hierarchy1N>
				</guiHierarchiesArray>
			</layer>
		</layers>
	</guiHierarchiesMaster>
	<guiHierarchiesSoR allRels="false">
		<guiHierarchiesArray/>
	</guiHierarchiesSoR>
</hierarchies>