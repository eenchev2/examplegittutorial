<?xml version='1.0' encoding='UTF-8'?>
<globalValidations>
	<globalValidationSettings>
		<attributeScoreThresholds warningErrorThreshold="10000000" infoWarningThreshold="10000"/>
		<recordScoreThresholds highDQThreshold="10000" lowDQThreshold="10000000"/>
		<validationIconsDQI high="Trusted record" low="Record with low data quality" medium="Record with data quality issues"/>
	</globalValidationSettings>
	<validationMessages>
		<validationMessage elemId="28168959" description="" message="This sample doesn&#39;t provide USA address cleansing rules. There is only Canadian address validation for Toronto and Leduc cities available." key="USA_ADDRESS"/>
		<validationMessage elemId="28177163" description="" message="Only sample Canadian address etalon is used! There are only Toronto and Leduc cities available. " key="CA_OUT_OF_RANGE"/>
		<validationMessage elemId="28177164" description="" message="Address cleansing is emulated! Only the pre-prepared addresses are cleansed/validated!  " key="CA_DISABLED"/>
		<validationMessage elemId="28177165" description="" message="Address is correct or differs only in standardization (state name, street direction etc.) " key="VALID"/>
		<validationMessage elemId="28177166" description="" message="Address quality is good, minor corrections performed (typos in names, small differences etc.) " key="CORRECTED_MINOR"/>
		<validationMessage elemId="28177167" description="" message="Address quality is poor, address components corrected (different city or several minor corrections in different address elements) " key="CORRECTED_MAJOR"/>
		<validationMessage elemId="28177168" description="" message="Address is invalid and was not parsed, or is ambiguous (unidentified to delivery point) " key="UNKNOWN"/>
		<validationMessage elemId="28177169" description="" message=" Input address elements are empty or not sufficient to be identified. " key="INSUFFICIENT_INPUT"/>
		<validationMessage elemId="28177170" description="" message="CA POST SERP VALIDITY CODE: No or minimal correction was done to the input value. Software package is able to detect all address components. The result address is valid." key="V"/>
		<validationMessage elemId="28177171" description="" message="CA POST SERP VALIDITY CODE: The result address is invalid. Software package is unable to detect all address components or make valid corrections." key="N"/>
		<validationMessage elemId="28177172" description="" message="CA POST SERP VALIDITY CODE: An invalid address is &quot;Correctable&quot; &quot;C&quot; when there are one or more components missing or inconsistent from an otherwise valid address; and only one address can be derived from the information provided. " key="C"/>
		<validationMessage elemId="28177173" description="" message="Address identified to specific delivery point (house, unit, PO BOX etc.)" key="DELIVERY_POINT"/>
		<validationMessage elemId="28177174" description="" message="Address identified up to specific building, building unit is ambiguous" key="BUILDING"/>
		<validationMessage elemId="28177175" description="" message="Address identified up to specific street, ambiguous house on street (box in rural route for RR addresses)" key="STREET"/>
		<validationMessage elemId="28177176" description="" message="Address identified to city level only (ambiguous street)" key="CITY"/>
		<validationMessage elemId="28177177" description="" message="Address identified to postal code level. Mainly for Large Volume Receivers addresses." key="POSTAL_CODE"/>
		<validationMessage elemId="28177178" description="" message="No address component (or component combination) was found in reference data. Address invalid. " key="NULL"/>
		<validationMessage elemId="28177179" description="" message="Gender is missing or was entered in an invalid format." key="GENDER_MISSING_INVALID"/>
		<validationMessage elemId="28177180" description="" message="SIN is empty" key="SIN_EMPTY"/>
		<validationMessage elemId="28177181" description="" message="SIN has invalid checksum" key="SIN_INVALID_CHECK"/>
		<validationMessage elemId="28177182" description="" message="SIN has invalid number of digits" key="SIN_INVALID_DIGIT_COUNT"/>
		<validationMessage elemId="28177183" description="" message="SIN wasn&#39;t parsed successfully" key="SIN_NOT_PARSED"/>
		<validationMessage elemId="28177184" description="" message="Date is missing." key="DATE_EMPTY_INPUT"/>
		<validationMessage elemId="28177185" description="" message="Date is in the future." key="DATE_IN_FUTURE"/>
		<validationMessage elemId="28177186" description="" message="Date is too far in the past." key="DATE_FAR_IN_PAST"/>
		<validationMessage elemId="28177187" description="" message="Date is invalid." key="DATE_INVALID"/>
		<validationMessage elemId="28177188" description="" message="Company name is missing." key="CN_EMPTY_INPUT"/>
		<validationMessage elemId="28177189" description="" message="Company name contains a blacklisted vulgar word." key="CN_VULGAR_WORD_FOUND"/>
		<validationMessage elemId="28177190" description="" message="Company legal form was not found." key="CN_LEGAL_FORM_NOT_FOUND"/>
		<validationMessage elemId="28177191" description="" message="Business number is empty." key="BN_EMPTY"/>
		<validationMessage elemId="28177192" description="" message="Business number is invalid." key="BN_INVALID"/>
		<validationMessage elemId="28177193" description="" message="Business number is valid but not in the correct format - DDDDDDDDDLLDDDD (D digits, L letters)" key="BN_CORRECTED"/>
		<validationMessage elemId="28177194" description="" message="Business number is almost valid but contains excess symbols." key="BN_BIG_CORRECTION"/>
		<validationMessage elemId="28177195" description="" message="Business number contains excess whitespace." key="BN_WHITESPACES_CORRECTED"/>
		<validationMessage elemId="28177196" description="" message="Missing or invalid party type. Must be either &#39;P&#39; (person) or &#39;C&#39; (company)." key="TYPE_MISSING_INVALID"/>
		<validationMessage elemId="28177197" description="" message="Both first name and last name are missing." key="NAME_EMPTY"/>
		<validationMessage elemId="28177198" description="" message="Last name was not verified." key="NAME_LAST_NOT_VERIFIED"/>
		<validationMessage elemId="28177199" description="" message="First name was not verified." key="NAME_FIRST_NOT_FOUND"/>
		<validationMessage elemId="27849635" description="" message="Middle name is missing" key="NAME_MIDDLE_FIRST_VERIFIED"/>
		<validationMessage elemId="27849636" description="" message="Middle name is missing" key="NAME_MIDDLE_EMPTY"/>
		<validationMessage elemId="28177200" description="" message="Phone number is missing an IDC." key="PHONE_NUMBER_MISSING_IDC"/>
		<validationMessage elemId="28177201" description="" message="Phone IDC is missing a plus sign." key="PHONE_IDC_MISSING_PLUS_SIGN"/>
		<validationMessage elemId="28177202" description="" message="E-mail contains synonyms for @ and ." key="EMAIL_AT_DOT_SYNONYMS"/>
		<validationMessage elemId="28177203" description="" message="Phone number is too short." key="PHONE_TOO_SHORT"/>
		<validationMessage elemId="28177204" description="" message="Phone number is too long." key="PHONE_TOO_LONG"/>
		<validationMessage elemId="28177205" description="" message="Phone number has an invalid pattern." key="PHONE_WRONG_PATTERN"/>
		<validationMessage elemId="27862465" description="" message="Fictive phone number" key="PHONE_FICTIVE_NUMBER"/>
		<validationMessage elemId="27862466" description="" message="Phone is missing" key="PHONE_EMPTY"/>
		<validationMessage elemId="27862230" description="" message="Phone area or country code not verified" key="PHONE_AREA_OR_CO_CODE_NOT_VERIFIED"/>
	</validationMessages>
</globalValidations>