<?xml version='1.0' encoding='UTF-8'?>
<auditing>
	<appenders>
		<appender draftAction="true" elemId="27890875" dataRead="true" issueAction="true" enable="true" count="4" limit="100000" name="rotate" pattern="../storage/mda_audit%g.log" workflowAction="true" append="true" exportAction="true"/>
	</appenders>
</auditing>