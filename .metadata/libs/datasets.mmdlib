<?xml version='1.0' encoding='UTF-8'?>
<metametadata-library>
	<nodes>
		<node icon="icons/data_sets.png" name="datasets" explorerLeaf="false" abstract="false" id="DatasetsNode" label="Data Sets">
			<editors>
				<editor openModal="true" class="com.ataccama.ame.core.editors.PropertyEditor"/>
			</editors>
			<generators>
				<gen fileNameTemplate="Files/etc/mda-datasets.gen.xml" source="." class="com.ataccama.ame.core.generators.XsltGenerator" templatePath="xslt/gen_mda_datasets_xml.xsl">
					<parameters>
						<param name="databaseModel" source="/preview/databaseModel"/>
					</parameters>
				</gen>
			</generators>
			<attributes/>
			<subNodes>
				<subNode min="1" max="1" id="DatasetsArrayNode"/>
			</subNodes>
		</node>
		<node expanded="true" name="sqlSource" id="SqlSourceNode" label="SQL Source" newTab="true">
			<attributes/>
			<subNodes>
				<subNode min="1" max="1" id="AdvancedColumnsNode"/>
				<subNode min="1" max="1" id="SqlCompNode"/>
				<subNode min="1" max="1" id="sqlOverNode"/>
			</subNodes>
		</node>
		<node expanded="true" name="datasetsArray" id="DatasetsArrayNode" label="Data Sets">
			<subNodes>
				<subNode min="0" max="-1" id="DatasetArrayNode"/>
			</subNodes>
		</node>
		<node icon="icons/data_set.png" name="datasetArray" explorerLeaf="true" id="DatasetArrayNode" label="{@name}">
			<attributes>
				<attribute defaultValue="true" name="enable" type="boolean" required="true"/>
				<attribute name="name" type="string"/>
				<attribute name="label" type="string"/>
				<attribute visible="false" defaultValue="dataset" name="layerName" type="string"/>
			</attributes>
			<subNodes>
				<subNode min="1" max="1" id="InstanceTablesNode"/>
				<subNode min="1" max="1" id="MasterTablesNode"/>
				<subNode min="1" max="1" id="SqlSourceNode"/>
				<subNode min="1" max="1" id="SoRTablesNode"/>
				<subNode min="1" max="1" id="SampleSettingNode"/>
			</subNodes>
			<validations/>
			<editors>
				<editor openModal="true" class="com.ataccama.ame.core.editors.PropertyEditor"/>
			</editors>
		</node>
		<node expanded="true" name="masterTables" explorerLeaf="false" id="MasterTablesNode" label="Master Tables">
			<attributes>
				
			</attributes>
			<subNodes>
				<subNode min="0" max="-1" id="MasterTableNode"/>
			</subNodes>
			<description>Selection of master entities to be kept in history.</description>
			<validations>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[count(masterTable[@useAsDetail=&#39;true&#39;])&gt;1]</expression>
					<message>Only one table can be set as detail.</message>
				</validation>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[$current/ancestor::datasetArray/instanceTables/instanceTable/@useAsDetail=&#39;true&#39; and masterTable/@useAsDetail=&#39;true&#39;]</expression>
					<message>Another Instance table is set as detail.</message>
				</validation>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[$current/ancestor::datasetArray/sorTables/sorTable/@useAsDetail=&#39;true&#39; and masterTable/@useAsDetail=&#39;true&#39;]</expression>
					<message>Another SoR table is set as detail.</message>
				</validation>
			</validations>
		</node>
		<node expanded="true" name="instanceTable" explorerLeaf="true" id="InstanceTableNode" label="Instance Table">
			<subNodes>
				<subNode min="1" max="1" id="InstanceColumnsNode"/>
			</subNodes>
			<attributes>
				<attribute name="entityName" label="Entity Name" type="string" required="true">
					<valuesProvider fillColumns="true" class="com.ataccama.ame.core.assist.PathValuesProvider">
						<selectPath>
							/preview/databaseModel/instanceTables/*
						
						</selectPath>
						<valueTemplate>{@name}</valueTemplate>
						<labelTemplate>{@name}</labelTemplate>
					</valuesProvider>
					<description>Name of a table from the Instance Layer of the Logical Model.</description>
				</attribute>
				<attribute defaultValue="false" name="useAsDetail" label="Use As Detail" type="boolean" required="true"/>
			</attributes>
			<description>A particular selected table.</description>
			<validations/>
		</node>
		<node expanded="false" visible="true" name="columns" explorerLeaf="false" abstract="false" id="InstanceColumnsNode" label="Columns" visibleInParent="true">
			<subNodes>
				<subNode min="0" max="-1" id="InstanceColumnNode"/>
			</subNodes>
			<description>List of columns defined for a selected table from the &lt;i&gt;Instance Layer&lt;/i&gt;.&lt;br/&gt;
The following columns are added by default:
&lt;li&gt;&lt;kbd&gt;id&lt;/kbd&gt; (long)&lt;/li&gt;
&lt;li&gt;&lt;kbd&gt;eng_active&lt;/kbd&gt; (boolean)&lt;/li&gt;
&lt;li&gt;&lt;kbd&gt;eng_origin&lt;/kbd&gt; (string)&lt;/li&gt;
&lt;li&gt;&lt;kbd&gt;eng_source_system&lt;/kbd&gt; (string)&lt;/li&gt;
&lt;li&gt;&lt;kbd&gt;eng_source_timestamp&lt;/kbd&gt; (datetime)&lt;/li&gt;
&lt;!-- 1040 --&gt;
</description>
		</node>
		<node expanded="true" name="column" explorerLeaf="false" id="InstanceColumnNode" label="Instance Column" visibleInParent="true">
			<attributes>
				<attribute name="name" label="Name" type="string" required="true">
					<description>Name of column from a selected table.</description>
					<valuesProvider fillColumns="true" class="com.ataccama.ame.core.assist.PathValuesProvider">
						<selectPath>/preview/databaseModel/instanceTables/physicalTable[@name=lower-case($current/ancestor::instanceTable/@entityName)]/columns/column[@name!=&#39;id&#39; and @load=&#39;true&#39; and @name!=&#39;eng_origin&#39;]
</selectPath>
						<valueTemplate>{@name}</valueTemplate>
						<labelTemplate>{@name}</labelTemplate>
					</valuesProvider>
				</attribute>
				<attribute name="label" label="Label Override" type="string"/>
			</attributes>
			<subNodes>
				<subNode min="1" max="1" id="ccColumnNode"/>
			</subNodes>
		</node>
		<node expanded="true" name="masterTable" explorerLeaf="true" id="MasterTableNode" label="Master Table">
			<subNodes>
				<subNode min="1" max="1" id="MasterColumnsNode"/>
				<subNode min="1" max="1" id="ccMasterTableNode"/>
			</subNodes>
			<attributes>
				<attribute name="entityName" label="Entity Name" type="string" required="true">
					<valuesProvider fillColumns="true" class="com.ataccama.ame.core.assist.PathValuesProvider">
						<selectPath>
							/preview/databaseModel/masterTables/*
						
						</selectPath>
						<valueTemplate>{@name} ({@layerName})</valueTemplate>
						<labelTemplate>{@name} ({@layerName})</labelTemplate>
					</valuesProvider>
					<description>Name of a table from a particular Master Data Layer of the Logical Model.</description>
				</attribute>
				<attribute defaultValue="false" name="useAsDetail" label="Use As Detail" type="boolean" required="true"/>
			</attributes>
			<description>A particular selected table.</description>
			<validations>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[cc/@layer!=preceding-sibling::masterTable/cc/@layer]</expression>
					<message>Same layers have to be used.</message>
				</validation>
			</validations>
		</node>
		<node name="columns" id="MasterColumnsNode" label="Columns">
			<subNodes>
				<subNode min="0" max="-1" id="MasterColumnNode"/>
			</subNodes>
			<description>List of columns defined for a selected table from the &lt;i&gt;Master Data Layer&lt;/i&gt;.&lt;br/&gt;
The following columns are added by default:
&lt;li&gt;&lt;kbd&gt;id&lt;/kbd&gt; (long)&lt;/li&gt;
&lt;li&gt;&lt;kbd&gt;eng_active&lt;/kbd&gt; (boolean)&lt;/li&gt;
&lt;!-- 1040 --&gt;</description>
		</node>
		<node expanded="true" name="column" id="MasterColumnNode" label="Master Column">
			<attributes>
				<attribute name="name" label="Name" type="string" required="true">
					<description>Name of column from a selected table.</description>
					<valuesProvider fillColumns="true" class="com.ataccama.ame.core.assist.PathValuesProvider">
						<selectPath>/preview/databaseModel/masterTables/physicalTable[(@name = $current/ancestor::masterTable/cc/@entity) and  (@layerName = $current/ancestor::masterTable/cc/@layer)]/columns/column[@name!=&#39;id&#39; and @name!=&#39;eng_origin&#39;]</selectPath>
						<valueTemplate>{@name}</valueTemplate>
						<labelTemplate>{@name}</labelTemplate>
					</valuesProvider>
				</attribute>
				<attribute name="label" label="Label Override" type="string"/>
			</attributes>
			<subNodes>
				<subNode min="1" max="1" id="ccColumnNode"/>
			</subNodes>
		</node>
		<node visible="false" generated="true" name="cc" id="ccMasterTableNode" sortChildren="false">
			<attributes>
				<attribute name="entity" type="string"/>
				<attribute name="layer" type="string"/>
			</attributes>
		</node>
		<node expanded="true" name="instanceTables" explorerLeaf="false" id="InstanceTablesNode" label="Instance Tables">
			<subNodes>
				<subNode min="0" max="-1" id="InstanceTableNode"/>
			</subNodes>
			<attributes>
			</attributes>
			<editors/>
			<description>Selection of instance entities to be kept in history.</description>
			<validations>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[count(instanceTable[@useAsDetail=&#39;true&#39;])&gt;1]</expression>
					<message>Only one table can be set as detail.</message>
				</validation>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[$current/ancestor::datasetArray/masterTables/masterTable/@useAsDetail=&#39;true&#39; and instanceTable/@useAsDetail=&#39;true&#39;]</expression>
					<message>Another Master table is set as detail.</message>
				</validation>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[$current/ancestor::datasetArray/sorTables/sorTable/@useAsDetail=&#39;true&#39; and instanceTable/@useAsDetail=&#39;true&#39;]</expression>
					<message>Another SoR table is set as detail.</message>
				</validation>
			</validations>
		</node>
		<node expanded="true" generated="true" name="cc" id="SqlCompNode" label="SQL Predefined">
			<attributes>
				<attribute name="sql" extendable="true" type="text"/>
			</attributes>
		</node>
		<node expanded="true" name="sqlOver" id="sqlOverNode" label="SQL Override">
			<attributes>
				<attribute name="sql" extendable="true" type="text"/>
			</attributes>
			<validations>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>WARNING</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[@sql=&#39;&#39; and ancestor::datasetArray/masterTables/masterTable and ancestor::datasetArray/instanceTables/instanceTable]</expression>
					<message>You have to override sql command - fill some columns.</message>
				</validation>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>WARNING</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[@sql=&#39;&#39; and ancestor::sqlSource/columns/column]</expression>
					<message>Please fill your advanced columns to sql query.</message>
				</validation>
			</validations>
		</node>
		<node expanded="true" name="columns" id="AdvancedColumnsNode">
			<subNodes>
				<subNode min="0" max="-1" id="AdvancedColumnNode"/>
			</subNodes>
		</node>
		<node name="column" id="AdvancedColumnNode">
			<attributes>
				<attribute name="name" type="string"/>
				<attribute name="label" type="string"/>
				<attribute name="type" type="enum" enumValues="string,integer,datetime,day,long,boolean,float"/>
			</attributes>
		</node>
		<node visible="false" generated="true" name="cc" id="ccColumnNode">
			<attributes>
				<attribute name="name" type="string"/>
				<attribute name="engName" type="string"/>
			</attributes>
		</node>
		<node expanded="true" name="sorTables" id="SoRTablesNode" label="SoR Tables">
			<subNodes>
				<subNode min="0" max="-1" id="SoRTableNode"/>
			</subNodes>
			<validations>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[count(sorTable[@useAsDetail=&#39;true&#39;])&gt;1]</expression>
					<message>Only one table can be set as detail.</message>
				</validation>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[$current/ancestor::datasetArray/masterTables/masterTable/@useAsDetail=&#39;true&#39; and sorTable/@useAsDetail=&#39;true&#39;]</expression>
					<message>Another Master table is set as detail.</message>
				</validation>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[$current/ancestor::datasetArray/instanceTables/instanceTable/@useAsDetail=&#39;true&#39; and sorTable/@useAsDetail=&#39;true&#39;]</expression>
					<message>Another Instance table is set as detail.</message>
				</validation>
			</validations>
		</node>
		<node expanded="true" name="sorTable" explorerLeaf="true" id="SoRTableNode">
			<attributes>
				<attribute name="entityName" type="string">
					<valuesProvider fillColumns="true" class="com.ataccama.ame.core.assist.PathValuesProvider">
						<selectPath>
							/preview/databaseModel/sorTables/*
						
						</selectPath>
						<valueTemplate>{@name}</valueTemplate>
						<labelTemplate>{@name}</labelTemplate>
					</valuesProvider>
				</attribute>
				<attribute defaultValue="false" name="useAsDetail" label="Use As Detail" type="boolean" required="true"/>
			</attributes>
			<subNodes>
				<subNode min="1" max="1" id="SoRColumnsNode"/>
			</subNodes>
		</node>
		<node name="columns" id="SoRColumnsNode" label="SoR Columns">
			<subNodes>
				<subNode min="0" max="-1" id="SoRColumnNode"/>
			</subNodes>
		</node>
		<node name="column" id="SoRColumnNode">
			<subNodes>
				<subNode min="1" max="1" id="ccColumnNode"/>
			</subNodes>
			<attributes>
				<attribute name="name" type="string" required="true">
					<valuesProvider fillColumns="true" class="com.ataccama.ame.core.assist.PathValuesProvider">
						<selectPath>/preview/databaseModel/sorTables/physicalTable[@name=lower-case($current/ancestor::sorTable/@entityName)]/columns/column[@name!=&#39;id&#39; and @load=&#39;true&#39; and @name!=&#39;eng_origin&#39;]
</selectPath>
						<valueTemplate>{@name}</valueTemplate>
						<labelTemplate>{@name}</labelTemplate>
					</valuesProvider>
					<description>Name of column from a selected table.</description>
				</attribute>
				<attribute name="label" label="Label Override" type="string"/>
			</attributes>
		</node>
		<node expanded="true" name="sampleSetting" id="SampleSettingNode" label="Sampling Setting" newTab="true">
			<attributes>
				<attribute defaultValue="false" name="enable" label="Enable Sampling" type="boolean" required="true"/>
				<attribute name="sampleSize" label="Sample Size" type="integer">
					<description>Limit of records displayed in the sample</description>
				</attribute>
				<attribute defaultValue="false" name="allowed" label="Allow Basic Sampling" type="boolean" required="true">
					<description>Use this setting to activate &lt;em&gt;Basic Strategy&lt;/em&gt; data sampling.</description>
				</attribute>
			</attributes>
			<validations>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[@sampleSize&gt;100]</expression>
					<message>Sample Size can be max 100.</message>
				</validation>
			</validations>
		</node>
	</nodes>
</metametadata-library>