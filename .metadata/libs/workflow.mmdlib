<?xml version='1.0' encoding='UTF-8'?>
<metametadata-library>
	<nodes>
		<node expanded="true" visible="true" icon="icons/workflows.png" name="wfConfig" explorerLeaf="false" id="WorkflowConfigNode" label="Workflow Configuration" newTab="true">
			<subNodes>
				<subNode min="1" max="1" id="DefaultWFNode"/>
				<subNode min="1" max="1" id="WorkflowsNode"/>
				<subNode min="1" max="1" id="StatusesNode"/>
			</subNodes>
			<description>Here is where you configure approval workflows.</description>
			<editors>
				<editor openModal="true" class="com.ataccama.ame.core.editors.PropertyEditor"/>
			</editors>
			<generators>
				<gen fileNameTemplate="Files/etc/mda-workflow.gen.xml" source="." class="com.ataccama.ame.core.generators.XsltGenerator" templatePath="xslt/gen_mda_workflow.xsl">
					<parameters>
			</parameters>
				</gen>
			</generators>
		</node>
		<node expanded="true" icon="icons/workflows.png" name="workflows" id="WorkflowsNode" label="Workflows" newTab="false">
			<subNodes>
				<subNode min="0" max="-1" id="WorkflowNode"/>
			</subNodes>
			<editors>
				<editor openModal="true" class="com.ataccama.ame.core.editors.PropertyEditor"/>
				<editor label="New workflow..." class="com.ataccama.ame.core.editors.CreateChild"/>
			</editors>
			<description>Here is where you can configure custom workflows. If you configure several workflows, the users will select one when creating a draft.</description>
		</node>
		<node icon="icons/branch.png" name="workflow" explorerLeaf="true" id="WorkflowNode" label="{@name}">
			<attributes>
				<attribute defaultValue="false" name="enable" type="boolean" required="true">
					<description>If enabled, this workflow will appear in the web application.</description>
				</attribute>
				<attribute name="name" type="string">
					<description>Workflow ID</description>
				</attribute>
				<attribute name="label" type="string">
					<description>Workflow label that is shown in the web application</description>
				</attribute>
			</attributes>
			<subNodes>
				<subNode min="1" max="1" id="StepsNode"/>
			</subNodes>
			<validations/>
			<editors>
				<editor showTree="true" openModal="true" class="com.ataccama.ame.core.editors.PropertyEditor"/>
				<editor targetNodePath="steps" label="New wf step..." class="com.ataccama.ame.core.editors.CreateChild"/>
				<editor label="Open diagram" class="com.ataccama.ame.core.editors.FlowVisualization">
					<nodeTypes>
						<nodeType idPath="@name" selectorPath="steps/step" labelTemplate="{ancestor::wfConfig/statuses/status[@name = $current/@name]/@label}"/>
					</nodeTypes>
					<connectionTypes>
						<connectionType selectorPath="steps/step[@transitionTarget!=&#39;&#39;]" targetIdPath="@transitionTarget" sourceIdPath="@name" labelTemplate="{@transitionLabel}"/>
					</connectionTypes>
					<additionalWatchedNodes>
						<watchNode nodePath="../../../../statuses"/>
					</additionalWatchedNodes>
				</editor>
				<editor label="Delete workflow" class="com.ataccama.ame.core.editors.DeleteChild"/>
			</editors>
			<description>In the context of RDM, a workflow is a process, which starts with making changes to the data and ends with publishing them.</description>
		</node>
		<node expanded="true" name="steps" id="StepsNode" label="Transitions" sortChildren="false">
			<subNodes>
				<subNode min="0" max="-1" id="StepNode"/>
			</subNodes>
			<editors>
			</editors>
			<validations/>
			<description>Here is where you define the statuses used in this workflow and transitions between them. If you need to move back and forth between the same two statuses, define two transitions between them with opposite directions.</description>
		</node>
		<node icon="icons/element.png" name="step" explorerLeaf="true" id="StepNode" label="{@id} : {ancestor::wfConfig/statuses/status[@id=$current/@id]/@label}">
			<attributes>
				<attribute name="name" label="Outgoing Status" type="string" required="true">
					<description>This is the status from which the current transition starts.</description>
					<valuesProvider class="com.ataccama.ame.core.assist.PathValuesProvider">
						<selectPath>ancestor::wfConfig/statuses/status</selectPath>
						<valueTemplate>{@name}</valueTemplate>
						<labelTemplate>{@label} ({@name})</labelTemplate>
					</valuesProvider>
				</attribute>
				<attribute name="transitionTarget" label="Target Status" type="string" required="false">
					<valuesProvider class="com.ataccama.ame.core.assist.PathValuesProvider">
						<selectPath>ancestor::wfConfig/statuses/status</selectPath>
						<valueTemplate>{@name}</valueTemplate>
						<labelTemplate>{@label} ({@name})</labelTemplate>
					</valuesProvider>
					<description>This is the status to which the current transition leads.</description>
				</attribute>
				<attribute name="transitionName" label="Transition Name" type="string">
					<description>Unique ID of the transition.</description>
				</attribute>
				<attribute name="transitionLabel" label="Transition Label" type="string">
					<description>Label of the transition shown as button text in the web application.</description>
				</attribute>
			</attributes>
			<subNodes/>
			<editors>
				<editor showTree="true" openModal="true" class="com.ataccama.ame.core.editors.PropertyEditor"/>
			</editors>
			<validations>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[@name=ancestor::wfConfig/statuses/status[@enable=&#39;false&#39;]/@name and ancestor::workflow/@enable=&#39;true&#39;]</expression>
					<message>You cannot use disabled status.</message>
				</validation>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>true</inverseCondition>
					<expression>.[@transitionTarget=ancestor::wfConfig/statuses/status[@enable=&#39;false&#39;]/@name and ancestor::workflow/@enable=&#39;true&#39;]</expression>
					<message>You cannot use disabled status.</message>
				</validation>
			</validations>
		</node>
		<node expanded="true" icon="icons/element_add.png" name="statuses" id="StatusesNode" label="Statuses" sortChildren="false">
			<subNodes>
				<subNode min="0" max="-1" id="StatusNode"/>
			</subNodes>
			<editors>
				<editor openModal="true" class="com.ataccama.ame.core.editors.PropertyEditor"/>
				<editor label="New status..." class="com.ataccama.ame.core.editors.CreateChild"/>
			</editors>
			<description>Here is where you you define workflow statuses between which a draft can move before it is published.</description>
			<validations>
				<validation class="com.ataccama.ame.core.validations.MDPathValidation">
					<severity>ERROR</severity>
					<inverseCondition>false</inverseCondition>
					<expression>.[count(status[@first=&#39;true&#39;])&lt;=1]</expression>
					<message>Only one step can be set as first.</message>
				</validation>
			</validations>
		</node>
		<node icon="icons/element.png" name="status" explorerLeaf="true" id="StatusNode" label="{@name} : {@label}">
			<attributes>
				<attribute defaultValue="false" name="enable" type="boolean" required="true"/>
				<attribute name="name" label="Name" type="string" required="true">
					<description>Id of the status.</description>
				</attribute>
				<attribute name="label" label="Label" type="string" required="true">
					<description>The status label is shown to users in the web application.</description>
				</attribute>
				<attribute defaultValue="false" name="first" type="boolean" required="true">
					<description>Marks the status that comes first in all defined workflows.</description>
				</attribute>
			</attributes>
			<validations>
				<validation class="com.ataccama.ame.core.validations.UniqueValuesValidation">
					<selectPath>../../statuses/status/@id</selectPath>
					<message>Value @id is not unique.</message>
				</validation>
				<validation class="com.ataccama.ame.core.validations.UniqueValuesValidation">
					<selectPath>../../statuses/status/@label</selectPath>
					<message>Value @label is not unique.</message>
				</validation>
			</validations>
			<subNodes>
			</subNodes>
			<editors>
				<editor openModal="true" class="com.ataccama.ame.core.editors.PropertyEditor"/>
				<editor label="Delete status" class="com.ataccama.ame.core.editors.DeleteChild"/>
			</editors>
		</node>
		<node expanded="true" name="workflowLabels" id="DefaultWFNode" label="Default Workflow Settings" newTab="false">
			<subNodes/>
			<attributes>
				<attribute defaultValue="true" name="enableCons" label="Enable Default Consolidation WF" type="boolean" required="true">
					<description>Enables the default approval workflow for drafts created by editing consolidated records, i.e., master or instance records.</description>
				</attribute>
				<attribute defaultValue="true" name="enableSor" label="Enable Default SoR WF" type="boolean" required="true">
					<description>Enables the default approval workflow for drafts created by editing or creating records in the System or Record mode/hub.</description>
				</attribute>
				<attribute defaultValue="Consolidation WF" name="consolidation" label="Label for Consolidation Workflow" type="string">
					<description>Define the label displayed in the web application for the default Consolidation workflow.</description>
				</attribute>
				<attribute defaultValue="SoR WF" name="sor" label="Label for SoR Workflow" type="string">
					<description>Define the label displayed in the web application for the default System of Record workflow.</description>
				</attribute>
				<attribute defaultValue="In Progress" name="draft" label="Label for In Progress Status" type="string">
					<description>Define the label for the first status in the default workflow (both for the Consolidation and SoR workflows).</description>
				</attribute>
				<attribute defaultValue="Waiting for Approval" name="waiting_for_publish" label="Label for Waiting for Approval Status" type="string">
					<description>Define the label for the second status in the default Consolidation workflow.</description>
				</attribute>
				<attribute defaultValue="Submit for Approval" name="move_publish" label="Label for Submit for Approval Transition" type="string">
					<description>Define the label for the transition from the In Progress status to the Waiting for Approval status.</description>
				</attribute>
				<attribute defaultValue="Back to In Progress" name="return_draft" label="Label for Back to In Progress Transition" type="string">
					<description>Define the label for the transition from the Waiting for Approval status to the In Progress status.</description>
				</attribute>
			</attributes>
		</node>
	</nodes>
</metametadata-library>