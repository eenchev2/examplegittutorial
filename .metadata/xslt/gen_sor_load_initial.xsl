<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:mrg="http://www.ataccama.com/dqc/plan-merge"
	xmlns:comm="http://www.ataccama.com/purity/comment"
	exclude-result-prefixes="sf fn mrg">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="sorTables" select="document('param:sorTables')/*"/> <!--/preview/databaseModel/sorTables -->
<xsl:param name="instanceTables" select="document('param:instanceTables')/*"/> <!--/preview/databaseModel/instanceTables -->
<xsl:param name="system" select="document('param:system')/*"/> <!--/system -->
<xsl:param name="dictionary" select="document('param:dictionary')/*"/>
<xsl:param name="instanceModel" select="document('param:instanceModel')/*"/>
<xsl:param name="initialLoad" select="document('param:initialLoad')/*"/>
<xsl:param name="settings" select="document('param:settings')/*"/>

<xsl:param name="refData" select="document('param:refData')/*"/>
<xsl:param name="sorModel" select="document('param:sorModel')/*"/>

<xsl:include href="incl_plan_comments.xsl"/>
<xsl:include href="incl_constants.xsl"/>
<xsl:include href="_constants.xsl"/>
<xsl:include href="incl_gen_plan_templates.xsl"/>

<xsl:template match="/*">
	<purity-config xmlns:comm="http://www.ataccama.com/purity/comment" version="{$version}">
		<modelComment bounds="24,24,937,60" borderColor="183,183,0" backgroundColor="255,255,180" foregroundColor="51,51,51">
			<xsl:call-template name="load_plan_comment"/>
		</modelComment>
		<!-- all entities -->
		<xsl:if test="@allEntities='true'">
			<xsl:apply-templates select="$sorTables/physicalTable|$refData/physicalTable[@refData=$sorModel/tables/table/columns/column/@refData]">
				<xsl:with-param name="allowLoad" select="@allowLoad"/>
			</xsl:apply-templates>
		</xsl:if>
		<!-- selected entities -->
		<xsl:if test="@allEntities='false'">
			<xsl:apply-templates select="$sorTables/physicalTable[@name=current()/selectedEntities/selectedEntity/@name] | $refData/physicalTable[@name=current()/selectedEntities/selectedEntity/@name]">
				<xsl:with-param name="allowLoad" select="@allowLoad"/>
			</xsl:apply-templates>							
		</xsl:if>
</purity-config>
</xsl:template>

<xsl:template match="physicalTable"> 
	<xsl:param name="allowLoad"/>
	<xsl:variable name="columns" select="columns"/>
	<xsl:variable name="tableName" select="@name"/>
	<xsl:variable name="dic" select="@dic"/>	
	
	<xsl:if test="$allowLoad='false'">
		<step id="map_{$tableName}_source_columns" className="com.ataccama.dqc.tasks.flow.AlterFormat" disabled="false" mode="NORMAL">
	        <properties>
	            <addedColumns>
	            <mrg:mergeChildren key="@name">
					 <xsl:for-each select="columns/column[@origin!='internal']">
	                	<xsl:call-template name="column_af"/>
	                </xsl:for-each>
	                <addedColumn name="source_timestamp" type="DATETIME" mrg:retainNodes="expression" />
	            </mrg:mergeChildren>
	            </addedColumns>
	            <removedColumns/>
	            <comm:comment>
	            	<xsl:call-template name="generated_AFmapping_step"/>
	            </comm:comment>
	        </properties>
	       <visual-constraints layout="vertical">
				<xsl:attribute name="bounds">
					<xsl:value-of select="((position()-1)*336)+48"/><xsl:text>,240,-1,-1</xsl:text>
				</xsl:attribute>
			</visual-constraints>
	    </step> 
		
		<xsl:choose>
			<xsl:when test="$settings/@enableRC='true'">
		    	<step id="counter_{$tableName}_load_out" className="com.ataccama.dqc.tasks.flow.RecordCounter" disabled="false" mode="NORMAL">
					<properties reportPerfPerBatch="true" append="true" reportFileName="\\logger" timeStampFormat="yyyy-MM-dd HH:mm:ss" batchSize="{$settings/@batchSizeRC}" reportPerfPerSecond="true">
					<comm:comment>
		        		<xsl:call-template name="generated_step"/>
		        	</comm:comment>
		        	</properties>
		      		<visual-constraints layout="vertical">
						<xsl:attribute name="bounds">
							<xsl:value-of select="((position()-1)*336)+48"/><xsl:text>,336,-1,-1</xsl:text>
						</xsl:attribute>
					</visual-constraints>	        
				</step>		
				<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
					<source step="counter_{$tableName}_load_out" endpoint="out"/>
					<target step="{$tableName}" endpoint="in"/>
					<visual-constraints>
						<bendpoints/>
					</visual-constraints>
				</connection>			
				<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
					<source step="map_{$tableName}_source_columns" endpoint="out"/>
					<target step="counter_{$tableName}_load_out" endpoint="in"/>
					<visual-constraints>
						<bendpoints/>
					</visual-constraints>
				</connection>
			</xsl:when>
			<xsl:otherwise>
				<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
					<source step="map_{$tableName}_source_columns" endpoint="out"/>
					<target step="{$tableName}" endpoint="in"/>
					<visual-constraints>
						<bendpoints/>
					</visual-constraints>
				</connection>
			</xsl:otherwise>
		</xsl:choose>	

	</xsl:if>        
     
	<step id="{@name}" className="com.ataccama.dqc.tasks.common.usersteps.io.OutputStep" disabled="false" mode="NORMAL">
        <properties>
            <requiredColumns>
                <requiredColumn name="source_timestamp" type="DATETIME" />
                <xsl:for-each select="columns/column[@origin!='internal']">
                	<xsl:call-template name="column_required"/>
                </xsl:for-each>
            </requiredColumns>
            <comm:comment>
            	<xsl:call-template name="generated_step"/>
            </comm:comment>
        </properties>
        
        <visual-constraints layout="vertical">
			<xsl:attribute name="bounds">
				<xsl:if test="$allowLoad='false'"> <!-- v pripade, ze neni pouzito full -->
					<xsl:value-of select="((position()-1)*336)+48"/><xsl:text>,432,-1,-1</xsl:text>
				</xsl:if>
				<xsl:if test="$allowLoad='true'"> <!-- v pripade, ze je pouzito full -->
					<xsl:value-of select="((position()-1)*336)+48"/><xsl:text>,432,-1,-1</xsl:text>
				</xsl:if>
			</xsl:attribute>
		</visual-constraints>        
    </step>
	
</xsl:template>

</xsl:stylesheet>