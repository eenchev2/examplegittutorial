<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:mrg="http://www.ataccama.com/dqc/plan-merge"
	xmlns:comm="http://www.ataccama.com/purity/comment"
	exclude-result-prefixes="sf fn mrg">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="instanceTables" select="document('param:instanceTables')/*"/>  <!--/preview/databaseModel/instanceTables -->
<xsl:param name="system" select="document('param:system')/*"/>  <!--/system -->
<xsl:param name="dictionary" select="document('param:dictionary')/*"/> <!-- /logicalModel/dictionary-->
<xsl:param name="instanceModel" select="document('param:instanceModel')/*"/>
<xsl:param name="settings" select="document('param:settings')/*"/>
<xsl:include href="incl_plan_comments.xsl"/>
<xsl:include href="incl_constants.xsl"/>
<xsl:include href="_constants.xsl"/>
<xsl:include href="incl_gen_plan_templates.xsl"/>

<xsl:template match="/*">
	<purity-config xmlns:comm="http://www.ataccama.com/purity/comment" version="{$version}">
	<modelComment bounds="24,24,937,60" borderColor="183,183,0" backgroundColor="255,255,180" foregroundColor="51,51,51">
		<xsl:call-template name="load_plan_comment"/>
	</modelComment>
	
		<xsl:variable name="selTables" select="selectedTables"/>
		<xsl:choose>
			<!-- allTables OPTION -->	
			<xsl:when test="@allTables='true'"> 
				<!-- generate integration output -->
				<xsl:apply-templates select="$instanceTables/physicalTable[@name=$system/sourceMappings/mapping[not(@entity = preceding::*/@entity)]/lower-case(@entity)]">
					<xsl:with-param name="selTables" select="$selTables"/>
				</xsl:apply-templates>							
			</xsl:when>
			<!-- ENTITIES DEFINED ON LOAD OP -->
			<xsl:otherwise> 
				<!-- generate integration output -->
				<xsl:apply-templates select="$instanceTables/physicalTable[@name=$selTables/*/@name]">
					<xsl:with-param name="selTables" select="$selTables"/>
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>
	</purity-config>
</xsl:template>

<xsl:template match="physicalTable"> 
	<xsl:param name="selTables"/>
	<xsl:variable name="tableName" select="@name"/>
	<xsl:variable name="columns" select="columns"/>
	<xsl:variable name="selTable" select="$selTables/table[@name=$tableName]"/>
	
	<step id="map_{$tableName}_source_columns" className="com.ataccama.dqc.tasks.flow.AlterFormat" disabled="false" mode="NORMAL">
        <properties>
            <addedColumns>
				<mrg:mergeChildren key="@name">
					<xsl:choose>
						<xsl:when test="$selTable/columns/column">
	         				<xsl:variable name="partialColumns" select="$selTable/columns/column"/>
	         				<addedColumn name="source_id" type="STRING" mrg:retainNodes="expression"/>	
	         				<xsl:for-each select="columns/column[((@origin='source' and @artificial='false' and @load='true' and (@isPk!='false' or @originalPk!='true')) or (@name='source_id' and @originalPk='true')) and @name=$selTable/columns/column/@name]">
	         					<xsl:call-template name="column_af"/>
	         				</xsl:for-each>	
	         			</xsl:when>
			         	<xsl:otherwise>
			         		<xsl:for-each select="columns/column[(@origin='source' and @artificial='false' and @load='true' and (@isPk!='false' or @originalPk!='true')) or (@name='source_id' and @originalPk='true')]">
			         			<xsl:call-template name="column_af"/>
			         		</xsl:for-each>
			         	</xsl:otherwise>
			         </xsl:choose>   
	                <addedColumn name="change_type" type="STRING" mrg:retainNodes="expression"/>
	                <addedColumn name="source_timestamp" type="DATETIME" mrg:retainNodes="expression" />
                </mrg:mergeChildren>
            </addedColumns>
            <removedColumns/>
            <comm:comment>
            	<xsl:call-template name="generated_AFmapping_step"/>
            </comm:comment>
        </properties>
       <visual-constraints layout="vertical">
			<xsl:attribute name="bounds">
				<xsl:value-of select="((position()-1)*336)+48"/><xsl:text>,312,-1,-1</xsl:text>
			</xsl:attribute>
		</visual-constraints>
    </step>
	
    <xsl:if test="$settings/@enableRC='true'">    	  	       
	    <step id="counter_{@name}_load_out" className="com.ataccama.dqc.tasks.flow.RecordCounter" disabled="false" mode="NORMAL">
			<properties reportPerfPerBatch="true" append="true" reportFileName="\\logger" timeStampFormat="yyyy-MM-dd HH:mm:ss" batchSize="{$settings/@batchSizeRC}" reportPerfPerSecond="true">
			<comm:comment>
	        	<xsl:call-template name="generated_step"/>
	        </comm:comment>
	        </properties>
	      	<visual-constraints layout="vertical">
				<xsl:attribute name="bounds">
					<xsl:value-of select="((position()-1)*336)+48"/><xsl:text>,504,-1,-1</xsl:text>
				</xsl:attribute>
			</visual-constraints>	        
		</step>		
    </xsl:if>	
	
	<step id="{@name}" className="com.ataccama.dqc.tasks.common.usersteps.io.OutputStep" disabled="false" mode="NORMAL">
        <properties>
            <requiredColumns>
                <requiredColumn name="source_id" type="STRING"/>
                <requiredColumn name="origin" type="STRING"/>
                <requiredColumn name="change_type" type="STRING"/>
                <requiredColumn name="source_timestamp" type="DATETIME" />
				<xsl:choose>
					<xsl:when test="$selTable/columns/column">
         				<xsl:variable name="partialColumns" select="$selTable/columns/column"/>
         				<xsl:for-each select="columns/column[@origin='source' and @name=$selTable/columns/column/@name]">
         					<xsl:call-template name="column_required"/>
         				</xsl:for-each>	
         			</xsl:when>
		         	<xsl:otherwise>
		         		<xsl:for-each select="columns/column[@origin='source']">
		         			<xsl:call-template name="column_required"/>
		         		</xsl:for-each>
		         	</xsl:otherwise>
		         </xsl:choose>                
            </requiredColumns>
            <comm:comment>
            	<xsl:call-template name="generated_step"/>
            </comm:comment>
        </properties>
        <visual-constraints layout="vertical">
			<xsl:attribute name="bounds">
				<xsl:value-of select="((position()-1)*336)+48"/><xsl:text>,600,-1,-1</xsl:text>
			</xsl:attribute>
		</visual-constraints>
    </step>
       
    <!--lookups -->
    <xsl:apply-templates select="$instanceTables/*[@name=$tableName]/columns/column[@instUse='load']" mode="lookup">
		<xsl:with-param name="position" select="position()-1"/>
		<xsl:with-param name="system" select="$system"/>
		<xsl:with-param name="table_name" select="$tableName"/>
    </xsl:apply-templates>
    
<!--validate lookups -->
	<xsl:apply-templates select="$instanceTables/physicalTable[@name=$tableName]/columns/column[@refData!='' and @usageValidation='load']" mode="validLookup">
		<xsl:with-param name="position" select="position()-1"/>
		<xsl:with-param name="system" select="$system"/>
		<xsl:with-param name="table_name" select="$tableName"/>
   	</xsl:apply-templates>    
    
<!-- generate AF with origin mapping -->
	<xsl:apply-templates select="$system/sourceMappings/mapping[lower-case(@entity)=$tableName]">
		<xsl:with-param name="position" select="position()-1"/>
	</xsl:apply-templates>

<!-- connection map_internal map_name -->
    <xsl:if test="count($system/sourceMappings/mapping[@entity=$tableName])=1">
    	<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
			<source step="map_internal_columns_from_{$system/sourceMappings/mapping[@entity=$tableName]/@table}_to_{@name}" endpoint="out"/>
			<target step="map_{@name}_source_columns" endpoint="in"/>
			<visual-constraints>
				<bendpoints/>
			</visual-constraints>
		</connection>
    </xsl:if>
		
<!-- lookup connections -->
	<xsl:if test="count($instanceTables/*[@name=$tableName]/columns/column[@instUse='load'])=1 and count($instanceTables/physicalTable[@name=$tableName]/columns/column[@refData!='' and @usageValidation='load'])=0">
		<xsl:apply-templates select="$instanceTables/*[@name=$tableName]/columns/column[@instUse='load']" mode="lookupConnection">
			<xsl:with-param name="table_name" select="$tableName"/>
			<xsl:with-param name="origin_name" select="@name"/>
			<xsl:with-param name="enableRC" select="$settings/@enableRC"/>
    	</xsl:apply-templates>
	</xsl:if>
	
<!-- validate lookup connections -->
 	<xsl:if test="count($instanceTables/physicalTable[@name=$tableName]/columns/column[@refData!='' and @usageValidation='load'])=1 and count($instanceTables/*[@name=$tableName]/columns/column[@instUse='load'])=0"> 
		<xsl:apply-templates select="$instanceTables/physicalTable[@name=$tableName]/columns/column[@refData!='' and @usageValidation='load']" mode="validLookupConnection">
			<xsl:with-param name="table_name" select="$tableName"/>
			<xsl:with-param name="origin_name" select="@name"/>
			<xsl:with-param name="enableRC" select="$settings/@enableRC"/>
    	</xsl:apply-templates>
	</xsl:if> 	
	
<!-- connections -->	
	<xsl:if test="count($instanceTables/*[@name=$tableName]/columns/column[@instUse='load'])=0 and count($instanceTables/physicalTable[@name=$tableName]/columns/column[@refData!='' and @usageValidation='load'])=0">
		<xsl:choose>
			<xsl:when test="$settings/@enableRC='true'">
				<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
					<source step="map_{@name}_source_columns" endpoint="out"/>
					<target step="counter_{@name}_load_out" endpoint="in"/>
					<visual-constraints>
						<bendpoints/>
					</visual-constraints>
				</connection>
			</xsl:when>
			<xsl:otherwise>
				<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
					<source step="map_{@name}_source_columns" endpoint="out"/>
					<target step="{@name}" endpoint="in"/>
					<visual-constraints>
						<bendpoints/>
					</visual-constraints>
				</connection>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
    
	<xsl:if test="$settings/@enableRC='true'">
		<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
			<source step="counter_{@name}_load_out" endpoint="out"/>
			<target step="{@name}" endpoint="in"/>
			<visual-constraints>
				<bendpoints/>
			</visual-constraints>
		</connection>
	</xsl:if>		

</xsl:template>

<xsl:template match="mapping">
	<xsl:param name="position"/>
	<xsl:variable name="origin_entity" select="@entity"/>
	<xsl:variable name="origin_table" select="@table"/>
	<xsl:variable name="origin_system" select="$system/@name"/>
	<xsl:variable name="single" select="(count(preceding-sibling::mapping[@entity=$origin_entity])+count(following-sibling::mapping[@entity=$origin_entity]))=0"/>
	<xsl:variable name="def_origin">
		<xsl:value-of select="$origin_system"/><xsl:text>#</xsl:text><xsl:value-of select="$origin_table"/><xsl:text>#</xsl:text><xsl:value-of select="$origin_entity"/>
	</xsl:variable>
	
	<step id="map_internal_columns_from_{$origin_table}_to_{$origin_entity}" className="com.ataccama.dqc.tasks.flow.AlterFormat" disabled="false" mode="NORMAL">
        <properties>
            <addedColumns>
            <!-- origin -->
                <addedColumn expression="'{sf:nvl(@customOrigin,$def_origin)}'" name="origin" type="STRING"/>
            <!-- compound key -->
                  <xsl:for-each select="$instanceTables/*[@name=lower-case($origin_entity)]/columns/column[@artificial='true']">
					<addedColumn name="{@name}" type="STRING">
						<xsl:attribute name="expression">
							 <xsl:for-each select="compoundDefinition/column" >
								<xsl:text>toString(</xsl:text><xsl:value-of select="@name"/>
								<xsl:choose>
									<xsl:when test="@type='datetime'"><xsl:text>,'yyyy-MM-dd HH:mm:ss')</xsl:text></xsl:when>
									<xsl:when test="@type='day'"><xsl:text>,'yyyy-MM-dd')</xsl:text></xsl:when>
									<xsl:otherwise><xsl:text>)</xsl:text></xsl:otherwise>
								</xsl:choose>
								<xsl:if test="position() != last()"><xsl:text>+'#'+</xsl:text></xsl:if>
							</xsl:for-each>   
						</xsl:attribute>
					</addedColumn> 
				</xsl:for-each>
			<!-- source system for lookup -->	
				<xsl:if test="$instanceTables/*[@name=$origin_entity]/columns/column[@origin='source' and @dic != '']">
					<addedColumn expression="'{fn:upper-case($origin_system)}'" name="source_system" type="STRING"/>
				</xsl:if>	
            </addedColumns>
            <removedColumns/>
             <comm:comment>
            	<xsl:call-template name="generated_step"/>
            </comm:comment>
        </properties>
       <visual-constraints layout="vertical">
			<xsl:attribute name="bounds">
				<xsl:value-of select="(($position)*336)+48"/><xsl:text>,216,-1,-1</xsl:text>
			</xsl:attribute>
		</visual-constraints>
    </step>
    
    <xsl:if test="$instanceTables/*[@name=$origin_entity]/columns/column[@artificial='true']/compoundDefinition/column/@name != 'source_id'">
    	<step id="pk_name_from_{$origin_table}_to_{$origin_entity}" className="com.ataccama.dqc.tasks.flow.AlterFormat" disabled="false" mode="NORMAL">
        	<properties>
            	<addedColumns>
            		<mrg:mergeChildren key="@name">
            			<!-- key -->
                		<xsl:for-each select="$instanceTables/physicalTable[@name=$origin_entity]/columns/column[@artificial='true']/compoundDefinition/column">
							<addedColumn name="{@name}" mrg:retainNodes="expression">
							  <xsl:attribute name="type">
								  <xsl:call-template name="convertType"/>
							  </xsl:attribute>							
							</addedColumn>
						</xsl:for-each>
					</mrg:mergeChildren>
            	</addedColumns>
            	<removedColumns/>
             	<comm:comment>
            		<xsl:call-template name="generated_AFmapping_step"/>
            	</comm:comment>
        	</properties>
       		<visual-constraints layout="vertical">
				<xsl:attribute name="bounds">
					<xsl:value-of select="(($position)*336)+48"/><xsl:text>,120,-1,-1</xsl:text>
				</xsl:attribute>
			</visual-constraints>
    	</step>
    	
    	<connection className="com.ataccama.dqc.model.elements.connections.StandardFlowConnection" disabled="false">
			<source step="pk_name_from_{$origin_table}_to_{$origin_entity}" endpoint="out"/>
			<target step="map_internal_columns_from_{$origin_table}_to_{$origin_entity}" endpoint="in"/>
			<visual-constraints>
				<bendpoints/>
			</visual-constraints>
		</connection>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>