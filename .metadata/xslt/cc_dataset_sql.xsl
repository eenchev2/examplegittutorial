<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>

 <xsl:template match="/*">

	<cc>
		<sql>
			<xsl:variable name="newline"><xsl:text>
			</xsl:text></xsl:variable>
			
			<xsl:value-of select="'select '"/>
			<xsl:if test="instanceTables/instanceTable/@useAsDetail='true'">
				<xsl:value-of select="instanceTables/instanceTable[@useAsDetail='true']/@entityName"/>
				<xsl:value-of select="'_instance'"/>
				<xsl:value-of select="'.id'"/>
			</xsl:if>
			<xsl:if test="masterTables/masterTable/@useAsDetail='true'">
				<xsl:value-of select="masterTables/masterTable[@useAsDetail='true']/cc/@entity"/>
				<xsl:value-of select="'_'"/>
				<xsl:value-of select="masterTables/masterTable[@useAsDetail='true']/cc/@layer"/>
				<xsl:value-of select="'.id'"/>
			</xsl:if>
			<xsl:if test="sorTables/sorTable/@useAsDetail='true'">
				<xsl:value-of select="sorTables/sorTable[@useAsDetail='true']/@entityName"/>
				<xsl:value-of select="'_sor'"/>
				<xsl:value-of select="'.id'"/>
			</xsl:if>						
			<xsl:for-each select="instanceTables/instanceTable">
				<xsl:variable name="tableName">
					<xsl:value-of select="@entityName"/>
					<xsl:value-of select="'_instance'"/>
				</xsl:variable>
				<xsl:for-each select="columns/column">
					<xsl:value-of select="', '"/>
					<xsl:value-of select="$tableName"/>
					<xsl:value-of select="'.'"/>
					<xsl:value-of select="sf:nvl(cc/@engName,@name)"/>
					<xsl:value-of select="' as '"/>
					<xsl:value-of select="cc/@name"/>
				</xsl:for-each>
			</xsl:for-each>
			<xsl:for-each select="masterTables/masterTable">
				<xsl:variable name="tableName">
					<xsl:value-of select="cc/@entity"/>
					<xsl:value-of select="'_'"/>
					<xsl:value-of select="cc/@layer"/>
				</xsl:variable>
				<xsl:for-each select="columns/column">
					<xsl:value-of select="', '"/>
					<xsl:value-of select="$tableName"/>
					<xsl:value-of select="'.'"/>
					<xsl:value-of select="sf:nvl(cc/@engName,@name)"/>
					<xsl:value-of select="' as '"/>
					<xsl:value-of select="cc/@name"/>
				</xsl:for-each>
			</xsl:for-each>	
			<xsl:for-each select="sorTables/sorTable">
				<xsl:variable name="tableName">
					<xsl:value-of select="@entityName"/>
					<xsl:value-of select="'_sor'"/>
				</xsl:variable>
				<xsl:for-each select="columns/column">
					<xsl:value-of select="', '"/>
					<xsl:value-of select="$tableName"/>
					<xsl:value-of select="'.'"/>
					<xsl:value-of select="sf:nvl(cc/@engName,@name)"/>
					<xsl:value-of select="' as '"/>
					<xsl:value-of select="cc/@name"/>
				</xsl:for-each>
			</xsl:for-each>					
			<xsl:value-of select="' from'"/>
			<xsl:value-of select="$newline"/>
			
			<xsl:variable name="instanceDetailTable" select="instanceTables/instanceTable[@useAsDetail='true']/@entityName"/>
			<xsl:variable name="masterDetailTable" select="masterTables/masterTable[@useAsDetail='true']/cc/@entity"/>
			<xsl:variable name="masterDetaiLayer" select="masterTables/masterTable[@useAsDetail='true']/cc/@layer"/>
			<xsl:variable name="sorDetailTable" select="sorTables/sorTable[@useAsDetail='true']/@entityName"/>
			
			<xsl:for-each select="instanceTables/instanceTable[@useAsDetail='true']">
				<xsl:value-of select="'$INSTANCE_'"/>
				<xsl:value-of select="@entityName"/>
				<xsl:value-of select="'$'"/>
				<xsl:value-of select="' '"/>
				<xsl:value-of select="@entityName"/>
				<xsl:value-of select="'_instance'"/>
				<xsl:value-of select="$newline"/>		
			</xsl:for-each>
			<xsl:for-each select="masterTables/masterTable[@useAsDetail='true']">
				<xsl:value-of select="'$MASTER_'"/>
				<xsl:value-of select="cc/@layer"/>
				<xsl:value-of select="'_'"/>
				<xsl:value-of select="cc/@entity"/>
				<xsl:value-of select="'$'"/>
				<xsl:value-of select="' '"/>
				<xsl:value-of select="cc/@entity"/>
				<xsl:value-of select="'_'"/>
				<xsl:value-of select="cc/@layer"/>
				<xsl:value-of select="$newline"/>		
			</xsl:for-each>		
			<xsl:for-each select="sorTables/sorTable[@useAsDetail='true']">
				<xsl:value-of select="'$SOR_'"/>
				<xsl:value-of select="@entityName"/>
				<xsl:value-of select="'$'"/>
				<xsl:value-of select="' '"/>
				<xsl:value-of select="@entityName"/>
				<xsl:value-of select="'_sor'"/>
				<xsl:value-of select="$newline"/>		
			</xsl:for-each>			
			<xsl:choose>	
				<xsl:when test="not(masterTables/masterTable) and not(sorTables/sorTable)">
					<xsl:for-each select="instanceTables/instanceTable[@useAsDetail='false']">
						<xsl:variable name="currentTable" select="@entityName"/>	
						<xsl:value-of select="'left join '"/>
						<xsl:value-of select="'$INSTANCE_'"/>
						<xsl:value-of select="@entityName"/>
						<xsl:value-of select="'$'"/>
						<xsl:value-of select="' '"/>
						<xsl:value-of select="@entityName"/>
						<xsl:value-of select="'_instance'"/>
						<xsl:value-of select="$newline"/>
						<xsl:value-of select="'on '"/>
						<xsl:value-of select="$instanceDetailTable"/>
						<xsl:value-of select="'_instance.'"/>
						<xsl:value-of select="$logicalModel/instanceModel/relationships/relationship[@parentTable=$instanceDetailTable and @childTable=$currentTable]/foreignKey/column/@parentColumn"/>
						<xsl:value-of select="' = '"/>
						<xsl:value-of select="$currentTable"/>
						<xsl:value-of select="'_instance.'"/>
						<xsl:value-of select="$logicalModel/instanceModel/relationships/relationship[@parentTable=$instanceDetailTable and @childTable=$currentTable]/foreignKey/column/@childColumn"/>
						<xsl:value-of select="' and '"/>
						<xsl:value-of select="$instanceDetailTable"/>
						<xsl:value-of select="'_instance.eng_system = '"/>		
						<xsl:value-of select="$currentTable"/>
						<xsl:value-of select="'_instance.eng_system'"/>					
						<xsl:value-of select="$newline"/>					
					</xsl:for-each>		
				</xsl:when>
				<xsl:when test="not(instanceTables/instanceTable) and not(sorTables/sorTable)">			
					<xsl:for-each select="masterTables/masterTable[@useAsDetail='false']">
						<xsl:variable name="currentTable" select="cc/@entity"/>
						<xsl:variable name="currentLayer" select="cc/@layer"/>	
						<xsl:value-of select="'left join '"/>
						<xsl:value-of select="'$MASTER_'"/>
						<xsl:value-of select="cc/@layer"/>
						<xsl:value-of select="'_'"/>
						<xsl:value-of select="cc/@entity"/>
						<xsl:value-of select="'$'"/>
						<xsl:value-of select="' '"/>
						<xsl:value-of select="cc/@entity"/>
						<xsl:value-of select="'_'"/>
						<xsl:value-of select="cc/@layer"/>
						<xsl:value-of select="$newline"/>
						<xsl:value-of select="'on '"/>
						<xsl:value-of select="$masterDetailTable"/>
						<xsl:value-of select="'_'"/>
						<xsl:value-of select="$masterDetaiLayer"/>
						<xsl:value-of select="'.'"/>
						<xsl:value-of select="$logicalModel/masterModels/masterModel[@name=$masterDetaiLayer]/relationships/relationship[@parentTable=$masterDetailTable and @childTable=$currentTable]/foreignKey/column/@parentColumn"/>
						<xsl:value-of select="' = '"/>
						<xsl:value-of select="$currentTable"/>
						<xsl:value-of select="'_'"/>
						<xsl:value-of select="$currentLayer"/>
						<xsl:value-of select="'.'"/>
						<xsl:value-of select="$logicalModel/masterModels/masterModel[@name=$masterDetaiLayer]/relationships/relationship[@parentTable=$masterDetailTable and @childTable=$currentTable]/foreignKey/column/@childColumn"/>
						<xsl:value-of select="$newline"/>					
					</xsl:for-each>			
				</xsl:when>
				<xsl:when test="not(masterTables/masterTable) and not(instanceTables/instanceTable)">
					<xsl:for-each select="sorTables/sorTable[@useAsDetail='false']">
						<xsl:variable name="currentTable" select="@entityName"/>	
						<xsl:value-of select="'left join '"/>
						<xsl:value-of select="'$SOR_'"/>
						<xsl:value-of select="@entityName"/>
						<xsl:value-of select="'$'"/>
						<xsl:value-of select="' '"/>
						<xsl:value-of select="@entityName"/>
						<xsl:value-of select="'_sor'"/>
						<xsl:value-of select="$newline"/>
						<xsl:value-of select="'on '"/>
						<xsl:value-of select="$sorDetailTable"/>
						<xsl:value-of select="'_sor'"/>
						<xsl:value-of select="'.'"/>
						<xsl:value-of select="$logicalModel/sorModel/relationships/relationship[@parentTable=$sorDetailTable and @childTable=$currentTable]/sorForeignKey/column/@parentColumn"/>
						<xsl:value-of select="' = '"/>
						<xsl:value-of select="$currentTable"/>
						<xsl:value-of select="'_sor'"/>
						<xsl:value-of select="'.'"/>
						<xsl:value-of select="$logicalModel/sorModel/relationships/relationship[@parentTable=$sorDetailTable and @childTable=$currentTable]/sorForeignKey/column/@childColumn"/>
						<xsl:value-of select="$newline"/>					
					</xsl:for-each>		
				</xsl:when>			
				<xsl:otherwise>
					<xsl:for-each select="instanceTables/instanceTable[@useAsDetail='false']">
						<xsl:variable name="currentTable" select="@entityName"/>	
						<xsl:value-of select="'left join '"/>
						<xsl:value-of select="'$INSTANCE_'"/>
						<xsl:value-of select="@entityName"/>
						<xsl:value-of select="'$'"/>
						<xsl:value-of select="' '"/>
						<xsl:value-of select="@entityName"/>
						<xsl:value-of select="'_instance'"/>
						<xsl:value-of select="$newline"/>
						<xsl:value-of select="'on '"/>
						<xsl:if test="$instanceDetailTable">
							<xsl:value-of select="$instanceDetailTable"/>
							<xsl:value-of select="'_instance'"/>
						</xsl:if>
						<xsl:if test="$masterDetailTable">
							<xsl:value-of select="$masterDetailTable"/>
							<xsl:value-of select="'_'"/>
							<xsl:value-of select="$masterDetaiLayer"/>
						</xsl:if>
						<xsl:value-of select="'.'"/>						
						<xsl:choose>
							<xsl:when test="$instanceDetailTable">
								<xsl:value-of select="$logicalModel/instanceModel/relationships/relationship[@parentTable=$instanceDetailTable and @childTable=$currentTable]/foreignKey/column/@parentColumn"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'FILL COLUMN'"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="' = '"/>
						<xsl:value-of select="$currentTable"/>
						<xsl:value-of select="'_instance'"/>
						<xsl:value-of select="'.'"/>
						<xsl:choose>
							<xsl:when test="$instanceDetailTable">
								<xsl:value-of select="$logicalModel/instanceModel/relationships/relationship[@parentTable=$instanceDetailTable and @childTable=$currentTable]/foreignKey/column/@childColumn"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'FILL COLUMN'"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="$instanceDetailTable">
							<xsl:value-of select="' and '"/>
							<xsl:value-of select="$instanceDetailTable"/>
							<xsl:value-of select="'_instance.eng_system = '"/>		
							<xsl:value-of select="$currentTable"/>
							<xsl:value-of select="'_instance.eng_system'"/>						
							<xsl:value-of select="$newline"/>		
						</xsl:if>			
					</xsl:for-each>	
					<xsl:for-each select="masterTables/masterTable[@useAsDetail='false']">
						<xsl:variable name="currentTable" select="cc/@entity"/>
						<xsl:variable name="currentLayer" select="cc/@layer"/>	
						<xsl:value-of select="'left join '"/>
						<xsl:value-of select="'$MASTER_'"/>
						<xsl:value-of select="cc/@layer"/>
						<xsl:value-of select="'_'"/>
						<xsl:value-of select="cc/@entity"/>
						<xsl:value-of select="'$'"/>
						<xsl:value-of select="' '"/>
						<xsl:value-of select="cc/@entity"/>
						<xsl:value-of select="'_'"/>
						<xsl:value-of select="cc/@layer"/>
						<xsl:value-of select="$newline"/>
						<xsl:value-of select="'on '"/>
						<xsl:if test="$instanceDetailTable">
							<xsl:value-of select="$instanceDetailTable"/>
							<xsl:value-of select="'_instance'"/>
						</xsl:if>
						<xsl:if test="$masterDetailTable">
							<xsl:value-of select="$masterDetailTable"/>
							<xsl:value-of select="'_'"/>
							<xsl:value-of select="$masterDetaiLayer"/>
						</xsl:if>
						<xsl:value-of select="'.'"/>
						<xsl:choose>
							<xsl:when test="$masterDetailTable">
								<xsl:value-of select="$logicalModel/masterModels/masterModel[@name=$masterDetaiLayer]/relationships/relationship[@parentTable=$masterDetailTable and @childTable=$currentTable]/foreignKey/column/@parentColumn"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'FILL COLUMN'"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="' = '"/>
						<xsl:value-of select="$currentTable"/>
						<xsl:value-of select="'_'"/>
						<xsl:value-of select="$currentLayer"/>
						<xsl:value-of select="'.'"/>
						<xsl:choose>
							<xsl:when test="$masterDetailTable">
								<xsl:value-of select="$logicalModel/masterModels/masterModel[@name=$masterDetaiLayer]/relationships/relationship[@parentTable=$masterDetailTable and @childTable=$currentTable]/foreignKey/column/@childColumn"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'FILL COLUMN'"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="$newline"/>					
					</xsl:for-each>	
					<xsl:for-each select="sorTables/sorTable[@useAsDetail='false']">
						<xsl:variable name="currentTable" select="@entityName"/>	
						<xsl:value-of select="'left join '"/>
						<xsl:value-of select="'$SOR_'"/>
						<xsl:value-of select="@entityName"/>
						<xsl:value-of select="'$'"/>
						<xsl:value-of select="' '"/>
						<xsl:value-of select="@entityName"/>
						<xsl:value-of select="'_sor'"/>
						<xsl:value-of select="$newline"/>
						<xsl:value-of select="'on '"/>
						<xsl:if test="$sorDetailTable">
							<xsl:value-of select="$sorDetailTable"/>
							<xsl:value-of select="'_sor'"/>
						</xsl:if>
						<xsl:if test="$masterDetailTable">
							<xsl:value-of select="$masterDetailTable"/>
							<xsl:value-of select="'_'"/>
							<xsl:value-of select="$masterDetaiLayer"/>
						</xsl:if>
						<xsl:value-of select="'.'"/>						
						<xsl:choose>
							<xsl:when test="$sorDetailTable">
								<xsl:value-of select="$logicalModel/sorModel/relationships/relationship[@parentTable=$sorDetailTable and @childTable=$currentTable]/sorForeignKey/column/@parentColumn"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'FILL COLUMN'"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="' = '"/>
						<xsl:value-of select="$currentTable"/>
						<xsl:value-of select="'_sor'"/>
						<xsl:value-of select="'.'"/>
						<xsl:choose>
							<xsl:when test="$sorDetailTable">
								<xsl:value-of select="$logicalModel/sorModel/relationships/relationship[@parentTable=$sorDetailTable and @childTable=$currentTable]/sorForeignKey/column/@childColumn"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'FILL COLUMN'"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="$newline"/>					
					</xsl:for-each>																
				</xsl:otherwise>
			</xsl:choose>
		</sql>
	</cc>

 </xsl:template>

</xsl:stylesheet>

