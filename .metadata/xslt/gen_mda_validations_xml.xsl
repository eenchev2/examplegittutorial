<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="globalValidations" select="document('param:globalValidations')/*"/>
	
	<xsl:template match="/*">
	
		<validations>
			<defaultThresholds infoWarningThreshold="{$globalValidations/globalValidationSettings/attributeScoreThresholds/@infoWarningThreshold}" warningErrorThreshold="{$globalValidations/globalValidationSettings/attributeScoreThresholds/@warningErrorThreshold}" lowDQThreshold="{$globalValidations/globalValidationSettings/recordScoreThresholds/@lowDQThreshold}" highDQThreshold="{$globalValidations/globalValidationSettings/recordScoreThresholds/@highDQThreshold}"/>
			
			<instanceLayer>
				<entities>
					<xsl:for-each select="instanceModel/tables/table[guiTab/guiValidations/@enable='true' or columns/column/@enableValidation='true' or specialColumns/column/@enableValidation='true' or matchingTab/matchingTabColumns/column/@enableValidation='true']">
						<entity name="{@name}">
							<validationService>
								<entityDQI>
									<xsl:for-each select="guiTab/guiValidations[@enable='true']/validationScoColumns/validationScoColumn">
										<column name="{@columns}" type="sco">
											<xsl:variable name="highDQThreshold" select="../../validationTabOverrideDefaultSettings/@highDQThreshold"/>
											<xsl:variable name="lowDQThreshold" select="../../validationTabOverrideDefaultSettings/@lowDQThreshold"/>
											<xsl:if test="$highDQThreshold!='' or $lowDQThreshold!=''">
												<severity>
													<xsl:if test="$lowDQThreshold!=''">
														<xsl:attribute name="lowDQThreshold" select="$lowDQThreshold"/>
													</xsl:if>												
													<xsl:if test="$highDQThreshold!=''">
														<xsl:attribute name="highDQThreshold" select="$highDQThreshold"/>
													</xsl:if>	
												</severity>														
											</xsl:if>										
										</column>
									</xsl:for-each>
									<xsl:for-each select="guiTab/guiValidations[@enable='true']/validationExpColumns/validationExpColumn">
										<column name="{@name}" type="exp">
											<xsl:if test="validationTabKeys/validationTabKey[@enable='true']">
												<filter>
													<keys>
														<xsl:for-each select="validationTabKeys/validationTabKey[@enable='true']">
															<key name="{@name}">
																<xsl:if test="@quality!=''">
																	<xsl:attribute name="quality" select="upper-case(@quality)"/>	
																</xsl:if>
															</key>
														</xsl:for-each>
													</keys>
												</filter>
											</xsl:if>
										</column>
									</xsl:for-each>
								</entityDQI>							
								
								<columnDefinitions>
									<!-- SCO -->
									<xsl:for-each select="columns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customScoColumn='' and @isSco='true']">
										<column name="sco_{@name}" type="sco">
											<columns>
												<column name="src_{@name}" />
											</columns>
											<severity infoWarningThreshold="{sf:nvl(validations/overrideDefaultSettings/@infoWarningThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@infoWarningThreshold)}" warningErrorThreshold="{sf:nvl(validations/overrideDefaultSettings/@warningErrorThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@warningErrorThreshold)}" />
										</column>										
									</xsl:for-each>
									
									<!-- EXP -->
									<xsl:for-each select="columns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customExpColumn='' and @isExp='true']">
										<xsl:variable name="mainColumn" select="@name"/>
										<column name="exp_{@name}" type="exp">
											<xsl:if test="validations/validationKeys/validationKey[@enable='true']">
												<filter>
													<keys>
														<xsl:for-each select="validations/validationKeys/validationKey[@enable='true']">
															<key name="{@name}">
																<xsl:if test="@severity!=''">
																	<xsl:attribute name="severity" select="@severity"/>
																</xsl:if>
																<xsl:if test="validationColumns/validationColumn">
																	<columns>
																		<xsl:for-each select="validationColumns/validationColumn">
																			<column name="{@name}"/>
																			<column name="src_{$mainColumn}"/>
																		</xsl:for-each>
																	</columns>																
																</xsl:if>
															</key>
														</xsl:for-each>
													</keys>
												</filter>
											</xsl:if>
											<!-- <xsl:if test="count(not(validations/validationKeys/validationKey[@enable='true']/validationColumns/validationColumn))&gt;0"> -->
												<columns>
													<column name="src_{@name}" />
												</columns>
											<!-- </xsl:if> -->
										</column>										
									</xsl:for-each>	
									
									<xsl:variable name="cols" select="columns"/>
									<!-- SCO -->
									<xsl:for-each select="columns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customScoColumn!='' and not(validations/overrideDefaultSettings/@customScoColumn=preceding-sibling::column/validations/overrideDefaultSettings/@customScoColumn)]">
										<xsl:variable name="customScoColumn" select="validations/overrideDefaultSettings/@customScoColumn"/>
										<column name="{validations/overrideDefaultSettings/@customScoColumn}" type="sco">
											<columns>
												<xsl:for-each select="$cols/column[validations/overrideDefaultSettings/@customScoColumn=$customScoColumn]">
													<column name="src_{@name}"/>
												</xsl:for-each>
											</columns>
											<severity infoWarningThreshold="{sf:nvl(validations/overrideDefaultSettings/@infoWarningThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@infoWarningThreshold)}" warningErrorThreshold="{sf:nvl(validations/overrideDefaultSettings/@warningErrorThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@warningErrorThreshold)}" />
										</column>										
									</xsl:for-each>
									
									<!-- EXP -->
									<xsl:for-each select="columns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customExpColumn!='' and not(validations/overrideDefaultSettings/@customExpColumn=preceding-sibling::column/validations/overrideDefaultSettings/@customExpColumn)]">
										<xsl:variable name="customExpColumn" select="validations/overrideDefaultSettings/@customExpColumn"/>
										<column name="{validations/overrideDefaultSettings/@customExpColumn}" type="exp">
											<xsl:if test="$cols/column[validations/overrideDefaultSettings/@customExpColumn=$customExpColumn]/validations/validationKeys/validationKey[@enable='true']">
												<filter>
													<keys>
														<xsl:for-each select="$cols/column[validations/overrideDefaultSettings/@customExpColumn=$customExpColumn]/validations/validationKeys/validationKey[@enable='true' and not(@name=preceding::column[validations/overrideDefaultSettings/@customExpColumn=$customExpColumn]/validations/validationKeys/validationKey[@enable='true']/@name)]">	
															<xsl:variable name="thisKey" select="@name"/>
															<key name="{@name}">	
																<xsl:if test="@severity!=''">
																	<xsl:attribute name="severity" select="@severity"/>
																</xsl:if>																										
																<columns>
																	<xsl:for-each select="$cols/column[validations/overrideDefaultSettings/@customExpColumn=$customExpColumn and validations/validationKeys/validationKey[@enable='true']/@name=$thisKey]">
																		<column name="src_{@name}"/>
																	</xsl:for-each>
																</columns>																
															</key>		
														</xsl:for-each>
													</keys>
												</filter>
											</xsl:if>	
											
											<!--<columns>
												<xsl:for-each select="$cols/column[validations/overrideDefaultSettings/@customExpColumn=$customExpColumn]">
													<column name="src_{@name}"/>
												</xsl:for-each>
											</columns>-->	
												
										</column>										
									</xsl:for-each>	
									
									<!-- SPECIAL COLUMNS -->
									<!-- SCO -->
									<xsl:for-each select="advanced/specialColumns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customScoColumn!='']">
										<column name="{@validations/overrideDefaultSettings/@customScoColumn}" type="sco">
											<columns>
												<column name="{@name}" />
											</columns>
											<severity infoWarningThreshold="{sf:nvl(validations/overrideDefaultSettings/@infoWarningThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@infoWarningThreshold)}" warningErrorThreshold="{sf:nvl(validations/overrideDefaultSettings/@warningErrorThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@warningErrorThreshold)}" />
										</column>										
									</xsl:for-each>									
									
									<!-- EXP -->
									<xsl:for-each select="advanced/specialColumns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customExpColumn!='']">
										<xsl:variable name="mainColumn" select="@name"/>
										<column name="{@validations/overrideDefaultSettings/@customExpColumn}" type="exp">
											<xsl:if test="validations/validationKeys/validationKey[@enable='true']">
												<filter>
													<keys>
														<xsl:for-each select="validations/validationKeys/validationKey[@enable='true']">
															<key name="{@name}">
																<xsl:if test="@severity!=''">
																	<xsl:attribute name="severity" select="@severity"/>
																</xsl:if>														
																<xsl:if test="validationColumns/validationColumn">
																	<columns>
																		<xsl:for-each select="validationColumns/validationColumn">
																			<column name="{@name}"/>
																			<column name="src_{$mainColumn}"/>
																		</xsl:for-each>
																	</columns>																
																</xsl:if>
															</key>
														</xsl:for-each>
													</keys>
												</filter>
											</xsl:if>
											
											<columns>
												<column name="{@name}" />
											</columns>
											
										</column>										
									</xsl:for-each>										

									<!-- MATCHING COLUMNS -->
									<!-- SCO -->
									<xsl:for-each select="matchingTab/matchingTabColumns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customScoColumn!='']">
										<column name="{@validations/overrideDefaultSettings/@customScoColumn}" type="sco">
											<columns>
												<column name="{@name}" />
											</columns>
											<severity infoWarningThreshold="{sf:nvl(validations/overrideDefaultSettings/@infoWarningThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@infoWarningThreshold)}" warningErrorThreshold="{sf:nvl(validations/overrideDefaultSettings/@warningErrorThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@warningErrorThreshold)}" />
										</column>										
									</xsl:for-each>									
									
									<!-- EXP -->
									<xsl:for-each select="matchingTab/matchingTabColumns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customExpColumn!='']">
										<xsl:variable name="mainColumn" select="@name"/>
										<column name="{@validations/overrideDefaultSettings/@customExpColumn}" type="exp">
											<xsl:if test="validations/validationKeys/validationKey[@enable='true']">
												<filter>
													<keys>
														<xsl:for-each select="validations/validationKeys/validationKey[@enable='true']">
															<key name="{@name}">
																<xsl:if test="@severity!=''">
																	<xsl:attribute name="severity" select="@severity"/>
																</xsl:if>														
																<xsl:if test="validationColumns/validationColumn">
																	<columns>
																		<xsl:for-each select="validationColumns/validationColumn">
																			<column name="{@name}"/>
																			<column name="src_{$mainColumn}"/>
																		</xsl:for-each>
																	</columns>																
																</xsl:if>
															</key>
														</xsl:for-each>
													</keys>
												</filter>
											</xsl:if>
											
											<columns>
												<column name="{@name}" />
											</columns>
											
										</column>										
									</xsl:for-each>		
																																		
								</columnDefinitions>
							</validationService>
						</entity>
					</xsl:for-each>
				</entities>
			</instanceLayer>
		
			<masterLayers>

				<xsl:for-each select="masterModels/masterModel[masterTables/masterTable/guiTab/guiValidations/@enable='true' or masterTables/masterTable/columns/column/@enableValidation='true' or masterTables/masterTable/advanced/specialColumns/column/@enableValidation='true']">
					<masterLayer name="{@name}">
						<entities>
							<xsl:for-each select="masterTables/masterTable[columns/column/@enableValidation='true' or advanced/specialColumns/column/@enableValidation='true']">
								<entity name="{@name}">
									<validationService>
										<entityDQI>
											<xsl:for-each select="guiTab/guiValidations[@enable='true']/validationScoColumns/validationScoColumn">
												<column name="{@columns}" type="sco">
													<xsl:variable name="highDQThreshold" select="../../validationTabOverrideDefaultSettings/@highDQThreshold"/>
													<xsl:variable name="lowDQThreshold" select="../../validationTabOverrideDefaultSettings/@lowDQThreshold"/>
													<xsl:if test="$highDQThreshold!='' or $lowDQThreshold!=''">
														<severity>
															<xsl:if test="$lowDQThreshold!=''">
																<xsl:attribute name="lowDQThreshold" select="$lowDQThreshold"/>
															</xsl:if>												
															<xsl:if test="$highDQThreshold!=''">
																<xsl:attribute name="highDQThreshold" select="$highDQThreshold"/>
															</xsl:if>
														</severity>														
													</xsl:if>	
												</column>
											</xsl:for-each>
											<xsl:for-each select="guiTab/guiValidations[@enable='true']/validationExpColumns/validationExpColumn">
												<column name="{@name}" type="exp">
													<xsl:if test="validationTabKeys/validationTabKey[@enable='true']">
														<filter>
															<keys>
																<xsl:for-each select="validationTabKeys/validationTabKey[@enable='true']">
																	<key name="{@name}">
																		<xsl:if test="@quality!=''">
																			<xsl:attribute name="quality" select="upper-case(@quality)"/>
																		</xsl:if>	
																	</key>
																</xsl:for-each>
															</keys>
														</filter>
													</xsl:if>
												</column>
											</xsl:for-each>
										</entityDQI>	
										
										<columnDefinitions>
											<!-- SCO -->
											<xsl:for-each select="columns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customScoColumn='' and @isSco='true']">
												<column name="sco_{@name}" type="sco">
													<columns>
														<column name="cmo_{@name}" />
													</columns>
													<severity infoWarningThreshold="{sf:nvl(validations/overrideDefaultSettings/@infoWarningThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@infoWarningThreshold)}" warningErrorThreshold="{sf:nvl(validations/overrideDefaultSettings/@warningErrorThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@warningErrorThreshold)}" />
												</column>										
											</xsl:for-each>
											
											<!-- EXP -->
											<xsl:for-each select="columns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customExpColumn='' and @isExp='true']">
												<xsl:variable name="mainColumn" select="@name"/>
												<column name="exp_{@name}" type="exp">
													<xsl:if test="validations/validationKeys/validationKey">
														<filter>
															<keys>
																<xsl:for-each select="validations/validationKeys/validationKey">
																	<key name="{@name}">
																		<xsl:if test="@severity!=''">
																			<xsl:attribute name="severity" select="@severity"/>
																		</xsl:if>
																		<xsl:if test="validationColumns/validationColumn">
																			<columns>
																				<xsl:for-each select="validationColumns/validationColumn">
																					<column name="{@name}"/>
																					<column name="cmo_{$mainColumn}"/>
																				</xsl:for-each>
																			</columns>																
																		</xsl:if>
																	</key>
																</xsl:for-each>
															</keys>
														</filter>
													</xsl:if>
													
													<columns>
														<column name="cmo_{@name}" />
													</columns>
													
												</column>										
											</xsl:for-each>	
											
											<xsl:variable name="masCols" select="columns"/>
											<!-- SCO -->
											<xsl:for-each select="columns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customScoColumn!='' and not(validations/overrideDefaultSettings/@customScoColumn=preceding-sibling::column/validations/overrideDefaultSettings/@customScoColumn)]">
												<xsl:variable name="masCustomScoColumn" select="validations/overrideDefaultSettings/@customScoColumn"/>
												<column name="{validations/overrideDefaultSettings/@customScoColumn}" type="sco">
													<columns>
														<xsl:for-each select="$masCols/column[validations/overrideDefaultSettings/@customScoColumn=$masCustomScoColumn]">
															<column name="cmo_{@name}"/>
														</xsl:for-each>
													</columns>
													<severity infoWarningThreshold="{sf:nvl(validations/overrideDefaultSettings/@infoWarningThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@infoWarningThreshold)}" warningErrorThreshold="{sf:nvl(validations/overrideDefaultSettings/@warningErrorThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@warningErrorThreshold)}" />
												</column>										
											</xsl:for-each>
											
											
											<!-- EXP -->
											<xsl:for-each select="columns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customExpColumn!='' and not(validations/overrideDefaultSettings/@customExpColumn=preceding-sibling::column/validations/overrideDefaultSettings/@customExpColumn)]">
												<xsl:variable name="masCustomExpColumn" select="validations/overrideDefaultSettings/@customExpColumn"/>
												<xsl:variable name="mainColumn" select="@name"/>
												<column name="{validations/overrideDefaultSettings/@customExpColumn}" type="exp">
													<xsl:if test="$masCols/column[validations/overrideDefaultSettings/@customExpColumn=$masCustomExpColumn]/validations/validationKeys/validationKey[@enable='true']">
														<filter>
															<keys>
																<xsl:for-each select="$masCols/column[validations/overrideDefaultSettings/@customExpColumn=$masCustomExpColumn]">
																	<xsl:if test="not(validations/validationKeys/validationKey[@enable='true']/@name=preceding-sibling::column/validations/validationKeys/validationKey[@enable='true']/@name)">
																		<xsl:for-each select="validations/validationKeys/validationKey[@enable='true']">
																			<xsl:variable name="masThisKey" select="@name"/>
																			<key name="{@name}">	
																				<xsl:if test="@severity!=''">
																					<xsl:attribute name="severity" select="@severity"/>
																				</xsl:if>																										
																				<columns>
																					<xsl:for-each select="$masCols/column[validations/overrideDefaultSettings/@customExpColumn=$masCustomExpColumn and validations/validationKeys/validationKey[@enable='true']/@name=$masThisKey]">
																						<column name="cmo_{@name}"/>
																					</xsl:for-each>
																				</columns>																
																			</key>	
																		</xsl:for-each>
																	</xsl:if>
																	<xsl:if test="validations/validationKeys/validationKey/@name=preceding-sibling::column/validations/validationKeys/validationKey[@enable='true']/@name">
																		<xsl:for-each select="validations/validationKeys/validationKey[@enable='true'][1]">
																			<xsl:variable name="masThisKey" select="@name"/>
																			<key name="{@name}">	
																				<xsl:if test="@severity!=''">
																					<xsl:attribute name="severity" select="@severity"/>
																				</xsl:if>																										
																				<columns>
																					<xsl:for-each select="$masCols/column[validations/overrideDefaultSettings/@customExpColumn=$masCustomExpColumn and validations/validationKeys/validationKey[@enable='true']/@name=$masThisKey]">
																						<column name="cmo_{@name}"/>
																					</xsl:for-each>
																				</columns>																
																			</key>
																		</xsl:for-each>
																	</xsl:if>
																</xsl:for-each>
															</keys>
														</filter>
													</xsl:if>
													
													<!--<columns>
														<xsl:for-each select="$masCols/column[validations/overrideDefaultSettings/@customExpColumn=$masCustomExpColumn]">
															<column name="cmo_{@name}"/>
														</xsl:for-each>
													</columns>-->	
																						
												</column>										
											</xsl:for-each>	
											
											<!-- SPECIAL COLUMNS -->
											<!-- SCO -->
											<xsl:for-each select="advanced/specialColumns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customScoColumn!='']">
												<column name="{@validations/overrideDefaultSettings/@customScoColumn}" type="sco">
													<columns>
														<column name="{@name}" />
													</columns>
													<severity infoWarningThreshold="{sf:nvl(validations/overrideDefaultSettings/@infoWarningThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@infoWarningThreshold)}" warningErrorThreshold="{sf:nvl(validations/overrideDefaultSettings/@warningErrorThreshold,$globalValidations/globalValidationSettings/attributeScoreThresholds/@warningErrorThreshold)}" />
												</column>										
											</xsl:for-each>									
											
											<!-- EXP -->
											<xsl:for-each select="advanced/specialColumns/column[@enableValidation='true' and validations/overrideDefaultSettings/@customExpColumn!='']">
												<xsl:variable name="mainColumn" select="@name"/>
												<column name="{@validations/overrideDefaultSettings/@customExpColumn}" type="exp">
													<xsl:if test="validations/validationKeys/validationKey">
														<filter>
															<keys>
																<xsl:for-each select="validations/validationKeys/validationKey">
																	<key name="{@name}">
																		<xsl:if test="@severity!=''">
																			<xsl:attribute name="severity" select="@severity"/>
																		</xsl:if>														
																		<xsl:if test="validationColumns/validationColumn">
																			<columns>
																				<xsl:for-each select="validationColumns/validationColumn">
																					<column name="{@name}"/>
																					<column name="cmo_{$mainColumn}"/>
																				</xsl:for-each>
																			</columns>																
																		</xsl:if>
																	</key>
																</xsl:for-each>
															</keys>
														</filter>
													</xsl:if>
													
													<columns>
														<column name="{@name}" />
													</columns>
													
												</column>										
											</xsl:for-each>										
																																				
										</columnDefinitions>
									</validationService>									
								</entity>
							</xsl:for-each>
						</entities>
					</masterLayer>
				</xsl:for-each>
			</masterLayers>
		
			<messagesMappings class="com.ataccama.mda.core.validations.MdaValidationsFileMessagesMappings">
				<messagesFiles>
					<messageFile fileName="mda-validation_messages.properties" language="en" />
				</messagesFiles>
			</messagesMappings>
			
			<keysTokenizer />
			
		</validations>
		
	</xsl:template>
</xsl:stylesheet>