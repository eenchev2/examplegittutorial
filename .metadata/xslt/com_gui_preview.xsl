<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	
<xsl:param name="databaseModel" select="document('param:databaseModel')/*"/> <!-- /preview/databaseModel -->
<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>

<!-- Bound to /logicalModel -->
<xsl:template match="/logicalModel">
	<guiPreview>
		<masterModels>
			<!-- <xsl:apply-templates select="masterModels/masterModel[@enableGui='true']"> -->
			<xsl:apply-templates select="masterModels/masterModel">
				<xsl:with-param name="instanceTables" select="/logicalModel/instanceModel/tables"/>
				<xsl:with-param name="dicTables" select="/logicalModel/dictionary/tables"/>
			</xsl:apply-templates>
		</masterModels>
		<instanceModel label="{@label}">	
			<tables>
				<xsl:apply-templates select="instanceModel/tables/table" mode="instance">
					<xsl:with-param name="instanceTables" select="/logicalModel/instanceModel/tables"/>
					<xsl:with-param name="masterInstanceTables" select="/logicalModel/masterModels/masterModel/instanceTables"/>
				</xsl:apply-templates>	
			</tables>			
		</instanceModel>
		<sorModel label="{@label}">	
			<tables>
				<xsl:apply-templates select="sorModel/tables/table" mode="sor">
					<xsl:with-param name="sorTables" select="/logicalModel/sorModel/tables"/>
				</xsl:apply-templates>	
			</tables>			
		</sorModel>	
		<refData>	
			<tables>
				<xsl:apply-templates select="dictionary/tables/table[@loadInst='false']" mode="rd"/>
				<xsl:apply-templates select="dictionary/tables/table[@loadInst='true']" mode="rd"/>
			</tables>			
		</refData>				
  </guiPreview>
</xsl:template>

<xsl:template match="masterModel">
<xsl:param name="instanceTables"/>
<xsl:param name="dicTables"/>
	<model name="{lower-case(@name)}" label="{@label}">
		<xsl:variable name="modelName" select="lower-case(@name)"/> 
		<masterTables>
			<xsl:apply-templates select="masterTables/masterTable" >
				<xsl:with-param name="modelName" select="$modelName"/>
				<xsl:with-param name="relationships" select="relationships"/>
			</xsl:apply-templates>
		</masterTables>
		<instanceTables>
			<xsl:apply-templates select="instanceTables/instanceTable" >	
				<xsl:with-param name="instanceTables" select="$instanceTables"/>				
				<xsl:with-param name="modelName" select="$modelName"/>
			</xsl:apply-templates>
		</instanceTables>
		<!-- add rd_ tables into GUI preview-->
		<dictionaryTables>
			<!-- rewrite into apply template -->
			<xsl:variable name="refData" select="masterTables/masterTable/columns/column/@refData"/>	
			<xsl:for-each select="$dicTables/table[@loadMas='true' and @name=$refData]">
				<xsl:variable name="dicTableName" select="concat('rd_',@name)"/>
				<!-- label="{if(@label='') then @name else @label}" -->
				<dictionaryTable name="rd_{lower-case(@name)}" label="{@label}" topLevel="false">
					<columns>
						<xsl:for-each select="$databaseModel/refData/physicalTable[@name=lower-case($dicTableName)]/columns/column[@load='true']">
							<xsl:variable name="columnPrefix" select="fn:replace(@name,'^(eng|src|std|cio|exp|sco|dic)_(.+)$','$1')"/>
							<xsl:if test="$columnPrefix != 'eng'">
								<column name="{lower-case(@name)}" type="{if(@type='long_int') then 'long' else @type}" label="{@label}"/>
							</xsl:if>
						</xsl:for-each> 
					</columns>
				</dictionaryTable>
			</xsl:for-each> 
		</dictionaryTables>
		
		<relationships>
			<xsl:apply-templates select="relationships/relationship"> 
				<xsl:with-param name="instance" select="instanceTables"/>
				<xsl:with-param name="master" select="masterTables"/>
			</xsl:apply-templates>
			<!-- add relationships for rd_ tables into GUI preview-->
			<xsl:variable name="masterTable" select="masterTables/masterTable"/>
			<xsl:for-each select="$databaseModel/masterTables/physicalTable[@layerName=$modelName]">
			<xsl:variable name="child" select="@name"/> 
			<xsl:for-each select="relationships/relationship[substring(@parentTable,1,3)='rd_']"> <!-- [substring(@parentTable,1,3)='rd_']-->
				<xsl:variable name="childLabel" select="$masterTable[@name=$child]/@label"/>
				<xsl:variable name="parentLabel" select="substring(@parentTable,4)"/>
				<xsl:variable name="refData" select="masterTables/masterTable/columns/column/@refData"/>	
				<xsl:variable name="dicTableLabel" select="$dicTables/table[@loadMas='true' and @name=$parentLabel]/@label"/>
				<relationship name="{lower-case(@name)}" label="{$parentLabel} is used on {$childLabel}/{@childColumn}" parentRole="master_code" childRole="master_details" parentTable="{@parentTable}" childTable="{@childTable}" parentFk="{@parentColumn}" childFk="{@childColumn}" 
				parentLabel="{if($dicTableLabel='') then $parentLabel else $dicTableLabel}"				
				childLabel="{$childLabel}"/>
				<!-- parentLabel="{if($dicTableLabel='') then @parentLabel else $dicTableLabel}"	 -->
			</xsl:for-each>
			</xsl:for-each> 
			
		</relationships>	
		<mnRelationships>
			<xsl:for-each-group select="guiConfig/hierarchies/hierarchyMN" group-by="@tableMN">
				<mnRelationship name="{@tableMN}" label="{hierarchyDisplaySettings/relationshipLabel}" />	
			</xsl:for-each-group> 	
		</mnRelationships>
	</model>
</xsl:template>

<xsl:template match="masterTable">
	<xsl:param name="modelName"/>
	<xsl:param name="relationships"/>
	<xsl:variable name="currentTableName" select="@name"/>
	<masterTable name="{lower-case(@name)}" label="{@label}" topLevel="{@topLevel}" genView="{if(guiTab/views/breadcrumbView) then 'true' else 'false'}" genTemplate="{if(guiTab/views/detailView) then 'true' else 'false'}">
		<xsl:variable name="instTableName" select="@instanceTable"/>
		<columns>
			<column name="id" type="long" label="ID" indexType="NOT_ANALYZED"/>	
			<column name="eng_active" type="boolean" label="Active" indexType="NOT_ANALYZED"/>
			<column name="eng_last_update_date" type="datetime" label="Last Update Date" indexType="NOT_ANALYZED"/>	
			
			<xsl:apply-templates select="$databaseModel/masterTables/physicalTable[@name=lower-case($currentTableName) and @layerName=lower-case($modelName)]/columns/column[@origin!='internal' and @name!='id']" mode="master">				
				<xsl:with-param name="instTableName" select="$instTableName"/>
			</xsl:apply-templates>
			<xsl:apply-templates select="guiTab/computedColumns/column" mode="computed_m"/>
		</columns>
		<templates>
			<xsl:for-each select="guiTab/views/*">
				<template>
					<xsl:attribute name="templateType">
			      		<xsl:value-of select="name(.)"/>
			      	</xsl:attribute> 
					<detailTemplates>
						<xsl:for-each select="groups/*">
							<xsl:variable name="relationshipName" select="@relationship"/>
							<detailTemplate name="{@name}" relationship="{@relationship}" templatePosition="{@templatePosition}" parentRole="{$relationships/relationship[@name=$relationshipName]/@parentRole}" childRole="{$relationships/relationship[@name=$relationshipName]/@childRole}">
								<xsl:attribute name="templateType">
			      					<xsl:value-of select="name(.)"/>
			      				</xsl:attribute>
							</detailTemplate>
						</xsl:for-each>
					</detailTemplates>
				</template>
			</xsl:for-each>
		</templates>
	</masterTable>
</xsl:template>

<xsl:template match="instanceTable">
	<xsl:param name="instanceTables"/>
	<xsl:param name="modelName"/>
	<xsl:variable name="tableName" select="@name"/>
	<instanceTable name="{lower-case(@name)}" instanceName="{lower-case(@instanceTable)}" label="{@label}" topLevel="{@topLevel}" genView="{if(guiTab/views/breadcrumbView) then 'true' else 'false'}" genTemplate="{if(guiTab/views/detailView) then 'true' else 'false'}">	
		<xsl:variable name="instanceTableName" select="lower-case(@instanceTable)"/>
		<xsl:variable name="customLabel" select="labelDefinition/columns/column[@name=current()/@name]/@customLabel"/>
		<columns>	
			<column name="id" type="long" finalLabel="ID" indexType="NOT_ANALYZED"/>
			<column name="eng_origin" type="string" finalLabel="Origin" indexType="NOT_ANALYZED"/>
			<column name="source_id" type="string" finalLabel="Source ID" indexType="NOT_ANALYZED"/>			
			<column name="eng_active" type="boolean" finalLabel="Active" indexType="NOT_ANALYZED"/>
			<column name="eng_source_system" type="string" finalLabel="Source System" indexType="NOT_ANALYZED"/>
			<column name="eng_last_update_date" type="datetime" finalLabel="Last Update Date" indexType="NOT_ANALYZED"/>
			<column name="eng_source_timestamp" type="datetime" finalLabel="Source Timestamp" indexType="NOT_ANALYZED"/>														
			
			<xsl:apply-templates select="$databaseModel/instanceTables/physicalTable[@name=$instanceTableName]/columns/column[@origin!='internal' and @name!='id']" mode="instance">
				<xsl:with-param name="instanceTableName" select="$instanceTableName"/>
				<xsl:with-param name="labelDefinition" select="."/>
				<xsl:with-param name="instanceTables" select="$instanceTables"/>
				<xsl:with-param name="indexType" select="'BOTH'"/>
				<xsl:with-param name="modelName" select="$modelName"/>
				<xsl:with-param name="tableName" select="$tableName"/>
			</xsl:apply-templates>
			<xsl:apply-templates select="guiTab/computedColumns/column" mode="computed_i"/>			
		</columns>
		<templates>
			<xsl:for-each select="guiTab/views/*">
				<template>
					<xsl:attribute name="templateType">
			      		<xsl:value-of select="name(.)"/>
			      	</xsl:attribute> 
					<detailTemplates>
						<xsl:for-each select="groups/*">
							<detailTemplate name="{@name}" relationship="{@relationship}" templatePosition="{@templatePosition}">
								<xsl:attribute name="templateType">
			      					<xsl:value-of select="name(.)"/>
			      				</xsl:attribute>
							</detailTemplate>
						</xsl:for-each>
					</detailTemplates>
				</template>
			</xsl:for-each>
		</templates>
	</instanceTable>
</xsl:template>

<xsl:template match="table" mode="instance">
	<xsl:param name="instanceTables"/>
	<xsl:param name="masterInstanceTables"/>
	<table name="{lower-case(@name)}" label="{@label}" topLevel="{@topLevel}" genView="{if(guiTab/views/breadcrumbView) then 'true' else 'false'}" genTemplate="{if(guiTab/views/detailView) then 'true' else 'false'}">
		<xsl:variable name="instanceTableName" select="lower-case(@name)"/>
		<xsl:variable name="customLabel" select="labelDefinition/columns/column[@name=current()/@name]/@customLabel"/>
		<columns>
			<column name="id" type="long" finalLabel="ID" indexType="NOT_ANALYZED"/>
			<column name="eng_origin" type="string" finalLabel="Origin" indexType="NOT_ANALYZED"/>
			<column name="source_id" type="string" finalLabel="Source ID" indexType="NOT_ANALYZED"/>							
			<column name="eng_active" type="boolean" finalLabel="Active" indexType="NOT_ANALYZED"/>
			<column name="eng_source_system" type="string" finalLabel="Source System" indexType="NOT_ANALYZED"/>
			<column name="eng_last_update_date" type="datetime" finalLabel="Last Update Date" indexType="NOT_ANALYZED"/>
			<column name="eng_source_timestamp" type="datetime" finalLabel="Source Timestamp" indexType="NOT_ANALYZED"/>				
			
			<xsl:apply-templates select="$databaseModel/instanceTables/physicalTable[@name=lower-case(current()/@name)]/columns/column[@origin!='internal' and @name!='id']" mode="instance">
				<xsl:with-param name="instanceTableName" select="$instanceTableName"/>
				<xsl:with-param name="labelDefinition" select="."/>
				<xsl:with-param name="instanceTables" select="$instanceTables"/>
				<xsl:with-param name="indexType" select="'BOTH'"/>
			</xsl:apply-templates>
			<xsl:apply-templates select="guiTab/computedColumns/column" mode="computed_i"/>
		</columns>
		<templates>
			<xsl:for-each select="guiTab/views/*">
				<template>
					<xsl:attribute name="templateType">
			      		<xsl:value-of select="name(.)"/>
			      	</xsl:attribute> 
					<detailTemplates>
						<xsl:for-each select="groups/*">
							<detailTemplate name="{@name}" relationship="{@relationship}" templatePosition="{@templatePosition}">
								<xsl:attribute name="templateType">
			      					<xsl:value-of select="name(.)"/>
			      				</xsl:attribute>
							</detailTemplate>
						</xsl:for-each>
					</detailTemplates>
				</template>
			</xsl:for-each>
		</templates>
	</table>
</xsl:template>

<xsl:template match="table" mode="sor">
	<xsl:param name="sorTables"/>
	<table name="{lower-case(@name)}" label="{@label}" topLevel="{@topLevel}" genView="{if(guiTab/views/breadcrumbView) then 'true' else 'false'}" genTemplate="{if(guiTab/views/detailView) then 'true' else 'false'}">>	
		<xsl:variable name="sorTableName" select="lower-case(@name)"/>
		<xsl:variable name="customLabel" select="labelDefinition/columns/column[@name=current()/@name]/@customLabel"/>
		<columns>
			<column name="id" type="long" finalLabel="ID" indexType="NOT_ANALYZED"/>
			<column name="eng_origin" type="string" finalLabel="Origin" indexType="NOT_ANALYZED"/>
			<column name="source_id" type="string" finalLabel="Source ID" indexType="NOT_ANALYZED"/>							
			<column name="eng_active" type="boolean" finalLabel="Active" indexType="NOT_ANALYZED"/>
			<column name="eng_source_system" type="string" finalLabel="Source System" indexType="NOT_ANALYZED"/>
			<column name="eng_last_update_date" type="datetime" finalLabel="Last Update Date" indexType="NOT_ANALYZED"/>
			<column name="eng_source_timestamp" type="datetime" finalLabel="Source Timestamp" indexType="NOT_ANALYZED"/>			
				
			<xsl:apply-templates select="$databaseModel/sorTables/physicalTable[@name=lower-case(current()/@name)]/columns/column[@origin!='internal' and @name!='id']" mode="sor">
				<xsl:with-param name="sorTableName" select="$sorTableName"/>
				<xsl:with-param name="labelDefinition" select="."/>
				<xsl:with-param name="sorTables" select="$sorTables"/>
				<xsl:with-param name="indexType" select="'BOTH'"/>
			</xsl:apply-templates>
			<xsl:apply-templates select="guiTab/computedColumns/column" mode="computed_i"/>	
		</columns>
		<templates>
			<xsl:for-each select="guiTab/views/*">
				<template>
					<xsl:attribute name="templateType">
			      		<xsl:value-of select="name(.)"/>
			      	</xsl:attribute> 
					<detailTemplates>
						<xsl:for-each select="groups/*">
							<detailTemplate name="{@name}" relationship="{@relationship}" templatePosition="{@templatePosition}">
								<xsl:attribute name="templateType">
			      					<xsl:value-of select="name(.)"/>
			      				</xsl:attribute>
							</detailTemplate>
						</xsl:for-each>
					</detailTemplates>
				</template>
			</xsl:for-each>
		</templates>
	</table>
</xsl:template>

<xsl:template match="table" mode="rd">
	<xsl:choose>
		<xsl:when test="@loadInst='false'">
			<table name="rd_{lower-case(@name)}" label="{@label}" loadInst="false" loadMas="{@loadMas}" genView="{if(guiTab/views/breadcrumbView) then 'true' else 'false'}" genTemplate="{if(guiTab/views/detailView) then 'true' else 'false'}"/>
		</xsl:when>
		<xsl:otherwise>
			<table name="rd_{lower-case(@name)}" label="{@label}" loadInst="false" loadMas="{@loadMas}" genView="{if(guiTab/views/breadcrumbView) then 'true' else 'false'}" genTemplate="{if(guiTab/views/detailView) then 'true' else 'false'}"/>
			<table name="rd_{lower-case(@name)}_trans" label="{@label} Translation" loadInst="true" loadMas="{@loadMas}" genView="{if(guiTab/views/breadcrumbView) then 'true' else 'false'}" genTemplate="{if(guiTab/views/detailView) then 'true' else 'false'}"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="column" mode="master">
	<xsl:param name="instTableName"/>
	<xsl:variable name="currentColumn" select="@name"/>
	<xsl:variable name="instLabel" select="$logicalModel/instanceModel/tables/table[@name=$instTableName]/columns/column[@name=$currentColumn]/@label"/>
	<xsl:variable name="columnPrefix" select="fn:replace(@name,'^(exp|sco)_(.+)$','$1')"/> 
	<xsl:choose>
		<xsl:when test="@isSco='true' or @isExp='true'">
			<xsl:if test="@label!=''">
				<column name="{fn:lower-case(@name)}" type="{if(@type='long_int') then 'long' else @type}" label="{@label} ({$columnPrefix})" indexType="{if(@indexType!='') then @indexType else ''}"/>
			</xsl:if>
		</xsl:when>
		<xsl:otherwise>
			<column name="{fn:lower-case(@name)}" type="{if(@type='long_int') then 'long' else @type}" label="{sf:nvl(@label,$instLabel)}" indexType="{if(@indexType!='') then @indexType else ''}"/>			
		</xsl:otherwise>
	</xsl:choose>	
</xsl:template>

<xsl:template match="column" mode="instance">
	<xsl:param name="instanceTableName"/>
	<xsl:param name="labelDefinition"/>
	<xsl:param name="instanceTables"/>
	<xsl:param name="indexType"/>
	<xsl:param name="rc"/>
	<xsl:param name="modelName"/>
	<xsl:param name="tableName"/>
	<xsl:variable name="currentColumn" select="@name"/>
	<xsl:variable name="curIndexType">
		<xsl:choose>
			<xsl:when test="$rc[@name=$currentColumn]/@indexType!=''">
				<xsl:value-of select="$rc[@name=$currentColumn]/@indexType"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="''"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>		
	<xsl:variable name="columnName" select="fn:replace(@name,'^(src|std|cio|exp|sco|dic)_(.+)$','$2')"/> 
	<xsl:variable name="columnPrefix" select="fn:replace(@name,'^(eng|src|std|cio|exp|sco|dic)_(.+)$','$1')"/> 
	<!--<xsl:variable name="instanceLabel" select="/logicalModel/instanceModel/tables/table[@name=$instanceTableName]/columns/column[@name=$columnName]/@label"/>-->
	<xsl:variable name="instanceLabel" select="$instanceTables/table[lower-case(@name)=lower-case($instanceTableName)]/columns/column[@name=$columnName]/@label"/>
	<xsl:variable name="matLabel" select="$instanceTables/table[lower-case(@name)=lower-case($instanceTableName)]/matchingTab/matchingTabColumns/column[@name=$currentColumn]/@label"/>
	<xsl:variable name="aggLabel" select="$instanceTables/table[lower-case(@name)=lower-case($instanceTableName)]/aggregationTab/aggregationTabColumns/aggregationTabColumn[@name=$currentColumn]/@label"/>
	<xsl:variable name="specialLabel" select="$instanceTables/table[lower-case(@name)=lower-case($instanceTableName)]/advanced/specialColumns/column[@name=$currentColumn]/@label"/>
	<xsl:variable name="historicalLabel" select="$instanceTables/table[lower-case(@name)=lower-case($instanceTableName)]/advanced/historicalColumns/column[@name=$currentColumn]/@label"/>
	<xsl:variable name="customLabel" select="$labelDefinition/columns/column[lower-case(@name)=lower-case(current()/@name)]/@customLabel"/>

	<xsl:if test="$columnPrefix != 'eng'">
		<column name="{fn:lower-case(@name)}" type="{if(@type='long_int') then 'long' else @type}" indexType="{if($indexType!='') then '' else $curIndexType}">
			<xsl:attribute name="label">
				<xsl:if test="$instanceLabel !='' and $columnName != $columnPrefix">
					<xsl:value-of select="$instanceLabel"/><xsl:text> (</xsl:text><xsl:value-of select="$columnPrefix"/><xsl:text>)</xsl:text>
				</xsl:if>
				<xsl:if test="$instanceLabel !='' and $columnName = $columnPrefix">
					<xsl:value-of select="$instanceLabel"/>
				</xsl:if>
			</xsl:attribute>
			<xsl:attribute name="customLabel">
				<xsl:value-of select="$logicalModel/masterModels/masterModel[@name=$modelName]/instanceTables/instanceTable[@name=$tableName]/columns/column[@name=$currentColumn]/@customLabel"/>
			</xsl:attribute>			
			<xsl:attribute name="finalLabel">		
				<xsl:choose>
					<xsl:when test="$customLabel != ''"><xsl:value-of select="$customLabel"/></xsl:when>
					<xsl:when test="$instanceLabel !='' and $columnName != $columnPrefix ">
						<xsl:value-of select="$instanceLabel"/><xsl:text> (</xsl:text><xsl:value-of select="$columnPrefix"/><xsl:text>)</xsl:text>
					</xsl:when>
					<xsl:when test="$instanceLabel !=''">
						<xsl:value-of select="$instanceLabel"/>
					</xsl:when>
					<xsl:when test="$matLabel !='' or $aggLabel!='' or $specialLabel!='' or $historicalLabel!=''">
						<xsl:value-of select="sf:nvl($matLabel,$aggLabel,$specialLabel,$historicalLabel)"/>
					</xsl:when>		
					<xsl:otherwise>
						<xsl:value-of select="@label"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</column>
	</xsl:if>
</xsl:template>

<xsl:template match="column" mode="sor">
	<xsl:param name="sorTableName"/>
	<xsl:param name="labelDefinition"/>
	<xsl:param name="sorTables"/>
	<xsl:param name="indexType"/>
	<xsl:param name="rc"/>
	<xsl:variable name="currentColumn" select="@name"/>
	<xsl:variable name="curIndexType">
		<xsl:choose>
			<xsl:when test="$rc[@name=$currentColumn]/@indexType!=''">
				<xsl:value-of select="$rc[@name=$currentColumn]/@indexType"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="''"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>		
	<xsl:variable name="columnName" select="fn:replace(@name,'^(src|std|cio|exp|sco|dic)_(.+)$','$2')"/> 
	<xsl:variable name="columnPrefix" select="fn:replace(@name,'^(eng|src|std|cio|exp|sco|dic)_(.+)$','$1')"/> 
	<!--<xsl:variable name="sorLabel" select="/logicalModel/sorModel/tables/table[@name=$sorTableName]/columns/column[@name=$columnName]/@label"/>-->
	<xsl:variable name="sorLabel" select="$sorTables/table[lower-case(@name)=lower-case($sorTableName)]/columns/column[@name=$columnName]/@label"/>
	<xsl:variable name="matLabel" select="$sorTables/table[lower-case(@name)=lower-case($sorTableName)]/matchingTab/matchingTabColumns/column[@name=$currentColumn]/@label"/>
	<xsl:variable name="aggLabel" select="$sorTables/table[lower-case(@name)=lower-case($sorTableName)]/aggregationTab/aggregationTabColumns/aggregationTabColumn[@name=$currentColumn]/@label"/>
	<xsl:variable name="specialLabel" select="$sorTables/table[lower-case(@name)=lower-case($sorTableName)]/advanced/specialColumns/column[@name=$currentColumn]/@label"/>
	<xsl:variable name="historicalLabel" select="$sorTables/table[lower-case(@name)=lower-case($sorTableName)]/advanced/historicalColumns/column[@name=$currentColumn]/@label"/>
	<xsl:variable name="customLabel" select="$labelDefinition/columns/column[lower-case(@name)=lower-case(current()/@name)]/@customLabel"/>

	<xsl:if test="$columnPrefix != 'eng'">
		<column name="{fn:lower-case(@name)}" customLabel="{$customLabel}" type="{if(@type='long_int') then 'long' else @type}" indexType="{if($indexType!='') then '' else $curIndexType}">
			<xsl:attribute name="label">
				<xsl:if test="$sorLabel !='' and $columnName != $columnPrefix">
					<xsl:value-of select="$sorLabel"/><xsl:text> (</xsl:text><xsl:value-of select="$columnPrefix"/><xsl:text>)</xsl:text>
				</xsl:if>
				<xsl:if test="$sorLabel !='' and $columnName = $columnPrefix">
					<xsl:value-of select="$sorLabel"/>
				</xsl:if>
			</xsl:attribute>
			<xsl:attribute name="finalLabel">
				<xsl:choose>
					<xsl:when test="$customLabel != ''"><xsl:value-of select="$customLabel"/></xsl:when>
					<xsl:when test="$sorLabel !='' and $columnName != $columnPrefix ">
						<xsl:value-of select="$sorLabel"/><xsl:text> (</xsl:text><xsl:value-of select="$columnPrefix"/><xsl:text>)</xsl:text>
					</xsl:when>
					<xsl:when test="$sorLabel !=''">
						<xsl:value-of select="$sorLabel"/>
					</xsl:when>
					<xsl:when test="$matLabel !='' or $aggLabel!='' or $specialLabel!='' or $historicalLabel!=''">
						<xsl:value-of select="sf:nvl($matLabel,$aggLabel,$specialLabel,$historicalLabel)"/>
					</xsl:when>					
				</xsl:choose>
			</xsl:attribute>
		</column>
	</xsl:if>
</xsl:template>

<xsl:template match="column" mode="computed_i">	
	<column name="{fn:lower-case(@name)}" customLabel="{@label}" type="{if(@dataType='long_int') then 'long' else @dataType}" searchable="{@searchable}" label="{@label}" finalLabel="{@finalLabel}" indexType="{if(@indexType!='') then @indexType else''}">
		<producer value="{@producer|producer}"/>
	</column>
</xsl:template>

<xsl:template match="column" mode="computed_m">
	<column name="{fn:lower-case(@name)}" type="{if(@dataType='long_int') then 'long' else @dataType}" searchable="{@searchable}" label="{@label}" indexType="{if(@indexType!='') then @indexType else''}">
		<producer value="{@producer|producer}"/>
	</column>
</xsl:template>

<xsl:template match="column" mode="special">
	<xsl:param name="rc"/>
	<xsl:variable name="columnName" select="@name"/>
	<xsl:variable name="indexType" select="$rc[@name=$columnName]/@indexType"/>
	<column name="{@name}" label="{@label}" type="{if(@type='long_int') then 'long' else @type}" indexType="{$indexType}"/>	
</xsl:template>

<xsl:template match="relationship">
	<xsl:param name="instance"/>
	<xsl:param name="master"/>
	
	<xsl:variable name="parent" select="@parentTable"/>
	<xsl:variable name="child" select="@childTable"/>
	<xsl:variable name="parentLabel" select="$instance/instanceTable[@name=$parent]/@label|$master/masterTable[@name=$parent]/@label"/>
	<xsl:variable name="childLabel" select="$instance/instanceTable[@name=$child]/@label|$master/masterTable[@name=$child]/@label"/>
	<xsl:variable name="ignoreRel" select="advanced/@ignoreRelGui"/>
	
	<!-- self-reference was disabled for MDA
	<xsl:if test="@childTable != @parentTable">
	</xsl:if> -->
	<!-- self-relationship is now generated + validation is added -->
	<relationship name="{lower-case(@name)}" label="{@label}" type="{if(@type='long_int') then 'long' else @type}" parentRole="{@parentRole}" childRole="{@childRole}" parentTable="{lower-case(@parentTable)}" childTable="{lower-case(@childTable)}" parentFk="{foreignKey/column/@parentColumn}" childFk="{foreignKey/column/@childColumn}" parentLabel="{sf:nvl($parentLabel,@parentTable)}" childLabel="{sf:nvl($childLabel,@childTable)}" />
</xsl:template>

</xsl:stylesheet>