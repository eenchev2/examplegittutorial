<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>	

	<xsl:param name="thisMatching" select="document('param:thisMatching')/*"/>
	<xsl:param name="settings" select="document('param:settings')/*"/>
	<xsl:param name="currentTable" select="document('param:currentTable')/*"/>
	<xsl:param name="relationships" select="document('param:relationships')/*"/>
	
	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="$settings/advancedSettings/@matchingCompatibility='false'">
				<xsl:if test="$thisMatching/@matching='true'">
					<xsl:choose>
						<xsl:when test="$thisMatching/multipleMatching/@disableDefault='false'">
							<!-- idKeeperFlagColumn="id_keeper_flag" -->
							<defaultMatchingDefinition name="k" isolateFlagColumn="isolate_flag" masterIdColumn="master_id" rematchFlagColumn="rematch_flag" matchRuleNameColumn="match_rule_name" matchQualityColumn="match_quality" matchRelatedIdColumn="match_related_id">
								<xsl:attribute name="rematchIfChangedColumns">
									<xsl:for-each select="$relationships/relationship[@childTable=lower-case($currentTable/@name)]/parentToChild/column[@source='master_id']">
										<xsl:value-of select="@name"/>
										<xsl:if test="position() != last()">
									    	<xsl:value-of select="';'"/>
										</xsl:if>
									</xsl:for-each>
								</xsl:attribute>
							</defaultMatchingDefinition>
						</xsl:when>
						<xsl:when test="$thisMatching/multipleMatching/@disableDefault='true'">
							<!-- idKeeperFlagColumn="" -->
							<defaultMatchingDefinition name="" isolateFlagColumn="" masterIdColumn="" rematchFlagColumn="" rematchIfChangedColumns="" matchRuleNameColumn="" matchQualityColumn="" matchRelatedIdColumn=""/>
						</xsl:when>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$thisMatching/@matching='false'">
					<!-- idKeeperFlagColumn="" -->
					<defaultMatchingDefinition name="" isolateFlagColumn="" masterIdColumn="" rematchFlagColumn="" rematchIfChangedColumns="" matchRuleNameColumn="" matchQualityColumn="" matchRelatedIdColumn=""/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="$thisMatching/@matching='true'">
					<xsl:choose>
						<xsl:when test="$thisMatching/multipleMatching/@disableDefault='false'">
							<!-- idKeeperFlagColumn="" -->
							<defaultMatchingDefinition name="" isolateFlagColumn="" masterIdColumn="master_id" rematchFlagColumn="" rematchIfChangedColumns="" matchRuleNameColumn="" matchQualityColumn="" matchRelatedIdColumn=""/>
						</xsl:when>
						<xsl:when test="$thisMatching/multipleMatching/@disableDefault='true'">
							<!-- idKeeperFlagColumn="" -->
							<defaultMatchingDefinition name="" isolateFlagColumn="" masterIdColumn="" rematchFlagColumn="" rematchIfChangedColumns="" matchRuleNameColumn="" matchQualityColumn="" matchRelatedIdColumn=""/>
						</xsl:when>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="$thisMatching/@matching='false'">
					<!-- idKeeperFlagColumn="" -->
					<defaultMatchingDefinition name="" isolateFlagColumn="" masterIdColumn="" rematchFlagColumn="" rematchIfChangedColumns="" matchRuleNameColumn="" matchQualityColumn="" matchRelatedIdColumn=""/>
				</xsl:if>			
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>