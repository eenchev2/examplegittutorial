<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

	<xsl:template match="/*">
		<displaySettings>
			<dataTypeFormats>
				<format for="DAY" pattern="{@day}"/>
				<format for="DATETIME" pattern="{@datetime}"/>
				<format for="INTEGER" pattern="{@integer}"/>
				<format for="LONG" pattern="{@long}"/>
				<format for="LONG_INT" pattern="{@long}"/>
				<format for="FLOAT" pattern="{@float}"/>
				<!-- <format for="BOOLEAN" pattern="{@boolean}"/> -->
			</dataTypeFormats>
		</displaySettings>				
	</xsl:template>
</xsl:stylesheet>