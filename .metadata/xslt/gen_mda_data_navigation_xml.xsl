<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>

<xsl:template match="/*">
	<xsl:choose>
		<xsl:when test="@enable='true'">
			<dataNavigation label="{@name}">
				<items>		
					<xsl:for-each select="dataNavigationItems/dataNavigationItem">
						<dataNavigationItem label="{@label}" expanded="{@expanded}" icon="{if(@icon='FOLDER') then 'CUSTOM' else @icon}">
							<children>
								<xsl:for-each select="dataNavigationChildren/dataNavigationChild">
									<xsl:choose>
										<xsl:when test="@name!=''">
											<xsl:variable name="name" select="replace(@name,'(.+) [(]([\p{L}_]+)[)]','$1')"/>
											<xsl:variable name="layerName" select="replace(@name,'(.+) [(]([\p{L}_]+)[)]','$2')"/>
											<dataNavigationItem name="{$name}" icon="{if(@icon='FOLDER') then 'CUSTOM' else @icon}">
												<xsl:choose>
													<xsl:when test="$layerName='instance'">
														<xsl:attribute name="type">
															<xsl:value-of select="'INSTANCE'"/>
														</xsl:attribute>												
													</xsl:when>
													<xsl:when test="$layerName='sor'">
														<xsl:attribute name="type">
															<xsl:value-of select="'SOR'"/>
														</xsl:attribute>												
													</xsl:when>
													<xsl:when test="$layerName='dataset'">
														<xsl:attribute name="type">
															<xsl:value-of select="'DATASET'"/>
														</xsl:attribute>												
													</xsl:when>																		
													<xsl:otherwise>
														<xsl:attribute name="type">
															<xsl:value-of select="'MASTER'"/>
														</xsl:attribute>		
														<xsl:attribute name="layerName">
															<xsl:value-of select="$layerName"/>
														</xsl:attribute>																									
													</xsl:otherwise>
												</xsl:choose>	
												<xsl:if test="dataNavigationChildren/dataNavigationChild">
													<children>
														<xsl:call-template name="dataNavigationChildrenLoop"/>
													</children>
												</xsl:if>																						
											</dataNavigationItem>
										</xsl:when>
										<xsl:when test="@label!=''">
											<dataNavigationItem label="{@label}" expanded="{@expanded}" icon="{if(@icon='FOLDER') then 'CUSTOM' else @icon}">
												<xsl:if test="dataNavigationChildren/dataNavigationChild">
													<children>
														<xsl:call-template name="dataNavigationChildrenLoop"/>
													</children>
												</xsl:if>
											</dataNavigationItem>
										</xsl:when>
									</xsl:choose>
								</xsl:for-each>
							</children>
						</dataNavigationItem>
					</xsl:for-each>
				</items>
			</dataNavigation>
		</xsl:when>
		<xsl:otherwise>
			<dataNavigation/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="dataNavigationChildrenLoop">
	<xsl:for-each select="dataNavigationChildren/dataNavigationChild">
		<xsl:choose>
			<xsl:when test="@name!=''">
				<xsl:variable name="name" select="replace(@name,'(.+) [(]([\p{L}_]+)[)]','$1')"/>
				<xsl:variable name="layerName" select="replace(@name,'(.+) [(]([\p{L}_]+)[)]','$2')"/>
				<dataNavigationItem name="{$name}" icon="{if(@icon='FOLDER') then 'CUSTOM' else @icon}">
					<xsl:choose>
						<xsl:when test="$layerName='instance'">
							<xsl:attribute name="type">
								<xsl:value-of select="'INSTANCE'"/>
							</xsl:attribute>												
						</xsl:when>
						<xsl:when test="$layerName='sor'">
							<xsl:attribute name="type">
								<xsl:value-of select="'SOR'"/>
							</xsl:attribute>												
						</xsl:when>
						<xsl:when test="$layerName='dataset'">
							<xsl:attribute name="type">
								<xsl:value-of select="'DATASET'"/>
							</xsl:attribute>												
						</xsl:when>						
						<xsl:otherwise>
							<xsl:attribute name="type">
								<xsl:value-of select="'MASTER'"/>
							</xsl:attribute>		
							<xsl:attribute name="layerName">
								<xsl:value-of select="$layerName"/>
							</xsl:attribute>																									
						</xsl:otherwise>
					</xsl:choose>	
					<xsl:if test="dataNavigationChildren/dataNavigationChild">
						<children>
							<xsl:call-template name="dataNavigationChildrenLoop"/>
						</children>
					</xsl:if>																						
				</dataNavigationItem>
			</xsl:when>
			<xsl:when test="@label!=''">
				<dataNavigationItem label="{@label}" expanded="{@expanded}" icon="{if(@icon='FOLDER') then 'CUSTOM' else @icon}">
					<xsl:if test="dataNavigationChildren/dataNavigationChild">
						<children>
							<xsl:call-template name="dataNavigationChildrenLoop"/>
						</children>
					</xsl:if>
				</dataNavigationItem>
			</xsl:when>
		</xsl:choose>
	</xsl:for-each>
</xsl:template>
</xsl:stylesheet>