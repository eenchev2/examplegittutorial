<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="sorEventHandler" select="document('param:sorEventHandler')/*"/>

 <xsl:template match="/*">

	<cct entityName="{fn:replace(fn:replace(@entityName, '(.*):\s', ''),'\s(.*)','')}">
		<xsl:attribute name="layerName">
	 		<xsl:choose>
	 			<xsl:when test="$sorEventHandler">
	 				<xsl:value-of select="'sor'"/>
	 			</xsl:when>
	 			<xsl:otherwise>
	 				<xsl:value-of select="fn:replace(fn:replace(@entityName, '([^\s]+)\s\((.*)\)', '$2'),'(.*):\s','')"/>
	 			</xsl:otherwise>
	 		</xsl:choose>
	 	</xsl:attribute>
	 </cct>

 </xsl:template>

</xsl:stylesheet>

