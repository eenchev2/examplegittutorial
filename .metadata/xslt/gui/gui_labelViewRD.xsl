<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" cdata-section-elements="template" omit-xml-declaration="yes" />

	<xsl:template match="/*">
		<template>
			<mappings>
				<contextType>TREE_PARAMETERS</contextType>
				<slot>ENTITY_LABEL</slot>
				<entity>INSTANCE|<xsl:value-of select="@name"/></entity>
				<roles>
					<role></role>
				</roles>
				<modes>
					<mode></mode>
				</modes>
			</mappings>
			<content>
				<stringTemplate>
					<template>
						<xsl:text>&lt;div class='TEntityLabel'></xsl:text>						
						<xsl:choose>
		         			<xsl:when test="@loadInst='true'">
		         				<xsl:value-of select="'$source_system$ $source_code$'"/>
		         			</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'$master_name$'"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>&lt;/div&gt;</xsl:text>
					</template>
				</stringTemplate>
			</content>
		</template>
	</xsl:template>
</xsl:stylesheet>