<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"
		cdata-section-elements="template" omit-xml-declaration="yes" />

	<xsl:param name="instanceRelationships" select="document('param:instanceRelationships')/*" />  <!--/logicalModel/instanceModel/relationships -->
	<xsl:param name="masterModels" select="document('param:masterModels')/*" />  <!--/logicalModel/masterModels -->
	<xsl:param name="settings" select="document('param:settings')/*" />  <!--/settings -->
	<xsl:param name="modelName" select="document('param:modelName')/*" />  <!-- /preview/guiPreview/model[@enabled=&#39;true&#39;] -->
	<xsl:param name="guiConfig" select="document('param:guiConfig')/*" />
	<xsl:include href="incl_gui_templates.xsl" />

	<!-- bound to preview/guiPreview/masterModels/model/masterTables/masterTable -->
	<xsl:template match="/*">
		<xsl:variable name="model_name" select="$modelName/@name" />
		<xsl:variable name="table_name" select="@name" />
		<xsl:variable name="instance_table_name" select="@instanceName" />
		<xsl:variable name="table_label" select="@label" />
		<xsl:variable name="columns" select="$masterModels/masterModel[lower-case(@name)=lower-case($model_name)]/masterTables/masterTable[lower-case(@name)=lower-case($table_name)]/guiTab/views/breadcrumbView/columns|$masterModels/masterModel[lower-case(@name)=lower-case($model_name)]/instanceTables/instanceTable[lower-case(@name)=lower-case($table_name)]/guiTab/views/breadcrumbView/columns" />
		<template>
			<mappings>
				<contextType>TREE_PARAMETERS</contextType>
				<slot>ENTITY_LABEL</slot>
				<entity>MASTER|<xsl:value-of select="$model_name" />|<xsl:value-of select="$table_name" /></entity>
				<roles>
					<role></role>
				</roles>
				<modes>
					<mode></mode>
				</modes>
			</mappings>
			<content>
				<stringTemplate>
					<template>
						<xsl:text>&lt;div class='TEntityLabel'></xsl:text>
						<xsl:choose>
		         			<xsl:when test="$columns/name">
		         				<xsl:value-of select="replace(replace($columns/name,'\{',''),'\}','\$')"/>
		         			</xsl:when>
							<xsl:when test="$table_label">
								<xsl:value-of select="$table_label" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$table_name" />
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>&lt;/div&gt;</xsl:text>
					</template>
				</stringTemplate>
			</content>
		</template>
	</xsl:template>
</xsl:stylesheet>