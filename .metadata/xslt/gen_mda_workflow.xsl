<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:comm="http://www.ataccama.com/purity/comment"
	exclude-result-prefixes="sf fn comm">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:template match="/*">
<xsl:variable name="statuses" select="statuses"/>
<workflows>
	<workflowProvider class="com.ataccama.mda.core.config.workflow.MdaWorkflowProvider">
		<workflowBean class="com.ataccama.mda.core.config.workflow.MdaXmlWorkflowProviderConfig">
			<workflows>
				<xsl:if test="workflowLabels/@enableSor='true'">
					<workflow name="sor">
						<steps>
							<step name="draft" first="true"/>
						</steps>
						<transitions/>
					</workflow>
				</xsl:if>
				<xsl:if test="workflowLabels/@enableCons='true'">
					<workflow name="consolidation">
						<steps>
							<step name="draft" first="true"/>
							<step name="waiting_for_publish"/>
						</steps>
						<transitions>
							<transition name="move_publish" origin="draft" target="waiting_for_publish"/>
							<transition name="return_draft" origin="waiting_for_publish" target="draft"/>
						</transitions>
					</workflow>	
				</xsl:if>		
				<xsl:for-each select="workflows/workflow[@enable='true']">
			  		<workflow name="{@name}">
			  			<steps>
				  			<xsl:for-each select="$statuses/status[@name=current()/steps/step/@name]">
							      <step name="{@name}">
							      	<xsl:variable name="stepName" select="@name"/>
							      	<xsl:if test="ancestor::statuses/status[@name=$stepName]/@first='true'"><xsl:attribute name="first" select="'true'"/></xsl:if>
							      </step>
							</xsl:for-each>
				  			<xsl:for-each select="$statuses/status[@name=current()/steps/step/@transitionTarget and not(@name=current()/steps/step/@name)]">
							      <step name="{@name}">
							      	<xsl:variable name="stepName" select="@name"/>
							      	<xsl:if test="ancestor::statuses/status[@name=$stepName]/@first='true'"><xsl:attribute name="first" select="'true'"/></xsl:if>
							      </step>
							</xsl:for-each>							
						</steps>
						<transitions>
							<xsl:for-each select="steps/step">						    
						    	<transition name="{@transitionName}" origin="{@name}" target="{@transitionTarget}"/>						         
							</xsl:for-each>
						</transitions>
			 		</workflow>
				</xsl:for-each>
			</workflows>
		</workflowBean>   
    </workflowProvider>  
</workflows>	

</xsl:template>

</xsl:stylesheet>