<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="text" encoding="UTF-8" indent="yes"/>
<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>
<xsl:param name="databaseModel" select="document('param:databaseModel')/*"/>
<xsl:param name="wfConfig" select="document('param:wfConfig')/*"/>
<xsl:param name="workflowLabels" select="document('param:workflowLabels')/*"/>
<xsl:param name="filters" select="document('param:filters')/*"/>

<xsl:template match="/*">
<xsl:variable name="newline"><xsl:text>
</xsl:text></xsl:variable>

<!-- FILTERS -->
<xsl:for-each select="$filters/filter[@enable='true']">
	<xsl:if test="@label!=''">
		<xsl:value-of select="'FILTER'"/>
		<xsl:value-of select="'.'"/>
		<xsl:choose>
			<xsl:when test="substring-before(substring-after(@entity,'('),')')='instance'">
				<xsl:value-of select="'INSTANCE'"/>
			</xsl:when>
			<xsl:when test="substring-before(substring-after(@entity,'('),')')='sor'">
				<xsl:value-of select="'SOR'"/>
			</xsl:when>
			<xsl:when test="substring-before(substring-after(@entity,'('),')')='dataset'">
				<xsl:value-of select="'DATASET'"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'MASTER.'"/>
				<xsl:value-of select="substring-before(substring-after(@entity,'('),')')"/>
			</xsl:otherwise>
		</xsl:choose>	
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="substring-before(@entity,' ')"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="@label"/>
		<xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:if test="@description!=''">
		<xsl:value-of select="'FILTER'"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="'DESCRIPTION.'"/>
		<xsl:choose>
			<xsl:when test="substring-before(substring-after(@entity,'('),')')='instance'">
				<xsl:value-of select="'INSTANCE'"/>
			</xsl:when>
			<xsl:when test="substring-before(substring-after(@entity,'('),')')='sor'">
				<xsl:value-of select="'SOR'"/>
			</xsl:when>
			<xsl:when test="substring-before(substring-after(@entity,'('),')')='dataset'">
				<xsl:value-of select="'DATASET'"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'MASTER.'"/>
				<xsl:value-of select="substring-before(substring-after(@entity,'('),')')"/>				
			</xsl:otherwise>
		</xsl:choose>	
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="substring-before(@entity,' ')"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="@description"/>
		<xsl:value-of select="$newline"/>	
	</xsl:if>		
</xsl:for-each>
<!-- END FILTERS -->

<!-- DATASETS -->
<xsl:for-each select="$logicalModel/datasets/datasetsArray/datasetArray[@enable='true']">
	<xsl:if test="@label!=''">
		<xsl:value-of select="'DATASET'"/>
		<xsl:value-of select="'.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="@label"/>
		<xsl:value-of select="$newline"/>
	</xsl:if>	
	<xsl:variable name="datasetName" select="@name"/>
	
	<xsl:for-each select="instanceTables/instanceTable">
		<xsl:variable name="thisTable" select="@entityName"/>
		<xsl:for-each select="columns/column">
			<xsl:variable name="thisColumn" select="@name"/>	
			<xsl:variable name="columnLabel" select="$databaseModel/instanceTables/physicalTable[@name=$thisTable]/columns/column[@name=$thisColumn]/@label"/>
			<xsl:if test="sf:nvl(@label,$columnLabel)!=''">
				<xsl:value-of select="'DATASET.'"/>
				<xsl:value-of select="$datasetName"/>
				<xsl:value-of select="'.'"/>
				<xsl:value-of select="cc/@name"/>	
				<xsl:value-of select="'='"/>
				<xsl:value-of select="sf:nvl(@label,$columnLabel)"/>
				<xsl:value-of select="$newline"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:for-each>	
	
	<xsl:for-each select="masterTables/masterTable">
		<xsl:variable name="thisTable" select="substring-before(@entityName,' ')"/>
		<xsl:variable name="thisLayer" select="substring-before(substring-after(@entityName,'('),')')"/>
		<xsl:for-each select="columns/column">
			<xsl:variable name="thisColumn" select="@name"/>	
			<xsl:variable name="columnLabel" select="$databaseModel/masterTables/physicalTable[@name=$thisTable and @layerName=$thisLayer]/columns/column[@name=$thisColumn]/@label"/>
			<xsl:if test="sf:nvl(@label,$columnLabel)!=''">	
				<xsl:value-of select="'DATASET.'"/>
				<xsl:value-of select="$datasetName"/>
				<xsl:value-of select="'.'"/>
				<xsl:value-of select="cc/@name"/>		
				<xsl:value-of select="'='"/>
				<xsl:value-of select="sf:nvl(@label,$columnLabel)"/>
				<xsl:value-of select="$newline"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:for-each>	

	<xsl:for-each select="sorTables/sorTable">
		<xsl:variable name="thisTable" select="@entityName"/>
		<xsl:for-each select="columns/column">
			<xsl:variable name="thisColumn" select="@name"/>	
			<xsl:variable name="columnLabel" select="$databaseModel/sorTables/physicalTable[@name=$thisTable]/columns/column[@name=$thisColumn]/@label"/>
			<xsl:if test="sf:nvl(@label,$columnLabel)!=''">	
				<xsl:value-of select="'DATASET.'"/>
				<xsl:value-of select="$datasetName"/>
				<xsl:value-of select="'.'"/>
				<xsl:value-of select="cc/@name"/>	
				<xsl:value-of select="'='"/>
				<xsl:value-of select="sf:nvl(@label,$columnLabel)"/>
				<xsl:value-of select="$newline"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:for-each>	
	
	<xsl:for-each select="sqlSource/columns/column">
		<xsl:if test="@label!=''">
			<xsl:value-of select="'DATASET.'"/>
			<xsl:value-of select="$datasetName"/>
			<xsl:value-of select="'.'"/>
			<xsl:value-of select="@name"/>		
			<xsl:value-of select="'='"/>
			<xsl:value-of select="@label"/>
			<xsl:value-of select="$newline"/>
		</xsl:if>		
	</xsl:for-each>	
</xsl:for-each>
<!-- END DATASETS -->

<!-- WORKFLOWS -->
<xsl:if test="$workflowLabels/@enableCons='true'">
	<xsl:if test="$workflowLabels/@consolidation!=''">
		<xsl:value-of select="'WORKFLOW.consolidation='"/>
		<xsl:value-of select="$workflowLabels/@consolidation"/>
		<xsl:value-of select="$newline"/>	
	</xsl:if>
</xsl:if>
<xsl:if test="$workflowLabels/@enableSor='true'">
	<xsl:if test="$workflowLabels/@sor!=''">	
		<xsl:value-of select="'WORKFLOW.sor='"/>
		<xsl:value-of select="$workflowLabels/@sor"/>
		<xsl:value-of select="$newline"/>
	</xsl:if>
</xsl:if>
<xsl:if test="sf:nvl($workflowLabels/@draft,$wfConfig/statuses/status[@name='draft']/@label)!=''">
	<xsl:value-of select="'WFSTEP.draft='"/>
	<xsl:value-of select="sf:nvl($workflowLabels/@draft,$wfConfig/statuses/status[@name='draft']/@label)"/>
	<xsl:value-of select="$newline"/>
</xsl:if>
<xsl:if test="sf:nvl($workflowLabels/@waiting_for_publish,$wfConfig/statuses/status[@name='waiting_for_publish']/@label)!=''">
	<xsl:value-of select="'WFSTEP.waiting_for_publish='"/>
	<xsl:value-of select="sf:nvl($workflowLabels/@waiting_for_publish,$wfConfig/statuses/status[@name='waiting_for_publish']/@label)"/>
	<xsl:value-of select="$newline"/>
</xsl:if>
<xsl:if test="sf:nvl($workflowLabels/@move_publish,$wfConfig/workflows/workflow/steps/step[@transitionName='move_publish']/@transitionLabel)!=''">
	<xsl:value-of select="'WFTRANSITION.move_publish='"/>
	<xsl:value-of select="sf:nvl($workflowLabels/@move_publish,$wfConfig/workflows/workflow/steps/step[@transitionName='move_publish']/@transitionLabel)"/>
	<xsl:value-of select="$newline"/>
</xsl:if>
<xsl:if test="sf:nvl($workflowLabels/@return_draft,$wfConfig/workflows/workflow/steps/step[@transitionName='return_draft']/@transitionLabel)!=''">
	<xsl:value-of select="'WFTRANSITION.return_draft='"/>
<xsl:value-of select="sf:nvl($workflowLabels/@return_draft,$wfConfig/workflows/workflow/steps/step[@transitionName='return_draft']/@transitionLabel)"/>
	<xsl:value-of select="$newline"/>
</xsl:if>

<xsl:for-each select="$wfConfig/workflows/workflow[@enable='true']">
	<xsl:if test="@label!=''">
		<xsl:value-of select="'WORKFLOW.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="@label"/>
		<xsl:value-of select="$newline"/>
	</xsl:if>
</xsl:for-each>
<xsl:for-each select="$wfConfig/statuses/status[not(@name='draft') and not(@name='waiting_for_publish') and @enable='true']">
	<xsl:if test="@label!=''">
		<xsl:value-of select="'WFSTEP.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="@label"/>	
		<xsl:value-of select="$newline"/>
	</xsl:if>
</xsl:for-each>
<xsl:for-each select="$wfConfig/workflows/workflow[@enable='true']/steps/step[not(@transitionName=preceding::step/@transitionName) and not(@transitionName='move_publish') and not(@transitionName='return_draft')]">
	<xsl:if test="@transitionLabel!=''">
		<xsl:value-of select="'WFTRANSITION.'"/>
		<xsl:value-of select="@transitionName"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="@transitionLabel"/>
		<xsl:value-of select="$newline"/>
	</xsl:if>
</xsl:for-each>
<!-- END WORKFLOWS -->

<xsl:value-of select="'SOR=SoR'"/>
<xsl:value-of select="$newline"/>
<xsl:for-each select="$logicalModel/sorModel/tables/table">
	<xsl:variable name="tableName" select="@name"/>
	<xsl:if test="@label!=''">
		<xsl:value-of select="'SOR.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="@label"/>
		<xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:for-each select="columns/column">
		<xsl:if test="@label!=''">
			<xsl:value-of select="'SOR.'"/>
			<xsl:value-of select="$tableName"/>
			<xsl:value-of select="'.'"/>
			<xsl:value-of select="@name"/>
			<xsl:value-of select="'='"/>
			<xsl:value-of select="@label"/>
			<xsl:value-of select="$newline"/>
		</xsl:if>		
	</xsl:for-each>
	<xsl:for-each select="columns/column[@refData!='']">		
		<xsl:variable name="refData" select="concat('rd_',@refData)"/>
		<xsl:if test="$databaseModel/refData/physicalTable[@name=$refData]/@label!=''">
			<xsl:value-of select="'SOR.'"/>
			<xsl:value-of select="$databaseModel/refData/physicalTable[@name=$refData]/@name"/>
			<xsl:value-of select="'='"/>
			<xsl:value-of select="'SoR '"/>
			<xsl:value-of select="$databaseModel/refData/physicalTable[@name=$refData]/@label"/>
			<xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:for-each>
</xsl:for-each>

<xsl:value-of select="'INSTANCE'"/>
<xsl:value-of select="'='"/>	
<xsl:value-of select="sf:nvl(instanceModel/@label,'Instances')"/>
<xsl:value-of select="$newline"/>	
<xsl:for-each select="instanceModel/tables/table">
	<xsl:if test="@label!=''">
		<xsl:value-of select="'INSTANCE.'"/>
		<xsl:value-of select="@name"/>
		<xsl:value-of select="'='"/>		
		<xsl:value-of select="@label"/>
		<xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:variable name="tableName" select="@name"/>
	<xsl:for-each select="columns/column">
		<xsl:if test="sf:nvl(@label,@customLabel,@finalLabel)!=''">
			<xsl:value-of select="'INSTANCE.'"/>
			<xsl:value-of select="$tableName"/>
			<xsl:value-of select="'.'"/>
			<xsl:value-of select="@name"/>
			<xsl:value-of select="'='"/>		
			<xsl:value-of select="@finalLabel"/>
			<xsl:value-of select="$newline"/>
		</xsl:if>
	</xsl:for-each>	
</xsl:for-each>

<xsl:for-each select="masterModels/model">
	<xsl:variable name="modelName" select="@name"/>
	<xsl:if test="@label!=''">
		<xsl:value-of select="'MASTER.'"/>
		<xsl:value-of select="$modelName"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="@label"/>
		<xsl:value-of select="$newline"/>
	</xsl:if>
	<xsl:for-each select="masterTables/masterTable">
		<xsl:variable name="masterTableName" select="@name"/>
		<xsl:if test="@label!=''">	
			<xsl:value-of select="'MASTER.'"/>
			<xsl:value-of select="$modelName"/>
			<xsl:value-of select="'.'"/>		
			<xsl:value-of select="$masterTableName"/>
			<xsl:value-of select="'='"/>
			<xsl:value-of select="@label"/>	
			<xsl:value-of select="$newline"/>
		</xsl:if>	
		<xsl:for-each select="columns/column">
			<xsl:if test="@label!=''">
				<xsl:value-of select="'MASTER.'"/>
				<xsl:value-of select="$modelName"/>
				<xsl:value-of select="'.'"/>
				<xsl:value-of select="$masterTableName"/>
				<xsl:value-of select="'.'"/>
				<xsl:value-of select="@name"/>
				<xsl:value-of select="'='"/>
				<xsl:value-of select="@label"/>
				<xsl:value-of select="$newline"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:for-each>
	<xsl:for-each select="instanceTables/instanceTable">
		<xsl:variable name="instanceTableLabel" select="@label"/>
		<xsl:variable name="instanceTableName" select="@name"/>
		<xsl:if test="@label!=''">
			<xsl:value-of select="'MASTER.'"/>
			<xsl:value-of select="$modelName"/>
			<xsl:value-of select="'.'"/>		
			<xsl:value-of select="@name"/>
			<xsl:value-of select="'='"/>
			<xsl:value-of select="@label"/>
			<xsl:value-of select="$newline"/>
		</xsl:if>			
		<xsl:for-each select="columns/column">
			<xsl:variable name="columnName" select="@name"/>
			<xsl:if test="sf:nvl(@label,@customLabel,@finalLabel)!=''">
				<xsl:value-of select="'MASTER.'"/>
				<xsl:value-of select="$modelName"/>
				<xsl:value-of select="'.'"/>		
				<xsl:value-of select="$instanceTableName"/>
				<xsl:value-of select="'.'"/>
				<xsl:value-of select="@name"/>
				<xsl:value-of select="'='"/>
				<xsl:value-of select="@finalLabel"/>
				<xsl:value-of select="$newline"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:for-each>	
	<xsl:for-each select="$databaseModel/refData/physicalTable">
		<xsl:variable name="refTableName" select="@name"/>
		<xsl:variable name="refTableLabel" select="@label"/>
		<xsl:if test="$refTableLabel!=''">
			<xsl:value-of select="'INSTANCE.'"/>
			<xsl:value-of select="$refTableName"/>
			<xsl:value-of select="'='"/>
			<xsl:value-of select="$refTableLabel"/>
			<xsl:value-of select="$newline"/>
		</xsl:if>
		<xsl:for-each select="columns/column[@load='true']">	
			<xsl:variable name="columnPrefix" select="fn:replace(@name,'^(eng|src|std|cio|exp|sco|dic)_(.+)$','$1')"/>
			<xsl:if test="$columnPrefix != 'eng' and @label!=''">
				<xsl:value-of select="'INSTANCE.'"/>
				<xsl:value-of select="$refTableName"/>
				<xsl:value-of select="'.'"/>
				<xsl:value-of select="@name"/>
				<xsl:value-of select="'='"/>
				<xsl:value-of select="@label"/>
				<xsl:value-of select="$newline"/>		
			</xsl:if>				
		</xsl:for-each>
	</xsl:for-each>	
	<!-- <xsl:for-each select="dictionaryTables/dictionaryTable">
		<xsl:variable name="refTableName" select="@name"/>
		<xsl:variable name="refTableLabel" select="@label"/>
		<xsl:value-of select="'INSTANCE.'"/>
		<xsl:value-of select="$refTableName"/>
		<xsl:value-of select="'='"/>
		<xsl:value-of select="$refTableLabel"/>
		<xsl:value-of select="$newline"/>
		<xsl:for-each select="columns/column">
			<xsl:value-of select="'INSTANCE.'"/>
			<xsl:value-of select="$refTableName"/>
			<xsl:value-of select="'.'"/>
			<xsl:value-of select="@name"/>
			<xsl:value-of select="'='"/>
			<xsl:value-of select="@label"/>
			<xsl:value-of select="$newline"/>						
		</xsl:for-each>
	</xsl:for-each>	-->
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>