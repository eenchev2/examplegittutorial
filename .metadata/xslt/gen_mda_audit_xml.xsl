<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>

<xsl:template match="/*">
	<loggingConfig class='com.ataccama.dqc.commons.logging.config.LoggingConfigImpl'>
	    <appenders> 
	        <xsl:for-each select="appenders/appender[@enable='true']">
		        <iLogAppenderFactory name="{@name}" class='com.ataccama.dqc.commons.logging.factories.JDKHandledLogAppenderFactory'>
		            <handler class='com.ataccama.dqc.commons.logging.handlers.FileHandlerInfo'>
		                <pattern><xsl:value-of select="@pattern"/></pattern>
		                <limit><xsl:value-of select="@limit"/></limit>
		                <count><xsl:value-of select="@count"/></count>
		                <append><xsl:value-of select="@append"/></append>
		                <formatter class='com.ataccama.dqc.commons.logging.handlers.SimpleFormatterInfo'/>
		            </handler>
		        </iLogAppenderFactory>
			</xsl:for-each>	        
	    </appenders>
		<loggingRules>
			<xsl:for-each select="appenders/appender[@enable='true']">  
				<xsl:if test="@exportAction = 'true'">
					<loggingRule appender="{@name}" level="INFO">
						<trace>
							<item>Export action</item>
						</trace>
					</loggingRule>
				</xsl:if>
				<xsl:if test="@dataRead = 'true'">
					<loggingRule appender="{@name}" level="INFO">
						<trace>
							<item>Data read</item>
						</trace>
					</loggingRule>
				</xsl:if>
				<xsl:if test="@draftAction = 'true'">
					<loggingRule appender="{@name}" level="INFO">
						<trace>
							<item>Draft action</item>
						</trace>
					</loggingRule>
				</xsl:if>
				<xsl:if test="@dataExport = 'true'">
					<loggingRule appender="{@name}" level="INFO">
						<trace>
							<item>Data export</item>
						</trace>
					</loggingRule>
				</xsl:if>
				<xsl:if test="@issueAction = 'true'">
					<loggingRule appender="{@name}" level="INFO">
						<trace>
							<item>Issue action</item>
						</trace>
					</loggingRule>
				</xsl:if>
				<xsl:if test="@workflowAction = 'true'">
					<loggingRule appender="{@name}" level="INFO">
						<trace>
							<item>Workflow action</item>
						</trace>
					</loggingRule>
				</xsl:if>
			</xsl:for-each>
		</loggingRules>	       
	</loggingConfig>
</xsl:template>
</xsl:stylesheet>