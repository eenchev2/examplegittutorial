<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet xmlns:sf="http://www.ataccama.com/xslt/functions" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="sf fn" version="2.0">
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	
<!-- MDC version that is used in all plans in purity-config version -->
<xsl:variable select="&#39;12.2.0&#39;" name="version"/> 

</xsl:stylesheet>