<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="settings" select="document('param:settings')/*"/>
<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>

<xsl:variable name="global_deletion_strategy" select="$settings/@deletionStrategy"/>
<xsl:variable name="global_export_scope" select="$settings/@scope"/>
<xsl:variable name="not_default" select="'deactivate delete'"/>
<xsl:variable name="not_default_export" select="'active existing'"/>

<xsl:template match="/*">

<stream>
	<xsl:if test="@enable='true'">	
		<consumers>			
			<xsl:for-each select="consolidationStreaming/consumers/jmsStreamSource[@enable='true']">
				<consumer name="{@name}">
		
					<source class="com.ataccama.nme.engine.stream.JmsStreamSource">
						<connectionName><xsl:value-of select="source/@connectionName"/></connectionName>
						<inputDestination><xsl:value-of select="source/@inputDestination"/></inputDestination>
					</source>
		
					<batching>
						<count><xsl:value-of select="batching/@count"/></count>
						<seconds><xsl:value-of select="batching/@seconds"/></seconds> 
					</batching>
		
					<processor class="com.ataccama.nme.engine.stream.DeltaLoad">
						<xsl:choose>
							<xsl:when test="messageTransformer/transformers/planTransformer">						
								<transformer class="com.ataccama.nme.dqc.stream.PlanTransformer" planFileName="../engine/stream/{lower-case(@name)}.comp" >
									<xsl:if test="messageTransformer/headers/header or messageTransformer/properties/property">
										<headers>
											<xsl:if test="messageTransformer/headers/header">										
												<xsl:for-each select="messageTransformer/headers/header">
			        								<header header="{@header}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>
											<xsl:if test="messageTransformer/properties/property">
			        							<xsl:for-each select="messageTransformer/properties/property">
			        								<header header="{@property}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>		   
			    						</headers>
			    					</xsl:if>					
								</transformer>
							</xsl:when>
							<xsl:otherwise>
								<transformer class="com.ataccama.nme.engine.stream.ProcessDeltaTransformer" >
									<xsl:if test="messageTransformer/transformers/processDeltaTransformer/@format='SOAP'">
										<requestPath>
											<el>Envelope</el>
											<el>Body</el>
											<el><xsl:value-of select="messageTransformer/transformers/processDeltaTransformer/@processDeltaName"/></el>
										</requestPath> 										
									</xsl:if>
									<xsl:if test="messageTransformer/headers/header or messageTransformer/properties/property">
										<headers>
											<xsl:if test="messageTransformer/headers/header">										
												<xsl:for-each select="messageTransformer/headers/header">
			        								<header header="{@header}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>
											<xsl:if test="messageTransformer/properties/property">
			        							<xsl:for-each select="messageTransformer/properties/property">
			        								<header header="{@property}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>		   
			    						</headers>
			    					</xsl:if>
								</transformer>							
							</xsl:otherwise>
						</xsl:choose>
						<entities>
							<xsl:choose>
								<xsl:when test="processedEntities/@allEntities='true'">
									<xsl:for-each select="$logicalModel/instanceModel/tables/table">
										<entity name="{lower-case(@name)}" />
									</xsl:for-each>										
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="processedEntities/entities/entity">
										<entity name="{@name}">
											<xsl:if test="columns/column">
										      <columns>
										      	<xsl:for-each select="columns/column">
										        	<column><xsl:value-of select="@name"/></column>
										        </xsl:for-each>
										      </columns>							
											</xsl:if>											
										</entity>
									</xsl:for-each>		
								</xsl:otherwise>	
							</xsl:choose>				
						</entities>
					</processor>						
				</consumer>			
			</xsl:for-each>
			<xsl:for-each select="consolidationStreaming/consumers/kafkaStreamSource[@enable='true']">
				<consumer name="{@name}">
					<source class="com.ataccama.nme.engine.stream.KafkaStreamSource">						
						<kafkaProperties>
							<kafkaProperty>
								<key>group.id</key>
								<value><xsl:value-of select="@group.id"/></value>
							</kafkaProperty>
							<xsl:for-each select="kafkaProperties/kafkaProperty">
								<kafkaProperty>
									<key><xsl:value-of select="@key"/></key>
									<value><xsl:value-of select="@value"/></value>
								</kafkaProperty>
							</xsl:for-each>
						</kafkaProperties>		
						<xsl:if test="@readFromBeginning='true'">
							<readFromBeginning>true</readFromBeginning>
						</xsl:if>
						<kafkaResource><xsl:value-of select="@resourceName"/></kafkaResource>
						<kafkaTopics>
						    <topic><xsl:value-of select="@consumedTopic"/></topic>
						</kafkaTopics>																	
					</source>	
					<batching>
						<count><xsl:value-of select="batching/@count"/></count>
						<seconds><xsl:value-of select="batching/@seconds"/></seconds> 
					</batching>		
					<processor class="com.ataccama.nme.engine.stream.DeltaLoad">
						<xsl:choose>
							<xsl:when test="kafkaMessageTransformer/transformers/planTransformer">						
								<transformer class="com.ataccama.nme.dqc.stream.PlanTransformer" planFileName="../engine/stream/{lower-case(@name)}.comp" >
									<xsl:if test="kafkaMessageTransformer/headers/header or kafkaMessageTransformer/properties/property">
										<headers>
											<xsl:if test="kafkaMessageTransformer/headers/header">										
												<xsl:for-each select="kafkaMessageTransformer/headers/header">
			        								<header header="{@header}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>
											<xsl:if test="kafkaMessageTransformer/properties/property">
			        							<xsl:for-each select="kafkaMessageTransformer/properties/property">
			        								<header header="{@property}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>		   
			    						</headers>
			    					</xsl:if>					
								</transformer>
							</xsl:when>
							<xsl:otherwise>
								<transformer class="com.ataccama.nme.engine.stream.ProcessDeltaTransformer" >
									<xsl:if test="kafkaMessageTransformer/transformers/processDeltaTransformer/@format='SOAP'">
										<requestPath>
											<el>Envelope</el>
											<el>Body</el>
											<el><xsl:value-of select="kafkaMessageTransformer/transformers/processDeltaTransformer/@processDeltaName"/></el>
										</requestPath> 										
									</xsl:if>
									<xsl:if test="kafkaMessageTransformer/headers/header or messageTransformer/properties/property">
										<headers>
											<xsl:if test="kafkaMessageTransformer/headers/header">										
												<xsl:for-each select="kafkaMessageTransformer/headers/header">
			        								<header header="{@header}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>
											<xsl:if test="kafkaMessageTransformer/properties/property">
			        							<xsl:for-each select="kafkaMessageTransformer/properties/property">
			        								<header header="{@property}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>		   
			    						</headers>
			    					</xsl:if>
								</transformer>							
							</xsl:otherwise>
						</xsl:choose>
						<entities>
							<xsl:choose>
								<xsl:when test="processedEntities/@allEntities='true'">
									<xsl:for-each select="$logicalModel/instanceModel/tables/table">
										<entity name="{lower-case(@name)}" />
									</xsl:for-each>										
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="processedEntities/entities/entity">
										<entity name="{@name}">
											<xsl:if test="columns/column">
										      <columns>
										      	<xsl:for-each select="columns/column">
										        	<column><xsl:value-of select="@name"/></column>
										        </xsl:for-each>
										      </columns>							
											</xsl:if>											
										</entity>
									</xsl:for-each>		
								</xsl:otherwise>	
							</xsl:choose>				
						</entities>
					</processor>						
				</consumer>			
			</xsl:for-each>
			<xsl:for-each select="sorStreaming/consumers/jmsStreamSource[@enable='true']">
				<consumer name="{@name}">
		
					<source class="com.ataccama.nme.engine.stream.JmsStreamSource">
						<connectionName><xsl:value-of select="source/@connectionName"/></connectionName>
						<inputDestination><xsl:value-of select="source/@inputDestination"/></inputDestination>
					</source>
		
					<batching>
						<count><xsl:value-of select="batching/@count"/></count>
						<seconds><xsl:value-of select="batching/@seconds"/></seconds> 
					</batching>
		
					<processor class="com.ataccama.nme.engine.stream.SorDeltaLoad">
						<xsl:choose>
							<xsl:when test="messageTransformer/transformers/planTransformer">						
								<transformer class="com.ataccama.nme.dqc.stream.PlanTransformer" planFileName="../engine/stream/{lower-case(@name)}.comp" >
									<xsl:if test="messageTransformer/headers/header or messageTransformer/properties/property">
										<headers>
											<xsl:if test="messageTransformer/headers/header">										
												<xsl:for-each select="messageTransformer/headers/header">
			        								<header header="{@header}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>
											<xsl:if test="messageTransformer/properties/property">
			        							<xsl:for-each select="messageTransformer/properties/property">
			        								<header header="{@property}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>		   
			    						</headers>
			    					</xsl:if>					
								</transformer>
							</xsl:when>
							<xsl:otherwise>
								<transformer class="com.ataccama.nme.engine.stream.ProcessDeltaTransformer" >
									<xsl:if test="messageTransformer/transformers/processDeltaTransformer/@format='SOAP'">
										<requestPath>
											<el>Envelope</el>
											<el>Body</el>
											<el><xsl:value-of select="messageTransformer/transformers/processDeltaTransformer/@processDeltaName"/></el>
										</requestPath> 										
									</xsl:if>
									<xsl:if test="messageTransformer/headers/header or messageTransformer/properties/property">
										<headers>
											<xsl:if test="messageTransformer/headers/header">										
												<xsl:for-each select="messageTransformer/headers/header">
			        								<header header="{@header}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>
											<xsl:if test="messageTransformer/properties/property">
			        							<xsl:for-each select="messageTransformer/properties/property">
			        								<header header="{@property}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>		   
			    						</headers>
			    					</xsl:if>
								</transformer>							
							</xsl:otherwise>
						</xsl:choose>
						<entities>
							<xsl:choose>
								<xsl:when test="processedEntities/@allEntities='true'">
									<xsl:for-each select="$logicalModel/sorModel/tables/table">
										<entity name="{lower-case(@name)}" />
									</xsl:for-each>										
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="processedEntities/entities/entity">
										<entity name="{@name}">
											<xsl:if test="columns/column">
										      <columns>
										      	<xsl:for-each select="columns/column">
										        	<column><xsl:value-of select="@name"/></column>
										        </xsl:for-each>
										      </columns>							
											</xsl:if>											
										</entity>
									</xsl:for-each>		
								</xsl:otherwise>	
							</xsl:choose>				
						</entities>
					</processor>						
				</consumer>			
			</xsl:for-each>	
			<xsl:for-each select="sorStreaming/consumers/kafkaStreamSource[@enable='true']">
				<consumer name="{@name}">
					<source class="com.ataccama.nme.engine.stream.KafkaStreamSource">						
						<kafkaProperties>
							<kafkaProperty>
								<key>group.id</key>
								<value><xsl:value-of select="@group.id"/></value>
							</kafkaProperty>						
							<xsl:for-each select="kafkaProperties/kafkaProperty">
								<kafkaProperty>
									<key><xsl:value-of select="@key"/></key>
									<value><xsl:value-of select="@value"/></value>
								</kafkaProperty>
							</xsl:for-each>
						</kafkaProperties>	
						<xsl:if test="@readFromBeginning='true'">
							<readFromBeginning>true</readFromBeginning>
						</xsl:if>						
						<kafkaResource><xsl:value-of select="@resourceName"/></kafkaResource>
						<kafkaTopics>
						    <topic><xsl:value-of select="@consumedTopic"/></topic>
						</kafkaTopics>																			
					</source>	
					<batching>
						<count><xsl:value-of select="batching/@count"/></count>
						<seconds><xsl:value-of select="batching/@seconds"/></seconds> 
					</batching>		
					<processor class="com.ataccama.nme.engine.stream.SorDeltaLoad">
						<xsl:choose>
							<xsl:when test="kafkaMessageTransformer/transformers/planTransformer">						
								<transformer class="com.ataccama.nme.dqc.stream.PlanTransformer" planFileName="../engine/stream/{lower-case(@name)}.comp" >
									<xsl:if test="kafkaMessageTransformer/headers/header or kafkaMessageTransformer/properties/property">
										<headers>
											<xsl:if test="kafkaMessageTransformer/headers/header">										
												<xsl:for-each select="kafkaMessageTransformer/headers/header">
			        								<header header="{@header}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>
											<xsl:if test="kafkaMessageTransformer/properties/property">
			        							<xsl:for-each select="kafkaMessageTransformer/properties/property">
			        								<header header="{@property}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>		   
			    						</headers>
			    					</xsl:if>					
								</transformer>
							</xsl:when>
							<xsl:otherwise>
								<transformer class="com.ataccama.nme.engine.stream.ProcessDeltaTransformer" >
									<xsl:if test="kafkaMessageTransformer/transformers/processDeltaTransformer/@format='SOAP'">
										<requestPath>
											<el>Envelope</el>
											<el>Body</el>
											<el><xsl:value-of select="kafkaMessageTransformer/transformers/processDeltaTransformer/@processDeltaName"/></el>
										</requestPath> 										
									</xsl:if>
									<xsl:if test="kafkaMessageTransformer/headers/header or messageTransformer/properties/property">
										<headers>
											<xsl:if test="kafkaMessageTransformer/headers/header">										
												<xsl:for-each select="kafkaMessageTransformer/headers/header">
			        								<header header="{@header}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>
											<xsl:if test="kafkaMessageTransformer/properties/property">
			        							<xsl:for-each select="kafkaMessageTransformer/properties/property">
			        								<header header="{@property}" column="{@column}" />
			        							</xsl:for-each>
				    						</xsl:if>		   
			    						</headers>
			    					</xsl:if>
								</transformer>							
							</xsl:otherwise>
						</xsl:choose>
						<entities>
							<xsl:choose>
								<xsl:when test="processedEntities/@allEntities='true'">
									<xsl:for-each select="$logicalModel/instanceModel/tables/table">
										<entity name="{lower-case(@name)}" />
									</xsl:for-each>										
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="processedEntities/entities/entity">
										<entity name="{@name}">
											<xsl:if test="columns/column">
										      <columns>
										      	<xsl:for-each select="columns/column">
										        	<column><xsl:value-of select="@name"/></column>
										        </xsl:for-each>
										      </columns>							
											</xsl:if>											
										</entity>
									</xsl:for-each>		
								</xsl:otherwise>	
							</xsl:choose>				
						</entities>
					</processor>						
				</consumer>			
			</xsl:for-each>					
		</consumers>	
	</xsl:if>
</stream>


</xsl:template>

</xsl:stylesheet>