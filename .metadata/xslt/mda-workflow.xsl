<?xml version="1.0" encoding="UTF-8"?>
<workflows>
	<workflowProvider class="com.ataccama.mda.core.config.workflow.MdaWorkflowProvider">
		<workflowBean class="com.ataccama.mda.core.config.workflow.MdaXmlWorkflowProviderConfig">
			<workflows>
				<workflow name="sor">
					<steps>
						<step name="draft" first="true"/>
					</steps>
					<transitions/>
				</workflow>
				<workflow name="consolidation">
					<steps>
						<step name="draft" first="true"/>
						<step name="waiting_for_publish"/>
					</steps>
					<transitions>
						<transition name="move_publish" origin="draft" target="waiting_for_publish"/>
						<transition name="return_draft" origin="waiting_for_publish" target="draft"/>
					</transitions>
				</workflow>
			</workflows>
		</workflowBean>
	</workflowProvider>
</workflows>
