<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sf="http://www.ataccama.com/xslt/functions"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="sf fn">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="settings" select="document('param:settings')/*"/>  <!-- /settings -->
<xsl:param name="logicalModel" select="document('param:logicalModel')/*"/>

<xsl:template match="/*">

<lookups>
<lookups>
	<xsl:for-each select="tables/table[@name=$logicalModel/instanceModel/tables/table/columns/column/@refData]">
		<xsl:choose>
			<xsl:when test="columns/column/@useAsLabel='true'">
				<lookup name="rd_{@name}" type="INSTANCE" labelColumn="{columns/column[@useAsLabel='true']/@name}"/>
			</xsl:when>
			<xsl:otherwise>
				<lookup name="rd_{@name}" type="INSTANCE" labelColumn="master_name"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
	<xsl:for-each select="tables/table[@name=$logicalModel/instanceModel/tables/table/columns/column/@refData and @loadInst='true']">
		<lookup name="rd_{@name}_trans" type="INSTANCE" labelColumn="source_code"/>
	</xsl:for-each>
	<xsl:variable name="dicTables" select="tables"/>
	<xsl:for-each select="$logicalModel/masterModels/masterModel">
		<xsl:variable name="masterModel" select="@name"/>
		<xsl:for-each select="$dicTables/table[@name=$logicalModel/masterModels/masterModel[@name=$masterModel]/masterTables/masterTable/columns/column/@refData]">
			<xsl:choose>
				<xsl:when test="columns/column/@useAsLabel='true'">
	   				<lookup name="rd_{@name}" layerName="{$masterModel}" type="MASTER" labelColumn="{columns/column[@useAsLabel='true']/@name}"/>
	   			</xsl:when>
	   			<xsl:otherwise>
	   				<lookup name="rd_{@name}" layerName="{$masterModel}" type="MASTER" labelColumn="master_name"/>
	   			</xsl:otherwise>
	   		</xsl:choose>
		</xsl:for-each>  
	</xsl:for-each>
	<xsl:for-each select="tables/table[@name=$logicalModel/sorModel/tables/table/columns/column/@refData]">
		<xsl:choose>
			<xsl:when test="columns/column/@useAsLabel='true'">
				<lookup name="rd_{@name}" type="SOR" labelColumn="{columns/column[@useAsLabel='true']/@name}"/>
			</xsl:when>
			<xsl:otherwise>
				<lookup name="rd_{@name}" type="SOR" labelColumn="master_name"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>	 
</lookups>
</lookups>

</xsl:template>
</xsl:stylesheet>